//
//  NewController.swift
//  CarFixing
//
//  Created by volive solutions on 15/12/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class NewController: UIViewController {
    @IBOutlet weak var currentTF: UITextField!
    
    @IBOutlet weak var newpasswordTF: UITextField!
    
    @IBOutlet weak var cunfirmpasswordTF: UITextField!
    
    @IBOutlet weak var currentPW: UILabel!
    @IBOutlet weak var newPW: UILabel!
    @IBOutlet weak var confirmPW: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var updateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //scroll.isScrollEnabled = false
        self.title = languageChangeString(a_str: "CHANGE PASSWORD")
        let line = LineTextField()
        //let color : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: currentTF, lineColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), myView: self.view)
        line .textfieldAsLine(myTextfield: newpasswordTF, lineColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), myView: self.view)
        line .textfieldAsLine(myTextfield: cunfirmpasswordTF, lineColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), myView: self.view)
        
        let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        
        currentPW.text = languageChangeString(a_str: "CURRENT PASSWORD")
        newPW.text = languageChangeString(a_str: "NEW PASSWORD")
        confirmPW.text = languageChangeString(a_str: "CONFIRM PASSWORD")
        
        updateBtn.setTitle(languageChangeString(a_str: "CHANGE PASSWORD"), for: UIControlState.normal)
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func backBtnClicked(){
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func updateBtnAction(_ sender: Any) {
 
        if Reachability.isConnectedToNetwork()
        {
            changePasswordService ()
        }
        else{
           
    showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)

        }
        
        
        
    }
    
    func changePasswordService ()
    {
        
        //http://voliveafrica.com/carfix/services/change_password//user_id,old_password,new_password
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let statesCities = "\(Base_Url)change_password"
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        

        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "old_password" : self.currentTF.text ?? "" , "new_password": newpasswordTF.text ?? "" ,"lang" : language ?? ""
 ]
        print(parameters)
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(statesCities, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                     DispatchQueue.main.async {
                    Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    
                    }
                    //self.customerDataArr = responseData["user_info"] as? [[String:AnyObject]]
                    
                        
                    
                }
                else
                {
                    DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                        
                    }
                }
                
            }
        }
    }

   
}
extension NewController : UITextFieldDelegate{
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        var returnValue : Bool = false
        
        if textField == self.currentTF {
            self.cunfirmpasswordTF.becomeFirstResponder()
            
            returnValue = true
        }
        else if textField == self.newpasswordTF{
            self.cunfirmpasswordTF.becomeFirstResponder()
            returnValue = true
        }
        else if textField == self.cunfirmpasswordTF{
            
            textField.resignFirstResponder()
            
            returnValue = true
        }
        
        return returnValue
        
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.currentTF{
            scroll.contentOffset.y = +20
        }
        else if textField == self.newpasswordTF{
            scroll.contentOffset.y = +40
        }
        else if textField == self.cunfirmpasswordTF{
            scroll.contentOffset.y = +60
        }
        
    }
    public func textFieldDidEndEditing(_ textField: UITextField){
        
        self.currentTF.resignFirstResponder()
        self.newpasswordTF.resignFirstResponder()
       self.cunfirmpasswordTF.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }
    
}
