//
//  WebCalling.swift
//  CarFixing
//
//  Created by Suman Guntuka on 03/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class WebCalling: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var myUrl : NSURL!
    var linkStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(linkStr)
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Web")
        
        let url = URL (string: linkStr!)
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    
    @objc func backBtnClicked(){
        
        self.navigationController?.popViewController(animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
