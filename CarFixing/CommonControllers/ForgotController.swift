//
//  ForgotController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/6/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ForgotController: UIViewController {

    @IBOutlet var flagImg: UIImageView!
    @IBOutlet weak var countryPickerText: UITextField!
    @IBOutlet weak var mobileTexy: UITextField!
    @IBOutlet weak var scroll: UIScrollView!
    
    var countyPickerView: UIPickerView?
    var countyPicker: UIPickerView?
    
    var toolbar: UIToolbar?
    var selectedTextField: UITextField?
    var countryPickerData = [String]()
    var selectedStr : String!
     var countryFlogArr = [String]()
    
    
    @IBOutlet weak var forgotPassword: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    
    @IBOutlet weak var resend: UIButton!
    @IBOutlet weak var passwordLink: UILabel!
    @IBOutlet weak var didNotRecieve: UILabel!
    @IBOutlet weak var resetPassword: UIButton!
    @IBOutlet weak var detailLbl: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()

        forgotPassword.text = languageChangeString(a_str: "Forgot Password")
        detailLbl.text = languageChangeString(a_str: "We just need your registered mobile \no number to send your password reset link")
        mobileNo.text = languageChangeString(a_str: "MOBILE NUMBER")
        passwordLink.text = languageChangeString(a_str: "Password link?")
        didNotRecieve.text = languageChangeString(a_str: "Did not receive your Reset")
        resend.setTitle(languageChangeString(a_str: "Resend Again"), for: UIControlState.normal)
        resetPassword.setTitle(languageChangeString(a_str: "RESET PASSWORD"), for: UIControlState.normal)
        
        
        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: countryPickerText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: mobileTexy, lineColor: myColor, myView: self.view)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        countryCodeService ()
        
        countyPickerView = UIPickerView()
        countyPickerView?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (countyPickerView?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        countyPickerView?.delegate = self as UIPickerViewDelegate
        countyPickerView?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
    }
    func donePicker ()
    {
        
       // self.countryPickerText.text = selectedStr
        self.countryPickerText.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        self.countryPickerText.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func resetPasswordClicked(_ sender: Any) {
        
        
        if Reachability.isConnectedToNetwork(){
            
            forgotPasswordService ()
        }else
        {
              showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
    }
    
    @IBAction func resendClicked(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork(){
            
            forgotPasswordService ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
    }
    
   
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var backBtnClicked: UIButton!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func forgotPasswordService ()
    {
        
        //http://voliveafrica.com/carfix/services/state_cities?API-KEY=98745612
        
        let mobileString = self.mobileTexy.text
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let statesCities = "\(Base_Url)forgot_password?"
         Services.sharedInstance.loader(view: self.view)
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "mobile" : mobileString! , "lang" : language ?? ""]
        
        Alamofire.request(statesCities, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                //ServiceCell.sharedInstance.dissMissLoader()
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    Services.sharedInstance.dissMissLoader()
                   
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let verify = storyBoard.instantiateViewController(withIdentifier: "VerifyController") as? VerifyController
                    verify?.phoneNumberStr=mobileString
                    
                    self.navigationController?.pushViewController(verify!, animated: true)
                    
                    self.showToastForAlert(message:  message!!)
                    
                }
                else
                {
                     Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message:  message!!)
                }
                
            }
        }
    }
    func countryCodeService ()
    {
        
        //http://voliveafrica.com/carfix/services/countries?API-KEY=98745612
        
        let vehicles = "\(Base_Url)countries?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY]
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.countryFlogArr = [String]()
                    self.countryPickerData = [String]()
                    
                    let servicesCat = responseData["countries"] as? [[String:Any]]
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let codeStr = servicesCat["phonecode"]  as! String
                        let flags = servicesCat["country_flag"]  as! String
                        
                        let flogsImg = base_path + flags
                        
                        self.countryFlogArr.append(flogsImg)
                        self.countryPickerData.append(codeStr)
                        
                    }
                    print(self.countryPickerData)
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}
extension ForgotController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue: Bool = false
        if textField == countryPickerText {
            mobileTexy.becomeFirstResponder()
            returnValue = true
        }
        else if textField == mobileTexy{
            mobileTexy.resignFirstResponder()
            returnValue = true
        }
        return returnValue
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryPickerText {
           scroll.contentOffset.y = +20
        }
        else if textField == mobileTexy{
           scroll.contentOffset.y = +20
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        countryPickerText.inputView = countyPickerView
        countryPickerText.inputAccessoryView = toolbar
        
        //   countryCode.text = selectedTextField
        
        return true
    }
    
   public func textFieldDidEndEditing (_ textField : UITextField) {
        countryPickerText.resignFirstResponder()
        mobileTexy.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }

}
extension ForgotController : UIPickerViewDataSource,UIPickerViewDelegate{
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            
            return countryFlogArr.count
        }
        
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            
            return countryPickerData[row]
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
            if pickerView == countyPicker
            {
                self.countryPickerText.text = self.countryPickerData[row]
                
                self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
            }
            //self.view.endEditing(true)
            
            //selectedStr = countryPickerData[row]
        }
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            //names
            let temp = UILabel()
            let img = UIImageView()
            img.frame = CGRect(x: self.view.frame.origin.x+100 ,y: 0, width: 30,height: 25)
            
            temp.text = self.countryPickerData[row]
            
            self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
            img.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:""))
            
            temp.adjustsFontSizeToFitWidth = true
            temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
            //codes
            let aView = UIView()
            aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:40)
            aView.insertSubview(temp, at: 1)
            aView.insertSubview(img, at: 1)
            
            return aView
        }
        
}

