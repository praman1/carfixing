//
//  LineTextField.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/5/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Foundation

class LineTextField: NSObject {
    func textfieldAsLine(myTextfield : UITextField, lineColor : UIColor, myView : UIView) -> Void {
        
        myTextfield.borderStyle = UITextBorderStyle.none
        let border = CALayer ()
        let borderWidth : CGFloat = 2.0
        border.borderWidth = borderWidth
        border.borderColor = lineColor.cgColor
        border.frame = CGRect(x: 0,y: myTextfield.frame.size.height-borderWidth,width: myView.frame.size.width,height: myTextfield.frame.size.height )
        myTextfield.layer.addSublayer(border)
        myTextfield.layer.masksToBounds = true
        
       
    }
    

}
