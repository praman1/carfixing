//
//  NotificationsController.swift
//  carFixing
//
//  Created by Apple on 12/13/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import Alamofire

class NotificationsController: UIViewController{

    @IBOutlet weak var notificationTV: UITableView!
    
    var notifyData : [[String : AnyObject]]!
    var msgArr = [String]()
    var dateArr = [String]()
    var timeArr = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        //self.navigationItem.title = "Notifications"
        self.title = languageChangeString(a_str: "Notifications")
        
        // Do any additional setup after loading the view.
        
        notificationsServiceCall()
        
    }

    
    
     @objc func backBtnClicked(){
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func notificationsServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/notifications?API-KEY=98745612&user_id=8
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        //let details = "\(Base_Url)notifications?API-KEY=\(APIKEY)&user_id=\(userid ?? "")"
        let details = "\(Base_Url)notifications?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY,  "user_id" : userid ?? "" ,"lang" : language ?? ""]
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                        
                        Services.sharedInstance.dissMissLoader()
                        
                        self.msgArr = [String]()
                        self.dateArr = [String]()
                        
                        self.notifyData = responseData["notifications"] as? [[String:AnyObject]]
                        
                        
                        for i in self.notifyData!
                        {
                            let msg = i["message"] as? String!
                            let date = i["notification_date"] as? String!
                            let time = i["notification_time"] as? String!
                            
                            self.msgArr.append(msg!!)
                            self.dateArr.append(date!!)
                            self.timeArr.append(time!!)
                            
                    }
               
                    if self.msgArr.count == 0
                    {
                      self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                    }
                    
                    DispatchQueue.main.async {
                     
                        self.notificationTV.reloadData()
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
}

extension NotificationsController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.msgArr.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : NotificationMsgCell = tableView.dequeueReusableCell(withIdentifier: "NotificationMsgCell") as! NotificationMsgCell
            
        cell.msgLbl?.text = self.msgArr[indexPath.row]
        cell.dateLbl?.text = self.dateArr[indexPath.row]
        cell.timeLbl?.text = self.timeArr[indexPath.row]
            
            return cell
        

        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

}

