//
//  LoginController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/5/17.
//  Copyright © 2017 volivesolutions. All rights reserved.


import UIKit
import Alamofire

class LoginController: UIViewController {
    
    @IBOutlet var flagImg: UIImageView!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var newuser: UILabel!
    @IBOutlet weak var forgotpassword: UIButton!
    @IBOutlet weak var password1: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var signUpClicked: UIButton!
    var countyPicker: UIPickerView?
    
    @IBOutlet weak var rememberMeImage: UIImageView!
    @IBOutlet weak var rememberMe: UIButton!
    
    var toolbar: UIToolbar?
    var selectedTextField: UITextField?
    var countryPickerData = [String]()
    var selectedStr : String!
    var remember : String!
    var countryFlogArr = [String]()
    var isChecked : Bool = false
    @IBOutlet weak var loginBtn: UIButton!
    var customerDataArr: [[String:AnyObject]]!
    var typeStr : String!
    var role1 : String!
    
    var appDelegate = AppDelegate()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //0852369147 Tow
        //9299301685 Scrap
        
        remember = "NO"
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //language
        
        if UserDefaults.standard.object(forKey: "REM") != nil
        {
            let rem = UserDefaults.standard.object(forKey: "REM") as! String
            
            if rem == "Remember"
            {
                rememberMeImage.image = UIImage(named: "check color")
                
                typeStr = UserDefaults.standard.object(forKey: "uType") as! String
                
                print(typeStr)
                
                if typeStr == "user" {
                    
                    if UserDefaults.standard.object(forKey: "mobileNumber") != nil {
                        
                        mobileNumber.text = UserDefaults.standard.object(forKey: "mobileNumber") as? String
                        password.text = UserDefaults.standard.object(forKey: "password1") as? String
                    }
                    else
                    {
                        mobileNumber.text = ""
                        password.text = ""
                        rememberMeImage.image = UIImage(named: "unCheck")
                    }
                    
                }else if typeStr == "individual"
                {
                    
                    if UserDefaults.standard.object(forKey: "mobileNumbe") != nil {
                       
                        mobileNumber.text = UserDefaults.standard.object(forKey: "mobileNumbe") as? String
                        password.text = UserDefaults.standard.object(forKey: "passwor1") as? String
                    }
                    else
                    {
                        mobileNumber.text = ""
                        password.text = ""
                        rememberMeImage.image = UIImage(named: "unCheck")
                    }
                }else if typeStr == "autoshop"
                {
                    if UserDefaults.standard.object(forKey: "mobileNumb") != nil {
                        
                        mobileNumber.text = UserDefaults.standard.object(forKey: "mobileNumb") as? String
                        password.text = UserDefaults.standard.object(forKey: "passwo1") as? String
                    }
                    else
                    {
                        mobileNumber.text = ""
                        password.text = ""
                        rememberMeImage.image = UIImage(named: "unCheck")
                    }
                }else if typeStr == "scrapshop"
                {
                    if UserDefaults.standard.object(forKey: "mobileNum") != nil {
                        
                        mobileNumber.text = UserDefaults.standard.object(forKey: "mobileNum") as? String
                        password.text = UserDefaults.standard.object(forKey: "passw1") as? String
                    }
                    else
                    {
                        mobileNumber.text = ""
                        password.text = ""
                        rememberMeImage.image = UIImage(named: "unCheck")
                    }
                }
                
            } else {
                rememberMeImage.image = UIImage(named: "unCheck")
            }
        }else
        {
        }
        
        login.text = languageChangeString(a_str: "Login")
        mobileNo.text = languageChangeString(a_str: "MOBILE NUMBER*")
        password1.text = languageChangeString(a_str: "PASSWORD*")
        newuser.text = languageChangeString(a_str: "New User?")
        
        signUpClicked.setTitle(languageChangeString(a_str: "Sign Up"), for: UIControlState.normal)
        forgotpassword.setTitle(languageChangeString(a_str: "FORGOT PASSWORD"), for: UIControlState.normal)
        loginBtn.setTitle(languageChangeString(a_str: "LOGIN"), for: UIControlState.normal)
        rememberMe.setTitle(languageChangeString(a_str: "REMEMBER ME"), for: UIControlState.normal)
        
        let line = LineTextField()
        let color : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: countryCode, lineColor: color, myView: self.view)
        line .textfieldAsLine(myTextfield: mobileNumber, lineColor: color, myView: self.view)
        line .textfieldAsLine(myTextfield: password, lineColor: color, myView: self.view)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        countryCodeService ()
        
        countyPicker = UIPickerView()
        countyPicker?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (countyPicker?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        countyPicker?.delegate = self as UIPickerViewDelegate
        countyPicker?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
    }
    func donePicker ()
    {
        // self.countryCode.text = selectedStr
        self.countryCode.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        self.countryCode.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.mainView.endEditing(true)
    }
    
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let forgot = storyBoard.instantiateViewController(withIdentifier: "ForgotController") as? ForgotController
        self.navigationController?.pushViewController(forgot!, animated: true)
        
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        if appDelegate.loginUser == "customer"{
            
            if countryCode.text == ""
            {
                self.showToastForAlert(message: languageChangeString(a_str: "Please Pick Country Code")!)
            }
            else if mobileNumber.text == ""
            {
                self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Mobile No")!)
                
            }else if password.text == ""
            {
                self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Password")!)
            }else
            {
                if Reachability.isConnectedToNetwork() {
                    
                    loginService ()
                    
                }else
                {
                    showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
                }
            }
        }
        else{
            
            if countryCode.text == ""
            {
                self.showToastForAlert(message: "Please Pick Country Code")
            }
            else if mobileNumber.text == ""
            {
                self.showToast(message: "Please Enter Mobile No")
                
            }else if password.text == ""
            {
                self.showToast(message: "Please Enter Password")
                
            }else
            {
                appDelegate.jobsSepStr = "ojobs"
                
                if Reachability.isConnectedToNetwork() {
                    
                    loginService ()
                    
                }
                else
                {
                    showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
                }
            }
        }
    }
   
    @IBAction func rememberMeClicked(_ sender: Any) {
        
        UserDefaults.standard.set("Remember", forKey: "REM")
        
        isChecked = !isChecked;
        
        if (isChecked == true) {
            
            role1 = UserDefaults.standard.object(forKey: "role") as? String
            
            if role1 == "user"
            {
                UserDefaults.standard.set(mobileNumber.text, forKey: "mobileNumber")
                UserDefaults.standard.set(password.text, forKey: "password1")
            }else if role1 == "individual"
            {
                UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNumbe")
                UserDefaults.standard.set(self.password.text, forKey: "passwor1")
                
            }else if role1 == "autoshop"
            {
                UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNumb")
                UserDefaults.standard.set(self.password.text, forKey: "passwo1")
                
            }else if role1 == "scrapshop"
            {
                UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNum")
                UserDefaults.standard.set(self.password.text, forKey: "passw1")
            }
            
            rememberMeImage.image = UIImage(named: "check color")
            remember = "YES";
            
        } else {
            
            rememberMeImage.image = UIImage(named: "unCheck")
            remember = "NO";
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        UserDefaults.standard.removeObject(forKey: "idsString")
        UserDefaults.standard.removeObject(forKey: "saveoS")
        UserDefaults.standard.removeObject(forKey: "bandIdsString")
        UserDefaults.standard.removeObject(forKey: "saveS")
        UserDefaults.standard.removeObject(forKey: "savebS")
        UserDefaults.standard.removeObject(forKey: "IdsStringArr")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        
        if appDelegate.loginUser == "customer"
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let signup = storyBoard.instantiateViewController(withIdentifier: "RegisterController") as? RegisterController
            self.navigationController?.pushViewController(signup!, animated: true)
            
        }else
        {
            UserDefaults.standard.removeObject(forKey: "idsString")
            UserDefaults.standard.removeObject(forKey: "saveoS")
            UserDefaults.standard.removeObject(forKey: "bandIdsString")
            UserDefaults.standard.removeObject(forKey: "saveS")
            UserDefaults.standard.removeObject(forKey: "savebS")
            UserDefaults.standard.removeObject(forKey: "IdsStringArr")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let signup = storyBoard.instantiateViewController(withIdentifier: "ServiceProviderRegister") as? ServiceProviderRegister
            self.navigationController?.pushViewController(signup!, animated: true)
        }
    }
    
    func loginService ()
    {
        
        //http://voliveafrica.com/carfix/services/login..password,mobile,API-KEY
        let statesCities = "\(Base_Url)login"
        
        let tokenStr = UserDefaults.standard.string(forKey: "deviceToken")
        
        //let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "mobile" :mobileNumber.text! , "password" : password.text! , "device_type" :"iOS" , "device_token": "A76519BAF9F38878D901EC3E17AD1329E95C5A8928B9FDD3630C50AC100DBC0E"]
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "mobile" :mobileNumber.text! ,"country_code" :countryCode.text! , "password" : password.text! , "device_type" :"iOS" , "device_token":tokenStr ?? "", "lang" : language ?? ""]
        
        print(parameters)
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(statesCities, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    if let responseData1 = responseData["user_info"] as? Dictionary<String, AnyObject> {
                        
                        let role: String = (responseData1["role"] as? String)!
                        let onsiteType: String = (responseData1["towing_ios"] as? String)!
                        let towtype : String = (responseData1["towing_ios"] as? String)!
                        let currency : String = (responseData1["currency"] as? String)!
                        print("my role is \(role)")
                        
                        UserDefaults.standard.set(currency, forKey: "currencyVal")
                        
                        self.appDelegate.onsiteStr = onsiteType
                       
                        
                        UserDefaults.standard.set(role, forKey: "roleStr")
                        UserDefaults.standard.set(onsiteType, forKey: "towtype")
                        UserDefaults.standard.set(towtype, forKey: "onsiteTowType")
                        
                        let imagePic: String = (responseData1["image"] as? String)!
                        let email: String = (responseData1["email"] as? String)!
                        let name: String = (responseData1["user_name"] as? String)!
                        let mobile: String = (responseData1["mobile"] as? String)!
                        
                    // let defaults = UserDefaults.standard
                    // defaults.set(self.passwordTxt.text, forKey: "password")
                    // defaults.set(dic["phone"], forKey: "mobile_num")
                        
                        let profileImage = base_path + imagePic
                        
                       // UserDefaults.standard.set(self.remember, forKey: "remember")
                        
                        if UserDefaults.standard.object(forKey: "REM") != nil
                        {
                            let remStr = UserDefaults.standard.object(forKey: "REM") as! String
                            
                            if (remStr == "Remember") {
                                
                                if role == "user"
                                {
                                    UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNumber")
                                    UserDefaults.standard.set(self.password.text, forKey: "password1")
                                }else if role == "individual"
                                {
                                    UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNumbe")
                                    UserDefaults.standard.set(self.password.text, forKey: "passwor1")
                                    
                                }else if role == "autoshop"
                                {
                                    UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNumb")
                                    UserDefaults.standard.set(self.password.text, forKey: "passwo1")
                                    
                                }else if role == "scrapshop"
                                {
                                    UserDefaults.standard.set(self.mobileNumber.text, forKey: "mobileNum")
                                    UserDefaults.standard.set(self.password.text, forKey: "passw1")
                                }
                            }
                            else{
                                
                                UserDefaults.standard.removeObject(forKey: "mobileNumber")
                                UserDefaults.standard.removeObject(forKey: "password1")
                                UserDefaults.standard.removeObject(forKey: "mobileNumbe")
                                UserDefaults.standard.removeObject(forKey: "passwor1")
                                UserDefaults.standard.removeObject(forKey: "mobileNumb")
                                UserDefaults.standard.removeObject(forKey: "passwo1")
                                UserDefaults.standard.removeObject(forKey: "mobileNum")
                                UserDefaults.standard.removeObject(forKey: "passw1")
                            }
                        }
                        
                        UserDefaults.standard.set(profileImage, forKey: "profile")
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(mobile, forKey: "mobile")
                        UserDefaults.standard.set(self.password.text, forKey: "password")
                        UserDefaults.standard.set(role, forKey: "role")
                        
                        let userId: String = (responseData1["user_id"] as? String)!
                        UserDefaults.standard.set(userId, forKey: "user_id")
                        
                        if role == "user"
                        {
                            DispatchQueue.main.async {
                                
                                self.appDelegate.loginUser = "customer"
                                 self.appDelegate.scrapshop = role

                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
                self .present(tab, animated: true, completion: nil)
                                
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                               self.appDelegate.loginUser = "serviceProvider"
                                
                                if role == "individual"
                                {
                                    self.appDelegate.serviceStr = "indiv"
                                     self.appDelegate.towStr = towtype
                                    self.appDelegate.scrapshop = role
                                    
                                }else if role == "autoshop"
                                {
                                  self.appDelegate.serviceStr = "repair"
                                    self.appDelegate.scrapshop = role
                                    
                                }else if role == "scrapshop"
                                {
                                   self.appDelegate.serviceStr = "scrap"
                                    self.appDelegate.scrapshop = role
                                }
                                
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                                self .present(service, animated: true, completion: nil)
                                
                            }
                        }
                    }
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToast(message: message!!)
                }
            }
        }
    }
    func countryCodeService ()
    {
        
        //http://voliveafrica.com/carfix/services/countries?API-KEY=98745612
        
        let vehicles = "\(Base_Url)countries?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY]
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                
                if status == 1
                {
                    self.countryPickerData = [String]()
                    self.countryFlogArr = [String]()
                    
                    let servicesCat = responseData["countries"] as? [[String:Any]]
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let codeStr = servicesCat["phonecode"]  as! String
                        let flags = servicesCat["country_flag"]  as! String
                        
                        let flogsImg = base_path + flags
                        
                        self.countryPickerData.append(codeStr)
                        self.countryFlogArr.append(flogsImg)
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        
                        self.flagImg.image=UIImage(named: "saudi")
                        
                    }
                    print(self.countryPickerData)
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension LoginController : UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        var returnValue : Bool = false
        
        if textField == countryCode {
            
            mobileNumber.becomeFirstResponder()
            
            returnValue = true
        }
        else if textField == mobileNumber{
            password.becomeFirstResponder()
            returnValue = true
        }
        else if textField == password{
            password.resignFirstResponder()
            returnValue = true
        }
        return returnValue
        
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileNumber{
            scroll.contentOffset.y = +20
        }
        else if textField == countryCode{
            
            scroll.contentOffset.y = +20
        }
        else if textField == password{
            
            scroll.contentOffset.y = +60
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        countryCode.inputView = countyPicker
        countryCode.inputAccessoryView = toolbar
        
        //   countryCode.text = selectedTextField
        
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField){
        
        mobileNumber.resignFirstResponder()
        countryCode.resignFirstResponder()
        password.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }
}

extension LoginController : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return countryFlogArr.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return countryPickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == countyPicker
        {
            countryCode.text = self.countryPickerData[row]
            
            self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        }
        //self.view.endEditing(true)
        
        //selectedStr = countryPickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        //names
        let temp = UILabel()
        let img = UIImageView()
        img.frame = CGRect(x: self.view.frame.origin.x+100 ,y: 0, width: 30,height: 25)
        
        temp.text = self.countryPickerData[row]
        
        self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        img.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:""))
        
        temp.adjustsFontSizeToFitWidth = true
        temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
        //codes
        let aView = UIView()
        aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:40)
        aView.insertSubview(temp, at: 1)
        aView.insertSubview(img, at: 1)
        
        return aView
    }
    
}
