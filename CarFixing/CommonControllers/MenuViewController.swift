//
//  MenuViewController.swift
//  carFixing
//
//  Created by Apple on 12/14/17.
//  Copyright © 2017 Apple. All rights reserved.
//
import UIKit

class MenuViewController: UIViewController  {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var emailLbl: UILabel!
    var mobileNum:String!
    var pass:String!
    var appde = AppDelegate()
    var role : String!
    var menuNames_Array = [String]()
    var menuImagesNames_Array : [String] = [ "menu1","instant", "menu5", "jobs","menu2","post","bills","chatmenu", "menu7", "menu8", "menu9","contactus","contactus","select language","logout"]
    
    //onsite
    var service_Array = [String]()
    var service_Array1 = [String]()
    var serviceImages_Array : [UIImage] = [#imageLiteral(resourceName: "menu1"),#imageLiteral(resourceName: "menu6"),#imageLiteral(resourceName: "shop timings"),#imageLiteral(resourceName: "gallery"),#imageLiteral(resourceName: "Add"),#imageLiteral(resourceName: "chatmenu"),#imageLiteral(resourceName: "menu7"),#imageLiteral(resourceName: "menu8"),#imageLiteral(resourceName: "menu9"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "select language"),#imageLiteral(resourceName: "logout")]
    var serviceImages_Array1 : [UIImage] = [#imageLiteral(resourceName: "menu1"),#imageLiteral(resourceName: "instant"),#imageLiteral(resourceName: "services price"),#imageLiteral(resourceName: "shop timings"),#imageLiteral(resourceName: "gallery"),#imageLiteral(resourceName: "Add"),#imageLiteral(resourceName: "chatmenu"),#imageLiteral(resourceName: "menu7"),#imageLiteral(resourceName: "menu8"),#imageLiteral(resourceName: "menu9"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "select language"),#imageLiteral(resourceName: "logout")]
    
    //autoShop
    var autoService_Array = [String]()
    var autoServiceImages_Array : [UIImage] = [#imageLiteral(resourceName: "menu1"),#imageLiteral(resourceName: "menu6"),#imageLiteral(resourceName: "shop timings"),#imageLiteral(resourceName: "gallery"),#imageLiteral(resourceName: "Add"),#imageLiteral(resourceName: "chatmenu"),#imageLiteral(resourceName: "menu7"),#imageLiteral(resourceName: "menu8"),#imageLiteral(resourceName: "menu9"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "select language"),#imageLiteral(resourceName: "logout")]
    
    // scrapshop
    
    var scrapShopArr = [String]()
    var scrapShopImgs_Array : [UIImage] =  [#imageLiteral(resourceName: "menu1"),#imageLiteral(resourceName: "menu5"),#imageLiteral(resourceName: "shop timings"),#imageLiteral(resourceName: "gallery"),#imageLiteral(resourceName: "Add"),#imageLiteral(resourceName: "chatmenu"),#imageLiteral(resourceName: "menu7"),#imageLiteral(resourceName: "menu8"),#imageLiteral(resourceName: "menu9"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "select language"),#imageLiteral(resourceName: "logout")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuNames_Array = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "Towing Requests"), languageChangeString(a_str: "My Bids"),languageChangeString(a_str: "Automobile Repair Requests"),languageChangeString(a_str: "Onsite Requests"),languageChangeString(a_str: "Post Selling Request"),languageChangeString(a_str: "My Bills"),languageChangeString(a_str: "Chats"), languageChangeString(a_str: "Notifications") ,languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        service_Array = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "My Requests"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        service_Array1 = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "Towing Requests"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        autoService_Array = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "My Requests"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        scrapShopArr = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "My Bids"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        appde = UIApplication.shared.delegate as! AppDelegate
        
        self.navigationController?.isNavigationBarHidden = true
        
        let profile = UserDefaults.standard.object(forKey: "profile")
        let name  = UserDefaults.standard.object(forKey: "name")
        let email  = UserDefaults.standard.object(forKey: "email")
        
        self.profilePic.sd_setImage(with: URL(string: profile as! String), placeholderImage: UIImage(named:"profile pic"))
        nameLbl.text = name as? String
        emailLbl.text = email as? String
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.height/2
        self.profilePic.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(profileData), name: NSNotification.Name(rawValue:"profileData"), object: nil)
        
    }
    
    func profileData()
    {
        let profile = UserDefaults.standard.object(forKey: "profile")
        let name  = UserDefaults.standard.object(forKey: "name")
        let email  = UserDefaults.standard.object(forKey: "email")
        
        self.profilePic.sd_setImage(with: URL(string: profile as! String), placeholderImage: UIImage(named:"profile pic"))
        nameLbl.text = name as? String
        emailLbl.text = email as? String
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.height/2
        self.profilePic.clipsToBounds = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        menuNames_Array = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "Towing Requests"), languageChangeString(a_str: "My Bids"),languageChangeString(a_str: "Automobile Repair Requests"),languageChangeString(a_str: "Onsite Requests"),languageChangeString(a_str: "Post Selling Request"),languageChangeString(a_str: "My Bills"),languageChangeString(a_str: "Chats"), languageChangeString(a_str: "Notifications") ,languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        service_Array = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "My Requests"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        service_Array1 = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "Towing Requests"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        autoService_Array = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "My Requests"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        scrapShopArr = [ languageChangeString(a_str: "Home"),languageChangeString(a_str: "My Bids"),languageChangeString(a_str: "Work Hours"),languageChangeString(a_str: "Photo Album"),languageChangeString(a_str: "Add Bank Details"),languageChangeString(a_str: "Chats"),languageChangeString(a_str: "Notifications"),languageChangeString(a_str: "Profile"),languageChangeString(a_str: "My Account"),languageChangeString(a_str: "Contact Us"),languageChangeString(a_str: "About Us"),languageChangeString(a_str: "Change Language"),languageChangeString(a_str: "Logout")] as! [String]
        
        self.menuTableView.reloadData()
        
        animateTable()
    }
    func animateTable() {
        menuTableView.reloadData()
        
        let cells = menuTableView.visibleCells
        let tableHeight: CGFloat = menuTableView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    func changeLanguage()
    {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "English", style: .default) { action -> Void in
            
            UserDefaults.standard.set(ENGLISH_LANGUAGE, forKey: "currentLanguage")
            self.viewDidLoad()
            self.menuTableView.reloadData()
            
            if  self.appde.loginUser == "customer"
            {
                self.appde.sidemenuStr = "home"
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            else if  self.appde.loginUser == "serviceProvider"
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "عربى", style: .default) { action -> Void in
            
            UserDefaults.standard.set(ARABIC_LANGUAGE, forKey: "currentLanguage")
            self.viewDidLoad()
            self.menuTableView.reloadData()
            
            if  self.appde.loginUser == "customer"
            {
                self.appde.sidemenuStr = "home"
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            else if  self.appde.loginUser == "serviceProvider"
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)
        
        present(actionSheetController, animated: true, completion: nil)
    }
    
}
extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if appde.loginUser == "customer"{
           
            return menuNames_Array.count
            
        }
        else{
            if appde.scrapshop == "scrapshop"
            {
                return scrapShopArr.count
                
            }else if appde.scrapshop == "autoshop"
            {
                
                return autoService_Array.count
                
            }else
            {
                if appde.onsiteStr == "tow"
                {
                    return service_Array1.count
                }else
                {
                    return service_Array.count
                }
                
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell1") as! MenuCell
        
        if appde.loginUser == "customer"{
            cell.icon_imageView.image = UIImage(named: "\(menuImagesNames_Array[indexPath.row])")
            cell.nameLable.text = menuNames_Array[indexPath.row]
        }
        else{
            
            if appde.scrapshop == "scrapshop"
            {
                cell.icon_imageView.image = scrapShopImgs_Array[indexPath.row]
                cell.nameLable.text = scrapShopArr[indexPath.row]
                
            }
            else if appde.scrapshop == "autoshop"
            {
                cell.icon_imageView.image = autoServiceImages_Array[indexPath.row]
                cell.nameLable.text = autoService_Array[indexPath.row]
            }else
            {
                if appde.onsiteStr == "tow"
                {
                    cell.icon_imageView.image = serviceImages_Array1[indexPath.row]
                    cell.nameLable.text = service_Array1[indexPath.row]
                }else
                {
                    cell.icon_imageView.image = serviceImages_Array[indexPath.row]
                    cell.nameLable.text = service_Array[indexPath.row]
                }
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if appde.loginUser == "customer"{
            
            if indexPath.row == 0 {
                
               // _ = self.tabBarController?.selectedIndex = 0
                 self.tabBarController?.selectedIndex = 0
                
                appde.sidemenuStr = "home"
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }
                
            else if indexPath.row == 1 {
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let list = storyBoard.instantiateViewController(withIdentifier: "TowingList") as? TowingList
                
                self.navigationController?.pushViewController(list!, animated: true)
                
            }
                
            else if indexPath.row == 2 {
                
                appde.sidemenuStr = "bids"
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "BidsController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            else if indexPath.row == 3 {
               
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "JobsController2")
               appde.onsiteSepStr = "auto"
                self.navigationController?.pushViewController(homeVC, animated: true)
                
//                let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                let homeVC = storyboard.instantiateViewController(withIdentifier: "MyJobsVC")
//                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else if indexPath.row == 4 {
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "JobsController2")
                appde.onsiteSepStr = "onsite"
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
                
            else if indexPath.row == 5 {
                
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "PostCarRequestController")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else if indexPath.row == 6 {
                
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "MyBillsVC")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }
                //SettingsViewController
                
            else if indexPath.row == 7 {
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "CustomerChatController")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }
                
            else if indexPath.row == 8 {
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "NotificationsController")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else if indexPath.row == 9 {
                
                appde.profileStr = "menu"
                
               // self.tabBarController?.selectedIndex = 3
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "ProfileController2")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else if indexPath.row == 10 {
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "MyAccountController")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else if indexPath.row == 11{
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "ContactUsController")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else if indexPath.row == 12{
                
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "AboutUs")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }
            else if indexPath.row == 13 {
                
                self.changeLanguage()
                
            }else if indexPath.row == 14
            {

                UserDefaults.standard.removeObject(forKey: "mobileNumber")
                 UserDefaults.standard.removeObject(forKey: "password1")
                UserDefaults.standard.removeObject(forKey: "REM")
                
                UserDefaults.standard.set("en", forKey: "currentLanguage")
                let deviceToken = UserDefaults.standard.object(forKey: "deviceToken")
                
                dismiss(animated: true, completion: nil)
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
                window.rootViewController = storyboard.instantiateInitialViewController()
                
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionaryRepresentation()
                

                
                dictionary.keys.forEach
                    { key in   defaults.removeObject(forKey: key)
                }
                UserDefaults.standard.set("en", forKey: "currentLanguage")
                UserDefaults.standard.set(deviceToken , forKey: "deviceToken")

            }
            
        }
        else{
            //service provider
            
            //scrapshop
            
            if appde.scrapshop == "scrapshop"
            {
                if indexPath.row == 0 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController2")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }
                else if indexPath.row == 1 {
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let bids = storyBoard.instantiateViewController(withIdentifier: "MyBidsVC") as? MyBidsVC
                    //self.appde.bidStr = "obids"
                    bids?.sepStr = "obids"
                    self.navigationController?.pushViewController(bids!, animated: true)
                    
                }
                    
                    //                else if indexPath.row == 2 {
                    //
                    //                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    //                    let addBank = storyboard.instantiateViewController(withIdentifier: "AvailablePriceController")
                    //                    self.navigationController?.pushViewController(addBank, animated: true)
                    //
                    //                }
                    
                else if indexPath.row == 2 {
                    
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let addBank = storyboard.instantiateViewController(withIdentifier: "AvailableHoursController")
                    self.navigationController?.pushViewController(addBank, animated: true)
                    
                }else if indexPath.row == 3 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let addBank = storyboard.instantiateViewController(withIdentifier: "ShopImagesController")
                    self.navigationController?.pushViewController(addBank, animated: true)
                    
                }
                else if indexPath.row == 4 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let addBank = storyboard.instantiateViewController(withIdentifier: "AddBankDetailsVC")
                    self.navigationController?.pushViewController(addBank, animated: true)
                    
                }
                    
                else if indexPath.row == 5 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatController")
                    self.navigationController?.pushViewController(chatVC, animated: true)
                    
                    
                }
                    
                else if indexPath.row == 6 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "NotificationsController")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else if indexPath.row == 7 {
                    
                    appde.profileStr = "menu4"
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "ProfileController2")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else if indexPath.row == 8 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "MyAccountController")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else if indexPath.row == 9 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "ContactUsController")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else if indexPath.row == 10{
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "AboutUs")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }
                else if indexPath.row == 11
                {
                    self.changeLanguage()
                }
                else if indexPath.row == 12 {
                    
                    dismiss(animated: true, completion: nil)
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    window.rootViewController = storyboard.instantiateInitialViewController()
                    UserDefaults.standard.removeObject(forKey: "REM")
                   
                    let deviceToken = UserDefaults.standard.object(forKey: "deviceToken")
                    
                    let defaults = UserDefaults.standard
                    let dictionary = defaults.dictionaryRepresentation()
                    

                    
                    dictionary.keys.forEach
                        { key in   defaults.removeObject(forKey: key)
                            
                    }
                    UserDefaults.standard.set("en", forKey: "currentLanguage")
                    UserDefaults.standard.set(deviceToken , forKey: "deviceToken")

                }
                
            }else if appde.scrapshop == "autoshop"
                
            {
                
                if indexPath.row == 0 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController2")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }
                    
                else if indexPath.row == 1 {
                    
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let jobs = storyBoard.instantiateViewController(withIdentifier: "JobsVC") as? JobsVC
                    
                    self.navigationController?.pushViewController(jobs!, animated: true)
                    
                    
                }
                    
                    
                else if indexPath.row == 2 {
                    
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let addBank = storyboard.instantiateViewController(withIdentifier: "AvailableHoursController")
                    self.navigationController?.pushViewController(addBank, animated: true)
                    
                    
                }else if indexPath.row == 3 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let addBank = storyboard.instantiateViewController(withIdentifier: "ShopImagesController")
                    self.navigationController?.pushViewController(addBank, animated: true)
                    
                    
                }
                else if indexPath.row == 4 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let addBank = storyboard.instantiateViewController(withIdentifier: "AddBankDetailsVC")
                    self.navigationController?.pushViewController(addBank, animated: true)
                    
                    
                }
                    
                else if indexPath.row == 5 {
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatController")
                    self.navigationController?.pushViewController(chatVC, animated: true)
                    
                    
                }
                    
                else if indexPath.row == 6 {
                    
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "NotificationsController")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else if indexPath.row == 7 {
                    
                    appde.profileStr = "menu4"
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "ProfileController2")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else if indexPath.row == 8 {
                    
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "MyAccountController")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                    
                }else if indexPath.row == 9 {
                    
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "ContactUsController")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                    
                }else if indexPath.row == 10{
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "AboutUs")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }
                else if indexPath.row == 11
                {
                    self.changeLanguage()
                }
                else if indexPath.row == 12 {
                    
                    // UserDefaults.standard.set("en", forKey: "currentLanguage")
                    dismiss(animated: true, completion: nil)
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    window.rootViewController = storyboard.instantiateInitialViewController()
                    
                    let deviceToken = UserDefaults.standard.object(forKey: "deviceToken")
                    
                    UserDefaults.standard.removeObject(forKey: "REM")
                    
                    role = UserDefaults.standard.object(forKey: "role") as? String
                    
                    if role == "individual"
                    {
                        
                        let defaults = UserDefaults.standard
                        let dictionary = defaults.dictionaryRepresentation()
                        

                        
                        dictionary.keys.forEach
                            { key in   defaults.removeObject(forKey: key)
                                
                        }
                        UserDefaults.standard.set("en", forKey: "currentLanguage")
                        UserDefaults.standard.set(deviceToken , forKey: "deviceToken")
                        

                    }else if role == "autoshop"
                    {
                        let defaults = UserDefaults.standard
                        let dictionary = defaults.dictionaryRepresentation()
                        

                        
                        dictionary.keys.forEach
                            { key in   defaults.removeObject(forKey: key)
                                
                        }
                        UserDefaults.standard.set("en", forKey: "currentLanguage")
                        UserDefaults.standard.set(deviceToken , forKey: "deviceToken")
                        

                    }
                    
                    
                    
                }
            }else
            {
                if appde.onsiteStr == "tow"
                {
                    if indexPath.row == 0 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController2")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }
                    else if indexPath.row == 1 {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let list = storyBoard.instantiateViewController(withIdentifier: "TowingList") as? TowingList
                        
                        self.navigationController?.pushViewController(list!, animated: true)
                        
                    }
                        
                        
                        
                    else if indexPath.row == 2 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let addBank = storyboard.instantiateViewController(withIdentifier: "AvailableHoursController")
                        self.navigationController?.pushViewController(addBank, animated: true)
                        
                        
                    }else if indexPath.row == 3 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let addBank = storyboard.instantiateViewController(withIdentifier: "ShopImagesController")
                        self.navigationController?.pushViewController(addBank, animated: true)
                        
                        
                    }
                    else if indexPath.row == 4 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let addBank = storyboard.instantiateViewController(withIdentifier: "AddBankDetailsVC")
                        self.navigationController?.pushViewController(addBank, animated: true)
                        
                        
                    }
                        
                    else if indexPath.row == 5 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatController")
                        self.navigationController?.pushViewController(chatVC, animated: true)
                        
                        
                    }
                        
                    else if indexPath.row == 6 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "NotificationsController")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }else if indexPath.row == 7 {
                        
                        appde.profileStr = "menu4"
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "ProfileController2")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }else if indexPath.row == 8 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "MyAccountController")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                        
                    }else if indexPath.row == 9 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "ContactUsController")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                        
                    }else if indexPath.row == 10{
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "AboutUs")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }
                    else if indexPath.row == 11
                    {
                        self.changeLanguage()
                    }
                    else if indexPath.row == 12 {
                        
                        // UserDefaults.standard.set("en", forKey: "currentLanguage")
                        dismiss(animated: true, completion: nil)
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
                        window.rootViewController = storyboard.instantiateInitialViewController()
                        
                        let deviceToken = UserDefaults.standard.object(forKey: "deviceToken")
                        UserDefaults.standard.removeObject(forKey: "REM")
                        
                        
                        role = UserDefaults.standard.object(forKey: "role") as? String
                        
                        if role == "individual"
                        {
                            
                            let defaults = UserDefaults.standard
                            let dictionary = defaults.dictionaryRepresentation()
                            

                            
                            dictionary.keys.forEach
                                { key in   defaults.removeObject(forKey: key)
                                    
                            }
                            UserDefaults.standard.set("en", forKey: "currentLanguage")
                            UserDefaults.standard.set(deviceToken , forKey: "deviceToken")
                            

                        }else if role == "autoshop"
                        {
                            let defaults = UserDefaults.standard
                            let dictionary = defaults.dictionaryRepresentation()
                            
                            
                            dictionary.keys.forEach
                                { key in   defaults.removeObject(forKey: key)
                                    
                            }
                            UserDefaults.standard.set("en", forKey: "currentLanguage")
                            UserDefaults.standard.set(deviceToken , forKey: "deviceToken")
                            

                        }
                        
                    }
                    
                }else
                {
                    if indexPath.row == 0 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController2")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }
                        
                    else if indexPath.row == 1 {
                        
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let jobs = storyBoard.instantiateViewController(withIdentifier: "JobsVC") as? JobsVC
                        
                        self.navigationController?.pushViewController(jobs!, animated: true)
                        
                        
                    }
                        
                        
                    else if indexPath.row == 2 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let addBank = storyboard.instantiateViewController(withIdentifier: "AvailableHoursController")
                        self.navigationController?.pushViewController(addBank, animated: true)
                        
                        
                    }else if indexPath.row == 3 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let addBank = storyboard.instantiateViewController(withIdentifier: "ShopImagesController")
                        self.navigationController?.pushViewController(addBank, animated: true)
                        
                        
                    }
                    else if indexPath.row == 4 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let addBank = storyboard.instantiateViewController(withIdentifier: "AddBankDetailsVC")
                        self.navigationController?.pushViewController(addBank, animated: true)
                        
                        
                    }
                        
                    else if indexPath.row == 5 {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatController")
                        self.navigationController?.pushViewController(chatVC, animated: true)
                        
                        
                    }
                        
                    else if indexPath.row == 6 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "NotificationsController")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }else if indexPath.row == 7 {
                        
                        appde.profileStr = "menu4"
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "ProfileController2")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }else if indexPath.row == 8 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "MyAccountController")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                        
                    }else if indexPath.row == 9 {
                        
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "ContactUsController")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                        
                    }else if indexPath.row == 10{
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "AboutUs")
                        self.navigationController?.pushViewController(homeVC, animated: true)
                        
                    }
                    else if indexPath.row == 11
                    {
                        self.changeLanguage()
                    }
                    else if indexPath.row == 12 {
                        
                        // UserDefaults.standard.set("en", forKey: "currentLanguage")
                        dismiss(animated: true, completion: nil)
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
                        window.rootViewController = storyboard.instantiateInitialViewController()
                        
                        let deviceToken = UserDefaults.standard.object(forKey: "deviceToken")
                        UserDefaults.standard.removeObject(forKey: "REM")
                        
                        role = UserDefaults.standard.object(forKey: "role") as? String
                        
                        if role == "individual"
                        {
                            
                            let defaults = UserDefaults.standard
                            let dictionary = defaults.dictionaryRepresentation()
                            

                            
                            dictionary.keys.forEach
                                { key in   defaults.removeObject(forKey: key)
                                    
                            }
                            UserDefaults.standard.set("en", forKey: "currentLanguage")
                            UserDefaults.standard.set(deviceToken , forKey: "deviceToken")
                            

                        }else if role == "autoshop"
                        {
                            let defaults = UserDefaults.standard
                            let dictionary = defaults.dictionaryRepresentation()
                            

                            
                            dictionary.keys.forEach
                                { key in   defaults.removeObject(forKey: key)
                                    
                            }
                            UserDefaults.standard.set("en", forKey: "currentLanguage")
                            UserDefaults.standard.set(deviceToken , forKey: "deviceToken")
                            

                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55
        
    }
    
}
