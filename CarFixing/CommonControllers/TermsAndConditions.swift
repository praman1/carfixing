//
//  TermsAndConditions.swift
//  CarFixing
//
//  Created by Suman Guntuka on 07/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class TermsAndConditions: UIViewController {

    @IBOutlet var termsLbl: UITextView!
    @IBOutlet var termAndLbl: UILabel!
    //@IBOutlet var termsLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        termAndLbl.text = languageChangeString(a_str: "Terms And Conditions")
        
        self.termsAndConditionCall()
        
    }
    

    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func termsAndConditionCall ()
    {
        
        //http://voliveafrica.com/carfix/services/terms?API-KEY=98745612
        let vehicles = "\(Base_Url)terms?"
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"lang" : language ?? ""]
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    if let responseData1 = responseData["terms"] as? Dictionary<String, AnyObject> {
                        
                       // let termsStr : String = (responseData1["terms_en"] as? String)!
                        
                        let textStr = responseData1["terms_en"] as! String
                        
                        var attrStr: NSAttributedString? = nil
                        
                        if let anEncoding = textStr.data(using: String.Encoding(rawValue: String.Encoding.unicode.rawValue)) {
                            attrStr = try? NSAttributedString(data: anEncoding, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
                        }
                        
                        self.termsLbl.attributedText = attrStr
                        
                        
                     
//                        let str = termsStr.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
//                             self.termsLbl.text = str
                   
                    
                    }
                   
                    DispatchQueue.main.async {
                     
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    

}
