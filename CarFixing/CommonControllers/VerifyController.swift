//
//  VerifyController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/6/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class VerifyController: UIViewController {

    @IBOutlet weak var firstText: UITextField!
    
    @IBOutlet weak var secondText: UITextField!
    
    @IBOutlet weak var thirdText: UITextField!
    
    @IBOutlet weak var fourthText: UITextField!
    var textStr : String!
    
    var phoneNumberStr : String!
    
    @IBOutlet weak var verifyMobileNo: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var resetCode: UIButton!
    
    @IBOutlet weak var dontReceive: UILabel!
    
    @IBOutlet weak var scroll: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        print(phoneNumberStr)
      
        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: firstText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: secondText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: thirdText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: fourthText, lineColor: myColor, myView: self.view)
        
        verifyMobileNo.text = languageChangeString(a_str: "Verify Mobile Number")
        detail.text = languageChangeString(a_str: "OTP has been sent to you on your\n mobile number.please enter it below")
        dontReceive.text = languageChangeString(a_str: "Did not receive your verification code yet?")
        
        done.setTitle(languageChangeString(a_str: "DONE"), for: UIControlState.normal)
        resetCode.setTitle(languageChangeString(a_str: "Resend Verification Code"), for: UIControlState.normal)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)

        firstText.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        secondText.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        thirdText.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        fourthText.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case firstText:
                secondText.becomeFirstResponder()
            case secondText:
                thirdText.becomeFirstResponder()
            case thirdText:
                fourthText.becomeFirstResponder()
            case fourthText:
                fourthText.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        
       
        
        if Reachability.isConnectedToNetwork(){
            
            varifyServiceCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
    }
    
    @IBAction func resentClicked(_ sender: Any) {
    
        if Reachability.isConnectedToNetwork(){
            
            varifyServiceCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
    
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func varifyServiceCall ()
    {
        
        //http://voliveafrica.com/carfix/services/check_otp?API-KEY=98745612&mobile=5456456&otp=24545
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let statesCities = "\(Base_Url)check_otp?"
        
        textStr = String(format: "%@%@%@%@", firstText.text!,secondText.text!,thirdText.text!,fourthText.text!)
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "mobile" : phoneNumberStr! , "otp" : textStr! , "lang" : language ?? ""]
        
        print(parameters)
         Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(statesCities, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
               
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let reset = storyBoard.instantiateViewController(withIdentifier: "ResetController") as? ResetController
                    reset?.mobileStr = self.phoneNumberStr
                self.navigationController?.pushViewController(reset!, animated: true)
                   
                }
                else
                {
                     Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert (message: message!!)
                }
            }
        }
    }
}
extension VerifyController: UITextFieldDelegate{
    public func textFieldDidBeginEditing (_ textField : UITextField) {
        
        if textField == firstText {
            scroll.contentOffset.y = +20
            
            
        }
        else if textField == secondText{
            scroll.contentOffset.y = +20
        }else if textField == thirdText{
            scroll.contentOffset.y = +20

        }else if textField == fourthText{
            scroll.contentOffset.y = +20

        }
        
        
        
    }
    public func textFieldDidEndEditing (_ textField : UITextField){
        firstText.resignFirstResponder()
        secondText.resignFirstResponder()
        thirdText.resignFirstResponder()
        fourthText.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue : Bool = false
        if textField == firstText {
            
            secondText.becomeFirstResponder()
            
            returnValue = true
        }
        else if textField == secondText{
            
            thirdText.becomeFirstResponder()
            
            returnValue = true
        }else if textField == thirdText{
            fourthText.becomeFirstResponder()
        }
        else if textField == fourthText{
            fourthText.resignFirstResponder()
        }
        
        
        
        return returnValue
    }
    
    
    
}
