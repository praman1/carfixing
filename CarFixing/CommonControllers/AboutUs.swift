//
//  AboutUs.swift
//  CarFixing
//
//  Created by Suman Guntuka on 25/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class AboutUs: UIViewController {

    @IBOutlet weak var aboutText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = languageChangeString(a_str: "About Us")
        
        self.aboutusServiceCall()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuBtn(_ sender: Any) {
        
         present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
  
    }
    
    func aboutusServiceCall ()
    {
        //http://volive.in/carfix/services/aboutus?API-KEY=98745612&lang=en
        
        let vehicles = "\(Base_Url)aboutus?"
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"lang" : language ?? ""]
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    if let responseData1 = responseData["aboutus"] as? Dictionary<String, AnyObject> {
                        
                        let textStr = responseData1["about_en"] as! String

                        var attrStr: NSAttributedString? = nil
                    
                        if let anEncoding = textStr.data(using: String.Encoding(rawValue: String.Encoding.unicode.rawValue)) {
                            attrStr = try? NSAttributedString(data: anEncoding, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
                        }
                        
                        self.aboutText.attributedText = attrStr
                    }
                    
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    
    
}
