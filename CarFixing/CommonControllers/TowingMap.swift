//
//  TowingMap.swift
//  CarFixing
//
//  Created by Suman Guntuka on 03/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces





class TowingMap: UIViewController ,CLLocationManagerDelegate, GMSMapViewDelegate{

    @IBOutlet weak var gmapView1: GMSMapView!
    
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
    var gmapView: GMSMapView!
    var latitude : String!
    var longitude : String!
    var latitude1 : String!
    var longitude1 : String!
     var appDelegate : AppDelegate!
    var timerTracker:Timer! = nil
    
    var  userDetails : String!
    var parameters : Dictionary<String, Any>!
    
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var vendorImg: UIImageView!
    
    @IBOutlet var trackingLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    var userNameStr : String!
    var userImgStr : String!
    var messageStr : String!
    var vendorId : String!
    var towingId : String!
    var towStr : String!
    
    var pushDictionary:[String:Any] = [String:Any]()
    
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(pushDictionary)
           appDelegate = UIApplication.shared.delegate as! AppDelegate
        gmapView1.layer.borderWidth = 4
        gmapView1.layer.borderColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
        
        gmapView?.delegate = self
        gmapView1.delegate = self
        
        towStr = UserDefaults.standard.object(forKey: "towingSet") as! String
        
        if towStr == "towSet" {
          
            latitude1 = UserDefaults.standard.object(forKey: "towlatitude") as! String
            
            longitude1 = UserDefaults.standard.object(forKey: "towlongtude") as! String
            userImgStr = UserDefaults.standard.object(forKey: "towimage") as! String
            let names = UserDefaults.standard.object(forKey: "nameVendor") as! String
             vendorId = UserDefaults.standard.object(forKey: "vendor_idStr") as! String
            towingId = UserDefaults.standard.object(forKey: "towingID") as! String
            vendorImg.sd_setImage(with: URL(string: userImgStr), placeholderImage: UIImage(named:"as img4"))
            vendorImg.layer.cornerRadius = vendorImg.frame.size.height/2
            nameLbl.text = names
            messageLbl.text = ""
            
        }else if towStr == "towSt"{
            let message = pushDictionary["message"] as! String
            let name = pushDictionary["vendor_name"] as! String
            latitude1 = pushDictionary["latitude"] as! String
            longitude1 = pushDictionary["longitude"] as! String
            let image1 = pushDictionary["image"] as! String
            vendorId = pushDictionary["vendor_id"] as! String
            
            print("vendor id when post dict",vendorId)
            
            //  let towingId = infoDict!["towing_id"] as! String
            towingId = pushDictionary ["towing_id"] as! String
            //towingId = String(towing)
            
            userImgStr = base_path + image1
            
            vendorImg.sd_setImage(with: URL(string: userImgStr  ), placeholderImage: UIImage(named:"as img4"))
            vendorImg.layer.cornerRadius = vendorImg.frame.size.height/2
            nameLbl.text = name
            messageLbl.text = message
        }
        else{
           pushDictionary = UserDefaults.standard.object(forKey: "postDict") as! [String : Any]
            let message = pushDictionary["message"] as! String
            let name = pushDictionary["vendor_name"] as! String
            latitude1 = pushDictionary["latitude"] as! String
            longitude1 = pushDictionary["longitude"] as! String
            let image1 = pushDictionary["image"] as! String
            vendorId = pushDictionary["vendor_id"] as! String
            
            print("vendor id when post dict",vendorId)
            
            //            let towingId = infoDict!["towing_id"] as! String
            towingId = pushDictionary ["towing_id"] as! String
            //towingId = String(towing)
            
            userImgStr = base_path + image1
            
            vendorImg.sd_setImage(with: URL(string: userImgStr  ), placeholderImage: UIImage(named:"as img4"))
            vendorImg.layer.cornerRadius = vendorImg.frame.size.height/2
            nameLbl.text = name
            messageLbl.text = message
            
            
        }
         trackingLbl.text = languageChangeString(a_str: "Tracking Service Provider")
        
        
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
            if appDelegate.loginUser == "customer" {
                
                  towingRequestsTrackingServiceCall()
                
            }
           
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }

       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        timerTracker = Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(notificationReceived), userInfo: nil, repeats: true)
        //RunLoop.current.add(timerTracker, forMode: RunLoopMode.defaultRunLoopMode)
    }
    override func viewDidDisappear(_ animated: Bool) {
           timerTracker.invalidate()
    }
    @objc func notificationReceived () {
        print("timer run");
        
         towingRequestsTrackingServiceCall()
        
    }
        
    @IBAction func closebtn(_ sender: Any) {
        
        if towStr == "towSet"
        {
            
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
//            self .present(tab, animated: true, completion: nil)
            dismiss(animated: true, completion: nil)
            
        }else if towStr == "towSt"
        {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
            self .present(tab, animated: true, completion: nil)
        }
        else
        {
            
            self.navigationController?.popViewController(animated: true)
        }
        
    }

    //MARK:- NAVIGATE MAP
    @IBAction func navigateBtn(_ sender: Any) {
        print("navigatlat and long",self.latitude1,self.longitude1)
      
        DispatchQueue.main.async {
            if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL))
            {
                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(self.latitude!),\(self.longitude!)&daddr=\(self.latitude1!),\(self.longitude1!)&directionsmode=driving&zoom=16&views=traffic")!)
            }
            
        }
    }
    func pinGenerate ()
    {
        
        self.gmapView1.clear()
        
        let dLati = Double(latitude1 ?? "") ?? 0.0
        let dLong = Double(longitude1 ?? "") ?? 0.0
        
        let dLati2 = Double(self.latitude ?? "") ?? 0.0
        let dLong2 = Double(self.longitude ?? "") ?? 0.0
//
//        let locationMark :GMSMarker!
//        let posit = CLLocationCoordinate2D(latitude: dLati2 , longitude: dLong2 )
//        locationMark = GMSMarker(position: posit )
//        locationMark.map = self.gmapView1
//        locationMark.appearAnimation =  .pop
//        locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
//        locationMark.opacity = 0.75
//        locationMark.isFlat = true

//
//        let locationMarker1 :GMSMarker!
//
//        let position1 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
//        locationMarker1 = GMSMarker(position: position1 )
//        locationMarker1.map = self.gmapView1
//        locationMarker1.appearAnimation =  .pop
//        locationMarker1.icon = UIImage(named: "truck")?.withRenderingMode(.alwaysTemplate)
//        locationMarker1.opacity = 0.75
//        locationMarker1.isFlat = true
        
 
//
//        let dLati1 = Double(latitude ?? "") ?? 0.0
//        let dLong1 = Double(longitude ?? "") ?? 0.0
//
//        let path = GMSMutablePath()
//        path.add(CLLocationCoordinate2D(latitude: dLati1, longitude: dLong1))
//        path.add(CLLocationCoordinate2D(latitude: dLati, longitude: dLong))
//
//
//         // gmapView1.clear()
//
//        let polyline = GMSPolyline(path: path)
//
//        polyline.strokeColor = #colorLiteral(red: 0.9215686275, green: 0, blue: 0.1137254902, alpha: 1)
//        polyline.strokeWidth = 6.0
//        polyline.geodesic = true
//        polyline.map = gmapView1
        
        let locationstart = CLLocation(latitude: dLati2, longitude: dLong2)
        let locationEnd = CLLocation(latitude: dLati, longitude: dLong)
        
            self.drawPath(startLocation: locationstart, endLocation: locationEnd)
        
//         let loc : CLLocation = CLLocation(latitude: dLati, longitude: dLong)
//         updateMapFrame(newLocation: loc, zoom: self.gmapView1.camera.zoom)
       
    }
    //Zoom map with cordinate
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: 15)
        
        self.gmapView1.animate(to: camera)
    }
    
    
    
    //MARK: - this is function for create direction path, from start location to desination location
    
    func drawPath(startLocation: CLLocation, endLocation: CLLocation)
    {
        
         self.gmapView1.clear()
        
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        
        //print("origin and dest",origin,destination)
        
        createMarker(titleMarker: "", iconMarker: (UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate))!, latitude: startLocation.coordinate.latitude, longitude: startLocation.coordinate.longitude)
        createMarker(titleMarker: "", iconMarker: (UIImage(named: "truck")?.withRenderingMode(.alwaysTemplate))!, latitude: endLocation.coordinate.latitude, longitude: endLocation.coordinate.longitude)
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyCvkQqnhhAJ2mjqGsZoaoxIIPeYye05e4Y"
    
        Alamofire.request(url).responseJSON { response in
            
            print(response.request as Any)  // original URL request
            print(response.response as Any) // HTTP URL response
            print(response.data as Any)     // server data
            print(response.result as Any)   // result of response serialization
            
            let json = JSON(data: response.data!)
            print("json",json)
            let routes = json["routes"].arrayValue
            
            // print route using Polyline
           
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyline = GMSPolyline.init(path: path)
                polyline.strokeWidth = 4
                polyline.strokeColor = UIColor.red
                polyline.map = self.gmapView1
                 polyline.geodesic = true
            }
       
            
        }
        
    }
    
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.opacity = 0.75
        marker.appearAnimation =  .pop
       marker.isFlat = true
        marker.map = self.gmapView1
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    override func viewWillAppear(_ animated: Bool) {
//
//        self.navigationController?.isNavigationBarHidden = true
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//
//        self.navigationController?.isNavigationBarHidden = false
//    }

    @IBAction func acceptBtn(_ sender: Any) {
       
    }
    
    /// maping Details
    
    func isAuthorizedtoGetUserLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print(marker.snippet as Any)
        
        if marker.snippet == nil
        {
            
        }else{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let shop = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
            
            shop?.vendorId = marker.snippet
            
            //  UserDefaults.standard.set(marker.snippet, forKey: "user_id")
            
            self.navigationController?.pushViewController(shop!, animated: true)
            
        }
        
        return true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
        let geocoder = GMSGeocoder()
        
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
        
        latitude = String(format: "%.8f", currentLocation.latitude)
        longitude = String(format: "%.8f",currentLocation.longitude)
        
        let latLang = "\(latitude), \(longitude)"
        UserDefaults.standard.set( latitude, forKey: "latitude")
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
        print("current lat and long \(latLang)")
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                
                //print("adress of that location is \(result!)")
                
                // let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                
                // self.addressOfuser = address?.strstr(needle: ",")
                //print(self.addressOfuser)
                
                //UserDefaults.standard.set(self.addressOfuser, forKey: "address")
                
                let dLati = Double(self.latitude ?? "") ?? 0.0
                let dLong = Double(self.longitude ?? "") ?? 0.0
                
                let locationMark :GMSMarker!
                let posit = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                locationMark = GMSMarker(position: posit )
                locationMark.map = self.gmapView1
                locationMark.appearAnimation =  .pop
                locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
                locationMark.opacity = 0.75
                locationMark.isFlat = true
                
            }
        }
        
        DispatchQueue.main.async {
            self.gmapView?.camera = camera
            self.gmapView1?.camera = camera
        }
        self.gmapView?.animate(to: camera)
        self.gmapView1?.animate(to: camera)
        manager.stopUpdatingLocation()
        if appDelegate.loginUser == "customer" {
            
           
            
        }else{
            
              pinGenerate ()
        }
        
       
        
    }
    
    func setupLocationMarker() {
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
            
        }
        
    }
    
    
    
    func towingRequestsTrackingServiceCall ()
    {
        //http://volive.in/carfix/services/user_profile?API-KEY=98745612&user_id=390
        
        
        let vendorid = vendorId
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        //appde.loginUser = "customer"
        
        if appDelegate.loginUser == "customer" {
            
            userDetails = "\(Base_Url)user_profile"
            parameters = ["API-KEY": APIKEY , "user_id" : vendorid ?? "" , "lang" : language ?? "" ]
            
        }
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(self.userDetails, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print("response of tracking",responseData,self.userDetails)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    
                    
                    // DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
 
                    
                    let towingData = responseData["user_data"] as! NSDictionary
                    

                    self.longitude1 =  towingData["longitude"] as! String
                    self.latitude1  = towingData["latitude"] as! String
                    print("timer update latest lat and long",self.latitude1,self.longitude1)
                    
                    self.pinGenerate ()
                    
                    
 
                }
                else
                {
                    DispatchQueue.main.async {
                        Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    }
                }
            }
        }
    }
    

}

/*
func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
 
    let config = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
 
    //        let dLati = Double(latitude1 ?? "") ?? 0.0
    //        let dLong = Double(longitude1 ?? "") ?? 0.0
    //        let dLati1 = Double(latitude ?? "") ?? 0.0
    //        let dLong1 = Double(longitude ?? "") ?? 0.0
    source.latitude = latitude
    source.longitude = longitude
 
 
    let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving")!
 
    let task = session.dataTask(with: url, completionHandler: {
        (data, response, error) in
        if error != nil {
            print(error!.localizedDescription)
        }else{
            do {
                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
 
                    let routes = json["routes"] as? [Any]
                    let overview_polyline = routes?[0] as?[String:Any]
                    let polyString = overview_polyline?["points"] as?String
 
                    let path = GMSPath.init(fromEncodedPath: polyString!)
 
                    let polyline = GMSPolyline(path: path)
                    polyline.strokeColor = .black
                    polyline.strokeWidth = 10.0
                    polyline.map = gmapView1
 
 
 
 
                    //Call this method to draw path on map
                    //self.showPath(polyStr: polyString!)
                }
 
            }catch{
                print("error in JSONSerialization")
            }
        }
    })
    task.resume()
}
*/
