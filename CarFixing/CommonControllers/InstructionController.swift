//
//  InstructionController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/4/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class InstructionController: UIViewController {
    
    @IBOutlet weak var pageCollection: UICollectionView!
    @IBOutlet weak var getStartedBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
   
    @IBOutlet weak var skipBtn: UIButton!
    var testStr : String!

    var myInt: Int = 0
    var myStr: String = ""
    var myFloat: Float = 1.22
    var myBols : Bool = true
    var imageView = UIImageView()
    var indexCell: Int = 0
    
    var appDelegate : AppDelegate!
   
    //collectio types
     var customerDataArr: [[String:AnyObject]]!
   
    var imagesArray = [String]()
    var headersArray = [String]()
    var textArray = [String]()
    //var mydict = [String:Float]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        self .imageSlideShow()
        skipBtn.setTitle(languageChangeString(a_str: "SKIP"), for: UIControlState.normal)
        getStartedBtn.setTitle(languageChangeString(a_str: "NEXT"), for: UIControlState.normal)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func backBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
   
    func imageSlideShow() {
        
        if testStr == "serviceProvider" {
            
//            imagesArray = ["Im_1x","2Im_1x","3Im_1x"]
//            headersArray = ["Request from Customer","Sent Bid Quotation","Provide service to Customers"]
//            textArray = ["Lorem ipsum dummy printing text of typesetting industry","Lorem ipsum dummy printing text of typesetting industry","Lorem ipsum dummy printing text of typesetting industry"]
           
            self.pageCollection.reloadData()
            
        }else{
            
//            imagesArray .append("obs-1")
//            imagesArray.append("obs-2")
//            imagesArray.append("obs-3")
//            imagesArray.append("obs-4")
            // imagesArray = ["obs-1","obs-2","obs-3","obs-4"]
            // print("\(imagesArray)")
//            headersArray = ["Find Professional Services","Pick the best","Choose service provider","Service in process"]
//            textArray = ["Lorem ipsum dummy printing text of typesetting industry","Lorem ipsum dummy printing text of typesetting industry","Lorem ipsum dummy printing text of typesetting industry","Lorem ipsum dummy printing text of typesetting industry"]
            
            self.pageCollection.reloadData()
        }
        self.pageControl.numberOfPages = imagesArray.count

    }
    
    func startTimer() {

        let timer = Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: Selector("scrollToNextCell"), userInfo: nil, repeats: true);

    }
    
    func scrollToNextCell(){

        let contentOffset = pageCollection.contentOffset;
        let cellSize = CGSize(width: view.frame.width, height: view.frame.height)

        self.pageCollection.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated : true)

    }

    @IBAction func dontShowBtnClicked(_ sender: Any) {
   
    }
    
    @IBAction func getStartedClicked(_ sender: Any) {
        
        let indexOfScrolledCell : NSInteger!

        indexOfScrolledCell = indexCell

        if  appDelegate.loginUser == "customer"
        {
            if indexCell < 3 {
                var path = IndexPath(row: indexOfScrolledCell + 1, section: 0)
                pageCollection.scrollToItem(at: path, at: .right, animated: true)
                pageControl.currentPage = path.row
    
            }else
            {
                DispatchQueue.main.async {
                    
                    self.getStartedBtn.setTitle(self.languageChangeString(a_str: "Login"), for: UIControlState.normal)
                    self.startTimer()
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
                    self.navigationController?.pushViewController(login!, animated: true)
                }
            }
            
        }else
        {
            
            if indexCell < 2 {
                var path = IndexPath(row: indexOfScrolledCell + 1, section: 0)
                pageCollection.scrollToItem(at: path, at: .right, animated: true)
                pageControl.currentPage = path.row
                
            }else
            {
                 DispatchQueue.main.async {
                    
                self.getStartedBtn.setTitle(self.languageChangeString(a_str: "Login"), for: UIControlState.normal)
                self.startTimer()
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let login = storyBoard.instantiateViewController(withIdentifier: "ServiceLoginController") as? ServiceLoginController
                self.navigationController?.pushViewController(login!, animated: true)
            }
            }
            
        }
       
    }
    
    @IBAction func skipBtnClicked(_ sender: Any) {
       
        if appDelegate.loginUser == "customer"
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
            self.navigationController?.pushViewController(login!, animated: true)
        }else
        {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let login = storyBoard.instantiateViewController(withIdentifier: "ServiceLoginController") as? ServiceLoginController
            self.navigationController?.pushViewController(login!, animated: true)
        }
   
    
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)

        self.pageControl.currentPage = page

    }

}

extension InstructionController:  UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //page collection view delgates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pageCollection.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? InstructionCell
//        cell?.img.image = UIImage (named:self.imagesArray[indexPath.row])
//        let imagePath: String = self.imagesArray[indexPath.row]"
        
        cell?.img.sd_setImage(with: URL(string: self.imagesArray[indexPath.row]), placeholderImage: UIImage(named:"placeholder"))
        
        cell?.headerLbl.text = self.headersArray[indexPath.row]
        cell?.subLbl.text = self.textArray[indexPath.row]
        
        //self.pageCollection.reloadData()
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let width : CGFloat =  (CGFloat) (self.pageCollection.frame.size.width)
        
        return CGSize(width: self.pageCollection.frame.size.width,height:  self.pageCollection.frame.size.height)
 
        /*
          return CGSize(width: self.pageCollection.frame.size.width/2, height: self.pageCollection.frame.size.height/2);
 */
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        return UIEdgeInsetsMake(0,5,0,5)
//
//    }
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
     
        if testStr == "serviceProvider"{
            
        indexCell = indexPath.index(after: indexPath.item - 1)
        
        }else{
            
             indexCell = indexPath.index(after: indexPath.item - 1)
            
        }
      
    }
  
}
