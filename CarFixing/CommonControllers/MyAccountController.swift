//
//  MyAccountController.swift
//  carFixing
//
//  Created by Apple on 12/13/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class MyAccountController: UIViewController {
    
    @IBOutlet var scrollVW: UIScrollView!
    @IBOutlet weak var userNameTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var mobileNoTF: UITextField!
    @IBOutlet weak var personalInfo: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var emailId: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var changePw: UILabel!
    
    @IBOutlet weak var changePasswordBtn: UIButton!
    
    @IBOutlet weak var logout: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollVW.isScrollEnabled = false
        navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "My Account")
        personalInfo.text = languageChangeString(a_str: "PERSONAL INFORMATION")
        userName.text = languageChangeString(a_str: "USER NAME")
        emailId.text = languageChangeString(a_str: "EMAIL ID")
        mobileNo.text = languageChangeString(a_str: "MOBILE NUMBER")
        changePw.text = languageChangeString(a_str: "CHANGE PASSWORD")
        
        changePasswordBtn.setTitle(languageChangeString(a_str: "CHANGE PASSWORD"), for: UIControlState.normal)
        logout.text = languageChangeString(a_str: "LOGOUT")
        
        
        
        userNameTF.text = UserDefaults.standard.object(forKey: "name") as? String
        emailTF.text = UserDefaults.standard.object(forKey: "email") as? String
        mobileNoTF.text = UserDefaults.standard.object(forKey: "mobile") as? String
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func backBtnClicked(){
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    @IBAction func changePasswordBtn(_ sender: Any) {
        
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let list = storyBoard.instantiateViewController(withIdentifier: "NewController") as? NewController
        self.navigationController?.pushViewController(list!, animated: true)
        
    }
    
    @IBAction func logOutBtn(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let window : UIWindow = ((UIApplication.shared.delegate?.window)!)!
        window.rootViewController = storyboard.instantiateInitialViewController()
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
