//
//  VendorTowingMap.swift
//  CarFixing
//
//  Created by Suman Guntuka on 04/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces

class VendorTowingMap: UIViewController  ,CLLocationManagerDelegate, GMSMapViewDelegate{
    
    
    @IBOutlet var trackingLbl: UILabel!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var complte: UIButton!
    @IBOutlet weak var gmapView1: GMSMapView!
    
    @IBOutlet var chatBtn: UIButton!
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
    var gmapView: GMSMapView!
    var latitude : String!
    var longitude : String!
    var latitude1 : String!
    var longitude1 : String!
    var strSep : String!
    var notiStr : String!
    var strLat1 : String!
    var strLong2 : String!
    var fromStr : String!
    var jobUserId = NSString()
    var pushDictionary:[String:Any] = [String:Any]()
    var userImage = NSString()
    var completeBool : Bool!
    
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gmapView?.delegate = self
        gmapView1.delegate = self
        
        completeBool = false
        
        complte.setTitle(languageChangeString(a_str: "Complete"), for: UIControlState.normal)
          chatBtn.setTitle(languageChangeString(a_str: "Send"), for: UIControlState.normal)
        trackingLbl.text = languageChangeString(a_str: "Tracking Customer")
 
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
    }
   
    @IBAction func closeBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        strSep = UserDefaults.standard.object(forKey: "towingSep") as! String

        if strSep == "towList"
        {
            self.navigationController?.popViewController(animated: true)
        }else
        {
            //  self.dismiss(animated: true, completion: nil)
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
   
    }
    
    func pinGenerate ()
    {
        
      //  "towlat"  "towlong")
        if UserDefaults.standard.object(forKey: "towlat") != nil
        {
            latitude1 = UserDefaults.standard.object(forKey: "towlat") as! String
            longitude1 = UserDefaults.standard.object(forKey: "towlong") as! String
            
            let dLati = Double(latitude1 ?? "") ?? 0.0
            let dLong = Double(longitude1 ?? "") ?? 0.0
            
        let locationMarker1 :GMSMarker!
        let position1 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
        locationMarker1 = GMSMarker(position: position1 )
        locationMarker1.map = self.gmapView1
        locationMarker1.appearAnimation =  .pop
        locationMarker1.icon = UIImage(named: "truck")?.withRenderingMode(.alwaysTemplate)
        locationMarker1.opacity = 0.75
        locationMarker1.isFlat = true
        
        let dLati1 = Double(latitude ?? "") ?? 0.0
        let dLong1 = Double(longitude ?? "") ?? 0.0
        
        let path = GMSMutablePath()
        path.add(CLLocationCoordinate2D(latitude: dLati1, longitude: dLong1))
        path.add(CLLocationCoordinate2D(latitude: dLati, longitude: dLong))
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = #colorLiteral(red: 0.9215686275, green: 0, blue: 0.1137254902, alpha: 1)
        polyline.strokeWidth = 6.0
        polyline.geodesic = true
        polyline.map = gmapView1
            
             }
        
    }

    func isAuthorizedtoGetUserLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print(marker.snippet as Any)
        
        if marker.snippet == nil
        {
            
        }else{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let shop = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
            
            shop?.vendorId = marker.snippet
            
            //  UserDefaults.standard.set(marker.snippet, forKey: "user_id")
            
            self.navigationController?.pushViewController(shop!, animated: true)
            
        }
        
        return true
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
        let geocoder = GMSGeocoder()
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
        
        latitude = String(format: "%.8f", currentLocation.latitude)
        longitude = String(format: "%.8f",currentLocation.longitude)
        
        let latLang = "\(latitude), \(longitude)"
        UserDefaults.standard.set( latitude, forKey: "latitude")
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
        print("current lat and long \(latLang)")
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                
                let dLati = Double(self.latitude ?? "") ?? 0.0
                let dLong = Double(self.longitude ?? "") ?? 0.0
                
                let locationMark :GMSMarker!
                let posit = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                locationMark = GMSMarker(position: posit )
                locationMark.map = self.gmapView1
                locationMark.appearAnimation =  .pop
                locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
                locationMark.opacity = 0.75
                locationMark.isFlat = true
                
            }
        }
        
        DispatchQueue.main.async {
            self.gmapView?.camera = camera
            self.gmapView1?.camera = camera
        }
        self.gmapView?.animate(to: camera)
        self.gmapView1?.animate(to: camera)
        manager.stopUpdatingLocation()
        
        pinGenerate ()
        
        
    }
    
    func setupLocationMarker() {
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
            
        }
        
    }
   
    
    //MARK:- CHAT BTN ACTION
    @IBAction func chatBtnAction(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let chat = storyBoard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
         
        
        jobUserId = UserDefaults.standard.object(forKey: "userId") as! NSString
 
        chat?.profileImg =  userImage as String?
//
//        chat?.vendorIdS = senderstr
//        chat?.senderStr = senderstr
        
        chat?.check = "1"
        chat?.vendorIdS = jobUserId as String
        
        self.navigationController?.pushViewController(chat!, animated: true)
        
        
    }
    //  MARK:- NAVIGATE BTN ACTION
    @IBAction func navigateBtn(_ sender: Any) {
        
            DispatchQueue.main.async {
        
        let strLat : String = UserDefaults.standard.object(forKey: "latitude") as! String
        let strLong : String = UserDefaults.standard.object(forKey: "longitude") as! String
               
                print(UserDefaults.standard.object(forKey: "towingSep") as Any)
                
                self.notiStr = (UserDefaults.standard.object(forKey: "towingSep") as! String)
            
                if self.notiStr == "towList"
                 {
                    self.strLat1 = UserDefaults.standard.object(forKey: "towlat") as? String
                    self.strLong2 = UserDefaults.standard.object(forKey: "towlong") as? String
        
            }else
            {
                self.strLat1 = UserDefaults.standard.object(forKey: "lat") as? String
                self.strLong2 = UserDefaults.standard.object(forKey: "long") as? String
                
                }
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL))
                                {
                                    UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(strLat),\(strLong)&daddr=\(self.strLat1!),\(self.strLong2!)&directionsmode=driving&zoom=14&views=traffic")!)
                                }
        
                            }
        
    }
    
    @IBAction func compltedBtn(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let filter = storyBoard.instantiateViewController(withIdentifier: "CompltedTowing")
        
        self .present(filter, animated: true, completion: nil)
    
//        http://voliveafrica.com/carfix/services/complete_towing
//        parameters: towing_id,vendor_id,price
        
    }
    
    
}

extension VendorTowingMap : UITextFieldDelegate{
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        return true
        
    }
   
}


