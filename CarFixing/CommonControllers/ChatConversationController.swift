//
//  ChatConversationController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 23/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//
import UIKit
import Alamofire

class ChatConversationController: UIViewController {

    var senderStr : String = ""
    var recieverStr : String = ""
    
    @IBOutlet var sendBtnOutlet: UIButton!
    @IBOutlet weak var myTblview: UITableView!
    @IBOutlet weak var textViewBack: UIView!
    @IBOutlet weak var msgTxtView: UITextView!
    @IBOutlet weak var placeholderlbl: UILabel!
    
    var keyboardHeight : String!
    var checkString:String = ""
    var profileImg : String!
    var vendorIdS : String!
    var reciveTowID : String!
    var check = NSString()
    //var messages : NSMutableArray?
   // var typeDiffArr : NSMutableArray?
    var chatsData: [[String:AnyObject]]!
    
    var messages = [String]()
    var dateArr = [String]()
    var typeDiffArr = [String]()
    var appDelegate = AppDelegate()
    let date = Date()
    let formatter = DateFormatter()
    var roleStr : String!
    
//    var sendorMsgArr : NSMutableArray?
//    var recieverMsgArr : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         appDelegate = UIApplication.shared.delegate as! AppDelegate
        sendBtnOutlet.layer.cornerRadius = sendBtnOutlet.frame.size.height/2
        
        print(profileImg)
        
        if appDelegate.chatId == "chat"
        {
            senderStr = vendorIdS!
            
            recieverStr = UserDefaults.standard.object(forKey: "user_id") as! String
            
            print(senderStr,recieverStr)
            
        }else if appDelegate.chatId == "appChat"
        {
            print(vendorIdS)
            
            //senderStr = vendorIdS!
            
            recieverStr = UserDefaults.standard.object(forKey: "user_id") as! String
            
            senderStr = UserDefaults.standard.object(forKey: "sender") as! String
            
            print(senderStr,recieverStr)
        }
//        else if check == "1"{
//            recieverStr = vendorIdS!
//            senderStr = UserDefaults.standard.object(forKey: "user_id") as! String
//        }
//        else if appDelegate.chatId == "towChat"
//        {
//            print(vendorIdS)
//            senderStr = vendorIdS!
//            recieverStr = reciveTowID!
//        }
//        else if checkString == "detailsBid"
//        {
//            print(vendorIdS)
//
//            //senderStr = vendorIdS!
//
//            recieverStr = vendorIdS
//
//            senderStr = UserDefaults.standard.object(forKey: "user_id") as! String
//
//            print(senderStr,recieverStr)
//        }
        
        else
        {
            senderStr = vendorIdS!
            recieverStr = UserDefaults.standard.object(forKey: "user_id") as! String
            
        }
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Chats")
        
        myTblview.rowHeight = UITableViewAutomaticDimension
        myTblview.estimatedRowHeight = 10000
        
       // messages = ["Aamir Ali","Hey,How are you?","I'm Good.\n\n Lorem Ipsum is Simply \n Dummy text of the.","Printing and typesetting industry.","****"];
       // typeDiffArr = ["0","1","0","0","0"];
        //print(messages ?? NSMutableArray.self)
        
        print ("my strings are \(senderStr , recieverStr )")
        
        let borderColor: UIColor? = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
        msgTxtView.layer.borderColor = borderColor?.cgColor
        msgTxtView.layer.borderWidth = 2.0
        
        self.chatingHistrory()
       
// self.myTblview.setContentOffset(CGPoint(x: 0, y: -self.myTblview.contentInset.top), animated: true)
        
//        let sectionsA = self.myTblview.numberOfSections
//        let noRows = self.myTblview.numberOfRows(inSection: messages.count)
//
//        let indexPath = NSIndexPath(row: noRows - 1, section: sectionsA)
//
//        self.myTblview.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.bottom, animated: true)
//
//        self.myTblview.contentOffset = CGPoint(x: 0, y: UITableViewScrollPosition.bottom.rawValue)
//
         NotificationCenter.default.addObserver(self, selector: #selector(messageSentCall), name: NSNotification.Name(rawValue:"messageSent"), object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tapGesture)
        
    }
    func dismissKeyBoard()
    {
        msgTxtView.resignFirstResponder()
    }
    
    func messageSentCall()
    {
        self.chatingHistrory()
    }

    @objc func backBtnClicked (){
       
        if appDelegate.chatTypeStr == "appChat"
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
            self .present(service, animated: true, completion: nil)
            
        }else if appDelegate.chatTypeStr == "msg"
        {
            self.navigationController?.popViewController(animated: true)
        }else
        {
             self.navigationController?.popViewController(animated: true)
        }
    }

//    override func viewWillAppear(_ animated: Bool) {
//
//        self.tabBarController?.tabBar.isHidden = true
//
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//
//        self.tabBarController?.tabBar.isHidden = false
//
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMsgBtn(_ sender: Any) {
        
        if msgTxtView.text.count > 0 {
     /*
//            formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
//            let result = formatter.string(from: date)
//            print(result)
//            dateArr.append(result)
 
 */
            
            messages.append(("\(msgTxtView.text!)" as NSString) as String)
            typeDiffArr.append("1")

                        textViewBack.isHidden = true
            self.view.endEditing(true)

            textViewBack.isHidden = false

            self.myTblview.scrollsToTop  = true
            
            self.messageSendCall ()
            
        }
    }
  
    func keyboardWillShow(_ notification: Notification) {
       
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            UserDefaults.standard.set(keyboardHeight, forKey: "key")
            
            print(keyboardHeight)
        }
    }
    
    //MARK:- CHAT HISTORY SERVICE CALL
    func chatingHistrory()
    {
        //http://voliveafrica.com/carfix/services/chat_history?API-KEY=98745612&sender_id=22&receiver_id=29
        
        //let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let gallery = "\(Base_Url)chat_history?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "sender_id" : self.senderStr , "receiver_id" : self.recieverStr ,"lang" : language ?? ""]
        
        print(parameters)
        
        Alamofire.request(gallery, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    DispatchQueue.main.async {
                        
                        self.messages = [String]()
                        self.typeDiffArr = [String]()
                        self.dateArr = [String]()
                    
                    // Services.sharedInstance.dissMissLoader()
                    self.chatsData = responseData["chat_history"] as? [[String:AnyObject]]
                    print("chats data is\(self.chatsData!)")
                    for eachitem in self.chatsData! {
                        
                        let message = eachitem["message"] as? String
                        let date = eachitem["date"] as? String
                        
                        let sender_id = eachitem["sender_id"] as? String!
                        
                        // self.messages?.add("\(message!)" as NSString)
                        self.messages.append(("\(message!)" as NSString) as String)
                        self.dateArr.append(("\(date!)" as NSString) as String)
                        
                        if sender_id == self.senderStr
                        {
                            self.typeDiffArr.append("0")
                            
                        }else
                        {
                            self.typeDiffArr.append("1")
                        }
                    }
                    print(self.messages as Any)
                    print(self.typeDiffArr as Any)
                    
                    self.myTblview.reloadData()
                    
                        if self.messages.count > 0
                        {
                            let indexPath = IndexPath(row: self.messages.count-1, section: 0)
                            self.myTblview.scrollToRow(at: indexPath, at: .bottom, animated: true)
                        }else
                        {
                            
                        }
               
                    }
                   // self.myTblview.contentOffset = CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude)
                }
                else
                {
                    DispatchQueue.main.async {
                    
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    
    //MARK:- MESSAGE SEND SERVICE CALL
    func messageSendCall ()
    {
        //  http://lvoliveafrica.com/carfix/services/send_msg // sender_id,receiver_id,message
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let sentmessage = "\(Base_Url)send_msg"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "sender_id" : self.recieverStr , "receiver_id" : self.senderStr , "message" : self.msgTxtView.text! ,"lang" : language ?? ""]
        
        print(parameters)
        
         Services.sharedInstance.loader(view: self.view)
        Alamofire.request(sentmessage, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                  
                    self.chatingHistrory()
                
                 DispatchQueue.main.async {
                    Services.sharedInstance.dissMissLoader()
                    self.msgTxtView.text = ""
                    self.placeholderlbl .isHidden = false
                    }
                }
                else
                {
            DispatchQueue.main.async {
                    self.showToast(message: message!!)
                    }
                    //  Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension ChatConversationController : UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        NotificationCenter.default.addObserver(  self,  selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow,  object: nil
        )
        
        return true
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        placeholderlbl.isHidden = true;
        
        let myString = UserDefaults.standard.object(forKey: "key") as? CGFloat
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.textViewBack.frame = CGRect(x:self.textViewBack.frame.origin.x, y: self.textViewBack.frame.origin.y - myString!  , width:self.textViewBack.frame.size.width, height:self.textViewBack.frame.size.height);
            
            self.myTblview.frame = CGRect(x: self.myTblview.frame.origin.x, y: self.myTblview.frame.origin.y, width: self.myTblview.frame.size.width, height: self.myTblview.frame.size.height-myString!)
            
            if self.messages.count > 0{
                
                let indexPath = IndexPath(row: self.messages.count-1, section: 0)
                self.myTblview.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }else
            {
            }
        })
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        let myString = UserDefaults.standard.object(forKey: "key") as? CGFloat
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.roleStr = UserDefaults.standard.object(forKey: "role") as! String
            
            if self.roleStr == "user"
            {
                self.textViewBack.frame = CGRect(x:self.textViewBack.frame.origin.x, y:self.textViewBack.frame.origin.y + myString! , width:self.textViewBack.frame.size.width, height:self.textViewBack.frame.size.height)
                
                self.myTblview.frame = CGRect(x: self.myTblview.frame.origin.x, y: self.myTblview.frame.origin.y, width: self.myTblview.frame.size.width, height: self.view.frame.size.height - (self.textViewBack.frame.size.height + 60))
            }else
            {
                self.textViewBack.frame = CGRect(x:self.textViewBack.frame.origin.x, y:self.textViewBack.frame.origin.y + myString! , width:self.textViewBack.frame.size.width, height:self.textViewBack.frame.size.height)
                
                self.myTblview.frame = CGRect(x: self.myTblview.frame.origin.x, y: self.myTblview.frame.origin.y, width: self.myTblview.frame.size.width, height: self.view.frame.size.height - self.textViewBack.frame.size.height)
            }
           
            
        })
    }
    }

extension ChatConversationController: UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if messages.count == nil
        {
            return 0
        }
        else{
            return (messages.count)
        }
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : ChatTextCell
      
        if "\(typeDiffArr[indexPath.row])" == "0" {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! ChatTextCell
           
            cell.otherMsgLbl.text = "\(messages[indexPath.row] as NSString)"
           
            cell.othertimeLbl.text = "\(dateArr[indexPath.row] as NSString)"
            
            cell.otherBackView.layer.cornerRadius = 3.0
            
            if indexPath.row != 0{
                
                if "\(typeDiffArr[indexPath.row])" == "\(typeDiffArr[indexPath.row - 1])"{
                    
                    cell.otherPimage.isHidden = true
                    
                    cell.otherPimage.layer.cornerRadius = cell.otherPimage.frame.size.height/2
                    cell.otherPimage.clipsToBounds = true
                    
                    if self.profileImg == nil
                    {
                        cell.otherPimage.image = UIImage(named:"profile pic")
                    }else
                    {

                        cell.otherPimage.sd_setImage(with: URL(string: self.profileImg! ), placeholderImage: UIImage(named:"profile pic"))
                    }

                }else{
                    //cell.otherPimage.isHidden = true
                    
                    cell.otherPimage.layer.cornerRadius = cell.otherPimage.frame.size.height/2
                    cell.otherPimage.clipsToBounds = true
                    
                   // cell.otherPimage.sd_setImage(with: URL(string: self.profileImg! ), placeholderImage: UIImage(named:"profile pic"))
                    
                    cell.otherPimage.isHidden = false
                    if self.profileImg == nil
                    {
                        cell.otherPimage.image = UIImage(named:"profile pic")
                    }else
                    {
                        
                        cell.otherPimage.sd_setImage(with: URL(string: self.profileImg! ), placeholderImage: UIImage(named:"profile pic"))
                    }
                }
            }
            
        }else{
            
            cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! ChatTextCell
            cell.myMsgLbl.text = "\(messages[indexPath.row])"
            cell.mytimeLbl.text = "\(dateArr[indexPath.row] as NSString)"
            cell.myBackView.layer.cornerRadius = 3.0
           
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //print("UITableViewAutomaticDimension : \(UITableViewAutomaticDimension)")
        return UITableViewAutomaticDimension
    }
    
}
