//
//  ViewController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/4/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    var appde = AppDelegate()
   
    @IBOutlet weak var customerBtn: UIButton!
    @IBOutlet weak var logoImg: UIImageView!
    
    @IBOutlet weak var changeLanguage: UIButton!
    @IBOutlet weak var spBtn: UIButton!
    var customerDataArr1: [[String:AnyObject]]!
    
    var imagesArray1 = [String]()
    var headersArray1 = [String]()
    var textArray1 = [String]()
    
    var compareStr : String!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        appde = UIApplication.shared.delegate as! AppDelegate
        logoImg.image = UIImage(named:"splash screen logo")
        UserDefaults.standard.set("en", forKey: "currentLanguage")
        
//      let defaults = UserDefaults.standard
//      defaults.set("A76519BAF9F38878D901EC3E17AD1329E95C5A8928B9FDD3630C50AC100DBC0E", forKey: "deviceToken")
    
    }

    @IBAction func languageChange(_ sender: Any) {
    
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "English", style: .default) { action -> Void in
            
            UserDefaults.standard.set(ENGLISH_LANGUAGE, forKey: "currentLanguage")
            self.customerBtn.setTitle(self.languageChangeString(a_str: "Customer"), for: UIControlState.normal)
            self.spBtn.setTitle(self.languageChangeString(a_str: "Service Provider"), for: UIControlState.normal)
            self.changeLanguage.setTitle(self.languageChangeString(a_str: "Change Language"), for: UIControlState.normal)
  
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "عربى", style: .default) { action -> Void in
           
            UserDefaults.standard.set(ARABIC_LANGUAGE, forKey: "currentLanguage")
           
            self.customerBtn.setTitle(self.languageChangeString(a_str: "Customer"), for: UIControlState.normal)
            self.spBtn.setTitle(self.languageChangeString(a_str: "Service Provider"), for: UIControlState.normal)
            self.changeLanguage.setTitle(self.languageChangeString(a_str: "Change Language"), for: UIControlState.normal)
            
        }
        
          let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)
        
        // present an actionSheet...
        present(actionSheetController, animated: true, completion: nil)
    
    }
    func animationsEffect ()
    {
        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.customerBtn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                         self.spBtn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.logoImg.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.5) {
                            self.customerBtn.transform = CGAffineTransform.identity
                             self.spBtn.transform = CGAffineTransform.identity
                            self.logoImg.transform = CGAffineTransform.identity
                        }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
          //UserDefaults.standard.set("Remember", forKey: "REM")
        
        if UserDefaults.standard.object(forKey: "roleStr") != nil && UserDefaults.standard.object(forKey: "REM") != nil
        {
        
            let role = UserDefaults.standard.object(forKey: "roleStr") as! String
        
            
            if role == "user"
            {
                DispatchQueue.main.async {
                    
                    self.appde.loginUser = "customer"
                    self.appde.scrapshop = role
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
                    self .present(tab, animated: false, completion: nil)
                    
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.appde.loginUser = "serviceProvider"
                    
                    if role == "individual"
                    {
                        self.appde.serviceStr = "indiv"
                         self.appde.scrapshop = role
                        
                        if UserDefaults.standard.object(forKey: "towtype") != nil && UserDefaults.standard.object(forKey: "onsiteTowType") != nil
                        {

                          self.appde.onsiteStr = UserDefaults.standard.object(forKey: "towtype") as! String
                            self.appde.towStr = UserDefaults.standard.object(forKey: "onsiteTowType") as! String
                        }
                        
                    }else if role == "autoshop"
                    {
                        self.appde.serviceStr = "repair"
                         self.appde.scrapshop = role
                        
                    }else if role == "scrapshop"
                    {
                        self.appde.serviceStr = "scrap"
                         self.appde.scrapshop = role
                    }
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                    self .present(service, animated: false, completion: nil)
                    
                }
            }
        }else
        {
           animationsEffect ()
        }
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func customerBtnClicked(_ sender: Any) {
        
             appde.loginUser = "customer"
        
        UserDefaults.standard.set("user", forKey: "uType")
        
           if Reachability.isConnectedToNetwork() {
           
            CustomerAddsService ()
            
           }else {
            
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
            
        }
        
        
        
    }
    @IBAction func serviceBtnClicked(_ sender: Any) {
        
        appde.loginUser = "serviceProvider"
        
        if Reachability.isConnectedToNetwork() {
        
        serviceProviderAddsService()
        
        }else {
            
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
            
        }
       
    }
    
    func CustomerAddsService ()
    {
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        //http://voliveafrica.com/carfix/services/customer_adds?API-KEY=98745612
        let vehicles = "\(Base_Url)splash_adds?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "type" : 1 , "lang" : language ?? ""]
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                     DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.customerDataArr1 = responseData["adds"] as? [[String:AnyObject]]
                    
                    let pathStr = responseData["base_path"] as? String!
                    
                    print("customer data is\(self.customerDataArr1!)")
                    
                    for eachitem in self.customerDataArr1! {
                        
                        let addImg = eachitem["image"] as? String!
                        
                        let imageString = pathStr!! + addImg!!
                        
                        let nameAds = eachitem["heading_en"] as? String!
                        let textAds = eachitem["text_en"] as? String!
                        
                        self.imagesArray1.append(imageString)
                        print(self.imagesArray1)
                        
                        self.headersArray1.append(nameAds!!)
                        self.textArray1.append(textAds!!)
                        
                    }
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let instruct = storyBoard.instantiateViewController(withIdentifier: "InstructionController") as? InstructionController
                    
                    instruct?.testStr = "customer"
                    instruct?.customerDataArr = self.customerDataArr1
                    instruct?.imagesArray = self.imagesArray1
                    instruct?.headersArray = self.headersArray1
                    instruct?.textArray = self.textArray1
                    self.navigationController?.pushViewController(instruct!, animated: true)
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                    
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    
    func serviceProviderAddsService ()
    {
        //http://voliveafrica.com/carfix/services/customer_adds?API-KEY=98745612
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let vehicles = "\(Base_Url)splash_adds?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "type" : 2 , "lang" : language ?? ""]
        Services.sharedInstance.loader(view: self.view)
        print(vehicles)
         print(parameters)
       
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                     DispatchQueue.main.async {
                     Services.sharedInstance.dissMissLoader()
                    self.customerDataArr1 = responseData["adds"] as? [[String:AnyObject]]
                    
                    let pathStr = responseData["base_path"] as? String!
                    
                    print("customer data is\(self.customerDataArr1!)")
                    
                    for eachitem in self.customerDataArr1! {
                        
                        let addImg = eachitem["image"] as? String!
                        
                        let imageString = pathStr!! + addImg!!
                        
                        let nameAds = eachitem["heading_en"] as? String!
                        
                        let textAds = eachitem["text_en"] as? String!
                        
                        self.imagesArray1.append(imageString)
                        
                        ///print(self.imagesArray1)
                        self.headersArray1.append(nameAds!!)
                        self.textArray1.append(textAds!!)
                        
                    }
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let instruct = storyBoard.instantiateViewController(withIdentifier: "InstructionController") as? InstructionController
                    instruct?.testStr = "serviceProvider"
                    
                    instruct?.customerDataArr = self.customerDataArr1
                    instruct?.imagesArray = self.imagesArray1
                    instruct?.headersArray = self.headersArray1
                    instruct?.textArray = self.textArray1
                    
                    self.navigationController?.pushViewController(instruct!, animated: true)
                    }
                }
                else
                {
                     DispatchQueue.main.async {
                    
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                        
                    }
                }
                
            }
        }
    }
}


//  user , individual , autoshop , scrapshop
