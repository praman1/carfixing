//
//  TowingAlert.swift
//  CarFixing
//
//  Created by Suman Guntuka on 28/04/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class TowingAlert: UIViewController {

    @IBOutlet weak var doyouwantReq: UILabel!
    
    @IBOutlet weak var confirm: UIButton!
    
    @IBOutlet weak var cancel: UIButton!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
                self.view.addGestureRecognizer(tapGesture)
     
        doyouwantReq.text = languageChangeString(a_str: "Do You Want To Send A Request?")
        confirm.setTitle(languageChangeString(a_str: "Confirm"), for: UIControlState.normal)
        cancel.setTitle(languageChangeString(a_str: "Cancel"), for: UIControlState.normal)
        
        // Do any additional setup after loading the view.
    }

    func dismissAction()
        {
            self.dismiss(animated: true, completion: nil)
        }

    @IBAction func confirmBtn(_ sender: Any) {
        
        requestTowingService()
      
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
       
        self.dismiss(animated: true, completion: nil)
    }
    
    func requestTowingService()
    {
        
        // http://voliveafrica.com/carfix/services/book_towing //  user_id,latitude,longitude,address,lang
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        let address = UserDefaults.standard.object(forKey: "address")
        
        
        
        let vehicles = "\(Base_Url)book_towing"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat ?? "" , "longitude" : long ?? "" , "lang" : language ?? "" , "address" : address ?? ""]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    self.dismiss(animated: true, completion: nil)
                    
                     DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                    
                    
                    }
                    
                    //self.serviceProviders = responseData["service_providers"] as? [[String:AnyObject]]
                    
                    
            }
        }
    }
    
    }
  
}
