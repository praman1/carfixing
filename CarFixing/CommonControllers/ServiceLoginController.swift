//
//  ServiceLoginController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/16/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//
import UIKit
class ServiceLoginController: UIViewController {
    
    var appDelegate = AppDelegate()

    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var individual: UIButton!
    @IBOutlet weak var autorepair: UIButton!
    @IBOutlet weak var scrap: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        individual.setTitle(languageChangeString(a_str: "Individual Professional"), for: UIControlState.normal)
        autorepair.setTitle(languageChangeString(a_str: "Auto Repair Shop"), for: UIControlState.normal)
        scrap.setTitle(languageChangeString(a_str: "Scrap Shop"), for: UIControlState.normal)
        

        // Do any additional setup after loading the view.
        
        animationsEffect ()
    }
    
    func animationsEffect ()
    {
        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.individual.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.autorepair.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                         self.scrap.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.logoImg.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.5) {
                            self.individual.transform = CGAffineTransform.identity
                             self.autorepair.transform = CGAffineTransform.identity
                            self.scrap.transform = CGAffineTransform.identity
                            self.logoImg.transform = CGAffineTransform.identity
                        }
        })
  
    }

    @IBAction func individualClicked(_ sender: Any) {
        
        appDelegate.serviceStr = "indiv"
          UserDefaults.standard.set("individual", forKey: "uType")
        
        UserDefaults.standard.set("2", forKey: "ServiceType")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.navigationController?.pushViewController(login!, animated: true)
    }
    
    @IBAction func repairClicked(_ sender: Any) {
        
        appDelegate.serviceStr = "repair"
        
        UserDefaults.standard.set("autoshop", forKey: "uType")
        UserDefaults.standard.set("3", forKey: "ServiceType")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.navigationController?.pushViewController(login!, animated: true)
    }
    
    @IBAction func scrapClickd(_ sender: Any) {
        
        appDelegate.serviceStr = "scrap"
        
        UserDefaults.standard.set("scrapshop", forKey: "uType")
        UserDefaults.standard.set("4", forKey: "ServiceType")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.navigationController?.pushViewController(login!, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
