//
//  TowingList.swift
//  CarFixing
//
//  Created by Suman Guntuka on 04/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class TowingList: UIViewController {

    @IBOutlet weak var towingTable: UITableView!
    
    var towingImgArr = [String]()
    var towingStatusArr = [String]()
    var towingAddressArr = [String]()
    var towingTimeArr = [String]()
    var towingData : [[String : AnyObject]]!
    var appDelegate : AppDelegate!
    var userIdArr = [String]()
    var towingIdArr = [String]()
    var messageArr = [String]()
    var latArr = [String]()
    var longArr = [String]()
    var nameArr = [String]()
    var messageStr : String!
    var nameStr : String!
    var vendorIdArr = [String]()
    
    var  userDetails : String!
    var parameters : Dictionary<String, Any>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if Reachability.isConnectedToNetwork() {
            
           towingRequestsCall ()
            
        }else
        {
            
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
      
        NotificationCenter.default.addObserver(self, selector: #selector(towServiceCalling), name: NSNotification.Name(rawValue:"recallTow"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(ListUpdate), name: NSNotification.Name(rawValue:"ReloadList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListUpdate1), name: NSNotification.Name(rawValue:"ReloadList1"), object: nil)
       
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//
//
//    }
    func ListUpdate(){
        
        towingRequestsCall ()
    }
    func ListUpdate1(){
        
        towingRequestsCall ()
    }
   
    func towServiceCalling()
    {
        showToastForAlert (message: languageChangeString(a_str: "Request is Accepted")!)
        self.towingRequestsCall ()
        //self.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func statusBtn(_ sender: Any) {
      
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.towingTable)
        let indexPath1: IndexPath? = self.towingTable.indexPathForRow(at: btnPosition)
        
        UserDefaults.standard.set("towList", forKey: "towingSep")
        UserDefaults.standard.set(userIdArr[(indexPath1?.row)!], forKey: "user_idStr")
        UserDefaults.standard.set(towingIdArr[(indexPath1?.row)!], forKey: "towing_idStr")
        
        UserDefaults.standard.set(messageArr[(indexPath1?.row)!], forKey: "messageStr")
        UserDefaults.standard.set(nameArr[(indexPath1?.row)!], forKey: "namStr")
        UserDefaults.standard.set(latArr[(indexPath1?.row)!], forKey: "towlat")
        UserDefaults.standard.set(longArr[(indexPath1?.row)!], forKey: "towlong")
        UserDefaults.standard.set(towingImgArr[(indexPath1?.row)!], forKey: "towImg")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "TowingNotificationAlert")
//        payVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
//        [payVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        self .present(service, animated: true, completion: nil)
        
    }
    
    @IBAction func chatBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.towingTable)
        let indexPath1: IndexPath? = self.towingTable.indexPathForRow(at: btnPosition)
        
        //appDelegate.chatId = "towChat"
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let chat = storyboard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
        chat?.vendorIdS = self.vendorIdArr[(indexPath1?.row)!]
        //chat?.reciveTowID = self.userIdArr[(indexPath1?.row)!]
        chat?.profileImg = towingImgArr[(indexPath1?.row)!]
        
        appDelegate.chatTypeStr = "msg"
        
        self.navigationController?.pushViewController(chat!, animated: true)
        
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    
    }
    
    
    
    func towingRequestsCall ()
    {
        //http://voliveafrica.com/carfix/services/usertowings
        
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        //appde.loginUser = "customer"
        
        if appDelegate.loginUser == "customer" {
            
            userDetails = "\(Base_Url)usertowings"
            parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "lang" : language ?? "" ]
        
        }
        else
        {
            userDetails = "\(Base_Url)vendortowings"
            parameters = ["API-KEY": APIKEY , "vendor_id" : userid ?? "" , "lang" : language ?? "" ]
        }
        
     
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(userDetails, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    // DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                    self.towingTimeArr = [String]()
                     self.towingAddressArr = [String]()
                    self.towingStatusArr = [String]()
                    self.towingImgArr = [String]()
                    self.towingIdArr = [String]()
                    self.userIdArr = [String]()
                    self.messageArr = [String]()
                    self.latArr = [String]()
                   self.longArr = [String]()
                    self.nameArr = [String]()
                    self.vendorIdArr = [String]()
                    
                    self.towingData = responseData["Towings"] as? [[String:AnyObject]]
                   
                    print(self.towingData)
                    
                     for i in self.towingData! {
                        
                        let imgStr = i["image"] as? String
                        let status = i["status"] as? String
                        let time = i["booked_date"] as? String
                        let address = i["address"] as? String
                        let userIdStr = i["user_id"] as? String
                        let towingIdStr = i["id"] as? String
                        
                        let latStr = i["latitude"] as? String
                        let longStr = i["longitude"] as? String
                        let vendorid = i["vendor_id"] as? String
                        
                        if self.appDelegate.loginUser == "customer" {
                            
                            self.messageStr = "some" as String
                             self.nameStr = i["venodr_name"] as? String
                        }
                        else
                          {
                             self.messageStr = i["message"] as? String
                            self.nameStr = i["user_name"] as? String
                           }
                     
                        let img = base_path + imgStr!
                        
                        self.vendorIdArr.append(vendorid!)
                        self.towingTimeArr.append(time!)
                        self.towingAddressArr.append(address!)
                        self.towingStatusArr.append(status!)
                        self.towingImgArr.append(img)
                        self.towingIdArr.append(towingIdStr!)
                        self.userIdArr.append(userIdStr!)
                        self.messageArr.append(self.messageStr!)
                        self.latArr.append(latStr!)
                        self.longArr.append(longStr!)
                        self.nameArr.append(self.nameStr!)
                    }
                    
                    if self.towingTimeArr.count == 0
                    {
                     //   self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.towingTable.reloadData()
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    }
                }
            }
        }
    }
    
}
extension TowingList: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return towingImgArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = towingTable.dequeueReusableCell(withIdentifier: "TowingCell", for: indexPath) as! TowingCell
        
        let  strS = towingTimeArr[indexPath.row]
        
        cell.timeLbl.text = towingTimeArr[indexPath.row]
        cell.statusLbl.text = towingStatusArr[indexPath.row]
        cell.addressLbl.text = towingAddressArr[indexPath.row]
        if towingStatusArr[indexPath.row] == "accepted" {
            cell.chatImg.isHidden = false
        }else
        {
            cell.chatImg.isHidden = true
        }
        
        if strS == "accepted"
        {
            cell.statusLbl.textColor = #colorLiteral(red: 0.06666666667, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            
        }else
        {
            cell.statusLbl.textColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
            
        }
        
         if appDelegate.loginUser == "customer" {
            
            cell.statusBtn.isEnabled = false
            
            if self.towingStatusArr[indexPath.row] == "accepted"
            {
               cell.viewDetailsBtn.isHidden = false
            }else
            {
                cell.viewDetailsBtn.isHidden = true
            }
            
        }else
         {
            cell.statusBtn.isEnabled = true
             cell.viewDetailsBtn.isHidden = false
         }
        
        cell.towingImg.sd_setImage(with: URL(string: self.towingImgArr[indexPath.row]), placeholderImage: UIImage(named:"profile pic"))
        cell.towingImg.layer.cornerRadius = cell.towingImg.frame.size.height/2
        cell.viewDetailsBtn.setTitle(languageChangeString(a_str: "View Details"), for: UIControlState.normal)
     
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 125
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if towingStatusArr[indexPath.row] == "accepted" {
         
            UserDefaults.standard.set("towSet", forKey: "towingSet")
            
            UserDefaults.standard.set(latArr[indexPath.row], forKey: "towlatitude")
            UserDefaults.standard.set(longArr[indexPath.row], forKey: "towlongtude")
            UserDefaults.standard.set(vendorIdArr[indexPath.row], forKey: "vendor_idStr")
            UserDefaults.standard.set(towingImgArr[indexPath.row], forKey: "towimage")
            UserDefaults.standard.set(nameArr[indexPath.row], forKey: "nameVendor")
             UserDefaults.standard.set(towingIdArr[indexPath.row], forKey: "towingID")
            
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let detail = storyBoard.instantiateViewController(withIdentifier: "TowingMap1") as! TowingMap
            
            let detail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TowingMap1")
            self.present(detail, animated: true, completion: nil)
           // self.navigationController?.pushViewController(detail, animated: true)
            
        }else if towingStatusArr[indexPath.row] == "pending" {
            
            if appDelegate.loginUser == "customer"
            {
                
            }else
            {
                
                UserDefaults.standard.set("towList", forKey: "towingSep")
                UserDefaults.standard.set(userIdArr[indexPath.row], forKey: "user_idStr")
                UserDefaults.standard.set(towingIdArr[indexPath.row], forKey: "towing_idStr")
                
                UserDefaults.standard.set(messageArr[indexPath.row], forKey: "messageStr")
                UserDefaults.standard.set(nameArr[indexPath.row], forKey: "namStr")
                UserDefaults.standard.set(latArr[indexPath.row], forKey: "towlat")
                UserDefaults.standard.set(longArr[indexPath.row], forKey: "towlong")
                UserDefaults.standard.set(towingImgArr[indexPath.row], forKey: "towImg")
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let service = storyBoard.instantiateViewController(withIdentifier: "TowingNotificationAlert")
                //        payVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
                //        [payVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                
                self .present(service, animated: true, completion: nil)
            }
        }else
        {
            
        }
        
        
        
    }
}
