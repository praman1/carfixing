//
//  ContactUsController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 25/04/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class ContactUsController: UIViewController {
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailId: UILabel!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    var contactusDetails : [[String : AnyObject]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactUsService ()
        
        address.text = languageChangeString(a_str: "ADDRESS")
        mobileNo.text = languageChangeString(a_str: "MOBILE NUMBER")
        emailId.text = languageChangeString(a_str: "EMAIL ID")
        self.title = languageChangeString(a_str: "Contact Us")

        // Do any additional setup after loading the view.
    }

  
    @IBAction func sideMenuBtn(_ sender: Any) {
        
    present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    
    func contactUsService ()
    {
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        //http://voliveafrica.com/carfix/services/contact?API-KEY=98745612
        let vehicles = "\(Base_Url)contact?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY,  "lang" : language ?? ""]
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        
                        if let responseData1 = responseData["contact"] as? Dictionary<String, AnyObject> {
                            
                            let address : String = (responseData1["address"] as? String)!
                            let email : String = (responseData1["email"] as? String)!
                            let mobile : String = (responseData1["mobile"] as? String)!
                            
                             DispatchQueue.main.async {
                                self.emailLbl.text = email
                                self.mobileNoLbl.text = mobile
                                self.addressLbl.text = address
                            }
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    
}
