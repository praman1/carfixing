
//
//  ResetController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/6/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ResetController: UIViewController {

    @IBOutlet weak var newPasswrdText: UITextField!
    @IBOutlet weak var confPasswordText: UITextField!
    @IBOutlet weak var scroll: UIScrollView!
    var mobileStr : String!
    
    
    @IBOutlet weak var resetPW: UILabel!
    
    @IBOutlet weak var detail: UILabel!
    
    @IBOutlet weak var newPW: UILabel!
    @IBOutlet weak var confirmPW: UILabel!
    
    @IBOutlet weak var done: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: newPasswrdText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: confPasswordText, lineColor: myColor, myView: self.view)
        // Do any additional setup after loading the view.
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        
        
        if Reachability.isConnectedToNetwork()
        {
            resetPasswordService ()
        }else{
            
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
//                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                let reset = storyBoard.instantiateViewController(withIdentifier: "ResetController") as? ResetController
//                self.navigationController?.pushViewController(reset!, animated: true)
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let verify = storyBoard.instantiateViewController(withIdentifier: "VerifyController") as? VerifyController
//        self.navigationController?.pushViewController(verify!, animated: true)
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func resetPasswordService ()
    {
        
        //http://voliveafrica.com/carfix/services/state_cities?API-KEY=98745612
        
        let statesCities = "\(Base_Url)update_password"
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "mobile" : mobileStr! , "password" : newPasswrdText.text! , "lang" : language ?? ""]
        print(parameters)
          Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(statesCities, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
                    self.navigationController?.pushViewController(login!, animated: true)
                    
//                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let home = storyBoard.instantiateViewController(withIdentifier: "LoginController")
//                        self .present(home, animated: true, completion: nil)
                    
                }
                else
                {
                    self.showToast(message: message!!)
                }
                
            }
        }
    }
    
}
extension ResetController : UITextFieldDelegate{
   
    public func textFieldDidBeginEditing (_ textField : UITextField) {
        
        if textField == newPasswrdText {
            scroll.contentOffset.y = +20
            
        }
        else if textField == confPasswordText{
            scroll.contentOffset.y = +80
        }
                
    }
    
    public func textFieldDidEndEditing (_ textField : UITextField){
        newPasswrdText.resignFirstResponder()
        confPasswordText.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue : Bool = false
        if textField == newPasswrdText {
            
            confPasswordText.becomeFirstResponder()
            
            returnValue = true
        }
        else if textField == confPasswordText{
            
            confPasswordText.resignFirstResponder()
            
            returnValue = true
        }
        
        return returnValue
    }
}
