//
//  TowingNotificationAlert.swift
//  CarFixing
//
//  Created by Suman Guntuka on 02/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class TowingNotificationAlert: UIViewController {
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var towingImg: UIImageView!
    
    @IBOutlet weak var reject: UIButton!
    @IBOutlet weak var accept: UIButton!
    var userNameStr : String!
    var userImgStr : String!
    var messageStr : String!
    var userId : String!
    var towingId : String!
    var lat : String!
    var long : String!
    var towStr : String!
    var image:String!
    
    var pushDictionary:[String:Any] = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(pushDictionary)
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
        self.view.addGestureRecognizer(tapGesture)
        
        towStr = UserDefaults.standard.object(forKey: "towingSep") as! String
        
        accept.setTitle(languageChangeString(a_str: "Accept"), for: UIControlState.normal)
        
        if towStr == "towingSep"
        {
            let message = pushDictionary["message"] as! String
            let name = pushDictionary["user_name"] as! String
            lat = pushDictionary["latitude"] as! String
            long = pushDictionary["longitude"] as! String
            let image1 = pushDictionary["image"] as! String
            userId = pushDictionary["user_id"] as! String
            //            let towingId = infoDict!["towing_id"] as! String
            let towing = pushDictionary ["towing_id"] as! Int
            towingId = String(towing)
            
            userImgStr = base_path + image1
            
            UserDefaults.standard.set(lat, forKey: "lat")
            UserDefaults.standard.set(long, forKey: "long")
            UserDefaults.standard.set(towingId, forKey: "towingId")
             DispatchQueue.main.async {
                self.towingImg.sd_setImage(with: URL(string: self.userImgStr  ), placeholderImage: UIImage(named:"as img4"))
                self.towingImg.layer.cornerRadius = self.towingImg.frame.size.height/2
            self.nameLbl.text = name
            self.messageLbl.text = message
            }
        }else
        {
            DispatchQueue.main.async {
            self.lat = UserDefaults.standard.object(forKey: "towlat") as! String
            self.long = UserDefaults.standard.object(forKey: "towlong") as! String
                self.nameLbl.text = UserDefaults.standard.object(forKey: "namStr") as? String
                self.messageLbl.text = (UserDefaults.standard.object(forKey: "messageStr") as! String)
            self.userImgStr = UserDefaults.standard.object(forKey: "towImg") as! String
            self.towingImg.sd_setImage(with: URL(string: self.self.userImgStr  ), placeholderImage: UIImage(named:"as img4"))
            self.towingImg.layer.cornerRadius = self.towingImg.frame.size.height/2
                self.userId =  UserDefaults.standard.object(forKey: "user_idStr") as! String
                self.towingId =  UserDefaults.standard.object(forKey: "towing_idStr") as! String
                 UserDefaults.standard.set( self.towingId, forKey: "towingId")
            }
        }
    }
    func dismissAction()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func rejectBtn(_ sender: Any) {
       
       rejectTowingService()
        
    }
    
    @IBAction func accptBtn(_ sender: Any) {
        
        //self.dismiss(animated: true, completion: nil)
      
         confirmTowingService()
     
    }
   
    func confirmTowingService()
    {

        //http://voliveafrica.com/carfix/services/confirm_towing
        //Parameters: user_id, towing_id,vendor_id

        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        let userid = UserDefaults.standard.object(forKey: "user_id")
        UserDefaults.standard.set(userId, forKey: "userId")

        let vehicles = "\(Base_Url)confirm_towing"

        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userId! ,"towing_id" : towingId! , "vendor_id" : userid ?? "" ,"lang" : language ?? "", "status" : "1"]

        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)

        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)

                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                     // self.dismiss(animated: true, completion: nil)
                    UserDefaults.standard.set("noti", forKey: "towingSep")
                    
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadList1"), object: nil)
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let filter = storyBoard.instantiateViewController(withIdentifier: "VendorTowingMap1")
                    
                    UserDefaults.standard.set(self.lat, forKey: "towlat")
                    UserDefaults.standard.set(self.long, forKey: "towlong")
                    UserDefaults.standard.set(self.userId, forKey: "userId")
                    UserDefaults.standard.set(self.towingId, forKey: "towingId")
                    UserDefaults.standard.set(self.userImgStr, forKey: "image")
                    
                    self .present(filter, animated: true, completion: nil)

                }else
                {
                     Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    func rejectTowingService()
    {
        
        //http://voliveafrica.com/carfix/services/confirm_towing
        //Parameters: user_id, towing_id,vendor_id
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        let userid = UserDefaults.standard.object(forKey: "user_id")
        UserDefaults.standard.set(userId, forKey: "userId")
        
        let vehicles = "\(Base_Url)confirm_towing"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userId! ,"towing_id" : towingId! , "vendor_id" : userid ?? "" ,"lang" : language ?? "", "status" : "2"]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    Services.sharedInstance.dissMissLoader()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadList"), object: nil)

                    self.dismiss(animated: true, completion: nil)
                    
 
                    
                }else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
        
    }

}
