//
//  TowingConfirm.swift
//  CarFixing
//
//  Created by Suman Guntuka on 31/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class TowingConfirm: UIViewController {

    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var confirm: UIButton!
    @IBOutlet weak var areyousure: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        areyousure.text = languageChangeString(a_str: "Are you sure you want to accept the request?")
        confirm.setTitle(languageChangeString(a_str: "Confirm"), for: UIControlState.normal)
        cancel.setTitle(languageChangeString(a_str: "Cancel"), for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }

    @IBAction func cancelBtn(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
    }
    @IBAction func confirmBtn(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() {
            
              self.requestTowingService()
            
        }else
        {
            
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func requestTowingService()
    {
        // http://voliveafrica.com/carfix/services/confirm_towing_post
        
        let vendorId = UserDefaults.standard.object(forKey: "user_id")
        let towingidStr = UserDefaults.standard.object(forKey: "towing_idStr")
        let userIDStr = UserDefaults.standard.object(forKey: "user_idStr")
        
        let vehicles = "\(Base_Url)confirm_towing"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "vendor_id" : vendorId ?? "" , "towing_id" : towingidStr ?? "" , "user_id" : userIDStr ?? "" ]
       
        print(vehicles)
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "recallTow"), object: nil)
                    
                    let alert = UIAlertController(title: "", message:
                        message! , preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: self.languageChangeString(a_str: "OK"), style: .default, handler: { action in
                        switch action.style{
                        case .default:
                           
                             self.dismiss(animated: true, completion: nil)
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    self.present(alert, animated: true, completion: nil)
                   
                   
                  
                    
                }
            }
        }
        
    }
    
}


