//
//  ProfileController.swift
//  carFixing
//
//  Created by Apple on 12/13/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ProfileController: UIViewController , UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var descStaticHieght: NSLayoutConstraint!
    @IBOutlet var locationStaticHieght: NSLayoutConstraint!
    @IBOutlet var descHieght: NSLayoutConstraint!
    @IBOutlet var locationHieght: NSLayoutConstraint!
    @IBOutlet var desc: UILabel!
    @IBOutlet var descTV: UITextView!
    @IBOutlet var locationTF: UITextField!
    @IBOutlet var location: UILabel!
    @IBOutlet var flagImg: UIImageView!
    
    @IBOutlet weak var fullnameTF: UITextField!
    
    @IBOutlet weak var sidemenuImg: UIImageView!
   
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var genderTF: UITextField!
    
    @IBOutlet weak var DOB_TF: UITextField!
    
    @IBOutlet weak var dayTf: UITextField!
    
    @IBOutlet weak var yearTF: UITextField!
  
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var mobileCodeTF: UITextField!
    
    @IBOutlet weak var mobileNoTf: UITextField!
    
    @IBOutlet weak var availableTF: UITextField!
    
    @IBOutlet weak var textFiledsView: UIView!
    
    @IBOutlet weak var migrate: UIButton!
    
    @IBOutlet weak var menuBtn: UIButton!
    
    var profileData: [[String:AnyObject]]!
    
    @IBOutlet weak var profileImg: UIImageView!
    var pickerImage  = UIImage()
    var pickerImage1 = UIImage()
    var imagePicker = UIImagePickerController()
   
    var datepicker : UIDatePicker?
    var toolbar: UIToolbar?
    var appDelegate = AppDelegate()
    var genderPicker: UIPickerView!
    var genderArr = ["Male", "Female"]
    var stateStr : String!
    var cityStr : String!
    var userIdStr : String!
    var imageStr : String!
    var profileBool : Bool!
    var checkImgStr : String!
    var lat : String!
    var long : String!
    var currencyStr : String!
    
    
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var dateofbirth: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var fullname: UILabel!
    @IBOutlet weak var profile: UILabel!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var changeProfile: UIButton!
    
    @IBOutlet var updateBrands: UIButton!
    @IBOutlet var updateService: UIButton!
    
    @IBOutlet var brandsHieghts: NSLayoutConstraint!
    @IBOutlet var serviceHieght: NSLayoutConstraint!
    
    @IBOutlet var locationBtn: UIButton!
    
    var countyPicker: UIPickerView?
    var toolbar1: UIToolbar?
    var toolbar2: UIToolbar?
    var selectedTextField: UITextField?
    var countryPickerData = [String]()
    var selectedStr : String!
    var countryFlogArr = [String]()
    var countryCodeId = [String]()
    var countryIDStr : String!
    var countryIdStr : String!
    var countryIdArr = [String]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
         profileBool = false
         countryCodeService ()
        
//        tabBarController?.tabBar.items?[0].title = languageChangeString(a_str: "Home")
//        tabBarController?.tabBar.items?[1].title = languageChangeString(a_str: "Bids")
//        tabBarController?.tabBar.items?[2].title = languageChangeString(a_str: "Jobs")
//        tabBarController?.tabBar.items?[3].title = languageChangeString(a_str: "Chats")
//        tabBarController?.tabBar.items?[4].title = languageChangeString(a_str: "Profile")
        self.descTV.delegate = self
        profile.text = languageChangeString(a_str: "Profile")
        
        save.setTitle(languageChangeString(a_str: "EDIT/SAVE"), for: UIControlState.normal)
        
        changeProfile.setTitle(languageChangeString(a_str: "Change Profile Pic"), for: UIControlState.normal)
        
        username.text =  languageChangeString(a_str: "USER NAME")
        location.text =  languageChangeString(a_str: "LOCATION")
        desc.text =  languageChangeString(a_str: "DESCRIPTION")
        gender.text =  languageChangeString(a_str: "GENDER")
        dateofbirth.text =  languageChangeString(a_str: "DATE OF BIRTH")
        email.text =  languageChangeString(a_str: "EMAIL ID")
        mobileNo.text =  languageChangeString(a_str: "MOBILE NUMBER")
        detail.text =  languageChangeString(a_str: "Changing mobile number you may loss data Contact us to")
        migrate.setTitle(languageChangeString(a_str: "migrate number"), for: UIControlState.normal)
        updateBrands.setTitle(languageChangeString(a_str: "Update Brands"), for: UIControlState.normal)
        updateService.setTitle(languageChangeString(a_str: "Update Services"), for: UIControlState.normal)

        
        
        imagePicker.delegate = self
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if appDelegate.profileStr == "menu" {
            
            sidemenuImg.isHidden = false
            menuBtn.isHidden = false
            self.navigationController?.isNavigationBarHidden = true
           
        }else if appDelegate.profileStr == "menu4"{
           
            sidemenuImg.isHidden = false
            menuBtn.isHidden = false
            self.navigationController?.isNavigationBarHidden = true
            
        }else{
            
            sidemenuImg.isHidden = true
            menuBtn.isHidden = true
            self.navigationController?.isNavigationBarHidden = true
            
        }
        
        if self.appDelegate.scrapshop == "user"
        {
            updateBrands.isHidden = true
            updateService.isHidden = true
            serviceHieght.constant = 0
            brandsHieghts.constant = 0
            location.isHidden = true
            locationTF.isHidden = true
            locationBtn.isHidden = true
            locationBtn.isEnabled = false
            desc.isHidden = true
            descTV.isHidden = true
            locationHieght.constant = 0
            descHieght.constant = 0
            locationStaticHieght.constant = 0
            descStaticHieght.constant = 0
            scroll.isScrollEnabled = false
          
            fullname.text =  languageChangeString(a_str: "FULL NAME")
        }
        else if self.appDelegate.scrapshop == "individual" && self.appDelegate.towStr == "tow"
        {
            updateBrands.isHidden = true
            updateService.isHidden = true
            serviceHieght.constant = 0
            brandsHieghts.constant = 0
            fullname.text =  languageChangeString(a_str: "SHOP NAME")
            scroll.isScrollEnabled = true
            
        }else if self.appDelegate.scrapshop == "scrapshop"
        {
            updateService.isHidden = true
            serviceHieght.constant = 0
            fullname.text =  languageChangeString(a_str: "SHOP NAME")
            scroll.isScrollEnabled = true
        }
        else{
            
            updateBrands.isHidden = false
            updateService.isHidden = false
            serviceHieght.constant = 40
            brandsHieghts.constant = 40
            fullname.text =  languageChangeString(a_str: "SHOP NAME")
            scroll.isScrollEnabled = true
        }
       
        textFiledsView.layer.cornerRadius = 5.0
        self.navigationController?.isNavigationBarHidden = true
        navigationItem.hidesBackButton = true

        let tf7 = LineTextField()
        tf7 .textfieldAsLine(myTextfield: emailTF, lineColor: UIColor.lightGray, myView: self.view)
        
        let tf8 = LineTextField()
        tf8 .textfieldAsLine(myTextfield: mobileCodeTF, lineColor: UIColor.lightGray, myView: self.view)
        
        let tf9 = LineTextField()
        tf9 .textfieldAsLine(myTextfield: mobileNoTf, lineColor: UIColor.lightGray, myView: self.view)
        let tf10 = LineTextField()
        tf10 .textfieldAsLine(myTextfield: locationTF, lineColor: UIColor.lightGray, myView: self.view)
        
        mobileCodeTF.rightViewMode = UITextFieldViewMode.always
        mobileCodeTF.rightView?.frame = CGRect (x: 0, y: 0, width: 20, height: 15)
        let leftView4 = UIView()
        leftView4.frame = CGRect (x: 0, y: 0, width: 20, height: 15)
        let imageView4 = UIImageView()
        imageView4.frame = CGRect (x: 0, y: 0, width: 15, height: 10)
        imageView4.image = UIImage.init(named: "dropdown")
        leftView4.addSubview(imageView4)
        mobileCodeTF.rightView = leftView4
        
        genderPicker = UIPickerView()
        genderPicker.dataSource = self
        genderPicker.delegate = self
        self.genderTF.inputView = genderPicker
        self.genderTF.text = self.genderArr[0]
        
        self.profileImg.layer.borderWidth=1.0
        self.profileImg.layer.masksToBounds = false
        self.profileImg.layer.borderColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.height/2
        self.profileImg.clipsToBounds = true
        
        countyPicker = UIPickerView()
        countyPicker?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (countyPicker?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        countyPicker?.delegate = self as UIPickerViewDelegate
        countyPicker?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ProfileController.donePicker1))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ProfileController.canclePicker1))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
  
        self.showDatePicker()
        self.addDoneButtonOnKeyboard()
   
    }
func donePicker1 ()
{
    self.mobileCodeTF.text = selectedStr
    self.mobileCodeTF.resignFirstResponder()
    self.genderTF.resignFirstResponder()
    
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMMM yyyy"
    DOB_TF.text! = formatter.string(from: (datepicker?.date)!)
    self.view.endEditing(true)
    DOB_TF.resignFirstResponder()
    
}
func canclePicker1 ()
{
    self.mobileCodeTF.resignFirstResponder()
    self.DOB_TF.resignFirstResponder()
    self.genderTF.resignFirstResponder()
}

    func showDatePicker(){
       
        //Formate Date
        datepicker = UIDatePicker()
        datepicker?.datePickerMode = .date
        datepicker?.backgroundColor = UIColor.white
        toolbar1 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar1?.barStyle = .blackOpaque
        toolbar1?.autoresizingMask = .flexibleWidth
        toolbar1?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar1?.frame = CGRect(x: 0,y: (datepicker?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar1?.barStyle = UIBarStyle.default
        toolbar1?.isTranslucent = true
        toolbar1?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar1?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar1?.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ProfileController.donePickerDate))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ProfileController.canclePickerDate))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar1?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar1?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(descHidden))
        self.view.addGestureRecognizer(tapGesture1)
        
    }
    func descHidden()
    {
        self.descTV.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let address = UserDefaults.standard.object(forKey: "address") as? String
        
        self.locationTF.text = address ?? ""
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func donePickerDate ()
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        DOB_TF.text! = formatter.string(from: (datepicker?.date)!)
        self.view.endEditing(true)
         DOB_TF.resignFirstResponder()
    }
    
    @objc func canclePickerDate ()
    {
        self.view.endEditing(true)
         DOB_TF.resignFirstResponder()
    }
    func addDoneButtonOnKeyboard()
    {
        toolbar2 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar2?.barStyle = .blackOpaque
        toolbar2?.autoresizingMask = .flexibleWidth
        toolbar2?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancleBtn))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar2?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar2?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
       fullnameTF.inputAccessoryView = toolbar2
        locationTF.inputAccessoryView = toolbar2
        usernameTF.inputAccessoryView = toolbar2
        emailTF.inputAccessoryView = toolbar2
        descTV.inputAccessoryView = toolbar2
        mobileNoTf.inputAccessoryView = toolbar2
        
    }
    
    func doneButtonAction()
    {
        fullnameTF.resignFirstResponder()
        locationTF.resignFirstResponder()
        usernameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        descTV.resignFirstResponder()
        mobileNoTf.resignFirstResponder()
        
    }
    func cancleBtn()
    {
        fullnameTF.resignFirstResponder()
        locationTF.resignFirstResponder()
        usernameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        descTV.resignFirstResponder()
        mobileNoTf.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        DOB_TF.inputView = datepicker
        DOB_TF.inputAccessoryView = toolbar
        
        genderTF.inputView = genderPicker
        genderTF.inputAccessoryView = toolbar
        
        mobileCodeTF.inputView = countyPicker
        mobileCodeTF.inputAccessoryView = toolbar
        
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        fullnameTF.resignFirstResponder()
        locationTF.resignFirstResponder()
        usernameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        descTV.resignFirstResponder()
        mobileNoTf.resignFirstResponder()
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scroll.isScrollEnabled = true
        if textField == emailTF{
            
            scroll.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y - 80), animated: true)
            
        } else if textField == mobileNoTf{
            
            scroll.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y - 40), animated: true)
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descTV{
            
            scroll.setContentOffset(CGPoint(x: 0, y: textView.frame.origin.y - 40), animated: true)
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
         descTV.resignFirstResponder()
         scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }

   
    public func textFieldDidEndEditing(_ textField: UITextField){
        
        let genderStr = genderTF.text
       
        if genderStr == ""
        {
            genderTF.text = "Male"
        }else{
        }
        
        fullnameTF.resignFirstResponder()
        usernameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        mobileNoTf.resignFirstResponder()
        DOB_TF.resignFirstResponder()
        
          scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        if self.appDelegate.scrapshop == "user"
        {
            scroll.isScrollEnabled = false
        }
        
    }
    
   
    @IBAction func locationBtnAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "LocationController")
        self.present(service, animated: true, completion: nil)
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func updateServiceBtn(_ sender: Any) {
       
        let brand = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VendorServices")
        self.present(brand, animated: true, completion: nil)
    }
   
    @IBAction func updateBrandsBtn(_ sender: Any) {
        let brand = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VendorBrands")
        self.present(brand, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func saveClicked(_ sender: Any) {
 
            if Reachability.isConnectedToNetwork()
            {
                 saveProfile()
            }else
            {
            
        showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
    }
  
    func getProfile()
    {
       // http://voliveafrica.com/carfix/services/user_profile?API-KEY=98745612&user_id=2
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let vehicles = "\(Base_Url)user_profile?"
        Services.sharedInstance.loader(view: self.view)
        
        let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "user_id": userid ?? "" ,"lang" : language ?? ""]
        
        print(vehicles)
        print(parameters)
        
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                    //self.profileData = responseData["user_data"] as? [[String:AnyObject]]
                    print(self.profileData)
                    
                    if let profileData = responseData["user_data"] as? Dictionary<String, AnyObject> {
                        
                         let basePath = responseData["base_path"] as? String!
                       let role: String = (profileData["role"] as? String)!
                        print("my role is \(role)")
                       
                    //let userId: String = (profileData["user_id"] as? String)!
                       //let city = profileData["city"] as? String!
                        
                        let fullName  = (profileData["full_name"] as? String!)
                        let userName  = (profileData["user_name"] as? String!)
                        let gender  = (profileData["gender"] as? String!)
                        let DOB  = (profileData["dob"] as? String!)
                        let email  = (profileData["email"] as? String!)
                        let mobileNo  = (profileData["mobile"] as? String!)
                        self.stateStr = (profileData["city"] as? String!)
                        self.cityStr = (profileData["state"] as? String!)
                        self.userIdStr = (profileData["user_id"] as? String!)
                        print(self.userIdStr!)
                        self.imageStr = (profileData["image"] as? String!)
                        self.countryIDStr = (profileData["countrycode"] as? String!)
                        self.currencyStr = (profileData["currency"] as? String!)
                         UserDefaults.standard.set(self.currencyStr, forKey: "currencyVal")
                        
                        self.mobileCodeTF.text = self.countryPickerData[self.countryCodeId .index(of: self.countryIDStr)!]
                     
                        self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[self.countryCodeId .index(of: self.countryIDStr)!]), placeholderImage: UIImage(named:"profile pic"))
                        
                        
                        let address  = (profileData["address"] as? String!)
                        let descri  = (profileData["description"] as? String!)
                        
                        print("my data \(self.stateStr , self.cityStr ,self.userIdStr )")
                        
                        let profileImageStr = basePath!! + self.imageStr
                        
                        print(profileImageStr)
                        
                        self.profileImg.sd_setImage(with: URL(string: profileImageStr), placeholderImage: UIImage(named:"profile pic"))
                        
                        self.fullnameTF.text = fullName!
                        self.usernameTF.text = userName!
                        self.genderTF.text = gender!
                        self.DOB_TF.text = DOB!
                        self.emailTF.text = email!
                        self.mobileNoTf.text = mobileNo!
                        self.locationTF.text = address!
                        self.descTV.text = descri!
                        
                        UserDefaults.standard.set(profileImageStr, forKey: "profile")
                        UserDefaults.standard.set(userName, forKey: "name")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(mobileNo, forKey: "mobile")
                        UserDefaults.standard.set(self.userIdStr!, forKey: "user_id")
                       
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "profileData"), object: nil)
                        
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @IBAction func changeProfileImgBtn(_ sender: Any) {
        
        profileBool = true
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    func saveProfile()
    {
        
            //let vehicles = "\(Base_Url)updateprofile"
            Services.sharedInstance.loader(view: self.view)
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        //user_id,full_name,user_name,gender,dob,mobile,profile_image
        
        if self.countryIdStr == nil
        {
            self.countryIdStr = countryCodeId[0]
        }
        
        if UserDefaults.standard.object(forKey: "lat") != nil
        {
            self.lat = UserDefaults.standard.object(forKey: "latitude") as! String
            self.long = UserDefaults.standard.object(forKey: "longitude") as! String
        
        }else
        {
            
        }
  
        let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "user_name": self.usernameTF.text! ,  "email": self.emailTF.text! ,"full_name": self.fullnameTF.text! , "mobile": self.mobileNoTf.text! , "city" : self.cityStr! , "state" : self.stateStr! ,"user_id" : self.userIdStr! , "gender" : self.genderTF.text! , "dob" : self.DOB_TF.text! ,"lang" : language ?? "" ,  "address" : self.locationTF.text! , "description" : self.descTV.text! , "latitude" : self.lat ?? "" , "longitude" : self.long ?? "","countrycode" : self.countryIdStr!]
        
       if pickerImage.size.width != 0
       {
        let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
        
        print(imgData)
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "profile_image",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:"\(Base_Url)updateprofile")
        { (result) in
            
            print(result)
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print(response.result.value ?? "")
                    
                    let responseData = response.result.value as? Dictionary<String, Any>
                    
                    let status = responseData!["status"] as! Int
                    let message = responseData!["message"] as! String
                    
                    if status == 1
                    {
                        
                        self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                      
                        //self.getProfile()
                        self.countryCodeService ()
                        
                    }
                    else
                    {
                        self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
                Services.sharedInstance.dissMissLoader()
            }
        }
        }else
       {
        
        pickerImage1 = profileImg.image!
       
         let imgData1 = UIImageJPEGRepresentation(pickerImage1, 0.2)!
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData1, withName: "profile_image",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:"\(Base_Url)updateprofile")
        { (result) in
            
            print(result)
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    print(response.result.value ?? "")
                    
                    let responseData = response.result.value as? Dictionary<String, Any>
                    
                    let status = responseData!["status"] as! Int
                    let message = responseData!["message"] as! String
                    
                    if status == 1
                    {
                        
                        self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                       // self.getProfile()
                         self.countryCodeService ()
                        
                    }
                    else
                    {
                        self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
                Services.sharedInstance.dissMissLoader()
            }
        }
        }
        }
    
    func countryCodeService ()
    {

        //http://voliveafrica.com/carfix/services/countries?API-KEY=98745612

        let vehicles = "\(Base_Url)countries?"

        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY]
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)

                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!

                if status == 1
                {

                    let servicesCat = responseData["countries"] as? [[String:Any]]
                    print("services categorys are ***** \(servicesCat!)")

                    for servicesCat in servicesCat! {

                        let codeStr = servicesCat["phonecode"]  as! String
                        let flags = servicesCat["country_flag"]  as! String
                        let countryId = servicesCat["id"]  as! String

                        let flogsImg = base_path + flags

                        self.countryPickerData.append(codeStr)
                        self.countryFlogArr.append(flogsImg)
                        self.countryCodeId.append(countryId)

                    }
                    DispatchQueue.main.async {

                        Services.sharedInstance.dissMissLoader()

                        self.countyPicker?.reloadAllComponents()

                    }
                    print(self.countryPickerData)
                    
                    if Reachability.isConnectedToNetwork()
                    {
                        self.getProfile()
                    }else
                    {
                        self.showToastForAlert (message: self.languageChangeString(a_str: "You must Connect Internet!")!)
                    }

                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }

}
extension ProfileController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
       
        let pickedImage2 = info[UIImagePickerControllerOriginalImage]
        print(pickedImage2 ?? "")
        
        checkImgStr = "1"
        profileImg.image = pickedImage2  as? UIImage
        pickerImage = (pickedImage2 as? UIImage)!
        self.navigationController?.isNavigationBarHidden = true
        
        picker.dismiss(animated: true, completion: nil)
        
    }
}
extension ProfileController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if pickerView == genderPicker
        {
            return genderArr.count
        }else
        {
            return countryFlogArr.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

       // return genderArr[row]
        
        if pickerView == genderPicker
        {
            return genderArr[row]
        }else
        {
            return countryFlogArr[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
       
        if pickerView == genderPicker
        {
             genderTF.text = genderArr[row]
        }
        else
        {
           // let temp = UILabel()
            mobileCodeTF.text = self.countryPickerData[row]
            self.countryIdStr = self.countryCodeId[row]
            
            self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
       
        }
       
        self.view.endEditing(true)
   
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        //names
        
        if pickerView == genderPicker
        {
            //return genderArr[row]
            
            let temp = UILabel()
            temp.text = genderArr[row]
            
            temp.adjustsFontSizeToFitWidth = true
            temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
            //codes
            let aView = UIView()
            aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:30)
            aView.insertSubview(temp, at: 1)
            
            return aView
       
        }else
        {
            let temp = UILabel()
            let img = UIImageView()
            img.frame = CGRect(x: self.view.frame.origin.x+100 ,y: 0, width: 30,height: 25)
            
            self.countryIdStr = self.countryCodeId[row]

            temp.text = self.countryPickerData[row]
            self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
            img.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
            
            temp.adjustsFontSizeToFitWidth = true
            temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
            //codes
            let aView = UIView()
            aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:30)
            aView.insertSubview(temp, at: 1)
            aView.insertSubview(img, at: 1)
           
            return aView
        }
    }
}




