//
//  CompltedTowing.swift
//  CarFixing
//
//  Created by Suman Guntuka on 04/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.

import UIKit
import Alamofire

class CompltedTowing: UIViewController {
    
    var towingId : String!
  
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var confirm: UIButton!
    
    @IBOutlet var currencyLbl: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

         confirm.setTitle(languageChangeString(a_str: "Confirm"), for: UIControlState.normal)
         cancel.setTitle(languageChangeString(a_str: "Cancel"), for: UIControlState.normal)
        price.text = languageChangeString(a_str: "Price")
        priceLbl.text = languageChangeString(a_str: "Enter Price")
        
        if UserDefaults.standard.object(forKey: "currencyVal") != nil
        {
            self.currencyLbl.text = (UserDefaults.standard.object(forKey: "currencyVal") as! String)
        }else
        {
            
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func confirmBtn(_ sender: Any) {
        
        if priceTF.text == ""
        {
            self.showToast(message: "Enter Price")
        }
        else{
            
             self.confirmTowingService()
        }
       
    }
    
    func confirmTowingService()
    {
        
//        http://voliveafrica.com/carfix/services/complete_towing
//        parameters: towing_id,vendor_id,price
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let user = UserDefaults.standard.object(forKey: "userId")
        
        let towingID = UserDefaults.standard.object(forKey: "towingId")
        // let vendorId = UserDefaults.standard.object(forKey: "57")
        
        let vehicles = "\(Base_Url)complete_towing"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : user ?? "" ,"towing_id" : towingID ?? "" , "vendor_id" : userid ?? "" , "lang" : language ?? "" , "price" : priceTF.text!]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    // self.dismiss(animated: true, completion: nil)
                    
                    DispatchQueue.main.async {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                        self .present(service, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
}

extension CompltedTowing : UITextFieldDelegate{
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
       if textField == priceTF {
            
            priceTF.becomeFirstResponder()
            
            return true
        }
        return true
    }
    
public func textFieldDidEndEditing(_ textField: UITextField) {
    
    priceTF.resignFirstResponder()

}
}

