//
//  MyBillsVC.swift
//  CarFixing
//
//  Created by Suman Guntuka on 07/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class MyBillsVC: UIViewController {
    
    var appDelegate = AppDelegate()
    

    
    @IBOutlet var price: UILabel!
    @IBOutlet var dateTime: UILabel!
    @IBOutlet var jobtitle: UILabel!
    @IBOutlet var jobTable: UITableView!
    
    var jobNameArr = [String]()
    var typeArr = [String]()
    var jobPriceArr = [String]()
    var jobDateArr = [String]()
    var jobCostArr = [String]()
    var jobIdArr = [String]()
    var userIdArr = [String]()
    var jobIdArr1 = [String]()
    var allJobsData : [[String : AnyObject]]!
    var currencyArr = [String]()
    
    var idsArr = [String]()
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        self.title = languageChangeString(a_str: "My Bills")
        
        userBillsCall()
        
        jobtitle.text = languageChangeString(a_str: "My Bills")
        dateTime.text = languageChangeString(a_str: "Date and Time")
        price.text = languageChangeString(a_str: "Price")
        
        // Do any additional setup after loading the view.
    }
    func gobackFromInvoice ()
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func viewBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.jobTable)
        let indexPath: IndexPath? = self.jobTable.indexPathForRow(at: btnPosition)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "OnGoingJobDetailsVC") as? OnGoingJobDetailsVC
        
        detail?.jobIdStr = self.idsArr[(indexPath?.row)!];
        detail?.typeStr = self.typeArr[(indexPath?.row)!];
        
        appDelegate.jobsSepStr = "cjobs"
        detail?.typeStr1 = "bills"
       
        self.navigationController?.pushViewController(detail!, animated: true)
        
    }
    func userBillsCall()
       {
    //http://voliveafrica.com/carfix/services/user_bills?API-KEY=98745612&user_id=29
   // my bills service
    
    let userid = UserDefaults.standard.object(forKey: "user_id")
    let language = UserDefaults.standard.object(forKey: "currentLanguage")
    
    let details = "\(Base_Url)user_bills?"
    
    let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "user_id" : userid ?? "" ,"lang" : language ?? ""
    ]
    print(details)
    print(parameters)
    
    Services.sharedInstance.loader(view: self.view)
        
    Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
        
        if let responseData = response.result.value as? Dictionary<String, Any>{
            
            print(responseData)
            
            let status = responseData["status"] as? Int!
            let message = responseData["message"] as? String!
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                
                self.allJobsData = responseData["bills"] as? [[String:AnyObject]]
                
                for eachjob in self.allJobsData!
                {
                    
                    var jobNameStr = eachjob["vendor_name"] as? String?
                    //print("job name ",jobNameStr ?? "null")
                    let jobDateStr = eachjob["completed_date"] as? String?
                    let priceStr = eachjob["price"] as? String?
                    let idstr = eachjob["job_id"] as? String?
                    let type = eachjob["type"] as? String?
                     let curency = eachjob["currency"] as? String?
                    if jobNameStr == "<null>"{
                        jobNameStr = "null"
                    } else{
                           self.jobNameArr.append(jobNameStr!!)
                    }
                 
                    self.jobDateArr.append(jobDateStr!!)
                    self.jobPriceArr.append(priceStr!!)
                    self.idsArr.append(idstr!!)
                    self.typeArr.append(type!!)
                    self.currencyArr.append(curency!!)
                    
                }
                if self.jobNameArr.count == 0
                {
                    self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                }
                
                self.jobTable.reloadData()
            }
            else
            {
                  self.showToast(message: message!!)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
}
}

extension MyBillsVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return jobNameArr.count
        //return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = jobTable.dequeueReusableCell(withIdentifier: "JobCell1", for: indexPath) as! JobCell
        
        cell.billLbl.text = jobNameArr[indexPath.row]
        cell.dateBillLbl.text = jobDateArr[indexPath.row]
        cell.priceLbl.text = String(format: "%@ %@", currencyArr[indexPath.row], jobPriceArr[indexPath.row])
        //cell.details .setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

