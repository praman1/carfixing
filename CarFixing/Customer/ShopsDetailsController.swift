                           
//
//  ShopsDetailsController.swift
//  CarFixing
//  Created by Suman Guntuka on 26/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
                        

class ShopsDetailsController: UIViewController {

    @IBOutlet weak var detailTbl: UITableView!
    
    @IBOutlet var rateView: CosmosView!
    
    @IBOutlet weak var tableHieght: NSLayoutConstraint!
    @IBOutlet weak var bookBtn: UIButton!
    var appDelegate = AppDelegate()
    var vendorId : String!
    var daysArr = [String]()
    var nameStr : String!
    var ratingStr : String!
    var addressStr : String!
    var descriStr : String!
    var serviceIds : String!
    var vendorIdStr : String!
    var dateStr1 : String!
    var rateNo : NSNumber!
    
    var customerData: [[String:AnyObject]]!
    var vendorServicesPrice : [[String:AnyObject]]!
    var serviceNamesArr = [String]()
    var serviceIdsArr = [String]()
    var priceArr = [String]()
    var checkServiceArr = [String]()
      var serIdsArr = [String]()
    var defaultImg : String!
    var imageStr : String!
    
    
    var vendorTimings : [[String:AnyObject]]!
    var timeArr = [String]()
    
    var vendorGallery : [[String:AnyObject]]!
    var vendorImagesArr = [String]()
    var countNoStr : String!
    var rateNoStr : String!
    var addres : String!
    var dateStr : String!
    var shopType : String!
    var serviceBool : Bool!
    var dateBool : Bool!
    var chatImgStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        serviceBool = false
        dateBool = false
        
        if chatImgStr != nil
        {
           chatImgStr = UserDefaults.standard.object(forKey: "chatImgS") as! String
        }else
        {
            
        }
       
        
        
        //self.tabBarController?.tabBar.isHidden = true
        
        if shopType == "4"
        {
            print("Scrap shop pressed")
            
            bookBtn.setTitle(languageChangeString(a_str: "POST CAR SELLING REQUEST"),for: .normal)
           
            
            
        }else
        {
            bookBtn.setTitle(languageChangeString(a_str: "BOOK"),for: .normal)
        }
        
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
      
        daysArr = [languageChangeString(a_str: "Monday") ,languageChangeString(a_str: "Tuesday") , languageChangeString(a_str: "Wednesday") ,languageChangeString(a_str: "Thursday") ,languageChangeString(a_str: "Friday") , languageChangeString(a_str: "Saturday") ,languageChangeString(a_str: "Sunday")] as! [String]
        self.timeArr = ["00:00 PM" , "00:00 PM" , "00:00 PM" , "00:00 PM" , "00:00 PM" , "00:00 PM" , "00:00 PM"]
        dateStr = "select Date"
        
        self.vendorDetailsCall()
        
         NotificationCenter.default.addObserver(self, selector: #selector(dateData), name: NSNotification.Name(rawValue:"dateData"), object: nil)
    }
    
    func dateData ()
    {
        dateBool = true
        
        dateStr = ""
        
       let date = UserDefaults.standard.object(forKey: "date")
        self.dateStr = date as! String
        
        self.detailTbl.reloadData()
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
       
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
       
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
       
    }
    @IBAction func backBtn(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- POST BTN ACTION
    @IBAction func bookBtn(_ sender: Any) {
        
        if shopType == "4"
        {
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let post = storyboard.instantiateViewController(withIdentifier: "PostCarRequestController") as? PostCarRequestController
            post?.checkString = "1"
            post?.vendorID = vendorId! as NSString
            
            self.navigationController?.pushViewController(post!, animated: true)
            
        }else{
        
            if serviceBool == false
            {
                self.showToast(message: languageChangeString(a_str: "Please Select The Services")!)
                
            }else if dateBool == false
            {
                self.showToast(message: languageChangeString(a_str: "Please Select date")!)
                
            }else
            {
                 if Reachability.isConnectedToNetwork()
                
                 {
                     self.bookServiceCall ()
                }else
                 {
                    showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

                }
            }
          }
    }

    @IBAction func chatBtn(_ sender: Any) {
       
        appDelegate.chatId = "chat"
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        //let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatConversationController")
        let chat = storyboard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
        chat?.vendorIdS = self.vendorIdStr
        
        chat?.profileImg = self.imageStr
        
        appDelegate.chatTypeStr = "msg"
        
        self.navigationController?.pushViewController(chat!, animated: true)
    
    }
    
    func vendorDetailsCall()
    {
        // http://voliveafrica.com/carfix/services/vendor_details?API-KEY=98745612&user_id=2&vendor_id=8
        
         let userid = UserDefaults.standard.object(forKey: "user_id")
        print(userid ?? "")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
         let details = "\(Base_Url)vendor_details?API-KEY=\(APIKEY)&user_id=\(userid ?? "")&lang=\(language ?? "")&vendor_id=\(self.vendorId ?? "")"
        
//        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vender_id" : self.vendorId , "user_id" : userid ?? ""]
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.defaultImg = responseData["default_gallery_image"] as? String!
                    //print(self.defaultImg)
                    
                    
                    if let responseData1 = responseData["vendor_details"] as? Dictionary<String, AnyObject> {
                        
                        self.nameStr = (responseData1["shop_name"] as? String)!


                        let country: String = (responseData1["address"] as? String)!
                       
                        //self.addressStr = city + "," + country
                        self.addressStr = country
                       
                        self.descriStr = (responseData1["description"] as? String)!
                       self.vendorIdStr = (responseData1["user_id"] as? String)!
                       self.dateStr = (responseData1["created_at"] as? String)!
                        self.countNoStr = (responseData1["usercount"] as? String)!
                        self.rateNoStr =  (responseData1["rating"] as? String)!
                        let imgstr : String = (responseData1["image"] as? String)!
                        
                        self.imageStr = String(format: "%@%@",base_path,imgstr)
                       
                        
                    }
                    self.vendorServicesPrice = responseData["vendor_services"] as? [[String:AnyObject]]
                    self.vendorTimings = responseData["vendor_timings"] as? [[String:AnyObject]]
                    self.vendorGallery = responseData["vendor_gallery"] as? [[String:AnyObject]]
                    
                    print(self.vendorServicesPrice)
                    
                    for eachdata in self.vendorServicesPrice!{
                        
                      let price = eachdata["price"] as? String!
                      let service = eachdata["service_name_en"] as? String!
                      let serviceId = eachdata["service_id"] as? String!
                        
                        self.priceArr.append(price!!)
                        self.serviceNamesArr.append(service!!)
                        self.serviceIdsArr.append(serviceId!!)
                        
                        let check = "0"
                        self.checkServiceArr.append(check)
                        
                    }
                    
                    self.timeArr = [String]()
                    
                    for eachitem in self.vendorTimings!
                    {
                        
                        let fromTime = eachitem["from_time"] as? String!
                        let toTime = eachitem["to_time"] as? String!
                        let time = fromTime!! + " - " + toTime!!
                        self.timeArr.append(time)
                    }
                    
                    for eachImage in self.vendorGallery!
                    {
                        let image = eachImage["image"] as? String!
                        
                        let imagepath = base_path + image!!
                        
                        self.vendorImagesArr.append(imagepath)
                        
                    }
                    
                    if self.vendorGallery.count>0
                    {
                       print(self.vendorImagesArr)
                    
                    }else{
                         self.vendorImagesArr.append(self.defaultImg)
                    }
                    
                    if self.shopType == "4"
                    {
                        self.serviceNamesArr = [String]()
                    }
                    
                    DispatchQueue.main.async {
                        
                         self.detailTbl.reloadData()
                        
                    }
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func bookServiceCall ()
    {
        
        //http://voliveafrica.com/carfix/services/book_job
        //API-KEY,user_id,vendor_id,services,latitude,longitude,address,service_date

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
        let serviceDate = UserDefaults.standard.object(forKey: "date")
        self.addres = UserDefaults.standard.object(forKey: "address") as! String
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let statesCities = "\(Base_Url)book_job"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat ?? "" , "longitude" : long ?? "" , "services" : self.serviceIds! , "vendor_id" : self.vendorId! , "address" : self.addres ?? "" , "service_date" : serviceDate ?? "" , "lang" : language ?? ""]
        print(parameters)
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(statesCities, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
                    self .present(tab, animated: false, completion: nil)

                    self.showToast(message: message!!)
                   
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToast(message: message!!)
                }
                
            }
        }
    }
}

extension ShopsDetailsController: UITableViewDelegate , UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 13 + serviceNamesArr.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell1") as! DetailListCell1
            cell.imageArrCall(myArr: self.vendorImagesArr)
            cell.countStrCall(countA: self.countNoStr)
            cell.rateStrCall(rateA: self.rateNoStr)
            
            return cell
       
        }else if indexPath.row == 1{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell2") as! DetailListCell1
            
            cell.nameLbl.text = self.nameStr
            cell.addressLbl.text = self.addressStr
            
            cell.chat.text = languageChangeString(a_str: "chat")
            
            return cell
        }else if indexPath.row == 2{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell3") as! DetailListCell1
            cell.workingHrs.text = languageChangeString(a_str: "WORKING HOURS")
            return cell
        }else if indexPath.row == 3{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[0]
            cell.timeLbl.text = self.timeArr[0]
            
            return cell
           
        }else if indexPath.row == 4{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[1]
            cell.timeLbl.text = self.timeArr[1]
            return cell
        }else if indexPath.row == 5{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[2]
            cell.timeLbl.text = self.timeArr[2]
            return cell
        }else if indexPath.row == 6{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[3]
            cell.timeLbl.text = self.timeArr[3]
            return cell
        }else if indexPath.row == 7{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[4]
            cell.timeLbl.text = self.timeArr[4]
            return cell
        }else if indexPath.row == 8{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[5]
            cell.timeLbl.text = self.timeArr[5]
            return cell
        }else if indexPath.row == 9{
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell4") as! DetailListCell1
            cell.daysLbl.text = daysArr[6]
            cell.timeLbl.text = self.timeArr[6]
            return cell
        }
        
        else if indexPath.row == 10{
            
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell5") as! DetailListCell1
            
            cell.descriptionLbl.text = self.descriStr
            cell.descriptionText.text = languageChangeString(a_str: "DESCRIPTION")
            
            return cell
            
        }
        else if indexPath.row == 11{
            
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell6") as! DetailListCell1
            
            cell.servicesOffered.text = languageChangeString(a_str: "SERVICE OFFERED")
            
            return cell
            
        }else if indexPath.row == 12 + serviceNamesArr.count {
            
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell8") as! DetailListCell1
            
           cell.calenderDateLbl.text = self.dateStr
            cell.calender.text = languageChangeString(a_str: "Availability Calendar")
            
            // scrapshop
            
            if shopType == "4"
            {
               cell.calenderDateLbl.isHidden = true
               cell.calender.isHidden = true
              
                //self.tableHieght.constant = self.detailTbl.frame.size.height - 81
                
               //cell.accessoryType = false
                
            }
            return cell
            
        }
        else {
            
            let cell = detailTbl.dequeueReusableCell(withIdentifier: "cell7") as! DetailListCell1
          
            cell.serviceTypeLbl.text = serviceNamesArr[indexPath.row - 12]
           // cell.priceLbl.text = priceArr[indexPath.row - 12]
            
            //let idString = self.serviceIdsArr[indexPath.row - 12]
            
            if checkServiceArr[indexPath.row - 12] == "0" {

                cell.checkImg.image = UIImage(named: "unCheck")
                
                //self.serviceIdsArr.append(idString)

            }else if checkServiceArr[indexPath.row - 12] == "1"
            {
              
                cell.checkImg.image = UIImage(named: "check color")
               
               // self.serIdsArr = [String]()
              
                let idString = self.serviceIdsArr[indexPath.row - 12]
                
                self.serIdsArr.append(idString)
                
                self.serviceIds = self.serIdsArr.joined(separator: ",")
                
                print(" the selected ServicesIds Array \(self.serviceIds) ")

            }
            return cell
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        
        if indexPath.row == 0{
            return 196
        }
        else if indexPath.row == 1{
            return 77
        }else if indexPath.row == 2 {
            return 44;

        }else if indexPath.row == 3 {
            return 41;
            
        }else if indexPath.row == 4 {
            return 41;
            
        }else if indexPath.row == 5 {
            return 41;
            
        }else if indexPath.row == 6 {
            return 41;
            
        }else if indexPath.row == 7 {
            return 41;
            
        }else if indexPath.row == 8 {
            return 41;
            
        }else if indexPath.row == 9 {
            return 41;
        }
        else if indexPath.row == 10{

            return 128
        }
        else if indexPath.row == 11{
            
            if shopType == "4"
            {
                return 0
            }else
            {
                return 38
            }

        }else if  indexPath.row == 12 + serviceNamesArr.count {
            
            if shopType == "4"
            {
            return 0
            }else
            {
              return 117
            }

        }else {
          if shopType == "4"
          {
            return  0
            
          }else
          {
            return  48
            }
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
 
//        if checkServiceArr[indexPath.row - 12 ] == "0" {
//
//            checkServiceArr[indexPath.row - 12 ] = "1"
//            self.serviceIdsArr = [String]()
//        }
//        else if checkServiceArr[indexPath.row - 12] == "1"{
//
//            checkServiceArr[indexPath.row - 12] = "0"
//            self.serviceIdsArr = [String]()
//
//        }
//        self.detailTbl.reloadData()
        
        if indexPath.row == 0{
            
        }
        else if indexPath.row == 1{
            
        }else if indexPath.row == 2 {
            
            
        }else if indexPath.row == 3 {
            
            
        }else if indexPath.row == 4 {
           
            
        }else if indexPath.row == 5 {
           
            
        }else if indexPath.row == 6 {
            
            
        }else if indexPath.row == 7 {
            
            
        }else if indexPath.row == 8 {
            
        }else if indexPath.row == 9 {
            
        }
        else if indexPath.row == 10{
            
        }
        else if indexPath.row == 11{
            
            
        }else if  indexPath.row == 12 + serviceNamesArr.count {

            if shopType == "4"
            {
           print("scrapshop selected")
                
            }else
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let calender = storyBoard.instantiateViewController(withIdentifier: "CalenderController") as? CalenderController
                self.navigationController?.pushViewController(calender!, animated: true)
            }
            
        }else {
            
            if checkServiceArr[indexPath.row - 12 ] == "0" {
                
                serviceBool = true
            
                    checkServiceArr[indexPath.row - 12 ] = "1"
                
                self.serIdsArr = [String]()
                
                }else if checkServiceArr[indexPath.row - 12 ] == "1"  {
               
                    serviceBool = true
            
                    checkServiceArr[indexPath.row - 12] = "0"
                
                self.serIdsArr = [String]()
            
                    }
            
                    self.detailTbl.reloadData()
            
        }
   
    }
}
