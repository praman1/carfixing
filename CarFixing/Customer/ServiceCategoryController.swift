//
//  ServiceCategoryController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/11/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ServiceCategoryController: UIViewController {
    @IBOutlet weak var serviceTbl: UITableView!
//    var serviceArr = [String]()
//    var serviceCheckArr = [String]()
    
    var serviceCheckArr = [String]()
    //var servicesArr: [[String:AnyObject]]!
    var servicesArr: Array<String> = []
    var serviceIdArr: Array<String> = []
    var defaultCheckArr = [String]()
    var typeStr : String!
    
    
    @IBOutlet weak var serviceCategory: UILabel!
    var nameArr = [String]()
    var idArr = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        let saveBtn = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveBtnClicked))
        saveBtn.tintColor = UIColor.white
        self.navigationItem.title = "Service Category"
        
        self.navigationItem.rightBarButtonItem = saveBtn

//        serviceArr = ["Transmission Repair","Engine Repair","Brake Repair","Air         Conditioning","Shocks,Struts and suspensions","Emision Testing","Transmission Repair","Engine Repair","Brake Repair","Air Conditioning","Shocks,Struts and suspensions","Emision Testing"]
//        serviceCheckArr = ["0","0","0","0","0","0","0","0","0","0","0","0",]
//     Do any additional setup after loading the view.
        
        if Reachability.isConnectedToNetwork() {
           
                 self.SerivceListService ()
          
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
      
    }
    
    @objc func backBtnClicked(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @objc func saveBtnClicked(){
        
       self.navigationController?.popViewController(animated: true)
    }
    
    func SerivceListService ()
    {
        
        //http://voliveafrica.com/carfix/services/available_services
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        typeStr = UserDefaults.standard.object(forKey: "ServiceT") as! String
        let vehicles = "\(Base_Url)available_services"
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"lang" : language ?? "","type" : typeStr ?? "" ]
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    //self.servicesArr = responseData["services"] as! Array<String>
                    
                    let servicesCat = responseData["services"] as? [[String:Any]]
                    
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let name = servicesCat["service_name_en"]  as! String
                        let serviceId = servicesCat["id"]  as! String
                        
                        let myInt2 = (serviceId as NSString).integerValue
                        
                        let myString = String(myInt2)
                        
                        self.servicesArr.append(name)
                        
                        self.serviceIdArr.append(myString)
                        
                        let checkStr = "0"
                        self.serviceCheckArr.append(checkStr)
                        
//                        let defaultCheck = "0"
//                        self.defaultCheckArr.append(defaultCheck)
                    
                    }
                    
                    print("services ids Array are ***** \(self.serviceIdArr)")
                    
                    self.serviceTbl.reloadData()
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension ServiceCategoryController: UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = serviceTbl.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ServiceCategoryCell
        
        cell.nameLbl.text = servicesArr[indexPath.row]
        let idsting = self.serviceIdArr[indexPath.row]
        
        if serviceCheckArr[indexPath.row] == "0" {
            cell.img.image = UIImage(named: "unCheck")
            cell.nameLbl.textColor = UIColor.lightGray
            
        }else{
            cell.img.image = UIImage(named: "check color")
             cell.nameLbl.textColor = UIColor.black
            
            idArr.append(idsting)
            
            let IdStr = idArr.joined(separator: ",")
            
            UserDefaults.standard.set( IdStr , forKey: "filterids") //setObject
            print(UserDefaults.standard.object(forKey: "filterids") ?? "")
            
        }
 
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if serviceCheckArr[indexPath.row] == "0" {
            serviceCheckArr[indexPath.row] = "1"
            idArr = [String]()
            
        }
        else if serviceCheckArr[indexPath.row] == "1"{
             serviceCheckArr[indexPath.row] = "0"
            idArr = [String]()
        }
        
        tableView.reloadData()
        
    }
 }
