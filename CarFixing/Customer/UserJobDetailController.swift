//
//  UserJobDetailController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 08/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class UserJobDetailController: UIViewController {
    
    @IBOutlet var imgCal: UIImageView!
    @IBOutlet weak var jobImg: UIImageView!
    @IBOutlet weak var shopName: UILabel!
    
    @IBOutlet weak var jobStatus: UILabel!
    @IBOutlet weak var jobView: UIView!
    
    @IBOutlet weak var servicesLbl: UILabel!
    @IBOutlet weak var services: UILabel!
    @IBOutlet weak var locationsLbl: UILabel!
    @IBOutlet weak var locations: UILabel!
    @IBOutlet weak var shopNameLbl: UILabel!
    
    @IBOutlet weak var jobstatus: UILabel!
    @IBOutlet weak var availableDate: UILabel!
    @IBOutlet weak var serviceDateLbl: UILabel!
    @IBOutlet weak var servieceTbl: UITableView!
    var serviceNameArr = [String]()
    var isCheckArr = [String]()
    var idArr = [String]()
    var serviceIds = [String]()
    
    var jobServiceData : [[String : AnyObject]]!
   
    var jobidStr : String!
    var isSelected : String!
    var appde = AppDelegate()
    var editBtn : UIBarButtonItem!
    var lat : String!
    var long : String!
    var vendorID : String!
    var jobId : String!
    var serviceDate : String!
    var serviceIdsStr :String!
    var dateBool : Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         appde = UIApplication.shared.delegate as! AppDelegate
        
        dateBool = false
        
        shopName.text = languageChangeString(a_str: "SHOP NAME")
         locations.text = languageChangeString(a_str: "LOCATION")
         services.text = languageChangeString(a_str: "SERVICES")
         availableDate.text = languageChangeString(a_str: "SCHEDULED DATE")
        jobstatus.text = languageChangeString(a_str: "JOB STATUS")
        

        if Reachability.isConnectedToNetwork() {
           
            userJobDetails()
        
        }else
        {
           showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
       
        
        
        if appde.onsiteSepStr == "onsite"
        {
            self.navigationItem.title = languageChangeString(a_str: "Onsite Request")
           
        }else
        {
            self.navigationItem.title = languageChangeString(a_str: "Automobile Repair Requests")
        }
        
        editBtn = UIBarButtonItem(title: languageChangeString(a_str: "SAVE"), style: .plain, target: self, action: #selector(editBtnClicked))
        editBtn.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = editBtn
        
        if appde.jobStr == "edit"
        {
            editBtn.isEnabled = true
            editBtn.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            jobStatus.isHidden = true
            jobView.isHidden = true
           
            imgCal.isHidden = true
            self.imgCal.image=UIImage(named: "next")
            
            
            
        }else if appde.jobStr == "detail"
        {
            editBtn.isEnabled = false
            editBtn.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            jobStatus.isHidden = false
            jobView.isHidden = false
           
            imgCal.isHidden = false
            self.imgCal.image=UIImage(named: "")
            //imgCal.image = UIImage :name
          
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
    
       
        if dateBool == false
        {
            
        }else
        {
            self.serviceDate = UserDefaults.standard.object(forKey: "date") as! String
            
            self.serviceDateLbl.text = self.serviceDate
        }
        
        
    
    }
    
    func editBtnClicked()
    {
        if Reachability.isConnectedToNetwork() {
             self.editServiceCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func calendarBtn(_ sender: Any) {
        
        dateBool = true
        
        
        if appde.jobStr == "edit"
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let calender = storyBoard.instantiateViewController(withIdentifier: "CalenderController") as? CalenderController
            self.navigationController?.pushViewController(calender!, animated: true)
            
        }else if appde.jobStr == "detail"
        {
           
        }
        
    }
    func userJobDetails()
    {
        
        //http://voliveafrica.com/carfix/services/job_details?API-KEY=98745612&job_id=1
        let language = UserDefaults.standard.object(forKey: "currentLanguage")


        //let details = "\(Base_Url)job_details?API-KEY=\(APIKEY)&job_id=\(self.jobidStr ?? "")&lang=\(language ?? "")&device_type=\("iOS")"
        let details = "\(Base_Url)job_details?API-KEY=\(APIKEY)&job_id=\(self.jobidStr ?? "")&lang=\(language ?? "")"
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                    
                if let allJobsData = responseData["job_details"] as? Dictionary<String, AnyObject> {
                    
                    print(allJobsData)
                    
                    let jobDetails = allJobsData["job_data"] as? Dictionary<String,AnyObject>
                    
                    let shopNameStr = jobDetails!["shop_name"] as? String!
                     let statusStr = jobDetails!["status"] as? String!
                    let locationStr = jobDetails!["address"] as? String!
                    let imageStr = jobDetails!["shop_image"] as? String!
                    self.vendorID =  jobDetails!["vendor_id"] as? String!
                    self.lat =  jobDetails!["latitude"] as? String!
                    self.long =  jobDetails!["longitude"] as? String!
                    self.jobId =  jobDetails!["job_id"] as? String!
                    self.serviceDate = jobDetails!["service_date"] as? String!
                    
                    let shopImage = base_path + imageStr!!
                    
                    print(shopNameStr as Any,locationStr as Any)
                    
                    self.shopNameLbl.text = shopNameStr as! String
                    self.locationsLbl.text = locationStr as! String
                    self.serviceDateLbl.text = self.serviceDate
                    self.jobStatus.text = statusStr as! String
                   
                    self.jobImg.sd_setImage(with: URL(string: shopImage), placeholderImage: UIImage(named:"Castrol Services"))
                    
                    self.jobServiceData = allJobsData["job_services"] as? [[String:AnyObject]]
                    
                    print(self.jobServiceData)
                    
                    for service in self.jobServiceData!
                    {
                        let serviceStr = service["service_name_en"] as? String!
                        //self.isSelected = service["is_selected"] as? String!
                        let x : Int = service["is_selected"] as! Int
                        let ids = String(x)
                        let serviceId = service["service_id"] as? String!
                        self.serviceNameArr.append(serviceStr!!)
                        self.serviceIds.append(serviceId!!)
                        
                        self.isCheckArr.append(ids)
        
                        
                    }
                     self.servieceTbl.reloadData()
                
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                     
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                    
                    }
                }
            }
        }
    }
    
    func editServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/book_job // API-KEY,user_id,vendor_id,services,latitude,longitude,address,service_date,job_id

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let editJob = "\(Base_Url)book_job"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : self.lat! , "longitude" : self.long!, "services" : self.serviceIdsStr! , "vendor_id" : self.vendorID! , "address" : locationsLbl.text!  , "service_date" : self.serviceDate! , "job_id" : self.jobId! ,"lang" : language ?? ""]
       print(editJob)
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(editJob, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.showToast(message: message!!)
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "JobsController2")
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                  //  self.allJobsData = responseData["jobs"] as? [[String:AnyObject]]
                    
//                    for eachjob in self.allJobsData!
//                    {
//                        let jobNameStr = eachjob["shop_name"] as? String!
//                        let jobDateStr = eachjob["service_date"] as? String!
//
//                        self.jobIdStr = eachjob["job_id"] as? String
//
//                        self.jobNameArr.append(jobNameStr!)
//                        self.jobDateArr.append(jobDateStr!)
//                        self.jobIdsArr.append(self.jobIdStr!)
//
//                    }
//
//                    self.jobsTbl.reloadData()
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    
}
extension UserJobDetailController: UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.serviceNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = servieceTbl.dequeueReusableCell(withIdentifier: "JobServiceCell") as! JobServiceCell
        
        cell.serviceLbl.text = self.serviceNameArr[indexPath.row]
        //let idsting = self.isCheckArr[indexPath.row]
        let serviceIdStr = self.serviceIds[indexPath.row]
        
        if appde.jobStr == "edit"{
        
        if self.isCheckArr[indexPath.row] == "1" {
           
            
            cell.img.image = UIImage(named: "check color")
            cell.serviceLbl.textColor = UIColor.black
            
            idArr.append(serviceIdStr)
            
            let IdStr = idArr.joined(separator: ",")
            self.serviceIdsStr = IdStr
            
            print(self.serviceIdsStr)
            
//            UserDefaults.standard.set( IdStr , forKey: "filterids") //setObject
//
//            print(UserDefaults.standard.object(forKey: "filterids") ?? "")
            

        }else
        {
            cell.img.image = UIImage(named: "unCheck")
            cell.serviceLbl.textColor = UIColor.lightGray
            
            idArr.append(serviceIdStr)
            
//            let IdStr = idArr.joined(separator: ",")
//
//            self.serviceIdsStr = IdStr
//            print(self.serviceIdsStr)


        }
        }else
        {
            
            cell.img.image = UIImage(named: "")
            
            
            if self.isCheckArr[indexPath.row] == "1"
            {
                cell.serviceLbl.textColor = UIColor.black
            }else
            {
                cell.serviceLbl.textColor = UIColor.white
            }
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.isCheckArr[indexPath.row] == "0" {
            
            self.isCheckArr[indexPath.row] = "1"
            
            idArr = [String]()
            
        }
        else if self.isCheckArr[indexPath.row] == "1"{
            self.isCheckArr[indexPath.row] = "0"
            idArr = [String]()
        }
        
        tableView.reloadData()
        
    }
    
}

