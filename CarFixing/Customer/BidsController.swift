//
//  BidsController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/13/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class BidsController: UIViewController {
     var strArr = [String]()
    
    var allBidsData : [[String:AnyObject]]!
    var bidsTitleArr = [String]()
    var bidsDateArr = [String]()
    var bidsIdsArr = [String]()
    
    var bidIdStr : String!
    var bid : String!
    var appde : AppDelegate!
   
    @IBOutlet weak var bidsTbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appde = UIApplication.shared.delegate as! AppDelegate
strArr = ["1955 Chevrolet corvette car","Classic Car","1955 Chevrolet corvette car","1955 Chevrolet corvette car"]
        self.title = languageChangeString(a_str: "My Bids")
        
//        tabBarController?.tabBar.items?[0].title = languageChangeString(a_str: "Home")
//        tabBarController?.tabBar.items?[1].title = languageChangeString(a_str: "Bids")
//        tabBarController?.tabBar.items?[2].title = languageChangeString(a_str: "Jobs")
//        tabBarController?.tabBar.items?[3].title = languageChangeString(a_str: "Chats")
//        tabBarController?.tabBar.items?[4].title = languageChangeString(a_str: "Profile")
        
        // Do any additional setup after loading the view.
        
       // self.tabBarController?.tabBar.isHidden = true
        
        if Reachability.isConnectedToNetwork()
        {
            self.allBidsServiceCall()
            
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
        
        
    }

    @IBAction func editBtn(_ sender: Any) {
        
        appde.sepStr = "str"
        
//        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTbl)
//        let indexPath: IndexPath? = self.bidsTbl.indexPathForRow(at: btnPosition)
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTbl)
        let indexPath: IndexPath? = self.bidsTbl.indexPathForRow(at: btnPosition)
        
        let storybord = UIStoryboard(name: "Main", bundle:nil)
        let edit = storybord.instantiateViewController(withIdentifier: "EditBidController") as? EditBidController
        
        edit?.bidStr = self.bidsIdsArr[(indexPath?.row)!]
       
        self.navigationController?.pushViewController( edit! , animated: true)
        
    }
    
    @IBAction func deleteBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTbl)
        let indexPath: IndexPath? = self.bidsTbl.indexPathForRow(at: btnPosition)
        
        self.bid = self.bidsIdsArr[(indexPath?.row)!]
        
        if Reachability.isConnectedToNetwork()
        {
             self.bidsDeleteCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
       
        
    }
    
    @IBAction func detailsBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTbl)
        let indexPath: IndexPath? = self.bidsTbl.indexPathForRow(at: btnPosition)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailBid = storyBoard.instantiateViewController(withIdentifier: "DetailBidController") as?
        DetailBidController
        detailBid?.bididStr = self.bidsIdsArr[(indexPath?.row)!]
        
        self.navigationController?.pushViewController(detailBid!, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
    
    
    func allBidsServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/user_jobs?API-KEY=98745612&user_id=2
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let details = "\(Base_Url)user_bids?API-KEY=\(APIKEY)&user_id=\(userid ?? "")&lang=\(language ?? "")"
        
        //        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vender_id" : self.vendorId , "user_id" : userid ?? ""]
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
        
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.bidsTitleArr = [String]()
                    self.bidsDateArr = [String]()
                    self.bidsIdsArr = [String]()
                    
                    self.allBidsData = responseData["bids"] as? [[String:AnyObject]]
                    
                    for eachjob in self.allBidsData!
                    {
                        let jobNameStr = eachjob["title"] as? String!
                        let jobDateStr = eachjob["posted_date"] as? String!
                        self.bidIdStr = eachjob["b_id"] as? String!
                        self.bidsIdsArr.append(self.bidIdStr)
                        self.bidsTitleArr.append(jobNameStr!!)
                        self.bidsDateArr.append(jobDateStr!!)
                    }
                        if self.bidsTitleArr.count == 0
                        {
                           self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                        }
                    DispatchQueue.main.async {
                        
                    self.bidsTbl.reloadData()
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                   
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                    
                    }
                }
            }
        }
    }
    
    func bidsDeleteCall ()
    {
        
        //http://voliveafrica.com/carfix/services/delete_bid?API-KEY=98745612&bid_id=2
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let details = "\(Base_Url)delete_bid?API-KEY=\(APIKEY)&bid_id=\(self.bid!)&lang=\(language ?? "")"
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                 
                    Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    
                    //self.bidsTbl.reloadData()
                   
                    self.allBidsServiceCall()
                        
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
}
extension BidsController: UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bidsTitleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = bidsTbl.dequeueReusableCell(withIdentifier: "cell") as! BidsCell
        print("bid title ",self.bidsTitleArr[indexPath.row])
        cell.nameLbl.text = self.bidsTitleArr[indexPath.row]
         cell.dateLbl.text = self.bidsDateArr[indexPath.row]
        
        cell.postedOn.text = languageChangeString(a_str: "Posted on:")
        cell.edit.text = languageChangeString(a_str: "Edit")
        cell.delete.text = languageChangeString(a_str: "Delete")
        cell.viewDetails.setTitle(languageChangeString(a_str: "View Details"), for: UIControlState.normal)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//    }
    
    
}
