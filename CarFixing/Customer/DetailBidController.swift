//
//  DetailBidController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/13/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class DetailBidController: UIViewController {
    @IBOutlet weak var detailTbl : UITableView!
    
    @IBOutlet var currency: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var sericeName: UILabel!
    var details : ViewDetailsBidController!
   
    @IBOutlet var decsTextLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet weak var tableHieght: NSLayoutConstraint!
    @IBOutlet weak var bidsCollection: UICollectionView!
    @IBOutlet weak var bidsTableView: UITableView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var carDetailsLbl: UILabel!
    @IBOutlet weak var biddingPrice: UILabel!
    @IBOutlet weak var modelLbl: UILabel!
    @IBOutlet weak var makeLbl: UILabel!
    var bididStr : String!
    var width : CGFloat!
    var shopImagesArr = [String]()
    var bidImgsArr = [String]()
    var shopNameArr = [String]()
    var msgArr = [String]()
    var amountArr = [String]()
    var statusArr = [String]()
    var vendorIdArr = [String]()
    var bidIdArr = [String]()
    var currencyArr = [String]()
   var appDelegate = AppDelegate()
    var bidsDetailData : [[String : AnyObject]]!
    
    @IBOutlet var titleLable: UILabel!
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet weak var carSellingReq: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var biddingPrice1: UILabel!
    @IBOutlet weak var bidding: UILabel!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
         appDelegate = UIApplication.shared.delegate as! AppDelegate
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Bid Details")
        
       
        carSellingReq.text = languageChangeString(a_str: "POST CAR SELLING REQUEST")
        location.text = languageChangeString(a_str: "LOCATION")
        carDetails.text = languageChangeString(a_str: "CAR DETAILS")
        make.text = languageChangeString(a_str: "MAKE")
        model.text = languageChangeString(a_str: "MODEL")
        biddingPrice1.text = languageChangeString(a_str: "Bid Amount")
        titleLbl.text = languageChangeString(a_str: "Title")
        descriptionLbl.text = languageChangeString(a_str: "DESCRIPTION")
        bidding.text = languageChangeString(a_str: "Requests")
        
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        
        sericeName.text = languageChangeString(a_str: "Service Name")
        price.text = languageChangeString(a_str: "Price")
        currency.text = languageChangeString(a_str: "Currency")
        
        if Reachability.isConnectedToNetwork() {
            self.bidDetailsCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(bidDetails), name: NSNotification.Name(rawValue:"bidUpdate"), object: nil)
        
    
    }
    func bidDetails()
    {
        
        if Reachability.isConnectedToNetwork() {
            self.bidDetailsCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
        
    }
    
    @objc func backBtnClicked (){
        
        if appDelegate.chatTypeStr == "bid_status"
        {
             self.dismiss(animated: true, completion: nil)
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
//            self .present(service, animated: true, completion: nil)
            
        }else{
             self.navigationController?.popViewController(animated: true)
        }
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func chatBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTableView)
        let indexPath: IndexPath? = self.bidsTableView.indexPathForRow(at: btnPosition)
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        //let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatConversationController")
        let chat = storyboard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
        if appDelegate.chatTypeStr == "bid_status"
        {
            
            chat?.vendorIdS = UserDefaults.standard.object(forKey: "vendorid") as! String
           
           
            
            
        chat?.checkString = "detailsBid"
        }else{
            chat?.vendorIdS = self.vendorIdArr[(indexPath?.row)!]
            chat?.profileImg = self.shopImagesArr[(indexPath?.row)!]
        }
        
      
        
        self.navigationController?.pushViewController(chat!, animated: true)
        
    }
    @IBAction func acceptBtn(_ sender: Any) {
      
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTableView)
        let indexPath: IndexPath? = self.bidsTableView.indexPathForRow(at: btnPosition)
        
      
        
        if appDelegate.chatTypeStr == "bid_status"
        {
            
           
        }else{
            UserDefaults.standard.set(self.vendorIdArr[(indexPath?.row)!], forKey: "vendorid")
            UserDefaults.standard.set(self.bidIdArr[(indexPath?.row)!], forKey: "bidid")
            UserDefaults.standard.set(self.statusArr[(indexPath?.row)!], forKey: "status")
        }
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "AcceptBidController")
        
        self .present(service, animated: true, completion: nil)
        
    }
    @IBAction func viewDetailsBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.bidsTableView)
        let indexPath: IndexPath? = self.bidsTableView.indexPathForRow(at: btnPosition)
        
        
//        details?.shopNameStr = self.shopNameArr[(indexPath?.row)!]
//        details?.shopImgStr = self.shopImagesArr[(indexPath?.row)!]
//        details?.priceStr = self.amountArr[(indexPath?.row)!]
//        details?.msgStr = self.msgArr[(indexPath?.row)!]
        
        UserDefaults.standard.set(self.shopNameArr[(indexPath?.row)!], forKey: "shopName")
        UserDefaults.standard.set(self.shopImagesArr[(indexPath?.row)!], forKey: "shopImage")
        UserDefaults.standard.set(self.amountArr[(indexPath?.row)!], forKey: "price")
        UserDefaults.standard.set(self.msgArr[(indexPath?.row)!], forKey: "msg")
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "ViewDetailsBidController")
        
        self .present(service, animated: true, completion: nil)
        
    }
    
    func bidDetailsCall ()
    {
        
        //http://voliveafrica.com/carfix/services/bid_details?API-KEY=98745612&bid_id=2
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let details = "\(Base_Url)bid_details?API-KEY=\(APIKEY)&bid_id=\(self.bididStr!)&lang=\(language ?? "")"
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    self.shopImagesArr = [String]()
                    self.bidImgsArr = [String]()
                    self.shopNameArr = [String]()
                    self.msgArr = [String]()
                    self.amountArr = [String]()
                    self.statusArr = [String]()
                    self.vendorIdArr = [String]()
                    self.bidIdArr = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    let bidsImgsData = responseData["bid_images"] as? [[String:AnyObject]]
                    
                    for i in bidsImgsData!
                    {
                      let bidImg = i["image"] as? String!
                        
                        let image = base_path + bidImg!!
                        
                      self.bidImgsArr.append(image)
                        
                    }
                    
                    let bidsAuctionsData = responseData["auctions"] as? [[String:AnyObject]]
                    
                    for i in bidsAuctionsData!
                    {
                        let shopImg = i["shop_image"] as? String!
                        
                        let image = base_path + shopImg!!
                        let msg = i["message"] as? String!
                        let priceBid = i["amount"] as? String!
                        let shopName = i["shop_name"] as? String!
                        let status = i["status"] as? String!
                        let vendorId = i["vendor_id"] as? String!
                        let bidId = i["bid_id"] as? String!
                        let currency = i["currency"] as? String!
                        //0 pendig, 1 accepted, 2 rejected..
                        
                        self.msgArr.append(msg!!)
                        self.shopNameArr.append(shopName!!)
                        self.amountArr.append(priceBid!!)
                        self.statusArr.append(status!!)
                        self.vendorIdArr.append(vendorId!!)
                        self.shopImagesArr.append(image)
                        self.bidIdArr.append(bidId!!)
                        self.currencyArr.append(currency!!)
                    }
                    
                    if self.shopNameArr.count > 0
                    {
                        self.sericeName.isHidden = false
                        self.currency.isHidden = false
                        self.price.isHidden = false
                    }else
                    {
                      self.sericeName.isHidden = true
                      self.currency.isHidden = true
                      self.price.isHidden = true
                    }
                    
                if let bid_detailsData = responseData["bid_details"] as? Dictionary<String, AnyObject> {
                    
                    let location = bid_detailsData["address"] as? String!
                    let carDetails = bid_detailsData["description"] as? String!
                    let make = bid_detailsData["make"] as? String!
                    let model = bid_detailsData["model"] as? String!
                    let price = bid_detailsData["price"] as? String!
                    let titleStr = bid_detailsData["title"] as? String!
                    let descStr = bid_detailsData["description"] as? String!
                    
                    DispatchQueue.main.async {
                        
                        self.locationLbl.text = location!!
                        self.carDetailsLbl.text = carDetails!!
                        self.makeLbl.text = make!!
                        self.modelLbl.text = model!!
                        self.biddingPrice.text = price!!
                        self.titleLable.text = titleStr!!
                        self.decsTextLbl.text = descStr!!
                        
                        self.bidsCollection.reloadData()
                        self.bidsTableView.reloadData()
                    }
                    }
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}
extension DetailBidController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.shopNameArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
             var cell = DetailBidCell()
        
            cell = self.bidsTableView.dequeueReusableCell(withIdentifier: "DetailBidCell") as! DetailBidCell
        
            cell.bidImgTbl.sd_setImage(with: URL(string: self.shopImagesArr[indexPath.row]), placeholderImage: UIImage(named:"profile pic"))
        if self.currencyArr[indexPath.row] == ""
        {
             cell.PRICElABEL.text = String(format: "%@", self.amountArr[indexPath.row])
             cell.priceLbl.text = String(format: "%@  %@","|","SAR")
        }else
        {
            
            cell.PRICElABEL.text = String(format: "%@", self.amountArr[indexPath.row])
             cell.priceLbl.text = String(format: "%@       %@","|",self.currencyArr[indexPath.row])
        }
       
//            cell.priceLbl.text = self.amountArr[indexPath.row]
//        cell.currencyLbl.text = self.currencyArr[indexPath.row]
            cell.shopNamLbl.text = self.shopNameArr[indexPath.row]
            cell.detailsTxtView.text = self.msgArr[indexPath.row]
        
        let status = self.statusArr[indexPath.row]
        
        if status == "0"
        {
            
            cell.acceptLbl?.text = languageChangeString(a_str: "Accept Bid")
            cell.acceptLbl?.textColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
            cell.acceptButton.isUserInteractionEnabled = true
            
        }else if status == "1"
        {
            
            cell.acceptLbl?.text = languageChangeString(a_str: "Accepted")
            cell.acceptLbl?.textColor = #colorLiteral(red: 0.07690429688, green: 0.723531127, blue: 0, alpha: 1)
            cell.acceptButton.isUserInteractionEnabled = false
            
        }else if status == "2"
        {
            
           cell.acceptLbl?.text = languageChangeString(a_str: "Rejected")
            cell.acceptLbl?.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            cell.acceptButton.isUserInteractionEnabled = false
        }else
        {
            cell.acceptLbl?.text = languageChangeString(a_str: "Accept Request")
            cell.acceptLbl?.textColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
            cell.acceptButton.isUserInteractionEnabled = true
        }
        cell.viewDetails.setTitle(languageChangeString(a_str: "View Details"), for: UIControlState.normal)
        cell.chat.text = languageChangeString(a_str: "chat")
        cell.bidAmount.text = languageChangeString(a_str: "Request Amount")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return 132

    }

}

extension DetailBidController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return self.bidImgsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.bidsCollection.dequeueReusableCell(withReuseIdentifier: "BidsCollectCell", for: indexPath) as! BidsCollectCell
        
        // cell.img.image = UIImage(named:collectionImages[indexPath.row])
        
        cell.bidImg.sd_setImage(with: URL(string: self.bidImgsArr[indexPath.row]), placeholderImage: UIImage(named:"as img4"))
        
        // self.pageControl.currentPage = indexPath.row
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        width = CGFloat(bidsCollection.frame.size.width / 3)
        return CGSize(width: width, height: 90)
        
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "ImageZoom1")
        UserDefaults.standard.set(self.bidImgsArr, forKey: "bidImg")
        self .present(service, animated: true, completion: nil)
        
    }
    
}
