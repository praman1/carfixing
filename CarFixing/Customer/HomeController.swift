//
//  HomeController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/7/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import GoogleMaps
import GooglePlaces

class HomeController: UIViewController ,CLLocationManagerDelegate, GMSMapViewDelegate {
     @IBOutlet weak var gmapView1: GMSMapView!
    @IBOutlet weak var searchTxtFld: UITextField!
    @IBOutlet weak var optionsTable: UITableView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var filtersTF: UITextField!
    var customerDataArr1: [[String:AnyObject]]!
    var imagesArray1 = [String]()
    var linksArr = [String]()
    var pinsImgArr = [String]()
    var latArr = [Double]()
    var longArr = [Double]()
    var idsArr = [String]()
    var lat1 : Double!
    var long1 : Double!
    var appDelegate : AppDelegate!
    var locateMe : Bool!
    var latStr : String!
    var LongStr : String!
    var locationMark :GMSMarker!
    var fillStr : String!
    
    var serviceProviders : [[String:AnyObject]]!
    
    @IBOutlet weak var pageControl: UIPageControl!
 //   @IBOutlet weak var categoryCollectionview: UICollectionView!
    var gmapView: GMSMapView!
    //arrays
    var bannerImagesArr  = [String]()
    var categoryArr = [String]()
    var categoryNameArr  = [String]()
    var latitude : String!
    var longitude : String!
    var addressOfuser :String!
     var timer:Timer? = nil
    var filterArr = [String]()
    var selectStr : String!
    var filterPicker: UIPickerView?
    var roleArr = [String]()
    
    var toolbar: UIToolbar?
    var selectedTextField: UITextField?
    var parameters: Dictionary<String, Any>!
    
    var filter : Bool!
    var homeVal : Bool!
    
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
  
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        locateMe = false
        homeVal = false
        
         UserDefaults.standard.removeObject(forKey: "saveS")
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        tabBarController?.tabBar.items?[0].title = languageChangeString(a_str: "Home")
        tabBarController?.tabBar.items?[1].title = languageChangeString(a_str: "Individual Professional")
        tabBarController?.tabBar.items?[2].title = languageChangeString(a_str: "Auto Repair")
        tabBarController?.tabBar.items?[3].title = languageChangeString(a_str: "Scrap")
        tabBarController?.tabBar.items?[4].title = languageChangeString(a_str: "Towing")
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1

        let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")

        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuVC!)
        menuLeftNavigationController.leftSide = true

        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1

        let menu1VC = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")

        let menuNavigationController = UISideMenuNavigationController(rootViewController: menu1VC!)
        menuNavigationController.leftSide = true

        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
       
        NotificationCenter.default.addObserver(self, selector: #selector(openSideMenu), name: NSNotification.Name (rawValue: "openSideMenu"), object: nil)

        self.navigationController?.navigationBar.isTranslucent = false
    

        bannerImagesArr = ["banner ad","as img2","as img3" ,"as img3"]
        categoryArr = ["menu2","menu3","menu4","instant"]
        categoryNameArr = [ languageChangeString(a_str: "Individual Professional"),languageChangeString(a_str: "Auto Repair Shop"), languageChangeString(a_str: "Scrap Shop"),languageChangeString(a_str: "Instant/Emergency") ] as! [String]
     
        gmapView?.delegate = self
        gmapView1.delegate = self
      
        //self.pageControl.numberOfPages = bannerImagesArr.count
        self.tabBarController?.tabBar.isHidden = false
      
//        self.bannerCollectionView.reloadData()
        
        filterArr = ["5 KM","10 KM","15 KM","20 KM"]
        filter = false
        filterPicker = UIPickerView()
        filterPicker?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (filterPicker?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        filterPicker?.delegate = self as UIPickerViewDelegate
        filterPicker?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)

    }
    
    func donePicker ()
    {
        self.filter = true
        self.locateMe = false
        
        if homeVal == false
        {
            selectStr = filterArr[0]
        }else
        {
            
        }
        print(selectStr)
        
        homeAdds ()
       
        
        self.filtersTF.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        self.filtersTF.resignFirstResponder()
    }

     override func viewWillAppear(_ animated: Bool) {
        
        
         self.title = languageChangeString(a_str: "Home")
        
            _ = self.tabBarController?.selectedIndex = 0
      
       categoryNameArr = [ languageChangeString(a_str: "Individual Professional"),languageChangeString(a_str: "Auto Repair Shop"), languageChangeString(a_str: "Scrap Shop"),languageChangeString(a_str: "Instant/Emergency") ] as! [String]
        
        if Reachability.isConnectedToNetwork() {

            isAuthorizedtoGetUserLocation()

        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
      
    }
  
    
    override func viewWillDisappear(_ animated: Bool) {
       
        self.gmapView1.clear()
    
    }
    
    func allowLocation()
    {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                //print("No access")
                let alertController = UIAlertController(title: languageChangeString(a_str: "Location Services Disabled!") , message: languageChangeString(a_str: "Please enable Location Based Services for better results! We promise to keep your location private"), preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: NSLocalizedString( languageChangeString(a_str: "Cancel")!, comment: ""), style: .cancel, handler: nil)
                let settingsAction = UIAlertAction(title: NSLocalizedString( languageChangeString(a_str: "Settings")!, comment: ""), style: .default) { (UIAlertAction) in
                    
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                    //UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
                }
                
                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
                
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            
            print("Location services are not enabled")
        }
    }
  
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            
            self.allowLocation()
    case .notDetermined:
            print("Location status not determined.")
            self.allowLocation()
            
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
            
        }
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }

  
    @objc func openSideMenu ()  {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func sidemenuClicked(_ sender: Any) {
        
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    
    }
    
    @IBAction func notificationClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let location = storyBoard.instantiateViewController(withIdentifier: "NotificationsController") as? NotificationsController
        self.navigationController?.pushViewController(location!, animated: true)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        
        self.pageControl.currentPage = page
        
    }
    
    func isAuthorizedtoGetUserLocation() {
       
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    
    func homeAdds ()
    {
        //http://voliveafrica.com/carfix/services/inner_adds?API-KEY=98745612 //user_id,latitude,longitude

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let lat1 = UserDefaults.standard.object(forKey: "lat1")
        let long1 = UserDefaults.standard.object(forKey: "long1")
        
        let vehicles = "\(Base_Url)inner_adds?"
       
        
        if filter == true {
        
            
            parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat1! , "longitude" : long1! , "lang" : language ?? "" ,"distance" : selectStr!]
        
        }else
        {
            parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : latitude! , "longitude" : longitude! , "lang" : language ?? ""]
        }
        
        print(vehicles)
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    // DispatchQueue.main.async {
                    
                    self.imagesArray1 = [String]()
                    self.roleArr = [String]()
                    self.linksArr = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.customerDataArr1 = responseData["adds"] as? [[String:AnyObject]]
                    
                    let pathStr = responseData["base_path"] as? String!
                    
                    //print("customer data is\(self.customerDataArr1!)")
                    
                    for eachitem in self.customerDataArr1! {
                        
                        let addImg = eachitem["add_image"] as? String!
                        
                        let imageString = pathStr!! + addImg!!
                        
                        let links = eachitem["url"] as? String!
                        
                        self.linksArr.append(links!!)
                        
                        self.imagesArray1.append(imageString)
                        
                    }
                        self.serviceProviders = responseData["service_providers"] as? [[String:AnyObject]]
                        
                        for eachitem in self.serviceProviders! {
                            
                            let pinImg = eachitem["shop_image"] as? String!
                             let idStr = eachitem["user_id"] as? String!
                            let images = pathStr!! + pinImg!!
                            
                            let lat = eachitem["latitude"] as? String!
                            let long = eachitem["longitude"] as? String!
                            //let rolestr = eachitem["role"] as? String!
                            //"auth_level" = 4;
                            
                           let auth_level = eachitem["auth_level"] as? String!
                           
                            let dLati = Double(lat! ?? "") ?? 0.0
                            let dLong = Double(long! ?? "") ?? 0.0
     
                            if auth_level == "2"
                            {
                                
                                DispatchQueue.main.async {
                                    
                                     let typeOnsite = eachitem["towing_ios"] as? String!
                                    let locationMarker1 :GMSMarker!
                                    let position1 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                                    locationMarker1 = GMSMarker(position: position1 )
                                    locationMarker1.map = self.gmapView1
                                    locationMarker1.appearAnimation =  .pop
                                    if(typeOnsite == "tow")
                                    {
                                      locationMarker1.icon = UIImage(named: "tow")?.withRenderingMode(.alwaysTemplate)
                                     locationMarker1.snippet = nil
                                        self.fillStr = "1"
                                        
                                    }else
                                    {
                                        locationMarker1.icon = UIImage(named: "individual professional")?.withRenderingMode(.alwaysTemplate)
                                         locationMarker1.snippet = idStr as! String
                                        locationMarker1.title = auth_level as! String
                                        self.fillStr = "2"
                                    }
                                    locationMarker1.opacity = 0.75
                                    locationMarker1.isFlat = true
                                   
                                }
                            }
                            else if auth_level == "3"
                            {
                                 DispatchQueue.main.async {
                                let locationMarker2 :GMSMarker!
                                let position2 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                                locationMarker2 = GMSMarker(position: position2 )
                                locationMarker2.map = self.gmapView1
                                locationMarker2.appearAnimation =  .pop
                                locationMarker2.icon = UIImage(named: "autoshop repair")?.withRenderingMode(.alwaysTemplate)
                                locationMarker2.opacity = 0.75
                                locationMarker2.isFlat = true
                               self.fillStr = "3"
                                    locationMarker2.snippet = idStr as! String
                                    locationMarker2.title = auth_level as! String
                                }
                            }
                            else if auth_level == "4"
                            {
                                 DispatchQueue.main.async {
                                let locationMarker3 :GMSMarker!
                                let position3 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                                locationMarker3 = GMSMarker(position: position3 )
                                locationMarker3.map = self.gmapView1
                                locationMarker3.appearAnimation =  .pop
                                locationMarker3.icon = UIImage(named: "scrap shops")?.withRenderingMode(.alwaysTemplate)
                                locationMarker3.opacity = 0.75
                                locationMarker3.isFlat = true
                                locationMarker3.snippet = idStr as! String
                                    locationMarker3.title = auth_level as! String
                                self.fillStr = "4"
                                    
                                }
                            }
                            
                            self.idsArr.append(idStr!!)
                           // self.roleArr.append(self.fillStr!)

                        }
                    //print("my role array is \(self.roleArr)")
                    
                    let dLati = Double(self.latitude ?? "") ?? 0.0
                    let dLong = Double(self.longitude ?? "") ?? 0.0
                    UserDefaults.standard.set(dLati, forKey: "lat1")
                    UserDefaults.standard.set(dLong, forKey: "long1")
                   
                    if self.locateMe == false
                    {
                       
                        let posit = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                        self.locationMark = GMSMarker(position: posit )
                        self.locationMark.map = self.gmapView1
                        self.locationMark.appearAnimation =  .pop
                        self.locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
                        self.locationMark.opacity = 0.75
                        self.locationMark.isFlat = true
                    }else
                    {
                        
                    }

                    DispatchQueue.main.async {
                         self.bannerCollectionView.reloadData()
                    }
                  
                    self.timer?.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 6.5, target: self, selector: #selector(HomeController.autoScrollImageSlider), userInfo: nil, repeats: true)
                    RunLoop.main.add(self.timer!, forMode: .commonModes)
                        
                    
                }
                else
                {
                    DispatchQueue.main.async {
                    Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                }
                }
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    
        print(marker.snippet as Any)
        
        
        if marker.snippet == nil
        {
            
        }else{
        
            if marker.snippet != nil{
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let shop = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
                if marker.title == "4"
                {
                     shop?.shopType = "4"
                }else
                {
                    shop?.shopType = "2"
                }
                shop?.vendorId = marker.snippet
                UserDefaults.standard.set("", forKey: "chatImgS")
                self.navigationController?.pushViewController(shop!, animated: true)
            
            }else
            {
            }
        }
        return true
    }
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
         let geocoder = GMSGeocoder()
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
       
        latitude = String(format: "%.8f", currentLocation.latitude)
        longitude = String(format: "%.8f",currentLocation.longitude)
        
        let latLang = "\(latitude), \(longitude)"
        UserDefaults.standard.set( latitude, forKey: "latitude")
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
        print("current lat and long \(latLang)")
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                
                //print("adress of that location is \(result!)")
                
                let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                
                self.addressOfuser = address?.strstr(needle: ",")
                //print(self.addressOfuser)
                
                UserDefaults.standard.set(self.addressOfuser, forKey: "address")
                
                let dLati = Double(self.latitude ?? "") ?? 0.0
                let dLong = Double(self.longitude ?? "") ?? 0.0

                UserDefaults.standard.set(dLati, forKey: "lat1")
                UserDefaults.standard.set(dLong, forKey: "long1")

//                if self.locateMe == false
//                {
//                    let posit = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
//                    self.locationMark = GMSMarker(position: posit )
//                    self.locationMark.map = self.gmapView1
//                    self.locationMark.appearAnimation =  .pop
//                    self.locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
//                    self.locationMark.opacity = 0.75
//                    self.locationMark.isFlat = true
//
//                }else
//                {
//                }
                
            }
        }
      
        DispatchQueue.main.async {
            self.gmapView?.camera = camera
            self.gmapView1?.camera = camera
        }
        self.gmapView?.animate(to: camera)
        self.gmapView1?.animate(to: camera)
        manager.stopUpdatingLocation()
        
        homeAdds ()
         _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(HomeController.sayHello), userInfo: nil, repeats: true)
        
    }
    func sayHello()
    {
        //http://voliveafrica.com/carfix/services/update_location
        let userID = UserDefaults.standard.object(forKey: "user_id")
        let details = "\(Base_Url)update_location"
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"latitude" : latitude! , "longitude" :longitude! ,"user_id" : userID ?? ""]
        print(details)
         print(parameters)
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                   print("Timer is calling.........suman guntuka")
                        
                    }
                }else
                {
                    print("Timer is Not calling.........suman guntuka")
                }
                
                
            }
        }
    }

    
    func setupLocationMarker() {
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
            
                   }
       
        print(self.serviceProviders)

    }
    
    func autoScrollImageSlider() {

            DispatchQueue.main.async {
                
                let visibleItems = self.bannerCollectionView.indexPathsForVisibleItems
               
                if visibleItems.count > 0 {
                   
                    var currentItemIndex: IndexPath? = visibleItems[0]
                    
                    if currentItemIndex?.item == self.imagesArray1.count - 1 {
                        let nexItem = IndexPath(item: 0, section: 0)
                       
                        self.bannerCollectionView.scrollToItem(at: nexItem, at: .centeredHorizontally, animated: true)
                    
                    } else {
                      
                        let nexItem = IndexPath(item: (currentItemIndex?.item ?? 0) + 1, section: 0)
                        
                        self.bannerCollectionView.scrollToItem(at: nexItem, at: .centeredHorizontally, animated: true)
                    }
                }
        }
    }

    @IBAction func filterShopsBnt(_ sender: Any) {
        
    }
    @IBAction func cuttentLocationBtn(_ sender: Any) {
     
        locateMe = true
        
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
       
    }
}
// sending the location to server

extension HomeController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bannerCollectionView {
            return self.imagesArray1.count
        }
        return self.imagesArray1.count
        
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
             let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? BannerCell

            cell?.bannerImg.sd_setImage(with: URL(string: self.imagesArray1[indexPath.row]), placeholderImage: UIImage(named:"placeholder"))
            return cell!
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

            let width : CGFloat =  (CGFloat) (self.bannerCollectionView.frame.size.width)

            return CGSize(width: width ,height: self.bannerCollectionView.frame.size.height)

        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "WebCalling") as? WebCalling
       
        detail?.linkStr = self.linksArr[indexPath.row]
        
        self.navigationController?.pushViewController(detail!, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == self.bannerCollectionView {
            
            self.pageControl.currentPage = indexPath.row
        }
    }
}

extension HomeController : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return filterArr.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return filterArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        homeVal = true
        
        selectStr = filterArr[row]
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        //names
        
        let temp = UILabel()
        temp.text = self.filterArr[row]
        temp.adjustsFontSizeToFitWidth = true
        temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
        //codes
        let aView = UIView()
        aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:30)
        aView.insertSubview(temp, at: 1)
        
        return aView
    }
    
}

extension HomeController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
         self.gmapView1.clear()
        
        self.filtersTF.inputView = filterPicker
        self.filtersTF.inputAccessoryView = toolbar
        
        return true
    }
}


