//
//  RegisterController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/6/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class RegisterController: UIViewController
{
    @IBOutlet weak var usernameText: UITextField!
    
    @IBOutlet var flagImg: UIImageView!
    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var countryPicker: UITextField!
    
    @IBOutlet weak var mobileNumberText: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var confirmText: UITextField!
    
    @IBOutlet weak var stateText: UITextField!
    
    @IBOutlet weak var cityText: UITextField!
    
    @IBOutlet weak var scroll: UIScrollView!
    
    var countyPickerView: UIPickerView?
    
    var toolbar: UIToolbar?
     var toolbar1: UIToolbar?
    var selectedTextField: UITextField?
    var countryPickerData = [String]()
    var countryCodeId = [String]()
    var countryFlogArr = [String]()
    var selectedStr : String!
    var countryIDStr : String!
    
    @IBOutlet weak var signup: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var emailid: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var password1: UILabel!
    @IBOutlet weak var confirmPW: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var pick: UIButton!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var alreadyRegi: UILabel!
    
    
    @IBOutlet var termsImg: UIImageView!
    @IBOutlet var termsAndCondition: UILabel!
    
    var termsBool : Bool!
    
//     var cities: [[String:AnyObject]]!
//     var states: [[String:AnyObject]]!
    
//    var cityArr = ["one", "two", "three", "seven", "fifteen"]
//    var stateArr = ["riyad", "riaz", "soudi", "seven", "fifteen"]
//    var cityPickerView = UIPickerView()
//    var statePickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsBool = false
        
        UserDefaults.standard.removeObject(forKey: "address")
        
        //language
        
        signup.text = languageChangeString(a_str: "Sign Up")
        username.text = languageChangeString(a_str: "USER NAME*")
        emailid.text = languageChangeString(a_str: "EMAIL ID*")
        mobileNo.text = languageChangeString(a_str: "MOBILE NUMBER*")
        password1.text = languageChangeString(a_str: "PASSWORD*")
        confirmPW.text = languageChangeString(a_str: "CONFIRM PASSWORD*")
        location.text = languageChangeString(a_str: "LOCATION*")
        alreadyRegi.text = languageChangeString(a_str: "Already Registered?")
        termsAndCondition.text = languageChangeString(a_str: "I agree to terms and conditions")
        
        pick.setTitle(languageChangeString(a_str: "PICK"), for: UIControlState.normal)
        submit.setTitle(languageChangeString(a_str: "SUBMIT"), for: UIControlState.normal)
        login.setTitle(languageChangeString(a_str: "Login"), for: UIControlState.normal)

        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: usernameText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: emailText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: countryPicker, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: mobileNumberText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: passwordText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: confirmText, lineColor: myColor, myView: self.view)
        //line .textfieldAsLine(myTextfield: cityText, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: stateText, lineColor: myColor, myView: self.view)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        if Reachability.isConnectedToNetwork()
        {
            countryCodeService ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
        
        countyPickerView = UIPickerView()
        countyPickerView?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (countyPickerView?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        countyPickerView?.delegate = self as UIPickerViewDelegate
        countyPickerView?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        self.addDoneButtonOnKeyboard()
        
    }
    
    func addDoneButtonOnKeyboard()
    {
        toolbar1 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar1?.barStyle = .blackOpaque
        toolbar1?.autoresizingMask = .flexibleWidth
        toolbar1?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelButtonAction))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar1?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar1?.setItems([ cancelButton , spaceButton, doneButton], animated: false)
        
        //self.text.inputAccessoryView = toolbar1
        usernameText.inputAccessoryView = toolbar1
        emailText.inputAccessoryView = toolbar1
        mobileNumberText.inputAccessoryView = toolbar1
        passwordText.inputAccessoryView = toolbar1
        confirmText.inputAccessoryView = toolbar1
        
    }
    
    func doneButtonAction()
    {
        usernameText.resignFirstResponder()
        emailText.resignFirstResponder()
        mobileNumberText.resignFirstResponder()
        passwordText.resignFirstResponder()
        confirmText.resignFirstResponder()
    }
    
    func cancelButtonAction()
    {
        usernameText.resignFirstResponder()
        emailText.resignFirstResponder()
        mobileNumberText.resignFirstResponder()
        passwordText.resignFirstResponder()
        confirmText.resignFirstResponder()
    }
    
    func donePicker ()
    {
        
       // self.countryPicker.text = selectedStr
        self.countryPicker.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        self.countryPicker.resignFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let address = UserDefaults.standard.object(forKey: "address") as? String
       
        self.stateText.text = address ?? ""
    
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        if usernameText.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter User Name")!)
        }
        
        else if countryPicker.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Pick Country Code")!)
        }
        else if mobileNumberText.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Mobile No")!)
            
        }else if passwordText.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Password")!)
            
        }else if confirmText.text != passwordText.text
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Confirm Password")!)
        }
        else if stateText.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Pick Location")!)
        }else if termsBool == false
        {
             self.showToastForAlert(message: languageChangeString(a_str: "Please Accept Terms And Conditions")!)
        
        }else
        {
            if Reachability.isConnectedToNetwork(){
                 registerService ()
            }else
            {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
            }
        }
    }
    @IBAction func termsBtn(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let login = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditions") as? TermsAndConditions
        self.navigationController?.pushViewController(login!, animated: true)
        
    }
   
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    func registerService ()
    {
        // ,,,,,,country,city,state//country,address,pincode
        //http://voliveafrica.com/carfix/services/registration
        
        let vehicles = "\(Base_Url)registration"
        Services.sharedInstance.loader(view: self.view)
 
        let tokenStr = UserDefaults.standard.string(forKey: "deviceToken")
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
        let city = UserDefaults.standard.object(forKey: "city")
        let state = UserDefaults.standard.object(forKey: "state")
        let address = UserDefaults.standard.object(forKey: "address")
        let country = UserDefaults.standard.object(forKey: "country")
        let pincode = UserDefaults.standard.object(forKey: "pincode")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        //user_name,email,password,mobile,device_type,device_token,API-KEY,city,state
        
        if self.countryIDStr == nil
        {
            self.countryIDStr = countryCodeId[0]
        }
        
        let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "user_name":self.usernameText.text!,  "email":self.emailText.text!, "mobile":self.mobileNumberText.text!,"country_code" :self.countryPicker.text!, "password":self.passwordText.text!, "device_type" :"iOS" , "device_token":tokenStr!, "latitude" :lat ?? "" , "longitude" : long ?? "" , "city" : city ?? "", "state" : state ?? "" ,"country" : country ?? "" ,"pincode" : pincode ?? "" ,"address" : address ?? "" , "lang" : language ?? "", "countrycode" : self.countryIDStr!]
       
        print(parameters)
       
        //ServiceCell.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                //ServiceCell.sharedInstance.dissMissLoader()
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
                    self.navigationController?.pushViewController(login!, animated: true)
                    }
                }
                else
                {
                     DispatchQueue.main.async {
                   
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                    
                    }
                }
            }
        }
    }
   
    func countryCodeService ()
    {
        
        //http://voliveafrica.com/carfix/services/countries?API-KEY=98745612
        
        let vehicles = "\(Base_Url)countries?"
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? ""]
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                
                if status == 1
                {
                   
                    let servicesCat = responseData["countries"] as? [[String:Any]]
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let codeStr = servicesCat["phonecode"]  as! String
                        
                        let flags = servicesCat["country_flag"]  as! String
                         let countryId = servicesCat["id"]  as! String
                        
                        let flogsImg = base_path + flags
                        
                        self.countryFlogArr.append(flogsImg)
                        self.countryCodeId.append(countryId)
                        self.countryPickerData.append(codeStr)
                        
                    }
                    DispatchQueue.main.async {
                         Services.sharedInstance.dissMissLoader()
                        self.flagImg.image=UIImage(named: "saudi")
                    }
                    print(self.countryPickerData)
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }

    @IBAction func locationBtn(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "LocationController")
        self .present(service, animated: true, completion: nil)
    }
    
    @IBAction func termsAndConditionBtn(_ sender: Any) {
        
        termsBool = true
        termsImg.image = UIImage(named:"check")!

    }
    
}

extension RegisterController: UITextFieldDelegate{
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        usernameText.resignFirstResponder()
        emailText.resignFirstResponder()
        countryPicker.resignFirstResponder()
        passwordText.resignFirstResponder()
        confirmText.resignFirstResponder()
       // cityText.resignFirstResponder()
        stateText.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
        
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue: Bool = false
        if textField == usernameText {
            
            emailText.becomeFirstResponder()
            returnValue = true
            
        }
        else if textField == emailText{
            
            countryPicker.becomeFirstResponder()
            returnValue = true
        }
        else if textField == mobileNumberText{
            passwordText.becomeFirstResponder()
            returnValue = true
        }
        else if textField == passwordText{
            confirmText.resignFirstResponder()
            returnValue = true
        }
        else if textField == confirmText{
            //cityText.resignFirstResponder()
            returnValue = true
        }
        else if textField == stateText{
           // stateText.resignFirstResponder()
            returnValue = true
        }
  
        return returnValue
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        countryPicker.inputView = countyPickerView
        countryPicker.inputAccessoryView = toolbar
        
        //   countryCode.text = selectedTextField
        
        return true
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == usernameText {
            scroll.contentOffset.y = +20
        }
        else if textField == emailText{
            scroll.contentOffset.y = +60
        }else if textField == countryPicker{
            scroll.contentOffset.y = +100
        }
        else if textField == mobileNumberText{
            scroll.contentOffset.y = +100
        }
        else if textField == passwordText{
            scroll.contentOffset.y = +140
        }
        else if textField == confirmText{
            scroll.contentOffset.y = +160
        }else if textField == stateText{
            scroll.contentOffset.y = +220
        }
//        else if textField == cityText{
//            scroll.contentOffset.y = +280
//        }
    }
}

extension RegisterController : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return countryPickerData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return countryPickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == countyPickerView
        {
            countryPicker.text = self.countryPickerData[row]
            self.countryIDStr = self.countryCodeId[row]
            self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        }
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        //names
        let temp = UILabel()
        let img = UIImageView()
        img.frame = CGRect(x: self.view.frame.origin.x+100 ,y: 0, width: 30,height: 25)
        
        temp.text = self.countryPickerData[row]
        
        self.countryIDStr = self.countryCodeId[row]
        
        self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        img.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:""))
        
        temp.adjustsFontSizeToFitWidth = true
        temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
        //codes
        let aView = UIView()
        aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:40)
        aView.insertSubview(temp, at: 1)
        aView.insertSubview(img, at: 1)
        
        return aView
    }
    
}

