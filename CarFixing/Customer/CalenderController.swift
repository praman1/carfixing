//
//  CalenderController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 28/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import CVCalendar

    class CalenderController: UIViewController {
        
        struct Color {
            static let selectedText = UIColor.white
            static let text = UIColor.black
            static let textDisabled = UIColor.gray
            static let selectionBackground = UIColor.darkGray
            static let sundayText = UIColor(red: 1.0, green: 0.2, blue: 0.2, alpha: 1.0)
            static let sundayTextDisabled = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
            static let sundaySelectionBackground = sundayText
        }
        
        //for change lang
       
        @IBOutlet weak var menuView: CVCalendarMenuView!
        
        @IBOutlet weak var CVCalendarView: CVCalendarView!
        
        @IBOutlet weak var monthLabel: UILabel!
        
        @IBOutlet weak var dateCollectionView: UICollectionView!
        
        @IBOutlet weak var timeCollectionView: UICollectionView!
        
        var days = [String]()
        var nameOfDay = [String]()
        
        var month: String!
        var dateStr : String!
        
        var toPayAmount: Int!
        var billTotalAmount: Int!
        var couponDiscountAmount: String!
        var DeliveryCharge: String!
        
        var vendorID: String!
        
        var cartIds: Array<String>! = []
        var giftIds: Array<String>! = []
        
        var payMentType: String!
        
        var currentCalendar: Calendar?
        var selectedDay:DayView!
        
        var shouldShowDaysOut = true
        var animationFinished = true
        
        var deliveryDate: String!
        var deliveryTime: String!
        
        @IBOutlet weak var selectDate: UIButton!
        override func awakeFromNib() {
            let timeZoneBias = 480 // (UTC+08:00)
            currentCalendar = Calendar.init(identifier: .gregorian)
            currentCalendar?.locale = Locale(identifier: "fr_FR")
            if let timeZone = TimeZone.init(secondsFromGMT: -timeZoneBias * 60) {
                currentCalendar?.timeZone = timeZone
            }
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
            
            //for lang change
            
            
            self.title = languageChangeString(a_str: "Available Calender")
            selectDate.setTitle(languageChangeString(a_str: "SELECT BOOKING DATE"), for: UIControlState.normal)
            
            let height = CGFloat(84)
            self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: height)
            
            let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
            backButton.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = backButton
            self.title = "Available Calendar"
            let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
            self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
            self.navigationItem.title = languageChangeString(a_str: "Available Calender")
            
            if let currentCalendar = currentCalendar {
                self.monthLabel.text = CVDate(date: Date(), calendar: currentCalendar).globalDescription
            }
            
            let cal = Calendar.current
            let date = cal.startOfDay(for: Date())
            
            let dateFormatterForDate = DateFormatter()
            dateFormatterForDate.dateFormat = "dd"
            
            let dateFormatterForDayName = DateFormatter()
            dateFormatterForDayName.dateFormat = "E"
            
            for i in 1 ... 30 {
                let newdate = cal.date(byAdding: .day, value: +i, to: date)!
                let str = dateFormatterForDate.string(from: newdate)
                days.append(str)
                
                let dayName = dateFormatterForDayName.string(from: newdate)
                nameOfDay.append(dayName)
            }
            
        }
        
        func gobackFromInvoice()
        {
            
           self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func nextViewBtn(_ sender: Any) {
            
            CVCalendarView.loadNextView()
        }
        
        @IBAction func previousViewBtn(_ sender: Any) {
            
            CVCalendarView.loadPreviousView()
        }
        
        @IBAction func selectBookBtn(_ sender: Any) {
           
        UserDefaults.standard.set(self.dateStr, forKey: "date")
            
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dateData"), object: nil)
            
    self.navigationController?.popViewController(animated: true)
            
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let calender = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
//            self.navigationController?.pushViewController(calender!, animated: true)
            
            
        }
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            
            menuView.commitMenuViewUpdate()
            CVCalendarView.commitCalendarViewUpdate()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override var prefersStatusBarHidden: Bool {
            
            return true
            
        }
        
        @IBAction func backBtn(_ sender: Any) {
            
            self.navigationController?.popViewController(animated: true)
        }
        
}
    
    extension CalenderController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
        
        /// Required method to implement!
        func presentationMode() -> CalendarMode {
            return .monthView
        }
        
        /// Required method to implement!
        func firstWeekday() -> Weekday {
            return .sunday
        }
        
        
        func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
            selectedDay = dayView
            
            self.deliveryDate = selectedDay.date.commonDescription
           // print("selected date is \(selectedDay.date.commonDescription)")
            
            self.dateStr = selectedDay.date.commonDescription
            
            //print("final date string is \(self.dateStr)")
            
        }
        
        func presentedDateUpdated(_ date: CVDate) {
            
            if monthLabel.text != date.globalDescription && self.animationFinished {
                let updatedMonthLabel = UILabel()
                updatedMonthLabel.textColor = monthLabel.textColor
                updatedMonthLabel.font = monthLabel.font
                updatedMonthLabel.textAlignment = .center
                updatedMonthLabel.text = date.globalDescription
                updatedMonthLabel.sizeToFit()
                updatedMonthLabel.alpha = 0
                updatedMonthLabel.center = self.monthLabel.center
                
                let offset = CGFloat(48)
                updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
                updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                
                UIView.animate(withDuration: 0.35, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    self.animationFinished = false
                    self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
                    self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                    self.monthLabel.alpha = 0
                    
                    updatedMonthLabel.alpha = 1
                    updatedMonthLabel.transform = CGAffineTransform.identity
                    
                }) { _ in
                    
                    self.animationFinished = true
                    self.monthLabel.frame = updatedMonthLabel.frame
                    self.monthLabel.text = updatedMonthLabel.text
                    self.monthLabel.transform = CGAffineTransform.identity
                    self.monthLabel.alpha = 1
                    updatedMonthLabel.removeFromSuperview()
                }
                
                self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
            }
        }
        
        
        func disableScrollingBeforeDate() -> Date {
            return Date()
        }
        
        func maxSelectableRange() -> Int {
            return 1
        }
        
        func earliestSelectableDate() -> Date {
            return Date()
        }
        
        
    }
    
    extension CalenderController {
        func toggleMonthViewWithMonthOffset(offset: Int) {
            guard let currentCalendar = currentCalendar else {
                return
            }
            
            var components = Manager.componentsForDate(Foundation.Date(), calendar: currentCalendar) // from today
            
            components.month! += offset
            
            let resultDate = currentCalendar.date(from: components)!
            
            self.CVCalendarView.toggleViewWithDate(resultDate)
        }
        
        
        func didShowNextMonthView(_ date: Date) {
            guard let currentCalendar = currentCalendar else {
                return
            }
            
            let components = Manager.componentsForDate(date, calendar: currentCalendar) // from today
            
            print("Showing Month: \(components.month!)")
        }
        
        
        func didShowPreviousMonthView(_ date: Date) {
            guard let currentCalendar = currentCalendar else {
                return
            }
            
            let components = Manager.componentsForDate(date, calendar: currentCalendar) // from today
            
            print("Showing Month: \(components.month!)")
        }
        
    }




