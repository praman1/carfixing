//
//  AutoRepairController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 24/04/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import GoogleMaps
import GooglePlaces
import Alamofire

class AutoRepairController: UIViewController ,CLLocationManagerDelegate,UITabBarControllerDelegate, GMSMapViewDelegate
{
    @IBOutlet weak var filterTF: UITextField!
    @IBOutlet weak var gmapView1: GMSMapView!
    @IBOutlet weak var listBtnOutlet: UIButton!
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
    var gmapView: GMSMapView!
    var latitude : String!
    var longitude : String!
    
    var customerDataArr1: [[String:AnyObject]]!
    var imagesArray1 = [String]()
    var pinsImgArr = [String]()
    var latArr = [Double]()
    var longArr = [Double]()
    var idsArr = [String]()
    var lat1 : Double!
    var long1 : Double!
    var appDelegate : AppDelegate!
      var serviceProviders : [[String:AnyObject]]!
    
    var filterArr = [String]()
    var selectStr : String!
    var filterPicker: UIPickerView?
    
    var toolbar: UIToolbar?
    var selectedTextField: UITextField?
    
    var parameters: Dictionary<String, Any>!
    
    var filter : Bool!
    var locateMe : Bool!
    
    
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locateMe = false
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
       
        
        gmapView?.delegate = self
        gmapView1.delegate = self
        self.title = languageChangeString(a_str: "Auto Repair Shop")
        
        tabBarController?.tabBar.items?[0].title = languageChangeString(a_str: "Home")
        tabBarController?.tabBar.items?[1].title = languageChangeString(a_str: "Individual Professional")
        tabBarController?.tabBar.items?[2].title = languageChangeString(a_str: "Auto Repair")
        tabBarController?.tabBar.items?[3].title = languageChangeString(a_str: "Scrap")
        tabBarController?.tabBar.items?[4].title = languageChangeString(a_str: "Towing")
        
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
//        listBtnOutlet.layer.cornerRadius = listBtnOutlet.frame.size.height/2
//        listBtnOutlet.layer.borderWidth = 2
//        listBtnOutlet.layer.borderColor = #colorLiteral(red: 0.9570153356, green: 0.6725911498, blue: 0.00519436691, alpha: 1)
    
        if Reachability.isConnectedToNetwork() {
            
            isAuthorizedtoGetUserLocation()
            
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
     

        
        NotificationCenter.default.addObserver(self, selector: #selector(indivialNotifyCall), name: NSNotification.Name(rawValue:"serviceSP"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelNotifyCall), name: NSNotification.Name(rawValue:"cancelCall"), object: nil)
        
    }
    
    func indivialNotifyCall()
    {
       
        filterTF.resignFirstResponder()
       
        autolListCall ()
        
    }
    func cancelNotifyCall()
    {
        autolListCall ()
    }

    override func viewWillAppear(_ animated: Bool) {
       
       // _ = self.tabBarController?.selectedIndex = 2
        self.tabBarController?.selectedIndex = 2
        self.tabBarController!.delegate = self
        
       filter = false
        UserDefaults.standard.set("3", forKey: "ServiceType")
        autolListCall ()
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
      
      
        UserDefaults.standard.removeObject(forKey: "saveS")
    }

   
    @IBAction func sideMenuBtn(_ sender: Any) {
        
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
       
    }
    
    func isAuthorizedtoGetUserLocation() {
        
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print(marker.snippet as Any)
        
        if marker.snippet == nil
        {
            
        }else{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let shop = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
            
            shop?.vendorId = marker.snippet
            
            //  UserDefaults.standard.set(marker.snippet, forKey: "user_id")
            
            self.navigationController?.pushViewController(shop!, animated: true)
            
        }
        
        return true
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
        let geocoder = GMSGeocoder()
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
        
        latitude = String(format: "%.8f", currentLocation.latitude)
        longitude = String(format: "%.8f",currentLocation.longitude)
        
        let latLang = "\(latitude), \(longitude)"
        UserDefaults.standard.set( latitude, forKey: "latitude")
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
        print("current lat and long \(latLang)")
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                
                //print("adress of that location is \(result!)")
                
                let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                
                // self.addressOfuser = address?.strstr(needle: ",")
                //print(self.addressOfuser)
                
                //UserDefaults.standard.set(self.addressOfuser, forKey: "address")
                
                let dLati = Double(self.latitude ?? "") ?? 0.0
                let dLong = Double(self.longitude ?? "") ?? 0.0
                
                if self.locateMe == false
            {
                let locationMark :GMSMarker!
                let posit = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                locationMark = GMSMarker(position: posit )
                locationMark.map = self.gmapView1
                locationMark.appearAnimation =  .pop
                locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
                locationMark.opacity = 0.75
                locationMark.isFlat = true
                }else
                {
                    
                }

                
            }
            
            //self.autolListCall ()
        }
        
        DispatchQueue.main.async {
            self.gmapView?.camera = camera
            self.gmapView1?.camera = camera
        }
        self.gmapView?.animate(to: camera)
        self.gmapView1?.animate(to: camera)
        manager.stopUpdatingLocation()
        
    }
    
    @IBAction func filterBtn(_ sender: Any) {
        
         self.gmapView1.clear()
        
        filter = true
        UserDefaults.standard.set("3", forKey: "notify")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "ServiceSelection")
        self .present(service, animated: true, completion: nil)
        
    }
    func setupLocationMarker() {
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
            
        }
        
    }
    
    @IBAction func locateMeBtn(_ sender: Any) {
        
        locateMe = true
        
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
    }
    func autolListCall ()
    {
        //http://voliveafrica.com/carfix/services/inner_adds?API-KEY=98745612 //user_id,latitude,longitude
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        let services = UserDefaults.standard.object(forKey: "idsString")
        let lat1 = UserDefaults.standard.object(forKey: "lat1")
        let long1 = UserDefaults.standard.object(forKey: "long1")
        print(lat1 ,long1)
        
        let vehicles = "\(Base_Url)vendors_list"
        
        if filter == true {
        
        parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat1! , "longitude" : long1! , "lang" : language ?? "" , "service_filter" : services ?? "" , "vendor_type" : "3" ]
            
        }else
        {
               parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat1! , "longitude" : long1! , "lang" : language ?? "" , "vendor_type" : "3" ]
        }
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    // DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.customerDataArr1 = responseData["adds"] as? [[String:AnyObject]]
                    
                    let pathStr = responseData["baase_path"] as? String!
                    
                    self.serviceProviders = responseData["vendors_list"] as? [[String:AnyObject]]
                    
                    if self.serviceProviders.count > 0{
                    
                    for eachitem in self.serviceProviders! {
                        
                        let pinImg = eachitem["shop_image"] as? String!
                        let idStr = eachitem["user_id"] as? String!
                        let images = pathStr!! + pinImg!!
                        
                        let lat = eachitem["latitude"] as? String!
                        let long = eachitem["longitude"] as? String!
                        //"auth_level" = 4;
                        
                        let auth_level = eachitem["auth_level"] as? String!
                        
                        let dLati = Double(lat! ?? "") ?? 0.0
                        let dLong = Double(long! ?? "") ?? 0.0
                        
//                        if auth_level == "3"
//                        {
                            let locationMarker2 :GMSMarker!
                            let position2 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                            locationMarker2 = GMSMarker(position: position2 )
                            locationMarker2.map = self.gmapView1
                            locationMarker2.appearAnimation =  .pop
                            locationMarker2.icon = UIImage(named: "autoshop repair")?.withRenderingMode(.alwaysTemplate)
                            locationMarker2.opacity = 0.75
                            locationMarker2.isFlat = true
                            
                        locationMarker2.snippet = idStr as! String
                        //}
                        
                        self.idsArr.append(idStr!!)
                        
                    }
                    
                    let dLati = Double(self.latitude ?? "") ?? 0.0
                    let dLong = Double(self.longitude ?? "") ?? 0.0
                    UserDefaults.standard.set(dLati, forKey: "lat1")
                    UserDefaults.standard.set(dLong, forKey: "long1")
                    
                    if self.locateMe == false
                    {
                        let locationMark :GMSMarker!
                        let posit = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                        locationMark = GMSMarker(position: posit )
                        locationMark.map = self.gmapView1
                        locationMark.appearAnimation =  .pop
                        locationMark.icon = UIImage(named: "pin")?.withRenderingMode(.alwaysTemplate)
                        locationMark.opacity = 0.75
                        locationMark.isFlat = true

                    }else
                    {
                    }
                        
                    }else
                    {
                        Services.sharedInstance.dissMissLoader()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    }
                }
            }
        }
    }
    
    @IBAction func listBtn(_ sender: Any) {
     
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let list = storyBoard.instantiateViewController(withIdentifier: "ListController") as? ListController
                        list?.myStr = "second"
                        list?.vendor_TypeStr = "3"
                        appDelegate.typeStr = "3"
        self.navigationController?.pushViewController(list!, animated: true)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "AutoRepairController")
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
}

extension AutoRepairController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        

//        filter = true
//
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let service = storyBoard.instantiateViewController(withIdentifier: "ServiceSelection")
//        self .present(service, animated: true, completion: nil)
        
        return true
    }
}

