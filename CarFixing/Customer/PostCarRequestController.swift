//
//  PostCarRequestController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/13/17.
//  Copyright © 2017 volivesolutions. All rights reserved.

import UIKit
import Photos
import BSImagePicker
import Alamofire

class PostCarRequestController: UIViewController {

    @IBOutlet weak var imgCollection: UICollectionView!
   // @IBOutlet weak var titleTF: UITextField!
    @IBOutlet var titleTF: UITextView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var carDetailsTxtView: UITextView!
    @IBOutlet weak var locationTf: UITextField!
    @IBOutlet weak var makeTf: UITextField!
    @IBOutlet weak var modelTf: UITextField!
    @IBOutlet weak var sliderPriceLbl: UILabel!
    @IBOutlet weak var submit: UIButton!
    var checkString = NSString()
    var vendorID = NSString()
    var pickedImagsArr = [String]()
    var imageStr : UIImage!
    var assert : PHAsset!
    var myArray: [Any] = []
    var width : CGFloat!
    
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var collectionHieght: NSLayoutConstraint!
    
    @IBOutlet weak var postCarSellingReq: UILabel!
    @IBOutlet weak var uploadImg: UILabel!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var priceRange: UILabel!
     var toolbar1: UIToolbar?
    var yearBool : Bool!
   
    
    var datePickerView  : UIDatePicker = UIDatePicker()
    
    //var bidImgsArr: [Any] = []
    
    var bidImgsArr  = [UIImage]()
    
    var priceStr : String!
    var upload : Bool!
    var locationBool : Bool!
    var priceBool : Bool!
    var appde : AppDelegate!
    var toolbar: UIToolbar?
    
    @IBOutlet weak var priceSlider: UISlider!
    
    @IBOutlet weak var uploadImageLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("check string valuye and vendor id ",checkString,vendorID)
        upload = false
        locationBool = false
        priceBool = false
        yearBool = false
        
        collectionHieght.constant = 0
        
        appde = UIApplication.shared.delegate as! AppDelegate
        
        UserDefaults.standard.removeObject(forKey: "address")
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Post Request")
        
        title1.text = languageChangeString(a_str: "Title")
        postCarSellingReq.text = languageChangeString(a_str: "POST CAR SELLING REQUEST")
        location.text = languageChangeString(a_str: "LOCATION")
        carDetails.text = languageChangeString(a_str: "CAR DETAILS")
        uploadImg.text = languageChangeString(a_str: "Upload Image")
        make.text = languageChangeString(a_str: "MAKE")
        model.text = languageChangeString(a_str: "MODEL")
        priceRange.text = languageChangeString(a_str: "Price Range")
        submit.setTitle(languageChangeString(a_str: "SUBMIT"), for: UIControlState.normal)
        
        //biddingPrice1.text = languageChangeString(a_str: "Bid Amount")
        
        let line = LineTextField()
        let color = #colorLiteral(red: 0.8322482705, green: 0.8322482705, blue: 0.8322482705, alpha: 1)
        line .textfieldAsLine(myTextfield: locationTf, lineColor: color, myView: self.view)
        line .textfieldAsLine(myTextfield: makeTf, lineColor: color, myView: self.view)
        line .textfieldAsLine(myTextfield: modelTf, lineColor: color, myView: self.view)
        //line .textfieldAsLine(myTextfield: titleTF, lineColor: color, myView: self.view)
        
        carDetailsTxtView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        carDetailsTxtView.layer.borderWidth = 1.0
        carDetailsTxtView.layer.cornerRadius = 5.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textViewCall))
        self.view.addGestureRecognizer(tapGesture)
        addDoneButtonOnKeyboard()
        
    }
    func textViewCall()
    {
        DispatchQueue.main.async {
            self.titleTF.resignFirstResponder()
        self.carDetailsTxtView.resignFirstResponder()
        self.locationTf.resignFirstResponder()
        self.makeTf.resignFirstResponder()
        self.modelTf.resignFirstResponder()
        
      }
    }
    func addDoneButtonOnKeyboard()
    {
        toolbar1 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar1?.barStyle = .blackOpaque
        toolbar1?.autoresizingMask = .flexibleWidth
        toolbar1?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelButtonAction))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar1?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar1?.setItems([ cancelButton , spaceButton, doneButton], animated: false)
        
        //self.text.inputAccessoryView = toolbar1
        titleTF.inputAccessoryView = toolbar1
        carDetailsTxtView.inputAccessoryView = toolbar1
        makeTf.inputAccessoryView = toolbar1
        modelTf.inputAccessoryView = toolbar1
        
        
    }
    
    func doneButtonAction()
    {
        titleTF.resignFirstResponder()
        carDetailsTxtView.resignFirstResponder()
        makeTf.resignFirstResponder()
        modelTf.resignFirstResponder()
       
    }
    
    func cancelButtonAction()
    {
        titleTF.resignFirstResponder()
        carDetailsTxtView.resignFirstResponder()
        makeTf.resignFirstResponder()
        modelTf.resignFirstResponder()
    }
    
    @IBAction func makeText(_ sender: UITextField) {
        
        
//        datePickerView.datePickerMode = UIDatePickerMode.date
//        makeTf.inputView = datePickerView
//        datePickerView.addTarget(self, action: #selector(PostCarRequestController.handleDatePicker), for: UIControlEvents.valueChanged)
        
    }
//    func handleDatePicker() {
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy"
//
//        makeTf.text = dateFormatter.string(from: datePickerView.date)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let address = UserDefaults.standard.object(forKey: "address") as? String
        self.locationTf.text = address ?? ""
        
    }
    @IBAction func locationBtn(_ sender: Any) {
        locationBool = true
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "LocationController")
        self .present(service, animated: true, completion: nil)
        
        
    }
    @IBAction func priceSlider(_ sender: Any) {
        
        priceBool = true
        
        let currentValue = Int(self.priceSlider.value)
        
        print("Slider changing to \(currentValue)")
        self.priceStr = String(currentValue)
        
        self.sliderPriceLbl.text = self.priceStr
        
    }
    @IBAction func uploadImgBtn(_ sender: Any) {
        
        self.upload = true
        let vc = BSImagePickerViewController()
            vc.maxNumberOfSelections = 100
    self.bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            
            DispatchQueue.main.async {
                
                            let photoAsset = asset
                            let manager = PHImageManager.default()
                            var options: PHImageRequestOptions?
                                options = PHImageRequestOptions()
                            options?.resizeMode = .exact
                                options?.isSynchronous = true
                            manager.requestImage(
                                                for: photoAsset,
                                                targetSize: PHImageManagerMaximumSize,
                                                contentMode: .aspectFill,
                                                options: options
                                            ) { [weak self] result, _ in
//                                                print("my images are ******* \(result!)")
//                                                let imgData = UIImageJPEGRepresentation(result!, 0.2)!
//                                                print(imgData)
                                    
                                    self?.myArray.append(result!)
                                    print(self?.myArray as Any)
                                                
                                   self?.bidImgsArr.append(result!)
                                                
                                self?.collectionHieght.constant = 90
                                                
                                self?.imgCollection.reloadData()
                                      
                                                }
                                                }
                                            
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
           
            
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            print("Finish: \(assets)")
            
            //  print("Selected: \(asset)")
            DispatchQueue.main.async {
            
                self.uploadImageLbl.text = "Car Images Are Uploaded"
                self.uploadImageLbl.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
            }
            
        }, completion: nil)
            
        
        
    }
    @objc func backBtnClicked (){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        
        if upload == false
        {
            self.showToastForAlert(message: "Upload Images")
            
        } else if locationBool == false
        {
            self.showToastForAlert(message: "Please Select Location")
            
        }else if self.carDetailsTxtView.text == ""
        {
            self.showToastForAlert(message: "Please Enter Car Details")
            
        }else if self.makeTf.text == ""
        {
            self.showToastForAlert(message: "Please Enter Make")
            
        }else if self.modelTf.text == ""
        {
            self.showToastForAlert(message: "Please Enter Model Name")
            
        }
//        else if  priceBool == false
//        {
//           self.showToastForAlert(message: "Please Select Price")
//        }
        else
        {
            
            if Reachability.isConnectedToNetwork()
            {
                 self.addBidServiceCall()
            }else
            {
                showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            }
           
        }
    }
    
    
    func addBidServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/add_bid///user_id,title,description,make,model,price,bid_id, vehicle_image[] array

        Services.sharedInstance.loader(view: self.view)
        
        let vendor_id :String?
        
        if checkString == "1"{
            vendor_id = vendorID as String
        }else{
            vendor_id = ""
        }
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let address = UserDefaults.standard.object(forKey: "address") as? String
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let make = self.makeTf.text
        let model = self.modelTf.text
        let title = self.titleTF.text
        let textViewText = self.carDetailsTxtView.text
        
        //let parameters: Dictionary<String, Any> = [ "user_id": userid ?? "" , "title" : title! , "description" : textViewText! , "API-KEY" : APIKEY , "make" : make! , "model" : model! , "price" : self.priceStr! ,"address" : address ?? "" , "lang" : language ?? ""]
        
        let parameters: Dictionary<String, Any> = [ "user_id": userid ?? "" ,"vendor_id":vendor_id ?? "", "title" : title! , "description" : textViewText! , "API-KEY" : APIKEY , "make" : make! , "model" : model! ,"address" : address ?? "" , "lang" : language ?? "","price" : "10"]
    
        
        print("params of post request",parameters);
        Alamofire.upload(multipartFormData: { multipartFormData in
             //import image to request
            
            for (key, value) in parameters {
               
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
                for i in 0..<self.myArray.count {
                    
                    let imageData = UIImageJPEGRepresentation(self.myArray[i] as! UIImage, 0.2)!
                
                    multipartFormData.append(imageData , withName: "vehicle_image[]", fileName: "file.jpg", mimeType: "image/jpeg")
                    
                    print("image file",imageData as Any)
                }
           
            print(parameters)
            
        }
            
            ,  to:"\(Base_Url)add_bid")
        { (result) in
            
            print("my result is\(result)")
            
          switch result {
            
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    
                    print("Upload Progress: \(progress.fractionCompleted)")
                
                })
                
                upload.responseJSON { response in
                    
                    let responseData = response.result.value as? Dictionary<String, Any>
                    
                    print(responseData as Any)
                    
                    let status = responseData!["status"] as! Int
                    let message = responseData!["message"] as? String
                    let error = responseData!["error"] as? String
                    
                    if status == 1
                    {
                        DispatchQueue.main.async {
                            
                        self.showToast(message: message!)
                        Services.sharedInstance.dissMissLoader()
                            
//                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                            let homeVC = storyboard.instantiateViewController(withIdentifier: "BidsController")
//                            self.navigationController?.pushViewController(homeVC, animated: true)
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let list = storyBoard.instantiateViewController(withIdentifier: "BidsController2") as? BidsController
                            self.navigationController?.pushViewController(list!, animated: true)
                            
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            
                            self.showToast(message: message!)
                            
                        Services.sharedInstance.dissMissLoader()
                            
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                
                DispatchQueue.main.async {
                Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}
extension PostCarRequestController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return self.bidImgsArr.count
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath) as! BidsCollectCell
        
        cell.uploadImg.image = self.bidImgsArr[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        width = CGFloat(collectionView.frame.size.width / 3)
        
        return CGSize(width: width, height: 95)
        
    }
}

extension PostCarRequestController : UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        var returnValue : Bool = false
        
         if textField == self.makeTf{
            
            returnValue = true
        }
        else if textField == self.modelTf{
            
            returnValue = true
        }
        
        return returnValue
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
         if textField == self.makeTf{
            scroll.contentOffset.y = +330
            
            let expiryDatePicker = MonthYearPickerView()
            
            self.makeTf.inputView = expiryDatePicker
            
            expiryDatePicker.onDateSelected = { ( year: Int) in
                let string1 = String(format: "%d", year)
                print(string1)
              self.makeTf.text = string1
            
           }
        }
            
        else if textField == self.modelTf{
            scroll.contentOffset.y =  +330
        }
        
    }
    public func textFieldDidEndEditing(_ textField: UITextField){
    
        let textStr = self.makeTf.text
       
        if textStr == "" {
            self.makeTf.text = "2018"
        }else
        {
            
        }
        
        self.makeTf.resignFirstResponder()
        self.modelTf.resignFirstResponder()
       
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }
}
extension PostCarRequestController : UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == self.carDetailsTxtView{
           
            scroll.contentOffset.y = +280
        
    }
    }
    public func textViewDidEndEditing(_ textView: UITextView){
            
            scroll.contentOffset.y = 0
   
    }
}

