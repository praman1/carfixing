//
//  ViewDetailsBidController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 16/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.

import UIKit

class ViewDetailsBidController: UIViewController {

    @IBOutlet weak var bidImg: UIImageView!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    var shopImgStr : String!
    var shopNameStr : String!
    var msgStr : String!
    var priceStr : String!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var shopname: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removeView))
//        self.view.addGestureRecognizer(tapGesture)
        
        print(shopImgStr,shopNameStr,msgStr,priceStr)
        shopNameStr = UserDefaults.standard.object(forKey: "shopName") as! String
        msgStr = UserDefaults.standard.object(forKey: "msg") as! String
        priceStr = UserDefaults.standard.object(forKey: "price") as! String
        shopImgStr = UserDefaults.standard.object(forKey: "shopImage") as! String
        
        shopNameLbl.text = shopNameStr
        messageLbl.text = msgStr
        priceLbl.text = priceStr
        bidImg.sd_setImage(with: URL(string: shopImgStr), placeholderImage: UIImage(named:"as img4"))
        
    }
//    func removeView()
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
