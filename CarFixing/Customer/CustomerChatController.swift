//
//  CustomerChatController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 09/04/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import SideMenu

class CustomerChatController: UIViewController {
    
    var chatNamesArr = [String]()
    var textMsgArr = [String]()
    var senderIdArr = [String]()
    var recieverIdArr = [String]()
    var imageArr = [String]()
    var refreshControl = UIRefreshControl ()
    var chatsData: [[String:AnyObject]]!

    @IBOutlet weak var chat_Table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Chats")

        // Do any additional setup after loading the view.
        
//        tabBarController?.tabBar.items?[0].title = languageChangeString(a_str: "Home")
//        tabBarController?.tabBar.items?[1].title = languageChangeString(a_str: "Bids")
//        tabBarController?.tabBar.items?[2].title = languageChangeString(a_str: "Jobs")
//        tabBarController?.tabBar.items?[3].title = languageChangeString(a_str: "Chats")
//        tabBarController?.tabBar.items?[4].title = languageChangeString(a_str: "Profile")
        
       
        
        if Reachability.isConnectedToNetwork()
        {
             vendorChatingCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
       
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string:"")
        refreshControl.tintColor=#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        refreshControl.addTarget(self, action: #selector(ChatController.refresh), for: UIControlEvents.valueChanged)
        chat_Table.addSubview(refreshControl) // not required when using UITableViewController
    }

    func refresh()
    {
        
        refreshControl.endRefreshing()
        self.chat_Table.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    @objc func backBtnClicked (){
       
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func vendorChatingCall()
    {
        // http://voliveafrica.com/carfix/services/vendor_chats?API-KEY=98745612&vender_id=22
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
   
        let gallery = "\(Base_Url)vendor_chats?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vender_id" : userid ?? "","lang" : language ?? ""]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(gallery, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                  
                        
                        Services.sharedInstance.dissMissLoader()
                        
                        self.chatsData = responseData["chats"] as? [[String:AnyObject]]
                        
                        print("chats data is\(self.chatsData!)")
                        
                        for eachitem in self.chatsData! {
                            
                            let message = eachitem["message"] as? String!
                            let userName = eachitem["user_name"] as? String!
                            let sender_id = eachitem["sender_id"] as? String!
                            let receiver_id = eachitem["receiver_id"] as? String!
                            let imageStr = eachitem["image"] as? String!
                            
                            let image = base_path + imageStr!!
                            self.imageArr.append(image)
                            self.textMsgArr.append(message!!)
                            self.chatNamesArr.append(userName!!)
                            self.senderIdArr.append(sender_id!!)
                            self.recieverIdArr.append(receiver_id!!)
                            
                        }
                        print(self.senderIdArr)
                        print(self.recieverIdArr)
                        print("my array are \(self.textMsgArr ,  self.chatNamesArr , self.senderIdArr , self.recieverIdArr)")
                        
                        if self.textMsgArr.count == 0
                        {
                            self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                        }
                         DispatchQueue.main.async {
                        self.chat_Table.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension CustomerChatController: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatNamesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = chat_Table.dequeueReusableCell(withIdentifier: "chatsCell", for: indexPath) as! chatsCell
        cell.nameLbl.text = self.chatNamesArr[indexPath.row]
        cell.msgLbl.text = self.textMsgArr[indexPath.row]
        
        cell.chatImg.layer.cornerRadius = cell.chatImg.frame.size.height/2
        cell.chatImg.clipsToBounds = true
        cell.chatImg.sd_setImage(with: URL(string: self.imageArr[indexPath.row]), placeholderImage: UIImage(named:"profile pic"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let chat = storyBoard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
        
        let senderstr =  self.senderIdArr[indexPath.row]
        let recistr =  self.recieverIdArr[indexPath.row]
        
        chat?.profileImg = self.imageArr[indexPath.row]
        
        chat?.vendorIdS = senderstr
        chat?.senderStr = senderstr
        chat?.recieverStr = recistr
        
        self.navigationController?.pushViewController(chat!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 89
    }
    
}



