//
//  EmergencyController.swift
//  CarFixing
//
//  Created by Suman Volive on 2/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SideMenu

class EmergencyController: UIViewController ,CLLocationManagerDelegate,UITabBarControllerDelegate, GMSMapViewDelegate{
   
    @IBOutlet weak var filterTF: UITextField!
    @IBOutlet weak var gmapView: GMSMapView!
    var appDelegate : AppDelegate!
    
    var locationMarker1 :GMSMarker!
    var locationMarker2 :GMSMarker!
    var locationMarker3 :GMSMarker!
    
    @IBOutlet weak var bookInstantlyBtn: UIButton!
    var serviceProviders : [[String:AnyObject]]!
    var latitude1 : String!
    var longitude1 : String!
    var idStr : String!
    
     @IBOutlet weak var locateBtn: UIButton!
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
    
    
    var filterArr = [String]()
    var selectStr : String!
    var filterPicker: UIPickerView?
    
    var toolbar: UIToolbar?
    var selectedTextField: UITextField?
    
    var parameters: Dictionary<String, Any>!
    
    var filter : Bool!
    var locateMe : Bool!
    
    let lblNew = UILabel()
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locateMe = false
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        print(appDelegate.typeStr)
        
     
        // Do any additional setup after loading the view.
        self.gmapView.addSubview(bookInstantlyBtn)
       // self.gmapView.addSubview(locateBtn)
        gmapView?.delegate = self
        
       // self.tabBarController?.tabBar.isHidden = true
    
        filterArr = ["5","10","15","20"]
        filter = false
        filterPicker = UIPickerView()
        filterPicker?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (filterPicker?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        filterPicker?.delegate = self as UIPickerViewDelegate
        filterPicker?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EmergencyController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EmergencyController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        bookInstantlyBtn.setTitle(languageChangeString(a_str: "BOOK INSTANTLY"), for: UIControlState.normal)
        
        
       
        
    }
    func donePicker ()
    {
        filter = true
        
        homeAdds ()
        
        self.filterTF.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        self.filterTF.resignFirstResponder()
    }
        
        
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController!.delegate = self
        //_ = self.tabBarController?.selectedIndex = 4
        self.tabBarController?.selectedIndex = 4
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
       
    }
    
    
    @IBAction func locateMeBtn(_ sender: Any) {
       
        locateMe = true
        
        if Reachability.isConnectedToNetwork() {
            isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
        
    }
    
    @IBAction func bookInstantBtn(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "TowingAlert")
        self .present(service, animated: true, completion: nil)
        
    }
    
    @IBAction func sideMenuBtn(_ sender: Any) {
       
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
    
    func isAuthorizedtoGetUserLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func wrapperFunctionToShowPosition(mapView:GMSMapView){
        
        let geocoder = GMSGeocoder()
        let latitud = mapView.camera.target.latitude
        let longitud = mapView.camera.target.longitude
        
        let latitude = String(format: "%.8f", mapView.camera.target.latitude)
        let longitude = String(format: "%.8f", mapView.camera.target.longitude)
        
        let position = CLLocationCoordinate2DMake( latitud, longitud)
        
        let latLang = "\(latitude), \(longitude)"
        
        UserDefaults.standard.set( latitude, forKey: "latitude")
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
//        self.latitude1 = latitude
//        self.longitude1 = longitude
        
        print(self.latitude1,self.longitude1)
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
               /// let result = response?.results()?.first
                //print("adress of that location is \(result!)")
              //  let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                
            }
        }
    }
    
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("didchange")
        
        //called everytime
        wrapperFunctionToShowPosition(mapView: mapView)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print(marker.snippet as Any)
        
        if marker.snippet == nil
        {
            
        }else{
            
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let shop = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
//            UserDefaults.standard.set("", forKey: "chatImgS")
//            shop?.vendorId = marker.snippet
//            self.navigationController?.pushViewController(shop!, animated: true)
            
        }
        
        return true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        print("idleAt")
        
        wrapperFunctionToShowPosition(mapView: mapView)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
        self.setupLocationMarker(coordinate: position)
        
        DispatchQueue.main.async {
            
            self.gmapView.camera = camera
            
        }
        
        self.gmapView?.animate(to: camera)
        manager.stopUpdatingLocation()
        
        homeAdds ()
    }
    
    func setupLocationMarker(coordinate: CLLocationCoordinate2D) {
        
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
        }
        
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = gmapView
        locationMarker.appearAnimation =  .pop
        locationMarker.icon = UIImage(named: "pin")!.withRenderingMode(.alwaysTemplate)
        locationMarker.opacity = 0.75
        locationMarker.isFlat = true
        
    }
    
    func homeAdds ()
    {
        //http://voliveafrica.com/carfix/services/inner_adds?API-KEY=98745612 //user_id,latitude,longitude
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let vehicles = "\(Base_Url)towers"
        
        if filter == true {
        
       parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat ?? "" , "longitude" : long ?? "" , "lang" : language ?? "" ,"distance" : selectStr!]
        }else
        {
            parameters = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "latitude" : lat ?? "" , "longitude" : long ?? "" , "lang" : language ?? "" ]
        }

        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    // DispatchQueue.main.async {
                    
                    Services.sharedInstance.dissMissLoader()
                  
                    self.serviceProviders = responseData["service_providers"] as? [[String:AnyObject]]
                     if self.serviceProviders.count > 0{
                    
                    for eachitem in self.serviceProviders! {
                        
                        let lat = eachitem["latitude"] as? String!
                        let long = eachitem["longitude"] as? String!
                        
                        self.idStr = eachitem["user_id"] as? String!
                        //"auth_level" = 4;
                        
                        let auth_level = eachitem["auth_level"] as? String!
                        let roleStr = eachitem["role"] as? String!
                        
                        let dLati = Double(lat! ?? "") ?? 0.0
                        let dLong = Double(long! ?? "") ?? 0.0
                        
//                        if auth_level == "2"
//                        {
                        
                        let position1 = CLLocationCoordinate2D(latitude: dLati , longitude: dLong )
                            self.locationMarker1 = GMSMarker(position: position1 )
                            self.locationMarker1.map = self.gmapView
                            self.locationMarker1.appearAnimation =  .pop
                            self.locationMarker1.icon = UIImage(named: "tow")?.withRenderingMode(.alwaysTemplate)
                            self.locationMarker1.opacity = 0.75
                            self.locationMarker1.isFlat = true
                            self.locationMarker1.snippet = self.idStr
                       
                            print(self.idStr)

                    }
                     }else{
                        Services.sharedInstance.dissMissLoader()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                    }
                }
            }
        }
    }

    @IBAction func listBtn(_ sender: Any) {
        
    }
    
    // UITabBarControllerDelegate
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
       
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "EmergencyController")
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    
}

extension EmergencyController : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return filterArr.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return filterArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectStr = filterArr[row]
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        //names
        let temp = UILabel()
        temp.text = self.filterArr[row]
        temp.adjustsFontSizeToFitWidth = true
        temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
        //codes
        let aView = UIView()
        aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:30)
        aView.insertSubview(temp, at: 1)
        
        return aView
    }
}

extension EmergencyController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.gmapView.clear()
        filterTF.inputView = filterPicker
        filterTF.inputAccessoryView = toolbar
        
        return true
    }
}


