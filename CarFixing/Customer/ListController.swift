//
//  ListController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/8/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ListController: UIViewController {

    @IBOutlet weak var navigate: UINavigationBar!
    var appDelegate : AppDelegate!
    
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var titlesLbl: UILabel!
    @IBOutlet weak var listTbl: UITableView!
    var myStr : String!
    var vendorId : String!
    //arrays
    var shopNameArr = [String]()
    var addressArr = [String]()
    var imgArr = [String]()
    var serviceNameArr = [String]()
    var listTimeArr = [String]()
   var vendorIdArr = [String]()
    var maxPrice : String!
    var minPrice : String!
    var timingsData : [[String : AnyObject]]!
    var userCountArr = [String]()
    
    var ratingArr = [String]()
    
    var  myRating : String!
    var imageStr : String!
    
    var rateString : String!
//    var arrayOfInts: [Int] = [1, 2, 3]

//    var vendorIdArr = [Int]()
    
  //  var ratingArr : Array<String> = []
//   var listPriceArr : Array<String> = []
   
   // var ratingArr = [Any]()
   var listPriceArr = [Any]()
    
    var vendor_TypeStr : String!
    
    var customerData: [[String:AnyObject]]!
    var imagesArray1 = [String]()
    var priceArr = [String]()
   
   
    var filterPrice = [String: String]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        //custom data
//        shopNameArr = ["Castrol Services","Idemistu Services","Fasttrack Services","Engine Services"]
//        addressArr = ["Olaya Street,Riyadh","King Saud Road,Riyadh","Olaya Street,Riyadh","King Saud Road,Riyadh"]
//        serviceNameArr = ["Transmission & Engine Repairs","Fluid replacements","Shocks,Struts and suspensions","Clutch & Gear Repairs"]
//        listTimeArr = ["7AM - 10PM","8AM - 11PM","9AM - 12PM","10AM - 12PM"]
//        ratingArr = ["4.5","4.0","3.7","3.5"]
//        listPriceArr = ["$199.99","$189.99","$299.99","$159.99"]
//    imgArr = ["as img1","as img2","as img3","as img4"]
        
        if Reachability.isConnectedToNetwork()
        {
            vendorListServiceCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
        if vendor_TypeStr == "2"
        {
            self.titlesLbl.text = languageChangeString(a_str: "Individual Professional")
            
        }else if vendor_TypeStr == "3"
        {
             self.titlesLbl.text = languageChangeString(a_str: "Auto Repair Shop")
        }else
        {
             self.titlesLbl.text = languageChangeString(a_str: "Scrap Shop")
        }
        
         mapBtn.layer.cornerRadius = mapBtn.frame.size.height/2
        
       // NotificationCenter.defaultCenter.addObserver(self, selector: #selector(ListController.methodOfReceivedNotification(_:)), name:"NotificationIdentifier", object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(serviceFiltCall), name: NSNotification.Name(rawValue:"NofilterCall"), object: nil)
        
    }
    
    func serviceFiltCall()
    {
        if Reachability.isConnectedToNetwork()
        {
            vendorListServiceCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
    }
    
    func showSpinningWheel(_ notification: NSNotification) {
        
        let parameters: Dictionary<String, Any> = notification.userInfo as! Dictionary<String, Any>
       
        print(parameters)
        
        let filters = "\(Base_Url)vendors_list"
        
                Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(filters, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                    UserDefaults.standard.removeObject(forKey: "bandIdsString")
                    UserDefaults.standard.removeObject(forKey: "filterids")

                    self.listPriceArr = [String]()
                    self.shopNameArr = [String]()
                   self.addressArr = [String]()
                   self.imgArr = [String]()
                   self.serviceNameArr = [String]()
                    self.listTimeArr = [String]()
                    self.vendorIdArr = [String]()
                    self.userCountArr = [String]()
                    self.ratingArr = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.customerData = responseData["vendors_list"] as? [[String:AnyObject]]
                    
                    //                        let vendor = responseData["user_id"] as? Int!
                    //                        UserDefaults.standard.set(vendor!, forKey: "vendorId")
                    //                        print(vendor!)
                    //  self.filterPriceArr = responseData["price_range"]as? [[String:AnyObject]]
                    
                    self.filterPrice = (responseData["price_range"] as? [String: String])!
                    
                    print(self.filterPrice)
                    
                    self.maxPrice = self.filterPrice["maximum"]
                    self.minPrice = self.filterPrice["minimum"]
                    
                    UserDefaults.standard.set(self.maxPrice, forKey: "max")
                    UserDefaults.standard.set(self.minPrice, forKey: "min")
                    
                    print(self.maxPrice,self.minPrice)
                    
                    for eachitem in self.customerData! {
                 
                        var addImg = eachitem["shop_image"] as? String!
                        
                        if addImg == ""
                        {
                            addImg = "profile pic"
                        }

                        let price = eachitem["price"]
                        
                       // let totalPrice
                        
                        let shopName = eachitem["shop_name"] as? String!
                        let serviceName = eachitem ["service_name_en"] as? String!
                        
//                        if self.vendor_TypeStr == "4"
//                        {
//                            self.myRating = ""
//                        }else
//                        {
//                            let rating = eachitem ["rating"]
//                            let x : Float = rating as! Float
//                            self.myRating = String(x)
                      //  }
                        
                        let state = eachitem["state"] as? String!
                        let  city = eachitem ["city"] as? String!
                        let address = state!! + "," + city!!
                        
                        let vendor = eachitem["user_id"] as? String!
                        
                        let rateStr = eachitem["rating"] as? String!
                        
                         let noOfCount = eachitem["usercount"] as? String!
                        
                        print(vendor ?? "")
                        
                        self.shopNameArr.append(shopName!!)
                        
                        let imagestr = base_path + addImg!!
                        
                        self.imgArr.append(imagestr)
                        
                        self.timingsData = eachitem["timings"] as? [[String:AnyObject]]
                        print(self.timingsData)
                        
                        for i in self.timingsData
                        {
                            
                            let fromTime = i["from_time"] as? String!
                            let toTime = i["to_time"] as? String!
                            
                            let timeString = fromTime!! + " - " + toTime!!
                            self.listTimeArr.append(timeString)
                            
                        }
                        
                        if self.vendor_TypeStr == "4"
                        {
                            let emtpystr = ""
                            self.serviceNameArr.append(emtpystr)
                            let priceStr = ""
                            self.listPriceArr.append(priceStr)
                            self.vendorIdArr.append(vendor!!)
                            
                            self.ratingArr.append(rateStr!!)
                            let countStr = ""
                            self.userCountArr.append(countStr)
                            
                            //self.ratingArr.append(sts)
                            
                        }else
                        {
                            self.serviceNameArr.append(serviceName!!)
                            self.listPriceArr.append(price!)
                           // self.ratingArr.append(self.myRating)
                            self.vendorIdArr.append(vendor!!)
                            self.userCountArr.append(noOfCount!!)
                            self.ratingArr.append(rateStr!!)
                            
                        }
                        
                        self.addressArr.append(address)
                        
                    }
                    
                    self.listTbl.reloadData()
                  
                }
                else
                {
                    self.showToast(message: message!!)
                }
            }
        }
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    @IBAction func filterBtnClicked(_ sender: Any) {
        
        if appDelegate.typeStr == "2"{
            
            UserDefaults.standard.set("2", forKey: "ServiceT")
            
        }else if appDelegate.typeStr == "3"
        {
             UserDefaults.standard.set("3", forKey: "ServiceT")
        
        }else if appDelegate.typeStr == "4"
        {
            UserDefaults.standard.set("4", forKey: "ServiceT")
        }
         
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let filter = storyBoard.instantiateViewController(withIdentifier: "UserFiltersController")
        
        UserDefaults.standard.set(self.vendor_TypeStr, forKey: "vendorType")
   
        self .present(filter, animated: false, completion: nil)
    
    
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
      
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func MapBtn(_ sender: Any) {
        
        if appDelegate.typeStr == "2"{
           
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let service = storyBoard.instantiateViewController(withIdentifier: "TabController")
//            self .present(service, animated: true, completion: nil)
        
        }else if appDelegate.typeStr == "3"
        {
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let service = storyBoard.instantiateViewController(withIdentifier: "TabController")
//            self .present(service, animated: true, completion: nil)
        
        }else if appDelegate.typeStr == "4"
        {
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: true)

        }
    
    }
    func vendorListServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/vendors_list
         let userid = UserDefaults.standard.object(forKey: "user_id")
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
            let vehicles = "\(Base_Url)vendors_list"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "user_id" : userid ?? "" , "vendor_type" : vendor_TypeStr! , "latitude" : lat ?? "" , "longitude" : long ?? "" , "lang" : language ?? ""]
        
            Services.sharedInstance.loader(view: self.view)
        print(parameters)
            
            Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                if let responseData = response.result.value as? Dictionary<String, Any>{
                    print(responseData)
                    
                    let status = responseData["status"] as? Int!
                    
                    let message = responseData["message"] as? String!
                    if status == 1
                    {
                        Services.sharedInstance.dissMissLoader()
                        
                        self.listPriceArr = [String]()
                        self.shopNameArr = [String]()
                        self.addressArr = [String]()
                        self.imgArr = [String]()
                        self.serviceNameArr = [String]()
                        self.listTimeArr = [String]()
                        self.vendorIdArr = [String]()
                        self.userCountArr = [String]()
                        self.ratingArr = [String]()
  
                        self.customerData = responseData["vendors_list"] as? [[String:AnyObject]]

                        self.filterPrice = (responseData["price_range"] as? [String: String])!
                        print(self.filterPrice)
                        self.maxPrice = self.filterPrice["maximum"]
                        self.minPrice = self.filterPrice["minimum"]
                        
                        UserDefaults.standard.set(self.maxPrice, forKey: "max")
                        UserDefaults.standard.set(self.minPrice, forKey: "min")
                        
                        print(self.maxPrice,self.minPrice)
                        
                        for eachitem in self.customerData! {
                        
                        var addImg = eachitem["shop_image"] as? String!
                       // print(addImg)
                        
                        if addImg == ""
                        {
                            addImg = "as img1"
                        }
                        
                        let price = eachitem["price"]
                        let shopName = eachitem["shop_name"] as? String!
                        let serviceName = eachitem ["service_name_en"] as? String!
                            
                        
                        
                        let state = eachitem["state"] as? String!
                        let  city = eachitem["city"] as? String!
                            let address = city!! + "," + state!!
                        let vendor = eachitem["user_id"] as? String!
                            
                        let rateStr = eachitem["rating"] as? String!
                        
                            print(rateStr)
                            
                        let noOfCount = eachitem["usercount"] as? String!
                         
                            self.shopNameArr.append(shopName!!)
                            
                            let imagePic = base_path + addImg!!
                            
                        self.timingsData = eachitem["timings"] as? [[String:AnyObject]]
                         
                            for i in self.timingsData
                          {
                            let fromTime = i["from_time"] as? String!
                            let toTime = i["to_time"] as? String!
                            let timeString = fromTime!! + " - " + toTime!!
                            self.listTimeArr.append(timeString)
                        }
                            
                        self.imgArr.append(imagePic)
                       
                        if self.vendor_TypeStr == "4"
                        {
                            let emtpystr = ""
                            self.serviceNameArr.append(emtpystr)
                            let priceStr = ""
                            self.listPriceArr.append(priceStr)
                            self.vendorIdArr.append(vendor!!)
                            self.myRating = ""
                            
                        }else
                        {
                            self.serviceNameArr.append(serviceName!!)
                            self.listPriceArr.append(price!)
                            self.vendorIdArr.append(vendor!!)
//                            self.userCountArr.append(noOfCount!)
//                            self.ratingArr.append(rateStr!)
                            
                        }
                            self.ratingArr.append(rateStr!!)
                            self.userCountArr.append(noOfCount!!)
                            self.addressArr.append(address)
                        
                        }
                        
                        print(self.vendorIdArr)
                        print(self.imgArr)
                        print(self.ratingArr)
                        print(self.userCountArr)
                        
                        self.listTbl.reloadData()
                    }
                    else
                    {
                        self.showToast(message: message!!)
                    }
                }
            }
    }
}
extension ListController: UITableViewDelegate, UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = listTbl.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListCell
        
        cell.listNameLbl.text = shopNameArr[indexPath.row]
        
        cell.listImg.sd_setImage(with: URL(string: self.imgArr[indexPath.row]), placeholderImage: UIImage(named:"profile pic"))
         imageStr = self.imgArr[indexPath.row]
        
        UserDefaults.standard.set(imageStr, forKey: "chatImgS")
        
        cell.listCatLbl.text = serviceNameArr[indexPath.row]
        cell.listAddrLbl.text = addressArr[indexPath.row]
        cell.listTimeLbl.text = listTimeArr[indexPath.row]
        cell.startingFrom.text = languageChangeString(a_str: "Starting from")
     
        
            cell.noOfRateLbl.text = String(format: "%@%@%@", "(", userCountArr[indexPath.row] , ")")
            rateString = ratingArr[indexPath.row]
            let rate : Int = Int(rateString)!
            cell.rateView.rating =  Double(rate)
      
        cell.listPriceBtn.text = listPriceArr[indexPath.row] as? String
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 135
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "ShopsDetailsController") as? ShopsDetailsController
        detail?.vendorId = self.vendorIdArr[indexPath.row]
        //detail?.ratingStr = self.ratingArr[indexPath.row] as! String
        detail?.shopType = self.vendor_TypeStr
           // detail?.detailStr = myStr
        self.navigationController?.pushViewController(detail!, animated: true)
        
    }
}
