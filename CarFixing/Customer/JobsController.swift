//
//  JobsController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/13/17.
//  Copyright © 2017 volivesolutions. All rights reserved.

import UIKit
import SideMenu
import Alamofire

class JobsController: UIViewController {
    
    
    var strArr = [String]()
    @IBOutlet weak var jobsTbl: UITableView!
    @IBOutlet weak var filterBtn: UIButton!
    
    var allJobsData : [[String:AnyObject]]!
    var jobNameArr = [String]()
    var jobDateArr = [String]()
    var jobIdsArr = [String]()
    var jobIdStr : String!
    var sepStr : String!
    
    var appde = AppDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appde = UIApplication.shared.delegate as! AppDelegate
        
        filterBtn.layer.cornerRadius = 4
        filterBtn.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        filterBtn.layer.borderWidth = 1.2
        
        if Reachability.isConnectedToNetwork()
        {
            if appde.onsiteSepStr == "onsite"
            {
                self.title = languageChangeString(a_str: "Onsite Request")
                onsiteJobsServiceCall()
                
            }else
            {
                self.title = languageChangeString(a_str: "Automobile Repair Requests")
                
                self.allJobsServiceCall()
            }
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteBtn(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() {
            
            jobDeleteCall ()
            
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
    }
    @IBAction func editBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.jobsTbl)
        let indexPath1: IndexPath? = self.jobsTbl.indexPathForRow(at: btnPosition)
        
        appde.jobStr = "edit"
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailJob = storyBoard.instantiateViewController(withIdentifier: "UserJobDetailController") as?
        UserJobDetailController
        
        detailJob?.jobidStr = self.jobIdsArr[(indexPath1?.row)!]
        
        self.navigationController?.pushViewController(detailJob!, animated: true)
        
    }
    
    @IBAction func viewDetailsBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.jobsTbl)
        let indexPath1: IndexPath? = self.jobsTbl.indexPathForRow(at: btnPosition)
        
        appde.jobStr = "detail"
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailJob = storyBoard.instantiateViewController(withIdentifier: "UserJobDetailController") as?
        UserJobDetailController
        
        detailJob?.jobidStr = self.jobIdsArr[(indexPath1?.row)!]
        
        self.navigationController?.pushViewController(detailJob!, animated: true)
        
    }
    @IBAction func backClicked(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
    func onsiteJobsServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/user_onsite_jobs?API-KEY=98745612&user_id=282
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        
        let details = "\(Base_Url)user_onsite_jobs?API-KEY=\(APIKEY)&user_id=\(userid ?? "")&lang=\(language ?? "")"
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.jobNameArr = [String]()
                    self.jobDateArr = [String]()
                    
                    self.allJobsData = responseData["jobs"] as? [[String:AnyObject]]
                    
                    for eachjob in self.allJobsData!
                    {
                        
                        let jobNameStr = eachjob["shop_name"] as? String!
                        let jobDateStr = eachjob["booked_date"] as? String!
                        // let vendor
                        
                        self.jobIdStr = eachjob["job_id"] as? String
                        
                        self.jobNameArr.append(jobNameStr!!)
                        self.jobDateArr.append(jobDateStr!!)
                        self.jobIdsArr.append(self.jobIdStr!)
                        
                    }
                    if self.jobNameArr.count == 0
                    {
                        self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                    }
                    
                    self.jobsTbl.reloadData()
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func allJobsServiceCall()
    {
        
        //http://voliveafrica.com/carfix/services/user_jobs?API-KEY=98745612&user_id=2
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        
        let details = "\(Base_Url)user_jobs?API-KEY=\(APIKEY)&user_id=\(userid ?? "")&lang=\(language ?? "")"
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.jobNameArr = [String]()
                    self.jobDateArr = [String]()
                    
                    self.allJobsData = responseData["jobs"] as? [[String:AnyObject]]
                    
                    for eachjob in self.allJobsData!
                    {
                        
                        let jobNameStr = eachjob["shop_name"] as? String!
                        let jobDateStr = eachjob["booked_date"] as? String!
                        // let vendor
                        
                        self.jobIdStr = eachjob["job_id"] as? String
                        
                        self.jobNameArr.append(jobNameStr!!)
                        self.jobDateArr.append(jobDateStr!!)
                        self.jobIdsArr.append(self.jobIdStr!)
                        
                    }
                    if self.jobNameArr.count == 0
                    {
                        self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                    }
                    
                    self.jobsTbl.reloadData()
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func jobDeleteCall ()
    {
        
        //http://voliveafrica.com/carfix/services/delete_job?API-KEY=98745612&job_id=1
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let details = "\(Base_Url)delete_job?API-KEY=\(APIKEY)&job_id=\(self.jobIdStr!)&lang=\(language ?? "")"
        
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToast(message: message!!)
                    
                    self.jobsTbl.reloadData()
                    
                    self.allJobsServiceCall()
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
}
extension JobsController: UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = jobsTbl.dequeueReusableCell(withIdentifier: "cell") as! JobsCell
        
        cell.nameLbl.text = self.jobNameArr[indexPath.row]
        cell.dateLbl.text = self.jobDateArr[indexPath.row]
        
        cell.postedOn.text = languageChangeString(a_str: "Posted on:")
        cell.edit.text = languageChangeString(a_str: "Edit")
        cell.delete.text = languageChangeString(a_str: "Delete")
        cell.viewDetails.setTitle(languageChangeString(a_str: "View Details"), for: UIControlState.normal)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //        let detailJob = storyBoard.instantiateViewController(withIdentifier: "DetailJobController") as? DetailJobController
        //        self.navigationController?.pushViewController(detailJob!, animated: true)
    }
    
}
