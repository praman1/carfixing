//
//  EditBidController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 16/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import Photos
import BSImagePicker

class EditBidController: UIViewController {

    @IBOutlet var titleTF: UITextView!
    @IBOutlet weak var uploadLbl: UILabel!
    @IBOutlet weak var bidImgCollection: UICollectionView!
    //@IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var makeTF: UITextField!
    @IBOutlet weak var modelTF: UITextField!
    @IBOutlet weak var carDetailsTxtview: UITextView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceSlider: UISlider!
     @IBOutlet weak var scroll: UIScrollView!
   
    
    @IBOutlet weak var uploadBtn: UIButton!
    var pickedImagsArr = [String]()
    var imageStr : UIImage!
    var assert : PHAsset!
    var myArray: [Any] = []
    var bidImgsArr: [Any] = []
    var bidImgsArr1  = [UIImage]()
    
    var bidStr : String!
    var priceStr : String!
    var locationBool : Bool!
    var upload : Bool!
    var price : Bool!
    var width : CGFloat!
    var appde : AppDelegate!
    var datePickerView  : UIDatePicker = UIDatePicker()
    
    
    @IBOutlet weak var editCarSelling: UILabel!
    
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var update: UIButton!
    @IBOutlet weak var priceRange: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var carDetails: UILabel!
     var toolbar1: UIToolbar?
    override func viewDidLoad() {
        super.viewDidLoad()
        
       appde = UIApplication.shared.delegate as! AppDelegate
       
        locationBool = false
        upload = false
        price = false
        
//        let price1:Int? = Int(priceStr)
//        let maxPriceS = Float(price1!)
//        self.priceSlider.value = maxPriceS
        
        title1.text = languageChangeString(a_str: "Title")
        editCarSelling.text = languageChangeString(a_str: "POST CAR SELLING REQUEST")
        location.text = languageChangeString(a_str: "LOCATION")
        carDetails.text = languageChangeString(a_str: "CAR DETAILS")
        uploadLbl.text = languageChangeString(a_str: "Upload Image")
        make.text = languageChangeString(a_str: "MAKE")
        model.text = languageChangeString(a_str: "MODEL")
        priceRange.text = languageChangeString(a_str: "Price Range")
        
        uploadBtn.setTitle(languageChangeString(a_str: "SUBMIT"), for: UIControlState.normal)
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Edit Request")
        
        
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        
        self.bidDetailsCall ()

        let line = LineTextField()
        let color = #colorLiteral(red: 0.8322482705, green: 0.8322482705, blue: 0.8322482705, alpha: 1)
        line .textfieldAsLine(myTextfield: locationTF, lineColor: color, myView: self.view)
        line .textfieldAsLine(myTextfield: makeTF, lineColor: color, myView: self.view)
        line .textfieldAsLine(myTextfield: modelTF, lineColor: color, myView: self.view)
        //line .textfieldAsLine(myTextfield: titleTF, lineColor: color, myView: self.view)
        
        carDetailsTxtview.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        carDetailsTxtview.layer.borderWidth = 1.0
        carDetailsTxtview.layer.cornerRadius = 5.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textViewCall))
        self.view.addGestureRecognizer(tapGesture)
        
        addDoneButtonOnKeyboard()
        
    }
    func addDoneButtonOnKeyboard()
    {
        toolbar1 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar1?.barStyle = .blackOpaque
        toolbar1?.autoresizingMask = .flexibleWidth
        toolbar1?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelButtonAction))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar1?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar1?.setItems([ cancelButton , spaceButton, doneButton], animated: false)
        
        //self.text.inputAccessoryView = toolbar1
        titleTF.inputAccessoryView = toolbar1
        carDetailsTxtview.inputAccessoryView = toolbar1
        makeTF.inputAccessoryView = toolbar1
        modelTF.inputAccessoryView = toolbar1
        
        
    }
    
    func doneButtonAction()
    {
        titleTF.resignFirstResponder()
        carDetailsTxtview.resignFirstResponder()
        makeTF.resignFirstResponder()
        modelTF.resignFirstResponder()
        
    }
    
    func cancelButtonAction()
    {
        titleTF.resignFirstResponder()
        carDetailsTxtview.resignFirstResponder()
        makeTF.resignFirstResponder()
        modelTF.resignFirstResponder()
    }
    func textViewCall()
    {
        DispatchQueue.main.async {
            
            self.carDetailsTxtview.resignFirstResponder()
            self.locationTF.resignFirstResponder()
            self.makeTF.resignFirstResponder()
            self.modelTF.resignFirstResponder()
            self.titleTF.resignFirstResponder()
            
        }
    }
    func backBtnClicked ()
    {
    
        self.navigationController?.popViewController(animated: true)
    
    }
    
    func bidDetailsCall ()
    {
        
        //http://voliveafrica.com/carfix/services/bid_details?API-KEY=98745612&bid_id=2
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let details = "\(Base_Url)bid_details?API-KEY=\(APIKEY)&bid_id=\(self.bidStr!)&lang=\(language ?? "")"
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                   
                    Services.sharedInstance.dissMissLoader()
                    
                    let bidsImgsData = responseData["bid_images"] as? [[String:AnyObject]]
                    
                    for i in bidsImgsData!
                    {
                        let bidImg = i["image"] as? String!
                        let image = base_path + bidImg!!
                        
                        self.bidImgsArr.append(image)
                        
                    }
                    
                    if let bid_detailsData = responseData["bid_details"] as? Dictionary<String, AnyObject> {
                        
                        let location = bid_detailsData["address"] as? String!
                        let carDetails = bid_detailsData["description"] as? String!
                        let make = bid_detailsData["make"] as? String!
                        let model = bid_detailsData["model"] as? String!
                        let price = bid_detailsData["price"] as? String!
                        let title1 = bid_detailsData["title"] as? String!
                        
                        self.locationTF.text = location as! String
                        self.carDetailsTxtview.text = carDetails as! String
                        self.makeTF.text = make as! String
                        self.modelTF.text = model as! String
                        self.priceLbl.text = price as! String
                        self.titleTF.text = title1 as! String
                        
                    }
                        self.bidImgCollection.reloadData()
                    }
                    
//                    self.bidsTableView.reloadData()
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func uploadBtn(_ sender: Any) {self.upload = true
      
        self.bidImgsArr = [String]()
        
        appde.sepStr = "sno"
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = 100
        self.bs_presentImagePickerController(vc, animated: true,
                                             select: { (asset: PHAsset) -> Void in
                                                DispatchQueue.main.async {
                                                    let photoAsset = asset
                                                    let manager = PHImageManager.default()
                                                    var options: PHImageRequestOptions?
                                                    options = PHImageRequestOptions()
                                                    options?.resizeMode = .exact
                                                    options?.isSynchronous = true
                                                    manager.requestImage(
                                                        for: photoAsset,
                                                        targetSize: PHImageManagerMaximumSize,
                                                        contentMode: .aspectFill,
                                                        options: options
                                                    ) { [weak self] result, _ in
                                                        //                                                print("my images are ******* \(result!)")
                                                        //                                                let imgData = UIImageJPEGRepresentation(result!, 0.2)!
                                                        //                                                print(imgData)
                                                        
                                                        self?.myArray.append(result!)
                                                        
                                                        self?.bidImgsArr1.append(result!)
                                                        
                                                        print(self?.bidImgsArr1 ?? "")

                                                        self?.bidImgCollection.reloadData()
                                                        
                                                        print(self?.myArray as Any)
                                                        
                                                    }
                                                }
                                                
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            print("Finish: \(assets)")
            
            //  print("Selected: \(asset)")
            
            self.uploadLbl.text = "Car Images Are Uploaded"
            
            self.uploadLbl.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let address = UserDefaults.standard.object(forKey: "address") as? String
        self.locationTF.text = address ?? ""
        
    }

    //    func handleDatePicker() {
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy"
//        self.makeTF.text = dateFormatter.string(from: datePickerView.date)
//    }
    
    @IBAction func locationBtn(_ sender: Any) {
        
        self.locationTF.text = ""
        
        locationBool = true
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "LocationController")
        self .present(service, animated: true, completion: nil)
        
    }
    @IBAction func priceSliderChange(_ sender: Any) {
        
        let currentValue = Int(self.priceSlider.value)
        
        //print("Slider changing to \(currentValue)")
        self.priceStr = String(currentValue)
        
        self.priceLbl.text = self.priceStr
    }
    @IBAction func updateBtn(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork()
        {
             addBidServiceCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
      
    }
    


func addBidServiceCall()
{
    
    //http://voliveafrica.com/carfix/services/add_bid///user_id,title,description,make,model,price,bid_id, vehicle_image[] array
    
    Services.sharedInstance.loader(view: self.view)
    
    let userid = UserDefaults.standard.object(forKey: "user_id")
    let address = UserDefaults.standard.object(forKey: "address") as? String
    let make = self.makeTF.text
    let model = self.modelTF.text
    let title = self.titleTF.text
    let textViewText = self.carDetailsTxtview.text
    let language = UserDefaults.standard.object(forKey: "currentLanguage")

    
   // let parameters: Dictionary<String, Any> = [ "user_id": userid ?? "" ,"bid_id": bidStr! , "title" : title! , "description" : textViewText! , "API-KEY" : APIKEY , "make" : make! , "model" : model! , "price" : self.priceStr! ,"address" : address ?? "" , "lang" : language ?? ""]
    
    let parameters: Dictionary<String, Any> = [ "user_id": userid ?? "" ,"bid_id": bidStr! , "title" : title! , "description" : textViewText! , "API-KEY" : APIKEY , "make" : make! , "model" : model! , "address" : address ?? "" , "lang" : language ?? "","price" : "10"]
    
    Alamofire.upload(multipartFormData: { multipartFormData in
        //import image to request
        
        
        for (key, value) in parameters {
            
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)}
        
        for i in 0..<self.myArray.count {
            
            let imageData = UIImageJPEGRepresentation(self.myArray[i] as! UIImage, 0.2)!
            
            multipartFormData.append(imageData, withName: "vehicle_image[]", fileName: "file.jpg", mimeType: "image/jpeg")
            
            print(imageData)
        }
        print(parameters)
    }
        
        ,  to:"\(Base_Url)add_bid")
    { (result) in
        
        print(result)
        
        switch result {
        case .success(let upload, _, _):
            
            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            
            print(upload.response as Any)
            
            upload.responseJSON { response in
                    print(response.result.value ?? "")
                    
                    let responseData = response.result.value as? Dictionary<String, Any>
                    
                    print(responseData as Any)
                    
                    let status = responseData!["status"] as! Int
                    let message = responseData!["message"] as? String
                    //let error = responseData!["error"] as? String
                    
                    if status == 1
                    {
                        DispatchQueue.main.async {
                            
                            self.showToast(message: message!)
                           
                            Services.sharedInstance.dissMissLoader()
                            
//                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                            let bids = storyBoard.instantiateViewController(withIdentifier: "BidsController") as? BidsController
//                            self.navigationController?.pushViewController(bids!, animated: true)
                            
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            
                            self.showToast(message: message!)
             
                            Services.sharedInstance.dissMissLoader()
                            
                        }
                }
            }
            
        case .failure(let encodingError):
            print(encodingError)
            
            DispatchQueue.main.async {
                
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
}
}

extension EditBidController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
         if appde.sepStr == "str"{
        return self.bidImgsArr.count
         }else{
          return self.bidImgsArr1.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! BidsCollectCell
        
        if appde.sepStr == "str"{
            
            cell.editBidImg.sd_setImage(with: URL(string: self.bidImgsArr[indexPath.row] as! String ), placeholderImage: UIImage(named:"as img4"))
        }else
        {
            
            cell.editBidImg.image = self.bidImgsArr1[indexPath.row]
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        width = CGFloat(collectionView.frame.size.width / 3)
        return CGSize(width: width, height: 95)
        
    }
    
}
extension EditBidController : UITextFieldDelegate{
   
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        var returnValue : Bool = false
        
        if textField == self.makeTF{
            
            returnValue = true
        }
        else if textField == self.modelTF{
            
            returnValue = true
        }
        
        return returnValue
        
    }
   
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
         if textField == self.makeTF{
            scroll.contentOffset.y = +310
            
//            datePickerView.datePickerMode = UIDatePickerMode.date
//            makeTF.inputView = datePickerView
//            datePickerView.addTarget(self, action: #selector(PostCarRequestController.handleDatePicker), for: UIControlEvents.valueChanged)
            
            let expiryDatePicker = MonthYearPickerView()
            
            self.makeTF.inputView = expiryDatePicker
            
            expiryDatePicker.onDateSelected = { ( year: Int) in
                let string1 = String(format: "%d", year)
                print(string1)
                self.makeTF.text = string1
                
            }
        }
        else if textField == self.modelTF{
            scroll.contentOffset.y =  +310
        }
        
    }
    public func textFieldDidEndEditing(_ textField: UITextField){
        
        
        let textStr = self.makeTF.text
        
        if textStr == "" {
            self.makeTF.text = "2018"
        }else
        {
        }
        
        self.makeTF.resignFirstResponder()
        self.modelTF.resignFirstResponder()
        
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }
}

extension EditBidController : UITextViewDelegate{
    
   
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == self.carDetailsTxtview{
            
            scroll.contentOffset.y = +280
            
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView){
        
        scroll.contentOffset.y = 0
    }
    
}





