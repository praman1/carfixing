//
//  UserFiltersController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 28/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.

import UIKit
import Cosmos
import Alamofire

class UserFiltersController: UIViewController {

    @IBOutlet weak var selectbrand: UIButton!
    @IBOutlet weak var priceSlider: UISlider!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var priceToFrmLbl: UILabel!
    
    @IBOutlet var resetBtn: UIButton!
    @IBOutlet weak var distanceFilter: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    var rating: Int!
    var max : String!
    var min : String!
    var minPriceStr : String!
    var vendorType : String!
    var brandsStr : String!
    var serviceids : String!
    var typeStr : String!
    
    @IBOutlet weak var serviceCategory: UIButton!
     @IBOutlet weak var starRating: UILabel!
     @IBOutlet weak var priceRange: UILabel!
    @IBOutlet weak var apply: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rateView.didFinishTouchingCosmos = didFinishTouchingCosmos
        
        
        self.max = UserDefaults.standard.object(forKey: "max") as! String
        self.min = UserDefaults.standard.object(forKey: "min") as! String
        
        serviceCategory.setTitle(languageChangeString(a_str: "Service category"), for: UIControlState.normal)
        starRating.text = languageChangeString(a_str: "Star Rating")
        priceRange.text = languageChangeString(a_str: "Price Range")
        apply.setTitle(languageChangeString(a_str: "APPLY"), for: UIControlState.normal)
        self.title = languageChangeString(a_str: "Filters")
        distanceFilter.text = languageChangeString(a_str: "Distance")
        selectbrand.setTitle(languageChangeString(a_str: "Brands"), for: UIControlState.normal)
        resetBtn.setTitle(languageChangeString(a_str: "RESET"), for: UIControlState.normal)
        
        
        let maxPrice:Int? = Int(self.max)
        let minPrice:Int? = Int(self.min)
        
        let maxPriceS = Float(maxPrice!)
        let minPriceS = Float(minPrice!)
        
        self.priceSlider.minimumValue = minPriceS
        self.priceSlider.maximumValue = maxPriceS
        
        print(maxPriceS,minPriceS)
        //        let price = "\(self.min - self.max)"
        //        self.priceToFrmLbl.text = price
        
        let first = "SAR \((self.min) + " - " + (self.max))"
        
        self.priceToFrmLbl.text = first
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func resetBtn(_ sender: Any) {
       
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NofilterCall"), object: nil)
        self.dismiss(animated: false, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        
        self.rating = Int(rating)
        
        print("rating value is \(self.rating)")
        
    }
    
    @IBAction func applyBtn(_ sender: Any) {
        
            if Reachability.isConnectedToNetwork()
            {
                filtersServiceCall ()
            }else
            {
                showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

            }
       
    }
    func filtersServiceCall ()
    {
        
        //http://voliveafrica.com/carfix/services/vendors_list
        //API-KEY,user_id,vendor_type,service_filter =1,2 (if user check srvice category) price_starts, price_ends (if user check starting and ending price)
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
       
        self.max = UserDefaults.standard.object(forKey: "max") as! String
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
         let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        if UserDefaults.standard.object(forKey: "bandIdsString") != nil
        {
             self.brandsStr = UserDefaults.standard.object(forKey: "bandIdsString") as! String
        }else
        {
           self.brandsStr = ""
        }
         if UserDefaults.standard.object(forKey: "filterids") != nil
        {
             self.serviceids = UserDefaults.standard.object(forKey: "filterids") as! String
            
        }else
        {
            self.serviceids = ""
        }
        if (self.rating == nil)
        {
            self.rating = 0
        }
        
        
       
        self.vendorType = UserDefaults.standard.object(forKey: "vendorType") as! String
        
//        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"user_id" : userid ?? "" , "service_filter" : serviceids ?? "" , "price_ends" : self.max! , "price_starts" : self.minPriceStr! , "vendor_type" : self.vendorType! , "rating" : self.rating! , "latitude" : lat ?? "" , "longitude" : long ?? "" , "lang" : language ?? "" , "brands" : brandsStr! , "distance"  : self.distanceLbl.text!]
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"user_id" : userid ?? "" , "service_filter" : serviceids ?? "" ,  "vendor_type" : self.vendorType! , "rating" : self.rating! , "latitude" : lat ?? "" , "longitude" : long ?? "" , "lang" : language ?? "" , "brands" : brandsStr! , "distance"  : self.distanceLbl.text!]
        
        print(parameters)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: parameters)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func servicesBtn(_ sender: Any) {
        
        typeStr = UserDefaults.standard.object(forKey: "ServiceT") as! String
        
        if typeStr == "4"
        {
            self.showToast(message: languageChangeString(a_str: "Scrap Shop Don't Have Any Services")!)
            
        }else
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let list = storyBoard.instantiateViewController(withIdentifier: "ServiceCategoryController") as? ServiceCategoryController
            
            self.navigationController?.pushViewController(list!, animated: true)
        }
      
        // self .present(list!, animated: false, completion: nil)
        
    }
    @IBAction func priceSliderChanged(_ sender: Any) {
        
        let currentValue = Int(self.priceSlider.value)
        
        print("Slider changing to \(currentValue)")
        
        self.minPriceStr = String(currentValue)
        
        let first = "SAR \((self.minPriceStr) + " - " + (self.max))"
        
        self.priceToFrmLbl.text = first
        
        // self.priceToFrmLbl.text = currentValue
        //startTime.text = "\(currentValue) Km"
        
    }
    @IBAction func selectBrandBtn(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "FilterBrands")
        //service.modalPresentationStyle = .overCurrentContext
        self .present(service, animated: true, completion: nil)
    }
   
    @IBAction func distanceSlider(_ sender: Any) {
        
        let currentValue = Int(self.distanceSlider.value)
        
        print("Slider changing to \(currentValue)")
        let distanceStr = String(currentValue)
        self.distanceLbl.text = distanceStr
        
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
       
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NofilterCall"), object: nil)
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func sliderValueChange(sender: UISlider) {
        
        // Get the sliders value
        let currentValue = Int(sender.value)
        let sliderRow = sender.tag
        
        print(currentValue,sliderRow)
        
    }
}
