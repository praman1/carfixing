//
//  AcceptBidController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 16/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class AcceptBidController: UIViewController {

    @IBOutlet weak var bidAccptLbl: UILabel!
    
    @IBOutlet var reject: UIButton!
    @IBOutlet var accept: UIButton!
    var vendorID : String!
    var bidID : String!
    var status : String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        vendorID = UserDefaults.standard.object(forKey: "vendorid") as! String
        bidID = UserDefaults.standard.object(forKey: "bidid") as! String
        status = UserDefaults.standard.object(forKey: "status") as! String
        
        bidAccptLbl.text = languageChangeString(a_str: "Are you interested on this Request?")
        accept.setTitle(languageChangeString(a_str: "Accept"), for: UIControlState.normal)
        reject.setTitle(languageChangeString(a_str: "Reject"), for: UIControlState.normal)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    func hideView ()
    {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptBtn(_ sender: Any) {
       
        status = "1"
        
        if Reachability.isConnectedToNetwork()
        {
            acceptBidCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
    }
    
    @IBAction func RejectBtn(_ sender: Any) {
        
       // self.dismiss(animated: true, completion: nil)
        status = "2"
        
        if Reachability.isConnectedToNetwork()
        {
            acceptBidCall ()
        }else
        {
             showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
        
    }
    
    func acceptBidCall ()
    {
        
        //http://voliveafrica.com/carfix/services/accept_auction//API-KEY//vendor_id,bid_id,status
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let details = "\(Base_Url)accept_auction"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"vendor_id" : vendorID! , "bid_id" : bidID! , "status" : status!,"lang" : language ?? ""]
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        self.showToast(message: message!!)
                        
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "bidUpdate"), object: nil)
                        
                        self.dismiss(animated: true, completion: nil)
                       
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message!!)
                        Services.sharedInstance.dissMissLoader()
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }

     

}
