//
//  WriteReviewController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/12/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class WriteReviewController: UIViewController {

    @IBOutlet weak var headText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let line = LineTextField()
        let color : UIColor = UIColor.lightGray
        
        line .textfieldAsLine(myTextfield: headText, lineColor: color, myView: self.view)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
