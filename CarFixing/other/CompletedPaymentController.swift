//
//  CompletedPaymentController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/14/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class CompletedPaymentController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = "Payment success"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func backBtnClicked(){
        
        self.navigationController?.popViewController(animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
