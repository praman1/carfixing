
//
//  PaymentController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/14/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class PaymentController: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardTypeLbl: UILabel!
    @IBOutlet weak var cardTypeTF: UITextField!
    @IBOutlet weak var dropImg: UIImageView!
    @IBOutlet weak var nameCardTopLayout: NSLayoutConstraint!
    
    @IBOutlet weak var totalPaybleLayout: NSLayoutConstraint!
    @IBOutlet weak var CGLayout: NSLayoutConstraint!
    @IBOutlet weak var creditCardImg: UIImageView!
    
    @IBOutlet weak var DCLayout: NSLayoutConstraint!
    @IBOutlet weak var debitCardImg: UIImageView!
    
    @IBOutlet weak var cashImg: UIImageView!
    @IBOutlet weak var giftImg: UIImageView!
    
    @IBOutlet weak var totalPaybleLbl: UILabel!
    
    @IBOutlet weak var priceBtn: UILabel!
    
    @IBOutlet weak var giftVoucherLbl: UILabel!
    
    @IBOutlet weak var giftVoucherTF: UITextField!
    
    @IBOutlet weak var cardViewLayout: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //card type was hidden
        cardView.isHidden = false
        cardTypeLbl.isHidden = true
        cardTypeTF.isHidden = true
        dropImg.isHidden = true
        
        nameCardTopLayout.constant = 8
        //total payble
        totalPaybleLbl.isHidden = true
        priceBtn.isHidden = true
        //middle space
        CGLayout.constant = 236
        DCLayout.constant = 236
        //gift voucher
        giftVoucherLbl.isHidden = true
        giftVoucherTF.isHidden = true
        
        cardViewLayout.constant = 220
        
        //btn select image
        creditCardImg.image = #imageLiteral(resourceName: "check circle")
        debitCardImg.image = #imageLiteral(resourceName: "check select circle")
        giftImg.image = #imageLiteral(resourceName: "check select circle")
        cashImg.image = #imageLiteral(resourceName: "check select circle")
        let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back black"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = "Payment"
        // Do any additional setup after loading the view.
    }
    @objc func backBtnClicked(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func creditCardAction(_ sender: Any) {
       
        //card type was hidden
        cardView.isHidden = false
        cardTypeLbl.isHidden = true
        cardTypeTF.isHidden = true
        dropImg.isHidden = true
        
        nameCardTopLayout.constant = 8
        //total payble
        totalPaybleLbl.isHidden = true
        priceBtn.isHidden = true
        //middle space
        CGLayout.constant = 236
        DCLayout.constant = 236
        //gift voucher
        giftVoucherLbl.isHidden = true
        giftVoucherTF.isHidden = true
        
        cardViewLayout.constant = 220
        
        //btn select image
        creditCardImg.image = #imageLiteral(resourceName: "check circle")
        debitCardImg.image = #imageLiteral(resourceName: "check select circle")
        giftImg.image = #imageLiteral(resourceName: "check select circle")
        cashImg.image = #imageLiteral(resourceName: "check select circle")
        
    }
    
    
    
    @IBAction func debitCardAction(_ sender: Any) {
        //card type was enabled
        cardView.isHidden = false
        cardTypeLbl.isHidden = false
        cardTypeTF.isHidden = false
        dropImg.isHidden = false
        nameCardTopLayout.constant = 78
        //total payble
        totalPaybleLbl.isHidden = true
        priceBtn.isHidden = true
        //middle space
        CGLayout.constant = 303
        DCLayout.constant = 303
        //gift voucher
        giftVoucherLbl.isHidden = true
        giftVoucherTF.isHidden = true
        
        cardViewLayout.constant = 287
        //btn select imag
        creditCardImg.image = #imageLiteral(resourceName: "check select circle")
        debitCardImg.image = #imageLiteral(resourceName: "check circle")
        giftImg.image = #imageLiteral(resourceName: "check select circle")
        cashImg.image = #imageLiteral(resourceName: "check select circle")
        
        
    }
    
    
    @IBAction func giftVoucherAction(_ sender: Any) {
        cardView.isHidden = true
        //middle space
        CGLayout.constant = 0
        DCLayout.constant = 0
        //total payble
        totalPaybleLbl.isHidden = true
        priceBtn.isHidden = true
        
        //gift voucher
        giftVoucherLbl.isHidden = false
        giftVoucherTF.isHidden = false
        //btn select imag
        creditCardImg.image = #imageLiteral(resourceName: "check select circle")
        debitCardImg.image = #imageLiteral(resourceName: "check select circle")
        giftImg.image = #imageLiteral(resourceName: "check circle")
        cashImg.image = #imageLiteral(resourceName: "check select circle")
        
    }
    
    
    @IBAction func cashAction(_ sender: Any) {
        
        
        cardView.isHidden = true
        //middle space
        CGLayout.constant = 0
        DCLayout.constant = 0
        
        totalPaybleLayout.constant = 8
        totalPaybleLbl.isHidden = false
        priceBtn.isHidden = false
        
        //gift voucher
        giftVoucherLbl.isHidden = true
        giftVoucherTF.isHidden = true
        //btn select imag
        creditCardImg.image = #imageLiteral(resourceName: "check select circle")
        debitCardImg.image = #imageLiteral(resourceName: "check select circle")
        giftImg.image = #imageLiteral(resourceName: "check select circle")
        cashImg.image = #imageLiteral(resourceName: "check circle")
        
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let success = storyBoard.instantiateViewController(withIdentifier: "CompletedPaymentController") as? CompletedPaymentController
        self.navigationController?.pushViewController(success!, animated: true)
      //  self .present(success!, animated: true, completion: nil)
        
    }
    
    
    
    
}
