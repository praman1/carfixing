//
//  CalenderVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/15/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit
import CVCalendar


class CalenderVC: UIViewController {
    struct Color {
        
        static let selectedText = UIColor.white
        static let text = UIColor.black
        static let textDisabled = UIColor.gray
        static let selectionBackground = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
        static let sundayText = UIColor(red: 1.0, green: 0.2, blue: 0.2, alpha: 1.0)
        static let sundayTextDisabled = UIColor(red: 1.0, green: 0.6, blue: 0.6, alpha: 1.0)
        static let sundaySelectionBackground = sundayText
    }

    @IBOutlet weak var monthLabel: UILabel!

    @IBOutlet weak var menuView: CVCalendarMenuView!
   
    @IBOutlet weak var selectBookBtn: UIButton!
    @IBOutlet weak var calendarView: CVCalendarView!
    
    var selectedDay:DayView!
    var shouldShowDaysOut = true
    var animationFinished = true
    var currentCalendar: Calendar?
override func viewDidLoad() {
        super.viewDidLoad()
    let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back black"), style: .plain, target: self, action: #selector(backBtnClicked))
    backBtn.tintColor = UIColor.white
    self.navigationItem.leftBarButtonItem = backBtn
    self.navigationItem.title = "Select Booking date"
 
    
     DispatchQueue.main.async {
    if let currentCalendar = self.currentCalendar {
        self.monthLabel.text = CVDate(date: Date(), calendar: currentCalendar).globalDescription
    }
    }

        // Do any additional setup after loading the view.
    
    }

    @IBAction func OnClickNextMonthBtn_Act(_ sender: Any) {
        calendarView.loadNextView()
    }
    @IBAction func OnClickpreviousMonthBtn_Act(_ sender: Any) {
        calendarView.loadPreviousView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func backBtnClicked(){                self.navigationController?.popViewController(animated: true)            }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func bookActn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let payment = storyBoard.instantiateViewController(withIdentifier: "PaymentController") as? PaymentController
        self.navigationController?.pushViewController(payment!, animated: true)
    }
    
}
extension CalenderVC: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    /// Required method to implement!
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    /// Required method to implement!
    func firstWeekday() -> Weekday {
        return .sunday
    }
    
    
    
    
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        selectedDay = dayView
        
        //self.deliveryDate = selectedDay.date.commonDescription
       // print("selected date is \(selectedDay.date.commonDescription)")
        
    }
    
    
    func presentedDateUpdated(_ date: CVDate) {
        
        if monthLabel.text != date.globalDescription && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
            updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            
            UIView.animate(withDuration: 0.35, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
                self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransform.identity
                
            }) { _ in
                
                self.animationFinished = true
                self.monthLabel.frame = updatedMonthLabel.frame
                self.monthLabel.text = updatedMonthLabel.text
                self.monthLabel.transform = CGAffineTransform.identity
                self.monthLabel.alpha = 1
                updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    
    func disableScrollingBeforeDate() -> Date {
        return Date()
    }
    
    func maxSelectableRange() -> Int {
        return 1
    }
    
    func earliestSelectableDate() -> Date {
        return Date()
    }
    
    
}
// MARK: - CVCalendarViewAppearanceDelegate

extension CalenderVC: CVCalendarViewAppearanceDelegate {
    
    func dayLabelWeekdayDisabledColor() -> UIColor {
        return UIColor.lightGray
    }
    
    func dayLabelPresentWeekdayInitallyBold() -> Bool {
        return false
    }
    
    func spaceBetweenDayViews() -> CGFloat {
        return 0
    }
  
    func dayLabelFont(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIFont { return UIFont.systemFont(ofSize: 14) }
    
    func dayLabelColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay, status, present) {
        case (_, .selected, _), (_, .highlighted, _): return Color.selectedText
        case (.sunday, .in, _): return Color.sundayText
        case (.sunday, _, _): return Color.sundayTextDisabled
        case (_, .in, _): return Color.text
        default: return Color.textDisabled
        }
    }
    
    func dayLabelBackgroundColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay, status, present) {
        case (.sunday, .selected, _), (.sunday, .highlighted, _): return Color.sundaySelectionBackground
        case (_, .selected, _), (_, .highlighted, _): return Color.selectionBackground
        default: return nil
        }
    }
}
