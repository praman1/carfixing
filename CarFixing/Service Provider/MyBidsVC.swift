//
//  MyBidsVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/15/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit
import Alamofire

class MyBidsVC: UIViewController {

     var appde = AppDelegate()
    
    @IBOutlet weak var myBid_tableview: UITableView!
    @IBOutlet weak var previousBid_view: UIView!
    @IBOutlet weak var currentBid_View: UIView!
   
    @IBOutlet weak var titles: UILabel!
    @IBOutlet weak var datetime: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var currentBid: UIButton!
    @IBOutlet weak var prevoisBid: UIButton!
    var bidTitleArr = [String]()
    var bidDateArr = [String]()
    var bidStatusArr = [String]()
    var bidIdArr = [String]()
    var allbidData : [[String : AnyObject]]!
    var compltedData : [[String : AnyObject]]!
    var completbidTitleArr = [String]()
    var completbidDateArr = [String]()
    var completbidStatusArr = [String]()
    var completbidArr = [String]()
    var sepStr : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appde = UIApplication.shared.delegate as! AppDelegate
        
        print(sepStr)
        
        // Do any additional setup after loading the view.
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        self.title = languageChangeString(a_str: "My Bids")
        currentBid.setTitle(languageChangeString(a_str: "Current Bids"), for: UIControlState.normal)
         prevoisBid.setTitle(languageChangeString(a_str: "Previous Bids"), for: UIControlState.normal)
        titles.text = languageChangeString(a_str: "Title")
        datetime.text = languageChangeString(a_str: "Date and Time")
        status.text = languageChangeString(a_str: "Status")
        
//        bidNameArr = ["Need car clutcher repair","Need air conditioner repair","Need Break rod repair","Need Engine oil repair"]
//        bidDateArr = ["1 st november 2017 11.30 AM","5th november 2017 1.30 PM","9th november 2017 09.30 AM","10th november 2017 10.30 AM"]
       // problemStatusArr = ["Accepted","Active","Not Accepted","Accepted"]
        
        self.previousBid_view.isHidden=true
        self.currentBid_View.isHidden=false
        
        myBid_tableview.rowHeight = UITableViewAutomaticDimension
        myBid_tableview.estimatedRowHeight = 45
        myBid_tableview.reloadData()
        
        if Reachability.isConnectedToNetwork() {
            vendorbidsCall()
        }else
        {
             showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
    }
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickDetailsBtn_Act(_ sender: UIButton) {
        
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let detail = storyBoard.instantiateViewController(withIdentifier: "MyJobsVC") as? MyJobsVC
//
//        self.navigationController?.pushViewController(detail!, animated: true)
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.myBid_tableview)
        let indexPath: IndexPath? = self.myBid_tableview.indexPathForRow(at: btnPosition)
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let detail = storyBoard.instantiateViewController(withIdentifier: "BidNowController") as? BidNowController
        
        detail?.bididStr = bidIdArr[(indexPath?.row)!]
        detail?.bidsepStr = "bidDetail"
       
        self.navigationController?.pushViewController(detail!, animated: true)
       
    }
    
    @IBAction func complteDetailsBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.myBid_tableview)
        let indexPath: IndexPath? = self.myBid_tableview.indexPathForRow(at: btnPosition)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "BidNowController") as? BidNowController
        detail?.bididStr = self.completbidArr[(indexPath?.row)!]
        
        if self.completbidStatusArr[(indexPath?.row)!] == "Accepted" {
             detail?.checkAcceptString = "1"
        }else{
            
        }
        detail?.bidsepStr = "bidDetail"
        self.navigationController?.pushViewController(detail!, animated: true)
    
    }
    
    
    @IBAction func onClickPreviousBidbtn_Act(_ sender: Any) {
        
        DispatchQueue.main.async {
            
            self.sepStr = "cbids"
            self.previousBid_view.isHidden=false
            self.currentBid_View.isHidden=true
            
            self.myBid_tableview.reloadData()
            
        }
    }
    @IBAction func onClickCurrentBidsBtn_Act(_ sender: Any) {
        DispatchQueue.main.async {
            
            self.sepStr = "obids"
            self.previousBid_view.isHidden=true
            self.currentBid_View.isHidden=false
           
            self.myBid_tableview.reloadData()
            
        }
    }
    
    func vendorbidsCall()
    {
        //http://voliveafrica.com/carfix/services/vendor_bids?API-KEY=98745612&vendor_id=8

        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let details = "\(Base_Url)vendor_bids?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vendor_id" : userid ?? "" , "lang" : language ?? ""]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.allbidData = responseData["current_bids"] as? [[String:AnyObject]]
                    
                    for eachjob in self.allbidData!
                    {
                        
                        let jobNameStr = eachjob["message"] as? String!
                        let jobDateStr = eachjob["date"] as? String!
                        //let jobStatus = eachjob["bid_status"] as? String!
                        let jobId = eachjob["bid_id"] as? String!
                        
                        //self.jobIdStr = eachjob["vendor_id"] as? String
                        self.bidTitleArr.append(jobNameStr!!)
                        self.bidDateArr.append(jobDateStr!!)
                        self.bidStatusArr.append("0")
                        self.bidIdArr.append(jobId!!)
                        
                        
                    }
//                    if self.bidTitleArr.count == 0
//                    {
//                        self.showToast(message: "No Data Found!..")
//                    }
                    
                    self.compltedData = responseData["previous_bids"] as? [[String:AnyObject]]
                    
                    for i in self.compltedData!
                    {
                        let jobNameStr1 = i["message"] as? String!
                        let jobDateStr1 = i["date"] as? String!
                        let jobStatus1 = i["bid_status"] as? String!
                        let cjobId = i["bid_id"] as? String!
                        
                        self.completbidTitleArr.append(jobNameStr1!!)
                        self.completbidDateArr.append(jobDateStr1!!)
                        self.completbidStatusArr.append(jobStatus1!!)
                        self.completbidArr.append(cjobId!!)
                    }
                    DispatchQueue.main.async {
                      
                        self.myBid_tableview.reloadData()
                        
                    }
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension MyBidsVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if self.sepStr == "obids"{
            
             return bidTitleArr.count
        
        }else
        {
           return completbidTitleArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // let cell = home_tblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        if self.sepStr == "obids"{
        
            
            let cell = myBid_tableview.dequeueReusableCell(withIdentifier: "myBidsCell", for: indexPath) as! MyBidsOrJobsTableViewCell
            cell.problemName_lbl.text = bidTitleArr[indexPath.row]
            cell.date_lbl.text = String(format: "%@", bidDateArr[indexPath.row])
            cell.datailsBid.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
            status.isHidden = true
            
            return cell
        
        }else
        {
            let cell = myBid_tableview.dequeueReusableCell(withIdentifier: "myBidsCell1", for: indexPath) as! MyBidsOrJobsTableViewCell
            
            cell.cDetails.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
            //cell.status_lbl.isHidden = true
            cell.cproblemLbl.text = String(format: "%@", completbidTitleArr[indexPath.row])
            cell.cdateLbl.text = String(format: "%@", completbidDateArr[indexPath.row])
            cell.cstatusLbl.text = self.completbidStatusArr[indexPath.row]
            status.isHidden = false
            if self.completbidStatusArr[indexPath.row] == "Accepted" {
                cell.cstatusLbl.textColor=UIColor.green
            }
            else if self.completbidStatusArr[indexPath.row] == "Rejected"{
                
                cell.cstatusLbl.textColor=UIColor.red
            }
            else{
                cell.cstatusLbl.textColor=UIColor.black
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
 
}
