//
//  FilterViewController.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    @IBOutlet weak var rangeSlider: UISlider!
    
    @IBOutlet weak var reset: UIButton!
    @IBOutlet weak var filters: UILabel!
    @IBOutlet weak var filter_tableView: UITableView!
    //arrays
    var filterTypeArr = [String]()
    var checkBoolArr = [String]()
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        filterTypeArr=["Transmission repair","Engine repair","Brake repair","emission repair","silencer repair","light repair","mirror repair","seatbelt repair","Transmission repair","Engine repair",]
        checkBoolArr=["YES","NO","NO","NO","NO","NO","NO","NO","NO","NO"]
        
        filter_tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickClosebtn_Act(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickApplyFilterBtn_Act(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickResetBtn_Act(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    


}
extension FilterViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterTypeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // let cell = home_tblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        let cell = filter_tableView.dequeueReusableCell(withIdentifier: "filterCell", for: indexPath) as! FilterTableViewCell
        cell.filterType_lbl.text = filterTypeArr[indexPath.row]
        if checkBoolArr[indexPath.row] == "YES" {
            //cell.checkImgBtn.setTitle("Button Title",for: .normal)
            
            cell.checkImgBtn.setImage(UIImage(named: "check"), for: .normal)

        }
        else{
            cell.checkImgBtn.setImage(UIImage(named: "unCheck"), for: .normal)
        }
        
       //cell.checkImgBtn.tag=indexPath.row
        
   // cell.checkImgBtn.addTarget(self, action: Selector(("action:")), for: UIControlEvents.touchUpInside)
        
        
       
        return cell
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    

        
                
                    if self.checkBoolArr[indexPath.row] == "NO" {
                    self.checkBoolArr[indexPath.row] = "YES"
                        
                }
                    else{
                       self.checkBoolArr[indexPath.row] = "NO"
                    }
        DispatchQueue.main.async {
    self.filter_tableView.reloadData()
        }
    
    
    }
    
}

