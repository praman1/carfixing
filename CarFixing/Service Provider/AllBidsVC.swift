//
//  AllBidsVC.swift
//  CarFixing
//
//  Created by Suman Guntuka on 23/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class AllBidsVC: UIViewController {
    
    @IBOutlet weak var allBidsTable: UITableView!
    var bidNameArr = [String]()
    var bidDateArr = [String]()
    var bidCostArr = [String]()
    var bidIdArr = [String]()
    var allbidData : [[String : AnyObject]]!
    var compltedData : [[String : AnyObject]]!
    var completbidNameArr = [String]()
    var completbidDateArr = [String]()
    var completbidCostArr = [String]()
    var allBidNameArr = [String]()
   

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        if Reachability.isConnectedToNetwork() {
            vendorAllbidsCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(reloadServiceCall), name: NSNotification.Name(rawValue:"bidData"), object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func reloadServiceCall()
    {
        if Reachability.isConnectedToNetwork() {
            vendorAllbidsCall()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
    }
    
    @IBAction func viewDetailsBtn(_ sender: Any) {
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.allBidsTable)
        let indexPath: IndexPath? = self.allBidsTable.indexPathForRow(at: btnPosition)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailBid = storyBoard.instantiateViewController(withIdentifier: "BidNowController") as?
        BidNowController
        detailBid?.bididStr = self.bidIdArr[(indexPath?.row)!]
        
        
        self.navigationController?.pushViewController(detailBid!, animated: true)
        
    }
    
    func vendorAllbidsCall()
    {
        //http://voliveafrica.com/carfix/services/allbids?API-KEY=98745612&vendor_id=8

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let details = "\(Base_Url)allbids?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vendor_id" : userid ?? "" , "lang" : language ?? ""
 ]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.bidNameArr = [String]()
                    self.bidDateArr = [String]()
                    self.bidCostArr = [String]()
                    self.bidIdArr = [String]()
                    self.allBidNameArr  = [String]()
                    
                    self.allbidData = responseData["bids"] as? [[String:AnyObject]]

                    for eachjob in self.allbidData!
                    {

                        let jobNameStr = eachjob["title"] as? String!
                        let jobDateStr = eachjob["posted_date"] as? String!
                        let jobPrice = eachjob["price"] as? String!
                         let jobId = eachjob["b_id"] as? String!
                        let userName = eachjob["user_name"] as? String!
                        
                        self.bidNameArr.append(jobNameStr!!)
                        self.bidDateArr.append(jobDateStr!!)
                        self.bidCostArr.append(jobPrice!!)
                        self.bidIdArr.append(jobId!!)
                        self.allBidNameArr.append(userName!!)
                    }
                    
                    if self.bidNameArr.count == 0
                    {
                        self.showToast(message: self.languageChangeString(a_str: "No Data Found!..")!)
                    }
 
                    DispatchQueue.main.async {
                        
                        self.allBidsTable.reloadData()
                        
                    }
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }

}



extension AllBidsVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return bidNameArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = allBidsTable.dequeueReusableCell(withIdentifier: "AllBidCell", for: indexPath) as! AllBidCell
           cell.bidNameLbl.text = String(format: "%@", bidNameArr[indexPath.row])
         cell.postedDateLbl.text = String(format: "%@", bidDateArr[indexPath.row])
        cell.priceLbl.text =  String(format: "%@%@", "SAR" ,bidCostArr[indexPath.row])
        cell.userBidLbl.text = allBidNameArr[indexPath.row]
        
       // let aStr = String(format: "%@%x", "timeNow in hex: ", timeNow)
        
        cell.title.text = languageChangeString(a_str: "Title")
        cell.postedOn.text = languageChangeString(a_str: "Posted on:")
        cell.price.text = languageChangeString(a_str: "Price")
        
        cell.viewDetails.setTitle(languageChangeString(a_str: "View Details"), for: UIControlState.normal)
        
        
            return cell
        }
        
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 142
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let detail = storyBoard.instantiateViewController(withIdentifier: "BidDetailsVC") as? BidDetailsVC
//
//        self.navigationController?.pushViewController(detail!, animated: true)
        
    }
    
}
    
    


 
