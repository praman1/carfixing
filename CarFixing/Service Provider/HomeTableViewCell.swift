//
//  HomeTableViewCell.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var viewMore_btn: UIButton!
    @IBOutlet weak var problemDescription_lbl: UILabel!
    @IBOutlet weak var problemName_lbl: UILabel!
    @IBOutlet weak var car_imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
