//
//  JobStatus.swift
//  CarFixing
//
//  Created by Suman Guntuka on 31/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class JobStatus: UIViewController {

    @IBOutlet var rejectView: UIView!
    @IBOutlet var reasonLbl: UILabel!

    @IBOutlet var reasonTF: UITextField!
    @IBOutlet var submit: UIButton!
  
    @IBOutlet var request: UILabel!
    
    @IBOutlet var userNamLbl: UILabel!
    @IBOutlet var userImg: UIImageView!
    
    @IBOutlet var postMsg: UILabel!
    
    @IBOutlet var confirm: UIButton!
    @IBOutlet var reject: UIButton!
    
    var jobIdStr : String!
    var userIdStr : String!
    var userImgStr : String!
    
    var rejectBool : Bool!
   
    var parameters: Dictionary<String, Any>!

    var pushDictionary:[String:Any] = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rejectView.isHidden = true
        rejectBool = false
        
        //Parameters: job_id, user_id, job_status, reject_reason
       
        let message = pushDictionary["message"] as! String
        let name = pushDictionary["user_name"] as! String
        
        let image1 = pushDictionary["image"] as! String
        
        let jobid  = pushDictionary["job_id"] as! Int
        
        jobIdStr = String(jobid)
        
        print(jobIdStr)
        
        userIdStr = pushDictionary["user_id"] as! String
        
        let titleStr = pushDictionary["title"] as! String
        
        userImgStr = base_path + image1
     
       
        DispatchQueue.main.async {
          
            self.userImg.sd_setImage(with: URL(string: self.userImgStr!  ), placeholderImage: UIImage(named:"as img4"))
            self.userImg.layer.cornerRadius = self.userImg.frame.size.height/2
            
            self.request.text = titleStr
            self.userNamLbl.text = name
            self.postMsg.text = message
            
        }
 
    }
    //Tool bar
    
    lazy var inputToolbar: UIToolbar = {
        var toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(JobStatus.inputToolbarDonePressed))
        var flexibleSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var fixedSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        toolbar.setItems([fixedSpaceButton, fixedSpaceButton, flexibleSpaceButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        return toolbar
    }()
    
    func inputToolbarDonePressed() {
        
        
        self.view.endEditing(true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false

    }

    @IBAction func submitBtn(_ sender: Any) {
       
        rejectView.isHidden = true
        
        if reasonTF.text == ""
        {
           self.showToastForAlert(message:  "Please Enter Reason")
            
        }else
        {
           jobAcceptServiceCall()
        }
        
        //self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func confirmBtn(_ sender: Any) {
      
        if Reachability.isConnectedToNetwork() {
           
            jobAcceptServiceCall()
       
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
    }
    
    @IBAction func rejectBtn(_ sender: Any) {
        
        rejectView.isHidden = false
        
        rejectBool = true
       
    }
    
    func jobAcceptServiceCall()
    {
        
        self.dismiss(animated: true, completion: nil)
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
       
        // http://voliveafrica.com/carfix/services/acceptjob
        //  job_id, user_id, job_status, reject_reason
        
        let vehicles = "\(Base_Url)acceptjob"
        
        if rejectBool == true
        {
            parameters  = ["API-KEY": APIKEY ,  "lang" : language ?? "" , "user_id" : userIdStr! , "job_id" : jobIdStr! , "job_status" : "2" , "reject_reason" : reasonTF.text!]
        
        }else
        {
             parameters = ["API-KEY": APIKEY ,  "lang" : language ?? "" , "user_id" : userIdStr! , "job_id" : jobIdStr! , "job_status" : "1"]
        }
       
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post, parameters: parameters! , encoding: URLEncoding.default, headers: nil).responseJSON { response in
          
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                
                print(status)
                 print(message)
                
                if status == 1
                {
                   
                    DispatchQueue.main.async {
                         Services.sharedInstance.dissMissLoader()
                        
                      //  self.dismiss(animated: true, completion: nil)
                        
                    }
                    
                }else
                {
                    DispatchQueue.main.async {
                   Services.sharedInstance.dissMissLoader()
                    }
                }
                
            }
        }
        
    }
 
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
       
        self.view.endEditing(true)
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputAccessoryView = inputToolbar
        
        return true
    }
    
}
/*
 {
 alert = "Job request from user Suman Guntuka";
 badge = 1;
 info =     {
 body = "Job request from user Suman Guntuka";
 image = "assets/uploads/user_profiles/file48.jpg";
 "job_id" = 165;
 latitude = "17.43747084";
 longitude = "78.45318741";
 message = "Job request from user Suman Guntuka";
 title = "Job Request";
 type = "job_request";
 "user_id" = 27;
 "user_name" = "Suman Guntuka";
 };
 sound = default;
 }]
 */

