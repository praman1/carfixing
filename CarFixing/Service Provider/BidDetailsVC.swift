//
//  BidDetailsVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit

class BidDetailsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        
        self.title = languageChangeString(a_str: "Bid Details")
        // Do any additional setup after loading the view.
    }
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
