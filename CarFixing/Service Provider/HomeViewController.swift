//
//  HomeViewController.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit
import SideMenu
import GoogleMaps
import GooglePlaces
import Alamofire
class HomeViewController: UIViewController ,CLLocationManagerDelegate, GMSMapViewDelegate{

    @IBOutlet weak var filter_view: UIView!
    @IBOutlet weak var search_TF: UITextField!
    
    @IBOutlet weak var findWork_view: UIView!
    
    @IBOutlet weak var myjobs_view: UIView!
    @IBOutlet weak var findWork_lbl: UILabel!
    @IBOutlet weak var findWork_imgView: UIImageView!
    
    @IBOutlet weak var myJobs_lbl: UILabel!
    @IBOutlet weak var myJobs_imgView: UIImageView!
    
    @IBOutlet weak var messages_view: UIView!
    
    @IBOutlet weak var messages_lbl: UILabel!
    @IBOutlet weak var messages_imgView: UIImageView!
    
    @IBOutlet weak var home_tblView: UITableView!
    
    @IBOutlet weak var mainView: UIView!
    
    var chatVC = ChatController ()
    var jobVC = MyJobsVC ()
    var bidVC = AllBidsVC ()
    var appDelegate = AppDelegate()

  
   // var myStr : String!
    //arrays
    var problemNameArr = [String]()
    var problemDescriptionArr = [String]()
    var problemCarImgArr = [String]()
    
    // location
    
    var latitude : String!
    var longitude : String!
    var addressOfuser :String!
    var gmapView: GMSMapView!
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if UserDefaults.standard.object(forKey: "roleStr") != nil
        {
            appDelegate.scrapshop = UserDefaults.standard.object(forKey: "roleStr") as! String
        }
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1
        
        let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")
        
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuVC!)
        menuLeftNavigationController.leftSide = true
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1
        
        let menu1VC = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")
        
        let menuNavigationController = UISideMenuNavigationController(rootViewController: menu1VC!)
        menuNavigationController.leftSide = true
        
        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        
//        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openSideMenu), name: NSNotification.Name (rawValue: "openSideMenu"), object: nil)

        // Do any additional setup after loading the view.
        
        self.title = languageChangeString(a_str: "Home")
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        let imageView = UIImageView()
        let leftView = UIView()
        search_TF.placeholder="    search jobs"
        search_TF.borderStyle = UITextBorderStyle.none
        search_TF.leftViewMode = UITextFieldViewMode.always
        imageView.frame = CGRect(x: 8, y: 15, width: 15, height: 15)
        leftView.frame = CGRect(x: 8, y: 15, width: 15, height: 15)
        let image = UIImage(named: "search")
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        search_TF.leftView = leftView
        search_TF.addSubview(imageView)

//        let tapFindWork = UITapGestureRecognizer(target: self, action: #selector(handleFindWorkTap))
//
//        findWork_view.addGestureRecognizer(tapFindWork)
        
        let tapMyJobsWork = UITapGestureRecognizer(target: self, action: #selector(handleFindJobsTap))
        
        myjobs_view.addGestureRecognizer(tapMyJobsWork)
        
        let tapMessages = UITapGestureRecognizer(target: self, action: #selector(handleMessagesTap))
        
        messages_view.addGestureRecognizer(tapMessages)
        
//        let tapFilter = UITapGestureRecognizer(target: self, action: #selector(handleFilterTap))
//
//        filter_view.addGestureRecognizer(tapFilter)
        
        messages_view.layer.borderWidth = 1
        messages_view.layer.borderColor = UIColor.black.cgColor
        
        myjobs_view.layer.borderWidth = 1
        myjobs_view.layer.borderColor = UIColor.black.cgColor
        
//        findWork_view.layer.borderWidth = 1
//        findWork_view.layer.borderColor = UIColor.black.cgColor
        
        // view.userInteractionEnabled = true
        
       // self.view.addSubview(view)
        problemNameArr = ["My Car clutcher problem","my car air conditioner problem","Break rod problem","Engine oil problem"]
        problemDescriptionArr = ["my car is troubeling for clutcher from so many days please look on it","my car is troubeling for air conditioner from so many days please look on it","my car is troubeling for break rod  from so many days please look on it","my car is troubeling for engine oil from so many days please look on it"]
        problemCarImgArr = ["Img1","Img2","Img3","Img1"]
        
      if appDelegate.scrapshop == "scrapshop"
      {
        //AllBidsVC
        
        bidVC = (storyboard?.instantiateViewController(withIdentifier: "AllBidsVC") as? AllBidsVC)!
        bidVC.view.frame = mainView.bounds
        addChildViewController(bidVC)
        mainView.addSubview(bidVC.view)
        bidVC.didMove(toParentViewController: self)
       
//        self.myJobs_lbl.textColor=UIColor.white
//        self.myJobs_lbl.text = "All Bids"
//        self.messages_lbl.textColor=UIColor.black
//
//        self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
//        self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
//        self.myJobs_imgView.tintColor = UIColor.white
//        self.myjobs_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
        
        self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
        self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
        self.myJobs_imgView.tintColor = UIColor.white
         self.myJobs_lbl.text = languageChangeString(a_str: "All Bids")
        
        self.messages_imgView.image=UIImage(named: "myMessagesYellow")
        self.messages_imgView.image = self.messages_imgView.image!.withRenderingMode(.alwaysTemplate)
        self.messages_imgView.tintColor = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
        
        // self.findWork_lbl.textColor=UIColor.black
        self.myJobs_lbl.textColor=UIColor.white
        self.messages_lbl.textColor=UIColor.black
        self.messages_lbl.text = self.languageChangeString(a_str: "My Messages")
        
        // self.findWork_view.backgroundColor=UIColor.white
        self.myjobs_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
        self.messages_view.backgroundColor=UIColor.white
        
        
        
        }else
      {
        
        jobVC = (storyboard?.instantiateViewController(withIdentifier: "MyJobsVC") as? MyJobsVC)!
        jobVC.view.frame = mainView.bounds
        addChildViewController(jobVC)
        mainView.addSubview(jobVC.view)
        jobVC.didMove(toParentViewController: self)
        
        self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
        self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
        self.myJobs_imgView.tintColor = UIColor.white
        
        self.messages_imgView.image=UIImage(named: "myMessagesYellow")
        self.messages_imgView.image = self.messages_imgView.image!.withRenderingMode(.alwaysTemplate)
        self.messages_imgView.tintColor = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
        
        // self.findWork_lbl.textColor=UIColor.black
        self.myJobs_lbl.textColor=UIColor.white
        self.messages_lbl.textColor=UIColor.black
        self.myJobs_lbl.text = self.languageChangeString(a_str: "My Jobs")
        self.messages_lbl.text = self.languageChangeString(a_str: "My Messages")
        
        // self.findWork_view.backgroundColor=UIColor.white
        self.myjobs_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
        self.messages_view.backgroundColor=UIColor.white
        
        }
 
        gmapView?.delegate = self
        if Reachability.isConnectedToNetwork()
        {
             isAuthorizedtoGetUserLocation()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
    
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)

//     present(SideMenuManager.menuLeftNavigationController!, animated: true) {() -> Void in }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                
                //print("No access")
                
                let alertController = UIAlertController(title: languageChangeString(a_str: "Location Services Disabled!") , message: languageChangeString(a_str: "Please enable Location Based Services for better results! We promise to keep your location private"), preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: NSLocalizedString( languageChangeString(a_str: "Cancel")!, comment: ""), style: .cancel, handler: nil)
                let settingsAction = UIAlertAction(title: NSLocalizedString( languageChangeString(a_str: "Settings")!, comment: ""), style: .default) { (UIAlertAction) in
                    
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                    //UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
                }
                
                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
                
                
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            
            print("Location services are not enabled")
            
            let alertController = UIAlertController(title: languageChangeString(a_str: "Location Services Disabled!") , message: languageChangeString(a_str: "Please enable Location Based Services for better results! We promise to keep your location private"), preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString( languageChangeString(a_str: "Cancel")!, comment: ""), style: .cancel, handler: nil)
            let settingsAction = UIAlertAction(title: NSLocalizedString( languageChangeString(a_str: "Settings")!, comment: ""), style: .default) { (UIAlertAction) in
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
                
                // UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
                
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            self.present(alertController, animated: true, completion: nil)
            
            //  UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
            
        }
        
    }
    
    @IBAction func notificationClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let location = storyBoard.instantiateViewController(withIdentifier: "NotificationsController") as? NotificationsController
        self.navigationController?.pushViewController(location!, animated: true)
        
    }
    @objc func openSideMenu ()  {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }

    func handleFindJobsTap() {
     
        if appDelegate.scrapshop == "scrapshop" {
           
            bidVC = (storyboard?.instantiateViewController(withIdentifier: "AllBidsVC") as? AllBidsVC)!
            bidVC.view.frame = mainView.bounds
            addChildViewController(bidVC)
            mainView.addSubview(bidVC.view)

            DispatchQueue.main.async {
              
                self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
                self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
                self.myJobs_imgView.tintColor = UIColor.white
                
                self.messages_imgView.image=UIImage(named: "myMessagesYellow")
                self.messages_imgView.image = self.messages_imgView.image!.withRenderingMode(.alwaysTemplate)
                self.messages_imgView.tintColor = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
                
                self.myJobs_lbl.textColor=UIColor.white
                self.messages_lbl.textColor=UIColor.black
                self.myJobs_lbl.text = self.languageChangeString(a_str: "All Bids")
                self.messages_lbl.text = self.languageChangeString(a_str: "My Messages")
                
                self.myjobs_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
                self.messages_view.backgroundColor=UIColor.white
                
                self.shuffleArray()
                
            }
            
        }else
        {
            self.jobVC = (storyboard?.instantiateViewController(withIdentifier: "MyJobsVC") as? MyJobsVC)!
            self.jobVC.view.frame = self.mainView.bounds
            addChildViewController(self.jobVC)
            self.mainView.addSubview(self.jobVC.view)
            self.jobVC.didMove(toParentViewController: self)
            
            DispatchQueue.main.async {
               
                self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
                self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
                self.myJobs_imgView.tintColor = UIColor.white
                
                self.messages_imgView.image=UIImage(named: "myMessagesYellow")
                self.messages_imgView.image = self.messages_imgView.image!.withRenderingMode(.alwaysTemplate)
                self.messages_imgView.tintColor = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
                
                self.myJobs_lbl.textColor=UIColor.white
                self.messages_lbl.textColor=UIColor.black
                self.myJobs_lbl.text = self.languageChangeString(a_str: "My Jobs")
                self.messages_lbl.text = self.languageChangeString(a_str: "My Messages")
                
                self.myjobs_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
                self.messages_view.backgroundColor=UIColor.white
                
                self.shuffleArray()
                
            }
        }
    }
    
    @IBAction func onClickViewMoreBtn_Act(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "JobRequirementDetailsVC") as? JobRequirementDetailsVC
        detail?.problemNameStr = problemNameArr[sender.tag]
        self.navigationController?.pushViewController(detail!, animated: true)
        
    }
    
    func handleMessagesTap() {
       
        if appDelegate.scrapshop == "scrapshop"
        {
            //AllBidsVC
            
            self.chatVC = (storyboard?.instantiateViewController(withIdentifier: "ChatController") as? ChatController)!
            self.chatVC.view.frame = self.mainView.bounds
            addChildViewController(self.chatVC)
            self.mainView.addSubview(self.chatVC.view)
            self.chatVC.didMove(toParentViewController: self)
     
            self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
            self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
            self.myJobs_imgView.tintColor = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
          
            self.messages_imgView.image=UIImage(named: "myMessagesYellow")
            self.messages_imgView.image = self.messages_imgView.image!.withRenderingMode(.alwaysTemplate)
             self.messages_imgView.tintColor = UIColor.white
            
            // self.findWork_lbl.textColor=UIColor.black
             self.myJobs_lbl.text = languageChangeString(a_str: "All Bids")
            self.myJobs_lbl.textColor=UIColor.black
            self.messages_lbl.textColor=UIColor.white
            
            // self.findWork_view.backgroundColor=UIColor.white
            self.myjobs_view.backgroundColor=UIColor.white
            self.messages_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
            
        }else
        {
            self.chatVC = (storyboard?.instantiateViewController(withIdentifier: "ChatController") as? ChatController)!
            self.chatVC.view.frame = self.mainView.bounds
            addChildViewController(self.chatVC)
            self.mainView.addSubview(self.chatVC.view)
            self.chatVC.didMove(toParentViewController: self)
            
            DispatchQueue.main.async {
              
                self.myJobs_imgView.image=UIImage(named: "myJobsYellow")
                self.myJobs_imgView.image = self.myJobs_imgView.image!.withRenderingMode(.alwaysTemplate)
                self.myJobs_imgView.tintColor = UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
                
                self.messages_imgView.image=UIImage(named: "myMessagesYellow")
                self.messages_imgView.image = self.messages_imgView.image!.withRenderingMode(.alwaysTemplate)
                self.messages_imgView.tintColor = UIColor.white
                
                // self.findWork_lbl.textColor=UIColor.black
                self.myJobs_lbl.textColor=UIColor.black
                self.messages_lbl.textColor=UIColor.white
                self.myJobs_lbl.text = self.languageChangeString(a_str: "My Jobs")
                self.messages_lbl.text = self.languageChangeString(a_str: "My Messages")
                
                //  self.findWork_view.backgroundColor=UIColor.white
                self.myjobs_view.backgroundColor=UIColor.white
                self.messages_view.backgroundColor=UIColor(red: 242/255, green: 173/255, blue: 1/255, alpha: 1)
                
                self.shuffleArray()
                
            }
        }
        
    }
    
    func shuffleArray() {
        
        var tempArray = self.problemNameArr
        for index in 0...self.problemNameArr.count - 1 {
            let randomNumber = arc4random_uniform(UInt32(self.problemNameArr.count - 1))
            let randomIndex = Int(randomNumber)
            tempArray[randomIndex] = self.problemNameArr[index]
        }
        self.problemNameArr=tempArray
        
        
         tempArray = self.problemCarImgArr
        for index in 0...self.problemNameArr.count - 1 {
            let randomNumber = arc4random_uniform(UInt32(self.problemCarImgArr.count - 1))
            let randomIndex = Int(randomNumber)
            tempArray[randomIndex] = self.problemCarImgArr[index]
        }
       
        self.problemCarImgArr=tempArray
        
        tempArray = self.problemDescriptionArr
        for index in 0...self.problemNameArr.count - 1 {
            let randomNumber = arc4random_uniform(UInt32(self.problemDescriptionArr.count - 1))
            let randomIndex = Int(randomNumber)
            tempArray[randomIndex] = self.problemDescriptionArr[index]
        }
        
        self.problemDescriptionArr=tempArray
        
            }
    
    /// location need
    
    func isAuthorizedtoGetUserLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
        let geocoder = GMSGeocoder()
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
        
        latitude = String(format: "%.8f", currentLocation.latitude)
        longitude = String(format: "%.8f",currentLocation.longitude)
        
        let latLang = "\(latitude), \(longitude)"
        UserDefaults.standard.set( latitude, forKey: "latitude")
        
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
        print("current lat and long \(latLang)")
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                
                //print("adress of that location is \(result!)")
                
                let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                
                self.addressOfuser = address?.strstr(needle: ",")
                
                //print(self.addressOfuser)
                
                UserDefaults.standard.set(self.addressOfuser, forKey: "address")
                
            }
        }
        
        // self.setupLocationMarker(coordinate: position)
        DispatchQueue.main.async {
            self.gmapView?.camera = camera
        }
        self.gmapView?.animate(to: camera)
        //manager.stopUpdatingLocation()
        
       let timer2 = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(HomeController.sayHello), userInfo: nil, repeats: true)
        RunLoop.current.add(timer2, forMode: RunLoopMode.defaultRunLoopMode)
        
    }
    
    
    func sayHello()
    {
        //http://voliveafrica.com/carfix/services/update_location
        let userID = UserDefaults.standard.object(forKey: "user_id")
        let details = "\(Base_Url)update_location"
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"latitude" : latitude! , "longitude" :longitude! ,"user_id" : userID ?? ""]
        print(details)
        print(parameters)
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                        print("Timer is calling.........suman guntuka")
                        
                    }
                }else
                {
                    print("Timer is Not calling.........suman guntuka")
                }
                
                
            }
        }
    }
    
    func setupLocationMarker(coordinate: CLLocationCoordinate2D) {
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
        }
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = gmapView
        locationMarker.appearAnimation =  .pop
        locationMarker.icon = GMSMarker.markerImage(with: UIColor.blue)
        locationMarker.opacity = 0.75
        locationMarker.isFlat = true
        
    }
    
}




