//
//  JobRequirementDetailsVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit

class JobRequirementDetailsVC: UIViewController {

    @IBOutlet weak var problemName_lbl: UILabel!
    
    var problemNameStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      self.problemName_lbl.text=problemNameStr
        let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        self.title = "Job Requirement Detail"
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
    }
        // Do any additional setup after loading the view.
    
func gobackFromInvoice() {
    
    self.navigationController?.popViewController(animated: true)
    
}
@IBAction func onClickBidNowBtn_Act(_ sender: Any) {
    
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let detail = storyBoard.instantiateViewController(withIdentifier: "ApplyingBidVC") as? ApplyingBidVC
   
    self.navigationController?.pushViewController(detail!, animated: true)
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
