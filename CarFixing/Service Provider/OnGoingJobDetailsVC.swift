//
//  OnGoingJobDetailsVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces

class OnGoingJobDetailsVC: UIViewController ,CLLocationManagerDelegate, GMSMapViewDelegate{
    
    @IBOutlet var currency: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var tableHieght: NSLayoutConstraint!
    @IBOutlet weak var navigateToMap: UIButton!
    @IBOutlet weak var jobImg: UIImageView!
     @IBOutlet weak var shopNameLbl: UILabel!
     @IBOutlet weak var locationLbl: UILabel!
     @IBOutlet weak var postedOnLbl: UILabel!
    @IBOutlet weak var servicesLbl: UILabel!
    var appDelegate : AppDelegate!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var compltedBtn: UIButton!
    var sericeData : [[String : AnyObject]]!
    var serviceArr = [String]()
    var priceArr = [Any]()
    var serviceIdArr = [String]()
    var currencyArr = [Any]()
   
    var long : String!
    var lat : String!
    var jobIdStr : String!
    var vendorIdStr : String!
    var imagePathStr : String!
    var typeStr : String!
    var typeStr1 : String!
    var currencyStr : String!
    var curStr : String!
   
    
     var addDicToArr :[[String: AnyObject]] = []
    
   

    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var postedOn: UILabel!
    
    @IBOutlet weak var serviceType: UILabel!
    
    @IBOutlet var priceTV: UITableView!
    
    var priceBool : Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        priceBool = false
        
        print(typeStr)
        if typeStr1 == "bills"
        {
             self.title = languageChangeString(a_str: "My Bills")
            navigateToMap.isHidden = true
        }else
        {
            
             self.title = languageChangeString(a_str: "Job Details")
            navigateToMap.isHidden = false
        }
        
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
       
        location.text = languageChangeString(a_str: "Location")
        postedOn.text = languageChangeString(a_str: "Posted on:")
        serviceType.text = languageChangeString(a_str: "Service Name")
        price.text = languageChangeString(a_str: "Price")
        currency.text = languageChangeString(a_str: "Currency")
        sendBtn.setTitle(languageChangeString(a_str: "Send"), for: UIControlState.normal)
         compltedBtn.setTitle(languageChangeString(a_str: "Complete"), for: UIControlState.normal)
        
       
        if  appDelegate.jobsSepStr == "cjobs"
        {
            sendBtn.isHidden = true
            compltedBtn.isHidden = true
          //  navigateToMap.isHidden = false
        }else
        {
            sendBtn.isHidden = false
            compltedBtn.isHidden = false
            //navigateToMap.isHidden = false
        }
        
        OngoingServiceCall()
        
        //isAuthorizedtoGetUserLocation()
        
        // Do any additional setup after loading the view.
    }
    
    //Tool bar
    
    lazy var inputToolbar: UIToolbar = {
        var toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(OnGoingJobDetailsVC.inputToolbarDonePressed))
        var flexibleSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var fixedSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        toolbar.setItems([fixedSpaceButton, fixedSpaceButton, flexibleSpaceButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        return toolbar
    }()
    
    func inputToolbarDonePressed() {
        
        
        self.view.endEditing(true)
        
    }

    
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func onClickCompletedBtn_Act(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "MyBidsVC") as? MyBidsVC
        
        self.navigationController?.pushViewController(detail!, animated: true)
    }
    @IBAction func sendBtn(_ sender: Any) {
      
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        //let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatConversationController")
        let chat = storyboard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
        print(chat?.vendorIdS ?? "")
        
        chat?.vendorIdS = self.vendorIdStr!
        chat?.profileImg = self.imagePathStr!
        
        self.navigationController?.pushViewController(chat!, animated: true)
        
    }
    
    @IBAction func compltedBtn(_ sender: Any) {
        
         getPrice()
        
        if Reachability.isConnectedToNetwork() {
            
            if priceBool == true
            {
                savePriceServiceCall()
            }else
            {
               showToastForAlert (message: languageChangeString(a_str: "Enter Price For All Services")!)
            }
        
        }else{
            
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
        
    }
    
    @IBAction func OnClickSendMessageBtn_Act(_ sender: Any) {
    
    }
    
    func OngoingServiceCall()
    {
        //http://voliveafrica.com/carfix/services/job_details_forvendor?API-KEY=98745612&job_id=3
        
        let details = "\(Base_Url)job_details_forvendor?"
        
        let userid = UserDefaults.standard.object(forKey: "user_id")

        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "job_id" : jobIdStr! ,"type" : typeStr! ,"vendor_id" : userid ?? ""]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.serviceArr = [String]()
                    self.priceArr = [String]()
                    self.serviceIdArr = [String]()
                    self.currencyArr = [String]()
                    
                    if let allJobsData = responseData["job_details"] as? Dictionary<String, AnyObject> {
                        print(allJobsData)
                       
                        let jobDetails = allJobsData["job_data"] as? Dictionary<String,AnyObject>
                        
                        let shopNameStr = jobDetails!["user_name"] as? String!
                       
                        let locationStr = jobDetails!["address"] as? String!
                        
                         self.lat = jobDetails!["latitude"] as? String!
                         self.long = jobDetails!["longitude"] as? String!
                        
                        let bookDate = jobDetails!["booked_date"] as? String!
                       
                        let image = jobDetails!["image"] as? String!
                        
                        self.vendorIdStr = jobDetails!["user_id"] as? String!
                        
                        let imagePath = base_path + "" + image!!
                       
                        self.imagePathStr = imagePath
                        
                        self.sericeData = allJobsData["job_services"] as? [[String:AnyObject]]

                        for i in self.sericeData! {
                            
                           // let isSelectStr = i["is_selected"] as? Int!
                            
                            let serviceName = i["service_name_en"] as? String
                            let priceStr = i["price"] as? Any
                            let serviceId = i ["service_id"] as? String
                          //  let currency = i["currency"] as? Any
                            
                           // UserDefaults.standard.set(self.currencyStr, forKey: "currencyVal")
                            if UserDefaults.standard.object(forKey: "currencyVal") != nil
                            {
                                self.curStr = (UserDefaults.standard.object(forKey: "currencyVal") as! String)
                            }else
                            {
                            }
                                self.serviceArr.append(serviceName!)
                                self.priceArr.append(priceStr!)
                                self.serviceIdArr.append(serviceId!)
                            
                            self.currencyArr.append(self.curStr!)
                           // }
                            
                            print(self.serviceArr)
                            print(self.serviceIdArr)
                        }
                DispatchQueue.main.async {
                            
                    self.shopNameLbl.text = shopNameStr as! String
                    self.locationLbl.text = locationStr as! String
                    self.postedOnLbl.text = bookDate as! String
                self.jobImg.sd_setImage(with: URL(string: imagePath), placeholderImage: UIImage(named:"profile pic"))
                //let IdStr = self.serviceArr.joined(separator: ",")
               // self.servicesLbl.text = IdStr
                    
                    //self.isAuthorizedtoGetUserLocation()
                    
                    self.priceTV.reloadData()
                            
                        }
                    }
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func getPrice()
    {
        
        if priceBool == true
        {
        
        var i = 0
        while i < (self.sericeData?.count)! {
            print(i)
            
            var testdic :[String:AnyObject] = [:]
           
            testdic["service_id"] = "\(self.serviceIdArr[i])" as AnyObject
           
            testdic["price"] = "\(self.priceArr[i])" as AnyObject
            
            print(testdic)
            //messages?.add("\(msgTxtView.text!)" as NSString)
            //addDicToArr.add(testdic as AnyObject)
            //addDicToArr.add(testdic)
            addDicToArr.append(testdic as [String : AnyObject])
            
            print(addDicToArr)
            i = i + 1
        }
        print("addDicToArr",addDicToArr)
        
        }else
        {
           showToastForAlert (message: languageChangeString(a_str: "Enter Price For All Services")!)
        }
        
    }
    
    func savePriceServiceCall()
    {
        
        //    let vehicles = "\(Base_Url)update_service_price"
        let vendorid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        let locale = Locale.current
        let currencyCode = locale.currencyCode!
        
        let params : [NSString : Any] = ["API-KEY": APIKEY, "job_id" : jobIdStr!  ,"services":addDicToArr , "lang" : language ?? "" ,"currency" : currencyCode]
        
        print(" my parameters are \(params)")
        
        let url = URL(string: "\(Base_Url)job_complete")!
        print ("url")
        print(url)
        print ("perams")
        print(params)
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        print(request.httpBody ?? "")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check
                return
            }
            if error != nil {
                print(error as Any)
                //Services.sharedInstance.dissMissLoader()
            } else {
                
                DispatchQueue.main.async {
                    
                    let json = try? JSONSerialization.jsonObject(with: data, options: [])
                    
                    print("res : ",  json ?? [String : AnyObject]() )
                    
                   // self.showToast(message: "Job Completed...")
                   
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                    self .present(service, animated: true, completion: nil)
                    
                    
                    
                    let responseString = String(data: data, encoding: .utf8)
                    
                    print(responseString ?? "")
                    
                }
            }
        }
        
        DispatchQueue.main.async {
            
            task.resume()
            
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        print(textField.tag)
        
        priceBool = true
        
        priceArr[textField.tag] = "\(textField.text ?? "")"
        
        print(priceArr)
        
        self.view.endEditing(true)
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
          priceBool = true
        textField.inputAccessoryView = inputToolbar
        
        return true
    }
 
    @IBAction func navigateToMapBtn(_ sender: Any) {
        
        let strLat : String = UserDefaults.standard.object(forKey: "latitude") as! String
        let strLong : String = UserDefaults.standard.object(forKey: "longitude") as! String
        
        let strLat1 : String = lat
        let strLong2 : String = long
        
        
            if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL))
                {
                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(strLat),\(strLong)&daddr=\(strLat1),\(strLong2)&directionsmode=driving&zoom=14&views=traffic")!)
                }
    }
}

extension OnGoingJobDetailsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serviceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = priceTV.dequeueReusableCell(withIdentifier: "PriceServiceCell") as! PriceServiceCell
        
        if typeStr1 == "bills"
        {
             cell.priceTF.isUserInteractionEnabled = false
        }else
        {
            // currency.isHidden = true
             cell.priceTF.isUserInteractionEnabled = true
        }
       
        cell.serviceLbl.text = serviceArr[indexPath.row]
        cell.priceTF.text = (priceArr[indexPath.row] as! String)
        cell.priceTF.tag = indexPath.row
        cell.sarLbl.text = (self.currencyArr[indexPath.row] as! String)
        
        let str = priceArr[indexPath.row] as! String
        if str == "0"
        {
            cell.priceTF.text = ""
        }
        
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
       return 44
    
}
}

