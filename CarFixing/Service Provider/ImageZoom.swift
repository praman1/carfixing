//
//  ImageZoom.swift
//  CarFixing
//
//  Created by Suman Guntuka on 02/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class ImageZoom: UIViewController ,UIScrollViewDelegate {
    
    @IBOutlet weak var collectionImg: UICollectionView!
    var imgStr : String!
    var imageCell : imgCell!
    var imageArr = [String]()

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var bidImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }

    override func viewWillAppear(_ animated: Bool) {
        
        imageArr = UserDefaults.standard.object(forKey: "bidImg") as! [String]
        print(imageArr)
        
        collectionImg.reloadData()
        
      //  bidImg.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named:""))
        
    }

    @IBAction func closeBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
extension ImageZoom:  UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = collectionImg.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as? imgCell
        
        collectionImg.isPagingEnabled = true
        
        cell?.zooIimg.sd_setImage(with: URL(string: self.imageArr[indexPath.row]), placeholderImage: UIImage(named:"placeholder"))
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
  
        return CGSize(width: self.collectionImg.frame.size.width, height:self.collectionImg.frame.size.height)
      
    }
   
}

