//
//  JobsVC.swift
//  CarFixing
//
//  Created by Suman Guntuka on 01/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.

import UIKit
import Alamofire

class JobsVC: UIViewController {

    @IBOutlet var dateTime: UILabel!
    @IBOutlet var jobtitle: UILabel!
    @IBOutlet var jobTable: UITableView!
    
    var jobNameArr = [String]()
    var jobDateArr = [String]()
    var jobCostArr = [String]()
    var jobIdArr = [String]()
    var userIdArr = [String]()
    var jobIdArr1 = [String]()
    var allJobsData : [[String : AnyObject]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        self.title = languageChangeString(a_str: "My Requests")
        
        jobtitle.text = languageChangeString(a_str: "Job Title")
        dateTime.text = languageChangeString(a_str: "Date and Time")
        //price.text = languageChangeString(a_str: "Price")
        
        vendorJobsCall()

        // Do any additional setup after loading the view.
    }
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func viewBtn(_ sender: Any) {
        
        UserDefaults.standard.set("jobsStr", forKey: "jobRequest")
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.jobTable)
        let indexPath: IndexPath? = self.jobTable.indexPathForRow(at: btnPosition)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let detail = storyBoard.instantiateViewController(withIdentifier: "JobRequestDetails") as? JobRequestDetails
        
        detail?.jobIdStr = self.jobIdArr[(indexPath?.row)!];
        detail?.userIdStr = self.userIdArr[(indexPath?.row)!];
        
        self.navigationController?.pushViewController(detail!, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
   
    func vendorJobsCall()
    {
          //http://voliveafrica.com/carfix/services/pendingjobs?API-KEY=98745612&vendor_id=30
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let details = "\(Base_Url)pendingjobs?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vendor_id" : userid ?? "" ,"lang" : language ?? ""
        ]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.allJobsData = responseData["pending_jobs"] as? [[String:AnyObject]]
                    
                    for eachjob in self.allJobsData!
                    {
                        
                        let jobNameStr = eachjob["user_name"] as? String!
                        let jobDateStr = eachjob["booked_date"] as? String!
                        let jobId = eachjob["job_id"] as? String!
                        let userId = eachjob["user_id"] as? String!
                        
                        //self.jobIdStr = eachjob["vendor_id"] as? String
                        self.jobNameArr.append(jobNameStr!!)
                        self.jobDateArr.append(jobDateStr!!)
                        self.jobIdArr.append(jobId!!)
                        self.userIdArr.append(userId!!)
                        
                    }
                   
                    self.jobTable.reloadData()
               
                }
               
                else
               
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension JobsVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return jobNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = jobTable.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobCell
            
        cell.jobLbl.text = jobNameArr[indexPath.row]
        cell.dateLbl.text = jobDateArr[indexPath.row]
       // cell.viewDetails.setTitle(languageChangeString(a_str: "View Details"), for: UIControlState.normal)
        
            return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

