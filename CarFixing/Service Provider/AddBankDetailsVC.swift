//
//  AddBankDetailsVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit
import Alamofire

class AddBankDetailsVC: UIViewController {

    @IBOutlet var scrollVW: UIScrollView!
    @IBOutlet var save: UIButton!
    @IBOutlet weak var IBANcode_TF: UITextField!
    @IBOutlet weak var swiftCode_TF: UITextField!
    @IBOutlet weak var bankAddress_TF: UITextField!
    @IBOutlet weak var branchName_TF: UITextField!
    @IBOutlet weak var bankName_TF: UITextField!
    
    var bankIdStr : String!
    
     var toolbar1: UIToolbar?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        self.scrollVW.isScrollEnabled = false
        self.save.setTitle(languageChangeString(a_str: "SAVE"), for: UIControlState.normal)
        
        self.title = languageChangeString(a_str: "Add Bank Details")
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        
        line .textfieldAsLine(myTextfield: bankName_TF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: branchName_TF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: swiftCode_TF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: bankAddress_TF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: IBANcode_TF, lineColor: myColor, myView: self.view)
        
        bankName_TF.placeholder = languageChangeString(a_str: "Bank Name")
        branchName_TF.placeholder = languageChangeString(a_str: "Branch Name")
        swiftCode_TF.placeholder = languageChangeString(a_str: "Swift Code")
        bankAddress_TF.placeholder = languageChangeString(a_str: "Bank Address")
        IBANcode_TF.placeholder = languageChangeString(a_str: "IBAN")
        
        bankDetailsServiceCall()
        addDoneButtonOnKeyboard()
      
    }
    func addDoneButtonOnKeyboard()
    {
        toolbar1 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar1?.barStyle = .blackOpaque
        toolbar1?.autoresizingMask = .flexibleWidth
        toolbar1?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelButtonAction))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar1?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar1?.setItems([ cancelButton , spaceButton, doneButton], animated: false)
        
        //self.text.inputAccessoryView = toolbar1
        bankName_TF.inputAccessoryView = toolbar1
        branchName_TF.inputAccessoryView = toolbar1
        swiftCode_TF.inputAccessoryView = toolbar1
        bankAddress_TF.inputAccessoryView = toolbar1
        IBANcode_TF.inputAccessoryView = toolbar1
        
    }
    
    func doneButtonAction()
    {
        bankName_TF.resignFirstResponder()
        branchName_TF.resignFirstResponder()
        swiftCode_TF.resignFirstResponder()
        bankAddress_TF.resignFirstResponder()
        IBANcode_TF.resignFirstResponder()
    }
    
    func cancelButtonAction()
    {
        bankName_TF.resignFirstResponder()
        branchName_TF.resignFirstResponder()
        swiftCode_TF.resignFirstResponder()
        bankAddress_TF.resignFirstResponder()
        IBANcode_TF.resignFirstResponder()
    }
    
    
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func OnClickSaveBtn_Act(_ sender: Any) {
        
        updateServiceCall()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bankDetailsServiceCall()
    {
        // //http://voliveafrica.com/carfix/services/get_bank?API-KEY=98745612&user_id=8
        
        let details = "\(Base_Url)get_bank?"
        
       let userIdStr = UserDefaults.standard.object(forKey: "user_id") as! String
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"user_id" : userIdStr ]
        print(details)
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    
                      DispatchQueue.main.async {
                    if let bankDetails = responseData["bank_details"] as? Dictionary<String, AnyObject> {
                   
                        let bankname = bankDetails["bank_name"] as? String?
                         let branch_address = bankDetails["branch_address"] as? String?
                         let branch_name = bankDetails["branch_name"] as? String?
                         let iban = bankDetails["iban"] as? String?
                         let swift_code = bankDetails["swift_code"] as? String?
                        self.bankIdStr = bankDetails["id"] as? String
                        
                        self.bankName_TF.text = bankname as! String
                        self.bankAddress_TF.text = branch_address as! String
                        self.branchName_TF.text = branch_name as! String
                        self.IBANcode_TF.text = iban as! String
                        self.swiftCode_TF.text = swift_code as! String
                    }
                       
                    Services.sharedInstance.dissMissLoader()
                    //self.showToast(message: message!)
                   
                    }
                    
                    
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }



   func updateServiceCall()
    {
        //http://voliveafrica.com/carfix/services/bank_details

       // user_id,bank_name,branch_name,branch_address,swift_code,iban

        if bankIdStr == nil
        {
            bankIdStr = ""
        }
    
    let details = "\(Base_Url)bank_details"
    
    let userIdStr = UserDefaults.standard.object(forKey: "user_id") as! String
    
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY ,"user_id" : userIdStr , "bank_name" : self.bankName_TF.text! , "branch_name" : self.branchName_TF.text! , "branch_address" : self.bankAddress_TF.text! , "swift_code" : self.swiftCode_TF.text! , "iban" : self.IBANcode_TF.text! , "bank_id" : bankIdStr!]
        
    print(details)
    
    print(parameters)
    
    Services.sharedInstance.loader(view: self.view)
    
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
        if let responseData = response.result.value as? Dictionary<String, Any>{
            
            print(responseData)
            
            let status = responseData["status"] as? Int?
            let message = responseData["message"] as? String!
            if status == 1
            {
                
                DispatchQueue.main.async {
                  
                    self.showToast(message: message!!)
                    
                    self.bankDetailsServiceCall()
                    Services.sharedInstance.dissMissLoader()
               
                }
            }
            else
            {
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
}
}

extension AddBankDetailsVC: UITextFieldDelegate{
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
      
        bankName_TF.resignFirstResponder()
        branchName_TF.resignFirstResponder()
        bankAddress_TF.resignFirstResponder()
        IBANcode_TF.resignFirstResponder()
        swiftCode_TF.resignFirstResponder()
          scrollVW.contentOffset.y = +0
        scrollVW.isScrollEnabled = false
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue: Bool = false
        if textField == bankName_TF {
            
            branchName_TF.becomeFirstResponder()
            returnValue = true
            
        }
        else if textField == branchName_TF{
            
            bankAddress_TF.becomeFirstResponder()
            returnValue = true
        }
        else if textField == bankAddress_TF{
            swiftCode_TF.resignFirstResponder()
            returnValue = true
        }
        else if textField == swiftCode_TF{
            
            IBANcode_TF.becomeFirstResponder()
            returnValue = true
        }
        else if textField == IBANcode_TF{
            IBANcode_TF.resignFirstResponder()
            returnValue = true
        }
        
        return returnValue
        
        
    }
    
        public func textFieldDidBeginEditing(_ textField: UITextField) {
    scrollVW.isScrollEnabled = true
 
             if textField == swiftCode_TF{
                scrollVW.contentOffset.y = +80
            }
            else if textField == IBANcode_TF{
                scrollVW.contentOffset.y = +120
            }

            
            
        }
    
    
    
}

