//
//  BidNowController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 02/04/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class BidNowController: UIViewController {

    
   
    @IBOutlet var pageController: UIPageControl!
    @IBOutlet var bidTitle: UILabel!
    @IBOutlet weak var bidsCollection: UICollectionView!
  
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var carDetailsLbl: UILabel!
    @IBOutlet weak var biddingPrice: UILabel!
    @IBOutlet weak var modelLbl: UILabel!
    @IBOutlet weak var makeLbl: UILabel!
     var bidImagesArr = [String]()
    var bididStr : String!
    var width : CGFloat!
    var bidImgStr : String!
    var bidsepStr : String!
    var checkAcceptString = NSString()
    
    
    var cell1 = BidsCollectCell()
    
    
    @IBOutlet weak var carSellingReq: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var biddingPrice1: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var carDetails: UILabel!
    
    @IBOutlet weak var bidNow: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        print("bid completed check",checkAcceptString)
        // Do any additional setup after loading the view.
        
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        self.title = languageChangeString(a_str: "My Bids")
        carSellingReq.text = languageChangeString(a_str: "CAR SELLING REQUEST")
        location.text = languageChangeString(a_str: "LOCATION")
        carDetails.text = languageChangeString(a_str: "CAR DETAILS")
         make.text = languageChangeString(a_str: "MAKE")
         model.text = languageChangeString(a_str: "MODEL")
         biddingPrice1.text = languageChangeString(a_str: "Bid Amount")
       
        biddingPrice1.isHidden = true
        self.biddingPrice.isHidden = true
        
        
        if checkAcceptString == "1"{
            bidNow.isHidden = false
            bidNow.setTitle(languageChangeString(a_str: "Complete"), for: UIControlState.normal)
        }

        else
        {
            if bidsepStr == "bidDetail" {
                
                bidNow.isHidden = true
            }else{
                bidNow.setTitle(languageChangeString(a_str: "APPLY"), for: UIControlState.normal)
                bidNow.isHidden = false
            }
           
        }
        
        
        if Reachability.isConnectedToNetwork()
        {
         bidDetailsCall ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
        
    }

    func gobackFromInvoice()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func bidNowBtn(_ sender: UIButton) {
    
        print("btn title name ",sender.currentTitle!)
        
        
        if sender.currentTitle == languageChangeString(a_str: "Complete"){
            bidCompleteServiceCall()
        }else{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let detail = storyBoard.instantiateViewController(withIdentifier: "ApplyingBidVC") as? ApplyingBidVC
            
            detail?.bidId = bididStr
            
            self.navigationController?.pushViewController(detail!, animated: true)
        }
       
    
    }
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func bidDetailsCall ()
    {
        
        //http://voliveafrica.com/carfix/services/bid_details?API-KEY=98745612&bid_id=2
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let details = "\(Base_Url)bid_details?API-KEY=\(APIKEY)&bid_id=\(self.bididStr!)&lang=\(language ?? "")"
        
        print(details)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    let bidsImgsData = responseData["bid_images"] as? [[String:AnyObject]]
                    
                    for i in bidsImgsData!
                    {
                        let bidImg = i["image"] as? String!
                        
                        let image = base_path + bidImg!!
                        
                        self.bidImagesArr.append(image)
                        
                    }
                    
                    //self.pageController.numberOfPages = self.bidImagesArr.count
                    
                    print(self.bidImagesArr)
                    
                    if let bid_detailsData = responseData["bid_details"] as? Dictionary<String, AnyObject> {
                        
                        let location = bid_detailsData["address"] as? String!
                        let carDetails = bid_detailsData["description"] as? String!
                        let make = bid_detailsData["make"] as? String!
                        let model = bid_detailsData["model"] as? String!
                        let price = bid_detailsData["price"] as? String!
                        let title = bid_detailsData["title"] as? String!
                        
                        self.locationLbl.text = location as! String
                        self.carDetailsLbl.text = carDetails as! String
                        self.makeLbl.text = make as! String
                        self.modelLbl.text = model as! String
                        self.biddingPrice.text = price as! String
                        self.bidTitle.text = title as! String
                        
                    }
                    
                    self.bidsCollection.reloadData()
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    
    //MARK:- BIDCOMPLETE SERVICECALL
    func bidCompleteServiceCall()
    {

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        
        let details = "\(Base_Url)bid_complete?"
        print("bid complete url",details)
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vendor_id" : userid ?? "" ,"b_id" :self.bididStr!, "lang" : language ?? ""]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                let status = responseData["status"] as? Int!
               let message = responseData["message"] as? String!
                if status == 1
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                    
                  
                }
                else
                {
                     self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension BidNowController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.bidImagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.bidsCollection.dequeueReusableCell(withReuseIdentifier: "cellBid", for: indexPath) as! BidsCollectCell
        
        // cell.img.image = UIImage(named:collectionImages[indexPath.row])
        
        cell.bidImg1.sd_setImage(with: URL(string: self.bidImagesArr[indexPath.row]), placeholderImage: UIImage(named:"as img4"))
        
        // self.pageControl.currentPage = indexPath.row
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        width = CGFloat(bidsCollection.frame.size.width / 3)
        return CGSize(width: width, height: 100)
        
     //   return CGSize(width: collectionView.frame.size.width,height:  collectionView.frame.size.height)
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//
//        if collectionView == self.bidsCollection {
//
//            self.pageController.currentPage = indexPath.row
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let service = storyBoard.instantiateViewController(withIdentifier: "ImageZoom1")
        
          // bidImgStr = self.bidImagesArr
        
            UserDefaults.standard.set(self.bidImagesArr, forKey: "bidImg")
        
            self .present(service, animated: true, completion: nil)
        
    }

    
}

