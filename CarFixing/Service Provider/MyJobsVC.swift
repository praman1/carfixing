//
//  MyJobsVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/15/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit
import Alamofire
class MyJobsVC: UIViewController {
 
    var appde = AppDelegate()
    
    @IBOutlet var pview: UIView!
    @IBOutlet weak var myJobs_tableView: UITableView!
    @IBOutlet weak var completedJobs_view: UIView!
    @IBOutlet weak var ongoingJobs_view: UIView!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var compltedJobs: UIButton!
    @IBOutlet weak var ongoingJobs: UIButton!
    var jobNameArr = [String]()
    var jobDateArr = [String]()
    var jobCostArr = [String]()
    var jobIdArr = [String]()
    var jobIdArr1 = [String]()
    var allJobsData : [[String : AnyObject]]!
    var compltedData : [[String : AnyObject]]!
    var completJobNameArr = [String]()
    var completJobDateArr = [String]()
    var completJobCostArr = [String]()
    var latArr = [String]()
    var longArr = [String]()
    var userIdArr = [String]()
    var currencyArr = [String]()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
         appde = UIApplication.shared.delegate as! AppDelegate
        
         compltedJobs.setTitle(languageChangeString(a_str: "Completed Jobs"), for: UIControlState.normal)
         ongoingJobs.setTitle(languageChangeString(a_str: "Ongoing Jobs"), for: UIControlState.normal)
        jobTitle.text = languageChangeString(a_str: "Job Title")
        dateTime.text = languageChangeString(a_str: "Date and Time")
        price.text = languageChangeString(a_str: "Price")
        
       appde.jobsSepStr = "ojobs"

        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        self.title = languageChangeString(a_str: "Automobile Repair Requests")
        
//       Do any additional setup after loading the view.
//       jobNameArr = ["Need car clutcher repair","Need air conditioner repair","Need Break rod         repair","Need Engine oil repair"]
//       jobDateArr = ["1 st november 2017 11.30 AM","5th november 2017 1.30 PM","9th november    2017 09.30 AM","10th november 2017 10.30 AM"]
//       jobCostArr = ["100 SAR","200 SAR","300 SAR","400 SAR"]
        
        self.ongoingJobs_view.isHidden=false
        self.completedJobs_view.isHidden=true
        price.isHidden = true
        pview.isHidden = true

        myJobs_tableView.rowHeight = UITableViewAutomaticDimension
        myJobs_tableView.estimatedRowHeight = 45
        
        if Reachability.isConnectedToNetwork() {
            vendorJobsCall()

        }else{
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
    }
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CLICK DETAILS BTN
    
    @IBAction func onClickDetailsBtn_Act(_ sender: Any) {
   
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.myJobs_tableView)
        let indexPath1: IndexPath? = self.myJobs_tableView.indexPathForRow(at: btnPosition)
        
        if appde.onsiteStr == "tow"
        {
              UserDefaults.standard.set("towList", forKey: "towingSep")
            UserDefaults.standard.set(latArr[(indexPath1?.row)!], forKey: "towlat")
            UserDefaults.standard.set(longArr[(indexPath1?.row)!], forKey: "towlong")
            UserDefaults.standard.set(self.userIdArr[(indexPath1?.row)!], forKey: "userId")
            UserDefaults.standard.set(self.jobIdArr[(indexPath1?.row)!], forKey: "towingId")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let detail = storyBoard.instantiateViewController(withIdentifier: "VendorTowingMap") as? VendorTowingMap
            detail?.jobUserId = self.userIdArr[(indexPath1?.row)!] as NSString
            print("user id @my jobs",self.userIdArr[(indexPath1?.row)!])
            self.navigationController?.pushViewController(detail!, animated: true)
        }else
        {
      
        if appde.jobsSepStr == "ojobs"{
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detail = storyBoard.instantiateViewController(withIdentifier: "OnGoingJobDetailsVC") as? OnGoingJobDetailsVC
            detail?.typeStr = "job"
            detail?.typeStr1 = "jobs"
        detail?.jobIdStr = self.jobIdArr[(indexPath1?.row)!];
        self.navigationController?.pushViewController(detail!, animated: true)
            
        }else
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let detail = storyBoard.instantiateViewController(withIdentifier: "OnGoingJobDetailsVC") as? OnGoingJobDetailsVC
            
             detail?.typeStr = "job"
             detail?.typeStr1 = "jobs"
            detail?.jobIdStr = self.jobIdArr1[(indexPath1?.row)!];
            
            self.navigationController?.pushViewController(detail!, animated: true)
        }
        }
    }
   
    @IBAction func completedJobsBtn_Act(_ sender: Any) {
        
        appde.jobsSepStr = "cjobs"
        
        self.ongoingJobs_view.isHidden=true
        self.completedJobs_view.isHidden=false
         price.isHidden = false
        pview.isHidden = false
        
        
        self.myJobs_tableView.reloadData()
    }
    @IBAction func onGoingJobsBtn_Act(_ sender: Any) {
        
        appde.jobsSepStr = "ojobs"
        
        self.ongoingJobs_view.isHidden=false
        self.completedJobs_view.isHidden=true
         price.isHidden = true
        pview.isHidden = true
         self.myJobs_tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- VENDOR JOB SERVICE CALL
    func vendorJobsCall()
    {
        // http://voliveafrica.com/carfix/services/vendor_joblist

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        let details = "\(Base_Url)vendor_joblist"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vendor_id" : userid ?? "" ,"lang" : language ?? ""]
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.completJobCostArr = [String]()
                    
                    self.allJobsData = responseData["ongoing_jobs"] as? [[String:AnyObject]]

                    for eachjob in self.allJobsData!
                    {

                        let jobNameStr = eachjob["user_name"] as? String!
                        let jobDateStr = eachjob["booked_date"] as? String!
                        let jobPrice = eachjob["price"] as? String
                        let jobId = eachjob["job_id"] as? String!
                        let lat = eachjob["latitude"] as? String!
                        let long = eachjob["longitude"] as? String!
                        let useridStr = eachjob["user_id"] as? String!
                        let towStr = eachjob["towing_ios"] as? String!
                      

                        //self.jobIdStr = eachjob["vendor_id"] as? String
                        self.jobNameArr.append(jobNameStr!!)
                        self.jobDateArr.append(jobDateStr!!)
                        self.jobCostArr.append(jobPrice!)
                        self.jobIdArr.append(jobId!!)
                        self.latArr.append(lat!!)
                        self.longArr.append(long!!)
                        self.userIdArr.append(useridStr!!)
                     
                        
                    }
                    self.compltedData = responseData["completed_jobs"] as? [[String:AnyObject]]
                  
                    for i in self.compltedData!
                    {
                        let jobNameStr1 = i["user_name"] as? String!
                        let jobDateStr1 = i["booked_date"] as? String!
                        let jobPrice = i["price"] as? String!
                        let jobId1 = i["job_id"] as? String!
                        let curency = i["currency"] as? String!
                       
  
                        self.completJobNameArr.append(jobNameStr1!!)
                        self.completJobDateArr.append(jobDateStr1!!)
                        self.completJobCostArr.append(jobPrice!!)
                        self.jobIdArr1.append(jobId1!!)
                        self.currencyArr.append(curency!!)
                    }

                    self.myJobs_tableView.reloadData()
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}
extension MyJobsVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if appde.jobsSepStr == "ojobs"{
          
            return jobNameArr.count
        }else
        {
             return completJobNameArr.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if appde.jobsSepStr == "ojobs"{
        
        let cell = myJobs_tableView.dequeueReusableCell(withIdentifier: "myJobsCell", for: indexPath) as! MyBidsOrJobsTableViewCell
        
        cell.jobName_lbl.text = jobNameArr[indexPath.row]
            
        cell.details.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
        
            cell.price_lbl.isHidden = true
          //  cell.price_lbl.text = String(format: "%@ %@",currencyArr[indexPath.row],jobCostArr[indexPath.row])
       
            cell.jobdate_lbl.text = String(format: "%@", jobDateArr[indexPath.row])
            
       if appde.onsiteStr == "tow"
       {
        cell.details.isHidden = false
         cell.details.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
        
        
        }else
       {
        cell.details.isHidden = false
        cell.details.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
       }
        return cell
        
        }else
        {
            let cell = myJobs_tableView.dequeueReusableCell(withIdentifier: "myJobsCell", for: indexPath) as! MyBidsOrJobsTableViewCell
            
            cell.price_lbl.isHidden = false
            cell.jobName_lbl.text = completJobNameArr[indexPath.row]
            cell.details.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
          //  cell.price_lbl.text = String(format: "%@ %@","SAR", completJobCostArr[indexPath.row])
            cell.price_lbl.text = String(format: "%@ %@",currencyArr[indexPath.row], completJobCostArr[indexPath.row])
          
            cell.jobdate_lbl.text = String(format: "%@", completJobDateArr[indexPath.row])
            
            
            if appde.onsiteStr == "tow"
            {
               cell.details.isHidden = true
             
                
            }else
            {
                cell.details.isHidden = false
               cell.details.setTitle(languageChangeString(a_str: "Details"), for: UIControlState.normal)
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
         if appde.jobsSepStr == "ojobs"{
            
            if appde.onsiteStr == "tow"
            {
                UserDefaults.standard.set("towList", forKey: "towingSep")
                
                UserDefaults.standard.set(latArr[(indexPath.row)], forKey: "towlat")
                UserDefaults.standard.set(longArr[(indexPath.row)], forKey: "towlong")
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let detail = storyBoard.instantiateViewController(withIdentifier: "VendorTowingMap") as? VendorTowingMap
                self.navigationController?.pushViewController(detail!, animated: true)
            
            }else
            {
                let detail = storyBoard.instantiateViewController(withIdentifier: "OnGoingJobDetailsVC") as? OnGoingJobDetailsVC
                
                detail?.jobIdStr = self.jobIdArr[indexPath.row];
                
                appde.jobsSepStr = "ojobs"
                
                self.navigationController?.pushViewController(detail!, animated: true)
            }
        
            
        }else
         {
            if appde.onsiteStr == "tow"
            {
//                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                let detail = storyBoard.instantiateViewController(withIdentifier: "VendorTowingMap") as? VendorTowingMap
//                self.navigationController?.pushViewController(detail!, animated: true)
            
            }else
            {
            let detail = storyBoard.instantiateViewController(withIdentifier: "OnGoingJobDetailsVC") as? OnGoingJobDetailsVC
            
            detail?.jobIdStr = self.jobIdArr1[indexPath.row];
            appde.jobsSepStr = "cjobs"
            
            self.navigationController?.pushViewController(detail!, animated: true)
            }
        }
        
    }

}

