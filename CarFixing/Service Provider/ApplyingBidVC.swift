//
//  ApplyingBidVC.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.

import UIKit
import Alamofire

class ApplyingBidVC: UIViewController {

    @IBOutlet weak var enterMessage_TF: UITextField!
    
    @IBOutlet weak var enterAmount_TF: UITextField!
    
    @IBOutlet weak var time_TF: UITextField!
    var bidId : String!
    
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var enterAmount: UILabel!
    @IBOutlet weak var enterMsg: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        let rightButton = UIBarButtonItem (image: UIImage.init(named: "notification"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (goNotificationVC))
        rightButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightButton
        self.title = languageChangeString(a_str: "My Bids")
        enterMsg.text = languageChangeString(a_str: "Message to Customer")
        enterAmount.text = languageChangeString(a_str: "Enter Amount")
        submit.setTitle(languageChangeString(a_str: "SUBMIT"), for: UIControlState.normal)
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: enterAmount_TF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: enterMessage_TF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: time_TF, lineColor: myColor, myView: self.view)
        
        
        enterAmount_TF.placeholder="Enter your bid Amount"
        time_TF.placeholder="Enter Time"
        enterMessage_TF.placeholder="Message"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    func goNotificationVC() {
        
       // self.navigationController?.popViewController(animated: true)
        
    }
    

    @IBAction func onClickSubmitBtn_Act(_ sender: Any) {

        if Reachability.isConnectedToNetwork()
        {
            applyBidNow ()
        }else
        {
             showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
        
        
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let homeVC = storyboard.instantiateViewController(withIdentifier: "AllBidsVC")
//        self.navigationController?.pushViewController(homeVC, animated: true)

        
    }
    func applyBidNow ()
    {
        
        //http://voliveafrica.com/carfix/services/auction_bid // vendor_id,message,amount,bid_id
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let details = "\(Base_Url)auction_bid"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "vendor_id" : userid ?? "" , "message" : self.enterMessage_TF.text! , "amount" : enterAmount_TF.text! , "bid_id" : self.bidId! , "lang" : language ?? "" ]
        
        print("bid params",parameters);
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    

                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                    self .present(service, animated: true, completion: nil)
                   
                    //self.showToast(message: message!)
                    
//                    let bidsImgsData = responseData["bid_images"] as? [[String:AnyObject]]
//
//                    for i in bidsImgsData!
//                    {
//                        let bidImg = i["image"] as? String!
//
//                        let image = base_path + bidImg!
//
//                        self.bidImagesArr.append(image)
//
//                    }
                   
//                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                            let homeVC = storyboard.instantiateViewController(withIdentifier: "AllBidsVC")
//                            self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension ApplyingBidVC: UITextFieldDelegate{
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        enterAmount_TF.resignFirstResponder()
        enterMessage_TF.resignFirstResponder()
        time_TF.resignFirstResponder()
       
        //scroll.contentOffset.x = 0
       // scroll.contentOffset.y = 0
        
        
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue: Bool = false
        if textField == enterMessage_TF {
            
            enterAmount_TF.becomeFirstResponder()
            returnValue = true
            
        }
        else if textField == enterAmount_TF{
            
            time_TF.resignFirstResponder()
            returnValue = true
        }
        else if textField == time_TF{
            time_TF.resignFirstResponder()
            returnValue = true
        }
        
        
        return returnValue
        
        
    }
    
//    public func textFieldDidBeginEditing(_ textField: UITextField) {
//        
//        if textField == usernameText {
//            scroll.contentOffset.y = +20
//        }
//        else if textField == emailText{
//            scroll.contentOffset.y = +60
//        }
//        else if textField == mobileNumberText{
//            scroll.contentOffset.y = +100
//        }
//        else if textField == passwordText{
//            scroll.contentOffset.y = +140
//        }
//        else if textField == confirmText{
//            scroll.contentOffset.y = +160
//        }else if textField == locationText{
//            scroll.contentOffset.y = +200
//        }
//        
//        
//        
//    }
    
    
    
}

