//
//  SuccessReviewController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 13/12/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//
import UIKit
import Alamofire
import Cosmos

class SuccessReviewController: UIViewController {
    @IBOutlet weak var titleTf: UITextField!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var shopAddressLbl: UILabel!
    @IBOutlet weak var headline: UILabel!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var rateView: SwiftyStarRatingView!
    
    var checkStr = NSString()
    var rating: Int!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var reviewTF: UITextField!
    @IBOutlet weak var shopImg: UIImageView!
    var msgStr : String!
    var nameStr : String!
    var addressStr : String!
    var imgStr : String!
    var jobid : String!
    var value = NSString()
    
    @IBOutlet weak var writeReviewTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        rateView.didFinishTouchingCosmos = didFinishTouchingCosmos
//        rateView.didTouchCosmos = didTouchCosmos
//
//        rateView.didFinishTouchingCosmos = didFinishTouchingCosmos
//        rateView.didFinishTouchingCosmos = didFinishTouchingCosmos
//        rateView.didFinishTouchingCosmos = didFinishTouchingCosmos
        
        headline.text = languageChangeString(a_str: "HEADLINE FOR YOUR REVIEW")
        review.text = languageChangeString(a_str: "WRITE YOUR REVIEW HERE")
        if checkStr == "1"{
            msgStr =  UserDefaults.standard.object(forKey: "message") as! String
            nameStr = UserDefaults.standard.object(forKey: "shopname") as! String
            addressStr = UserDefaults.standard.object(forKey: "address") as! String
            imgStr =  UserDefaults.standard.object(forKey: "image") as! String
            jobid = UserDefaults.standard.object(forKey: "bid_id") as! String
        }else{
            msgStr =  UserDefaults.standard.object(forKey: "message") as! String
            nameStr = UserDefaults.standard.object(forKey: "shopname") as! String
            addressStr = UserDefaults.standard.object(forKey: "address") as! String
            imgStr =  UserDefaults.standard.object(forKey: "image") as! String
            jobid = UserDefaults.standard.object(forKey: "job_id") as! String
        }
       
         
        msgLbl.text = msgStr!
        shopNameLbl.text = nameStr!
        shopAddressLbl.text = addressStr!
        
         shopImg.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named:"as img1"))
        
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"close"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.title = languageChangeString(a_str: "Service Completed")
        let line = LineTextField ()
        line .textfieldAsLine(myTextfield: titleTf, lineColor: UIColor.lightGray, myView: self.view)
        line .textfieldAsLine(myTextfield: reviewTF, lineColor: UIColor.lightGray, myView: self.view)

        // Do any additional setup after loading the view.
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
//        self.view.addGestureRecognizer(tapGesture)
        
    }
    func dismissKeyBoard()
    {
        
        titleTf.resignFirstResponder()
         reviewTF.resignFirstResponder()
        
    }
//    private func didFinishTouchingCosmos(_ rating: Double) {
//
//    }
//    private func didTouchCosmos(_ rating: Double) {
//
//        self.rating = Int(rating)
//        print(self.rating)
//
//    }
   
    //MARK:- SUBMIT BTN ACTION
    @IBAction func submitBtn(_ sender: Any) {
        
        value = NSString(format: "%.2f", self.rateView.value)
        
        
        if checkStr == "1"{
            if Reachability.isConnectedToNetwork()
            {
              
                print("value rating",value)
                if value.contains("0.00"){
                    showToastForAlert (message: languageChangeString(a_str: "Please give rating")!)
                    
                }else if self.titleTf.text?.count == 0{
                    showToastForAlert (message: languageChangeString(a_str: "Please enter review header")!)
                    
                }else if self.reviewTF.text?.count  == 0 {
                    showToastForAlert (message: languageChangeString(a_str: "Please enter review")!)
                    
                } else{
                     reviewSubmitScrap ()
                }
               
            }else
            {
                showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            }
            
        }else{
            if Reachability.isConnectedToNetwork()
            {
                
                 print("value rating",value)
                if value.contains("0.00"){
                    showToastForAlert (message: languageChangeString(a_str: "Please give rating")!)
                    
                } else if self.titleTf.text?.count == 0{
                    showToastForAlert (message: languageChangeString(a_str: "Please enter review header")!)
                    
                }else if self.reviewTF.text?.count  == 0 {
                    showToastForAlert (message: languageChangeString(a_str: "Please enter review")!)
                    
                }else{
                    reviewSubmitCall ()
                }
               
            }else
            {
                showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            }
        }
        
        
        
    }
    @objc func backBtnClicked (){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SUBMIT REVIEW SERVICE CALL
    func reviewSubmitCall ()
    {
        //http://voliveafrica.com/carfix/services/submit_rating
        //job_id, user_id,rating,review,review_heading
        
        
        let value = NSString(format: "%.2f", self.rateView.value)
        
        
//        let xNSNumber = self.rating as NSNumber
//        let xString : String = xNSNumber.stringValue
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        jobid = UserDefaults.standard.object(forKey: "job_id") as! String
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        
        let vehicles = "\(Base_Url)submit_rating"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "job_id" : jobid! ,"user_id" :  userid ?? "" ,"rating" : value , "review" :self.reviewTF.text! , "review_heading" : self.titleTf.text! , "lang" : language ?? "" ]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    //self.servicesArr = responseData["services"] as! Array<String>
                    
                    DispatchQueue.main.async {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
                        self .present(tab, animated: true, completion: nil)
                   
                    }
                  }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    //MARK:- submit_rating_scrap
    func reviewSubmitScrap ()
    {
        //http://voliveafrica.com/carfix/services/submit_rating_scrap
        //job_id, user_id,rating,review,review_heading
        
//        let xNSNumber = self.rating as NSNumber
//
//        let xString : String = xNSNumber.stringValue
           let value = NSString(format: "%.2f", self.rateView.value)
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        jobid = UserDefaults.standard.object(forKey: "bid_id") as! String
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        
        let vehicles = "\(Base_Url)submit_rating_scrap"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "bid_id" : jobid! ,"user_id" :  userid ?? "" ,"rating" : value, "review" :self.reviewTF.text! ,"review_heading" : self.titleTf.text!,"lang" : language ?? "" ]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    //self.servicesArr = responseData["services"] as! Array<String>
                    
                    DispatchQueue.main.async {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
                        self .present(tab, animated: true, completion: nil)
                        
                    }
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension SuccessReviewController : UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        var returnValue : Bool = false
        
        if textField == titleTf {
            
            titleTf.becomeFirstResponder()
            
            returnValue = true
        }
        else if textField == reviewTF{
            reviewTF.becomeFirstResponder()
            returnValue = true
        }
        textField.resignFirstResponder()
        return returnValue
        
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
         scroll.isScrollEnabled = true
        if textField == titleTf{
             scroll.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y - 80), animated: true)
           // scroll.contentOffset.y = +40
        }
        else if textField == reviewTF{
             scroll.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y - 40), animated: true)
            //scroll.contentOffset.y = +120
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField){
        
        titleTf.resignFirstResponder()
        reviewTF.resignFirstResponder()
        scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        //scroll.isScrollEnabled = false
//        scroll.contentOffset.x = 0
//        scroll.contentOffset.y = 0
        
    }
}

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


