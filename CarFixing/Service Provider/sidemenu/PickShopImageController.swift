//
//  PickShopImageController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 22/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//
import UIKit
import Alamofire


class PickShopImageController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var shopImg: UIImageView!
    var pickerImage  = UIImage()
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var addImg: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        shopImg.layer.cornerRadius = 10.0
        
        addImg.setTitle(languageChangeString(a_str: "Add Photo!"), for: UIControlState.normal)
        
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
   
    
    @IBAction func addBtn(_ sender: Any) {
//        imagePicker.allowsEditing = false
//        imagePicker.sourceType = .photoLibrary
//        present(imagePicker, animated: true, completion: nil)
        
    }
    @IBAction func updateBtn(_ sender: Any) {
       
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
       
        // self.addGallery ()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addGallery ()
    {
        //voliveafrica.com/carfix/services/update_gallery//user_id,gallery_image
         let userid = UserDefaults.standard.object(forKey: "user_id")
        
       // Services.sharedInstance.loader(view: self.view)

        let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "user_id" : userid ?? "" ]
        
        let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "gallery_image",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:"\(Base_Url)update_gallery")
        { (result) in
            
            print(result)
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value ?? "")
                    
                    let responseData = response.result.value as? Dictionary<String, Any>
                    
                    let status = responseData!["status"] as! Int
                    let message = responseData!["message"] as! String
                    
                    if status == 1
                    {
                        self.showToast(message: message)
                       // Services.sharedInstance.dissMissLoader()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gellery"), object: nil)
                        
                        self.dismiss(animated: false, completion: nil)
                        
                    }
                    else
                    {
                        self.showToast(message: message)
                       // Services.sharedInstance.dissMissLoader()
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    }

extension PickShopImageController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        let pickedImage2 = info[UIImagePickerControllerOriginalImage]
        shopImg.image = pickedImage2  as? UIImage
        pickerImage = (pickedImage2 as? UIImage)!
        
        self.addGallery ()
        
        self.dismiss(animated: false, completion: nil)
        
        self.navigationController?.isNavigationBarHidden = true
        
        
        
        
    }
}
