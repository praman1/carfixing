//
//  AvailablePriceController.swift
//  CarFixing
//
//  Created by Suman Volive on 2/15/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class AvailablePriceController: UIViewController {
    
    @IBOutlet weak var priceTable: UITableView!
    var serviceNameArr = [String]()
    var rowsArr = [String]()
    var priceDataArr: [[String:AnyObject]]!
    var priceArr = [String]()
    var serviceIdArr = [String]()
     //var addDicToArr : NSMutableArray = []
    
    var addDicToArr :[[String: AnyObject]] = []
    var cell1 : PriceCell?
   
    override func viewDidLoad() {
        super.viewDidLoad()
   
        //serviceNameArr = [ "Service1","Service2","Service3","Service4","Service5"]
        //rowsArr = [ "Service1","Service2","Service3","Service4","Service5" , ""]
        
        let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
       
        self.title = languageChangeString(a_str: "Available Price")
       
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        if Reachability.isConnectedToNetwork()
        {
              getPriceService()
        }else{
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
    }
    
    //Tool bar
    
    lazy var inputToolbar: UIToolbar = {
        var toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(AvailablePriceController.inputToolbarDonePressed))
        var flexibleSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var fixedSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
       
        toolbar.setItems([fixedSpaceButton, fixedSpaceButton, flexibleSpaceButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        return toolbar
    }()
    
    func inputToolbarDonePressed() {
        
     
        self.view.endEditing(true)
        
    }

    
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        
        getPrice()
        if Reachability.isConnectedToNetwork()
        {
            savePriceServiceCall()
        }else{
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
        
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func getPriceService()
    {
        let vendor_id = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let vehicles = "\(Base_Url)vendor_services_list?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "vendor_id" : vendor_id ?? "" , "lang" : language ?? ""
]
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.priceDataArr = responseData["vendor_services"] as? [[String:AnyObject]]
                    
                     for eachitem in self.priceDataArr! {
                        
                        let price = eachitem["price"] as? String!
                        let serviceName = eachitem["service_name_en"] as? String!
                        let nameId = eachitem["id"] as? String!
                        let serviceId = eachitem["service_id"] as? String!
                        
                        self.serviceNameArr.append(serviceName!!)
                        
                        self.rowsArr.append(nameId!!)
                        self.priceArr.append(price!!)
                        self.serviceIdArr.append(serviceId!!)
                        
                    }
                    self.rowsArr.append("")
                    
                    self.priceTable.reloadData()
                        
                    }
                   // self.getPrice()
                }
                else
                {
                    DispatchQueue.main.async {
                        
                    Services.sharedInstance.dissMissLoader()
                    //self.showToast(message: message!)
                    }
                }
            }
        }
    }

    func getPrice()
    {
        
        var i = 0
       
        while i < (self.priceDataArr?.count)! {
            print(i)
            var testdic :[String:AnyObject] = [:]
            testdic["service_id"] = "\(self.serviceIdArr[i])" as AnyObject
            testdic["price"] = "\(self.priceArr[i])" as AnyObject
            
            print(testdic)
            //messages?.add("\(msgTxtView.text!)" as NSString)
            //addDicToArr.add(testdic as AnyObject)
            //addDicToArr.add(testdic)
            addDicToArr.append(testdic as [String : AnyObject])
            
            print(addDicToArr)
            i = i + 1
        }
         print("addDicToArr",addDicToArr)
        
    }
func savePriceServiceCall()
{

//    let vehicles = "\(Base_Url)update_service_price"
     let vendorid = UserDefaults.standard.object(forKey: "user_id")
    let language = UserDefaults.standard.object(forKey: "currentLanguage")
    
    let params : [NSString : Any] = ["API-KEY":98745612,"user_id":vendorid ?? "" ,"prices":addDicToArr , "lang" : language ?? ""]
    
    print(" my parameters are \(params)")
    
  //  Services.sharedInstance.loader(view: self.view)

    
    let url = URL(string: "\(Base_Url)update_service_price")!
    print ("url")
    print(url)
    print ("perams")
    print(params)
    
    var request = URLRequest(url: url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
    print(request.httpBody ?? "")
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {                                                 // check
            return
        }
        if error != nil {
            print(error as Any)
        } else {
            
            DispatchQueue.main.async {
 
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            
            print("res : ",  json ?? [String : AnyObject]() )
            
            self.showToast(message: "Prices are updated successfully...")
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString ?? "")
                
            }
        }
    }
    
     DispatchQueue.main.async {
    
    task.resume()
        
    }
    
    }
    
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
      
        print(textField.tag)
        
        priceArr[textField.tag] = "\(textField.text ?? "")"
        
        print(priceArr)
        
        self.view.endEditing(true)
   
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputAccessoryView = inputToolbar
        
        return true
    }
   
}

extension AvailablePriceController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rowsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == rowsArr.count - 1 {
            
            let cell = priceTable.dequeueReusableCell(withIdentifier: "PriceCell1") as! PriceCell
            
            //cell1?.submitBtn.isEnabled = true
             cell.submitBtn.setTitle(languageChangeString(a_str: "SUBMIT"), for: UIControlState.normal)
            
            return cell
        }else{
         
            
            cell1 = priceTable.dequeueReusableCell(withIdentifier: "PriceCell") as? PriceCell

            cell1?.serviceTypeLbl.text = serviceNameArr[indexPath.row]

            cell1?.priceLbl.text = priceArr[indexPath.row]
            
            cell1?.priceLbl.tag = indexPath.row
            

            return  (cell1)!
            
        }
       
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == serviceNameArr.count - 1 {
        
        return 67
        
        }else
        {
             return 67
        }
        
    }
 
}




