//
//  AvailableHoursController.swift
//  CarFixing
//
//  Created by Suman Volive on 2/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class AvailableHoursController: UIViewController {

    @IBOutlet weak var mondayToTF: UITextField!
    @IBOutlet weak var mondayFromTF: UITextField!
    @IBOutlet weak var tuesdayToTF: UITextField!
    @IBOutlet weak var tuesdayFromTF: UITextField!
    @IBOutlet weak var wednesdayToTF: UITextField!
    @IBOutlet weak var wednesdayFromTF: UITextField!
    @IBOutlet weak var thursdayToTF: UITextField!
    @IBOutlet weak var thursdayFromTF: UITextField!
    @IBOutlet weak var fridayToTF: UITextField!
    @IBOutlet weak var fridayFromTF: UITextField!
    @IBOutlet weak var saturdayToTF: UITextField!
    @IBOutlet weak var saturdayFromTF: UITextField!
    @IBOutlet weak var sundayToTF: UITextField!
    @IBOutlet weak var sundayFromTF: UITextField!
  
    
    @IBOutlet weak var monday: UILabel!
    @IBOutlet weak var tuesday: UILabel!
    @IBOutlet weak var wednesday: UILabel!
    @IBOutlet weak var thursday: UILabel!
    @IBOutlet weak var friday: UILabel!
    @IBOutlet weak var saturday: UILabel!
    @IBOutlet weak var sunday: UILabel!
    
    
    @IBOutlet weak var save: UIButton!
    var datepicker: UIDatePicker?
    var toolbar: UIToolbar?
    var selectedTextField: UITextField?
    var TFsArray: NSMutableArray = []
     var hoursArray: NSMutableArray = []
    
    var daysArr = [String] ()
    var timingsDataArr: [[String:AnyObject]]!
    var fromTimeArr = [String]()
    var toTimeArr = [String]()
    var finalTimeArr = [String]()
    var countyPickerData = [String]()
 
    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        self.title = languageChangeString(a_str: "Available Timings")
        save.setTitle(languageChangeString(a_str: "SAVE"), for: UIControlState.normal)
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        daysArr = [languageChangeString(a_str: "Monday") ,languageChangeString(a_str: "Tuesday") , languageChangeString(a_str: "Wednesday") ,languageChangeString(a_str: "Thursday") ,languageChangeString(a_str: "Friday") , languageChangeString(a_str: "Saturday") ,languageChangeString(a_str: "Sunday")] as! [String]
        
        monday.text = daysArr[0]
        tuesday.text = daysArr[1]
        wednesday.text = daysArr[2]
        thursday.text = daysArr[3]
        friday.text = daysArr[4]
        saturday.text = daysArr[5]
        sunday.text = daysArr[6]
        
//        fromTimeArr = ["-","-","-","-","-","-","-"]
//        toTimeArr = ["-","-","-","-","-","-","-"]
        
//        mondayToTF.text = "NA"
//        mondayFromTF.text = "NA"
//        tuesdayToTF.text = "NA"
//        tuesdayFromTF.text = "NA"
//        wednesdayToTF.text = "NA"
//        wednesdayFromTF.text = "NA"
//        thursdayToTF.text = "NA"
//        thursdayFromTF.text = "NA"
//        fridayToTF.text = "NA"
//        fridayFromTF.text = "NA"
//        saturdayToTF.text = "NA"
//        saturdayFromTF.text = "NA"
//        sundayToTF.text = "NA"
//        sundayFromTF.text = "NA"
        TFsArray = [mondayToTF, mondayFromTF, tuesdayToTF, tuesdayFromTF, wednesdayToTF, wednesdayFromTF, thursdayToTF, thursdayFromTF, fridayFromTF,fridayToTF, thursdayFromTF, saturdayToTF, saturdayFromTF, sundayToTF, sundayFromTF]
        hoursArray = ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
        
        datepicker = UIDatePicker()
        datepicker?.datePickerMode = .time
        datepicker?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (datepicker?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AvailableHoursController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AvailableHoursController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        if Reachability.isConnectedToNetwork()
        {
            getHoursService()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
        
        
        // Do any additional setup after loading the view.
    }
    
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        
        if  Reachability.isConnectedToNetwork() {
            
             saveTimeServiceCall()
        }else{
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
       
        
    }
    func donePicker ()
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        selectedTextField?.text = dateFormatter.string(from: datepicker!.date)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "HH"
        let index = Int(TFsArray.index(of: selectedTextField ?? ""))
        
        hoursArray[index] = dateFormatter2.string(from: datepicker!.date)
        selectedTextField?.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        selectedTextField?.text = "NA"
        let index = Int(TFsArray.index(of: selectedTextField ?? ""))
        hoursArray[index ] = "NA"
        selectedTextField?.resignFirstResponder()
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputView = datepicker
        textField.inputAccessoryView = toolbar
        selectedTextField = textField
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if (textField.text == "") {
            //textField.text = "NA"
            let index = Int(TFsArray.index(of: textField))
            //hoursArray[index] = "NA"
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getHoursService()
    {
        let vendor_id = UserDefaults.standard.object(forKey: "user_id")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        
        let vehicles = "\(Base_Url)vendor_timings?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "vendor_id" : vendor_id ?? "" ,"lang" : language ?? ""
]
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.timingsDataArr = responseData["vendor_timings"] as? [[String:AnyObject]]

//                    let pathStr = responseData["base_path"] as? String!
//
//                    print("customer data is\(self.customerDataArr1!)")

                    for eachitem in self.timingsDataArr! {

                        let fromTime = eachitem["from_time"] as? String!
                        let toTime = eachitem["to_time"] as? String!
                        
                        if fromTime == ""
                        {
                            self.fromTimeArr.append("NA")
                        }else{
                            self.fromTimeArr.append(fromTime!!)
                        }
                        if toTime == ""
                        {
                            self.toTimeArr.append("NA")
                        }else{
                            self.toTimeArr.append(toTime!!)
                        }

                    }
                    
                    self.mondayFromTF.text = self.fromTimeArr[0]
                    self.mondayToTF.text = self.toTimeArr[0]
                    
                    self.tuesdayFromTF.text = self.fromTimeArr[1]
                    self.tuesdayToTF.text = self.toTimeArr[1]
                    
                    self.wednesdayFromTF.text = self.fromTimeArr[2]
                    self.wednesdayToTF.text = self.toTimeArr[2]
                    
                    self.thursdayFromTF.text = self.fromTimeArr[3]
                    self.thursdayToTF.text = self.toTimeArr[3]
                    
                    self.fridayFromTF.text = self.fromTimeArr[4]
                    self.fridayToTF.text = self.toTimeArr[4]
                    
                    self.saturdayFromTF.text = self.fromTimeArr[5]
                    self.saturdayToTF.text = self.toTimeArr[5]
                    
                    self.sundayFromTF.text = self.fromTimeArr[6]
                    self.sundayToTF.text = self.toTimeArr[6]

                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
                
            }
        }
    }


func saveTimeServiceCall()
{
   let vendorid = UserDefaults.standard.object(forKey: "user_id")
   // let requestString = "\(Base_Url)update_timings"
    //let request = NSMutableURLRequest(url: NSURL(string: "\(Base_Url)update_timings")! as URL)
    //Services.sharedInstance.loader(view: self.view)
    let DicInArray1 : [NSString : Any] = ["day_name":"Monday","from_time":mondayFromTF.text ?? "","to_time":mondayToTF.text ?? ""]
    let DicInArray2 : [NSString : Any] = ["day_name":"Tuesday","from_time":tuesdayFromTF.text ?? "","to_time": tuesdayToTF.text ?? ""]
    let DicInArray3 : [NSString : Any] = ["day_name":"Wednesday","from_time":wednesdayFromTF.text ?? "","to_time":wednesdayToTF.text ?? ""]
    let DicInArray4 : [NSString : Any] = ["day_name":"Thursday","from_time":thursdayFromTF.text ?? "" ,"to_time":thursdayFromTF.text ?? ""]
    let DicInArray5 : [NSString : Any] = ["day_name":"Friday","from_time":fridayFromTF.text ?? "","to_time":fridayToTF.text ?? ""]
    let DicInArray6 : [NSString : Any] = ["day_name":"Saturday","from_time":saturdayFromTF.text ?? "","to_time":saturdayToTF.text ?? ""]
    let DicInArray7 : [NSString : Any] = ["day_name":"Sunday","from_time":sundayFromTF.text ?? "","to_time":sundayToTF.text ?? ""]
    let arrayData : NSArray = [DicInArray1,DicInArray2,DicInArray3,DicInArray4 ,DicInArray5 ,DicInArray6 ,DicInArray7]
    
    let params : [NSString : Any] = ["API-KEY":98745612,"user_id":vendorid ?? "", "timings":arrayData]

    let url = URL(string: "\(Base_Url)update_timings")!
    print ("url")
    print(url)
    print ("perams")
    print(params)
    
    var request = URLRequest(url: url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
    print(request.httpBody ?? "")
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {                                                 // check
            return
        }
        if error != nil {
            print(error as Any)
            //Services.sharedInstance.dissMissLoader()
        } else {
            //Services.sharedInstance.dissMissLoader()
            
            DispatchQueue.main.async {
              
            let json = try? JSONSerialization.jsonObject(with: data, options: [])

            print("res : ",  json ?? [String : AnyObject]() )
            
                self.showToast(message: self.languageChangeString(a_str: "Timings are updated successfully")!)
            
            let responseString = String(data: data, encoding: .utf8)
               
            }
           
            
        }
    }
     DispatchQueue.main.async {
    
    task.resume()
        
    }
    
}
}

