//
//  ShopImagesController.swift
//  CarFixing
//
//  Created by Suman Guntuka on 22/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ShopImagesController: UIViewController {

    @IBOutlet weak var shopImgTable: UITableView!
    
    var shopImgsArr = [String]()
    var galleryIdArr = [String]()
    
    var galleryData: [[String:AnyObject]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //shopImgsArr = ["1", "2","3","4"];
        
        
        
        let backButton = UIBarButtonItem (image: #imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        let nextButton = UIBarButtonItem (title: languageChangeString(a_str: "Add Photo!"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (nextButtonCall))
        nextButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = nextButton
        
        self.title = languageChangeString(a_str: "Gallery")
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont(name: "Raleway-SemiBold", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(galleryCall), name: NSNotification.Name(rawValue:"gellery"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    func galleryCall ()
    {
        if Reachability.isConnectedToNetwork()
        {
            getGallery ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
        shopImgsArr = [String]()
        
        if Reachability.isConnectedToNetwork()
        {
            getGallery ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)

        }
        
        
    }
    
    func getGallery (){
        
        //voliveafrica.com/carfix/services/gallery?API-KEY=98745612&user_id=2
        let userid = UserDefaults.standard.object(forKey: "user_id")

        let gallery = "\(Base_Url)gallery?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "user_id" : userid ?? ""]
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(gallery, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.shopImgsArr = [String]()
                     self.galleryIdArr = [String]()
                    
                    self.galleryData = responseData["vendor_gallery"] as? [[String:AnyObject]]
                    
                    print("gallery data is\(self.galleryData!)")
                    
                    for eachitem in self.galleryData! {
                        
                        let shopImg = eachitem["image"] as? String!
                        let image = base_path + shopImg!!
                        self.shopImgsArr.append(image)
                        
                        let galleryId = eachitem["g_id"] as? String!
                        self.galleryIdArr.append(galleryId!!)
                        
                    }
                    
                    print("my array are \(self.shopImgsArr ,  self.galleryIdArr)")
                    
                    self.shopImgTable.reloadData()
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func gobackFromInvoice() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    func nextButtonCall() {
        
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let service = storyBoard.instantiateViewController(withIdentifier: "PickShopImageController")
//        self .present(service, animated: true, completion: nil)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "PickShopImage")
        
        self .present(service, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func deleteBtn(_ sender: Any) {
        
        //voliveafrica.com/carfix/services/delete_gallery
        
        let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.shopImgTable)
        let indexPath: IndexPath? = self.shopImgTable.indexPathForRow(at: btnPosition)

        let gID = galleryIdArr[(indexPath?.row)!]

        let userid = UserDefaults.standard.object(forKey: "user_id")

        let gallery = "\(Base_Url)delete_gallery"

        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "user_id" : userid ?? "" , "g_id" : gID]

        print(parameters)

        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(gallery, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)

                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()

                    self.showToast(message: message!!)

                    //self.shopImgTable.reloadData()
                    self.getGallery ()

                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }


    }
 
}
extension ShopImagesController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopImgsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopImageTableCell", for: indexPath) as! ShopImageTableCell
        cell.shopImg.layer.cornerRadius = 8
        
        cell.shopImg.sd_setImage(with: URL(string: self.shopImgsArr[indexPath.row]), placeholderImage: UIImage(named:"Castrol Services"))
        cell.delete.setTitle(languageChangeString(a_str: "Delete"), for: UIControlState.normal)
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 158
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            print("Deleted")
//
//            self.shopImgsArr.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .automatic)
//
////            let btnPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.shopImgTable)
////            let indexPath: IndexPath? = self.shopImgTable.indexPathForRow(at: btnPosition)
//
//            let gID = galleryIdArr[indexPath.row]
//
//            let userid = UserDefaults.standard.object(forKey: "user_id")
//
//            let gallery = "\(Base_Url)delete_gallery"
//
//            let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "user_id" : userid ?? "" , "g_id" : gID]
//
//            print(parameters)
//
//            Services.sharedInstance.loader(view: self.view)
//            Alamofire.request(gallery, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
//                if let responseData = response.result.value as? Dictionary<String, Any>{
//                    print(responseData)
//
//                    let status = responseData["status"] as? Int!
//                    let message = responseData["message"] as? String!
//                    if status == 1
//                    {
//                        Services.sharedInstance.dissMissLoader()
//
//                        self.showToast(message: message!)
//
//                        //self.shopImgTable.reloadData()
//                        //self.getGallery ()
//
//                        self.shopImgTable.reloadData()
//
//                    }
//                    else
//                    {
//                        self.showToast(message: message!)
//                        Services.sharedInstance.dissMissLoader()
//                    }
//                }
//            }
//
//
//        }
//
//        }
    }
    
    
    

