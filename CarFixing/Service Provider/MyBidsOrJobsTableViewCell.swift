//
//  MyBidsOrJobsTableViewCell.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/15/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit

class MyBidsOrJobsTableViewCell: UITableViewCell {

    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var status_lbl: UILabel!
    @IBOutlet weak var problemName_lbl: UILabel!
    
    @IBOutlet weak var jobName_lbl: UILabel!
    
    @IBOutlet weak var jobdate_lbl: UILabel!
    
    
    @IBOutlet weak var price_lbl: UILabel!
    
    @IBOutlet weak var details: UIButton!
    
    @IBOutlet weak var datailsBid: UIButton!
    
    
    //bid1
    
    @IBOutlet weak var cdateLbl: UILabel!
    
    @IBOutlet weak var cstatusLbl: UILabel!
    @IBOutlet weak var cDetails: UIButton!
    @IBOutlet weak var cproblemLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
