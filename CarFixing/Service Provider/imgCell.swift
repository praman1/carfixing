//
//  imgCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 13/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class imgCell: UICollectionViewCell {
 
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var zooIimg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
     scroll.minimumZoomScale = 1.0
    scroll.maximumZoomScale = 4.0
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return zooIimg
    }
    }
}
