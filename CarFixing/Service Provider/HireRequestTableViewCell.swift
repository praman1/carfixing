//
//  HireRequestTableViewCell.swift
//  CarFixingSwift
//
//  Created by volive solutions on 12/14/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

import UIKit

class HireRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var reject_Btn: UIButton!
    @IBOutlet weak var accept_Btn: UIButton!
    @IBOutlet weak var requestDate_lbl: UILabel!
    @IBOutlet weak var hireRequestDescription_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
