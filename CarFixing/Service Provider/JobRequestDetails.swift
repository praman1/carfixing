//
//  JobRequestDetails.swift
//  CarFixing
//
//  Created by Suman Guntuka on 01/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import SideMenu

class JobRequestDetails: UIViewController {

    @IBOutlet weak var jobImg: UIImageView!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var postedOnLbl: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var postedOn: UILabel!
    
    @IBOutlet weak var serviceType: UILabel!
    
    @IBOutlet var priceTV: UITableView!
    
    @IBOutlet var reasonView: UIView!
    @IBOutlet var submit: UIButton!
    @IBOutlet var reasonTF: UITextField!
    @IBOutlet var enterreason: UILabel!
    @IBOutlet var reject: UIButton!
    @IBOutlet var accept: UIButton!
    
    
    var sericeData : [[String : AnyObject]]!
    var serviceArr = [String]()
    var priceArr = [String]()
    var serviceIdArr = [String]()
    
    var long : String!
    var lat : String!
    var jobIdStr : String!
    var vendorIdStr : String!
    
    var addDicToArr :[[String: AnyObject]] = []
    
   
    var userIdStr : String!
    var userImgStr : String!
    
    var rejectBool : Bool!
    
    var parameters: Dictionary<String, Any>!

    var pushDictionary:[String:Any] = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reasonView.isHidden = true
         rejectBool = false
        
        
        let str = UserDefaults.standard.object(forKey: "jobRequest") as! String
        
        if str == "jobReq" {
            
                    let jobid  = pushDictionary["job_id"] as! Int
            
                    jobIdStr = String(jobid)
            
                    print(jobIdStr)
            
                    userIdStr = pushDictionary["user_id"] as! String
        
        }
  
        let backButton = UIBarButtonItem (image:#imageLiteral(resourceName: "back black"), style: UIBarButtonItemStyle.plain, target: self, action: #selector (gobackFromInvoice))
        backButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
         self.title = languageChangeString(a_str: "Job Details")
   
        OngoingServiceCall()
       
    }
    
    lazy var inputToolbar: UIToolbar = {
        var toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(JobRequestDetails.inputToolbarDonePressed))
        var flexibleSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var fixedSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        toolbar.setItems([fixedSpaceButton, fixedSpaceButton, flexibleSpaceButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        return toolbar
    }()
    
    func inputToolbarDonePressed() {
        
        
        self.view.endEditing(true)
        
    }

    func gobackFromInvoice() {
        
        let str = UserDefaults.standard.object(forKey: "jobRequest") as! String
        
        if str == "jobReq" {
         
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
        }else
        
        {
           self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptBtn(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() {
            
            jobAcceptServiceCall()
            
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            
        }
        
        
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
       
        reasonView.isHidden = true
        
    }
    @IBAction func rejectBtn(_ sender: Any) {
        
        reasonView.isHidden = false
        
        rejectBool = true
        
       
    }
    @IBAction func submitBtn(_ sender: Any) {
        
            reasonView.isHidden = true
            jobAcceptServiceCall()
        
    }
    func OngoingServiceCall()
    {
        //http://voliveafrica.com/carfix/services/job_details_forvendor?API-KEY=98745612&job_id=3
        
        let details = "\(Base_Url)job_details_forvendor?"
        
        userIdStr = UserDefaults.standard.object(forKey: "user_id") as! String
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY, "job_id" : jobIdStr! ,"vendor_id" : userIdStr ]
        print(details)
        
        print(parameters)
        
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(details, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                
                print(responseData)
                
                let status = responseData["status"] as? Int!
                //let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    if let allJobsData = responseData["job_details"] as? Dictionary<String, AnyObject> {
                        print(allJobsData)
                        
                        let jobDetails = allJobsData["job_data"] as? Dictionary<String,AnyObject>
                        
                        let shopNameStr = jobDetails!["user_name"] as? String!
                        
                        let locationStr = jobDetails!["address"] as? String!
                        
                        self.lat = jobDetails!["latitude"] as? String!
                        self.long = jobDetails!["longitude"] as? String!
                        
                        let bookDate = jobDetails!["booked_date"] as? String!
                        
                        let image = jobDetails!["image"] as? String!
                        
                        self.vendorIdStr = jobDetails!["user_id"] as? String!
                        
                        let imagePath = base_path + "" + image!!
                        
                        self.sericeData = allJobsData["job_services"] as? [[String:AnyObject]]
                        
                        print(self.sericeData)
                        
                        for i in self.sericeData! {
                            
                            let isSelectStr = i["is_selected"] as? Int!
                            
                          if isSelectStr == 1
                            {
                                let service = i["service_name_en"] as? String!
                                let priceStr = i["price"] as? String
                                let serviceId = i ["service_id"] as? String
                                
                                
                                self.serviceArr.append(service!!)
                                //self.priceArr.append(priceStr!)
                                self.serviceIdArr.append(serviceId!)
                            }
                        
                        }
                        print("chats data is\(self.serviceArr)")
                        
                        DispatchQueue.main.async {
                            
                            self.shopNameLbl.text = shopNameStr as! String
                            self.locationLbl.text = locationStr as! String
                            self.postedOnLbl.text = bookDate as! String
                            self.jobImg.sd_setImage(with: URL(string: imagePath), placeholderImage: UIImage(named:"as img1"))
                            //let IdStr = self.serviceArr.joined(separator: ",")
                            // self.servicesLbl.text = IdStr
                            
                            //self.isAuthorizedtoGetUserLocation()
                            
                            self.priceTV.reloadData()
                            
                        }
                    }
                }
                else
                {
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
        func jobAcceptServiceCall()
        {
            
            //self.dismiss(animated: true, completion: nil)
            
            let language = UserDefaults.standard.object(forKey: "currentLanguage")
            
            // http://voliveafrica.com/carfix/services/acceptjob
            //  job_id, user_id, job_status, reject_reason
            
            let vehicles = "\(Base_Url)acceptjob"
            
            if rejectBool == true
            {
                
                if reasonTF.text == ""
                {
                    reasonTF.text = " "
                }else
                {
                    
                }
               
                parameters  = ["API-KEY": APIKEY ,  "lang" : language ?? "" , "user_id" : userIdStr! , "job_id" : jobIdStr! , "job_status" : "2" , "reject_reason" : reasonTF.text!]
                
            }else
            {
                parameters = ["API-KEY": APIKEY ,  "lang" : language ?? "" , "user_id" : userIdStr! , "job_id" : jobIdStr! , "job_status" : "1"]
            }
            
            
            print(parameters)
            
            Services.sharedInstance.loader(view: self.view)
            
            Alamofire.request(vehicles, method: .post, parameters: parameters! , encoding: URLEncoding.default, headers: nil).responseJSON { response in
                
                if let responseData = response.result.value as? Dictionary<String, Any>{
                   
                    print(responseData)
                    
                    let status = responseData["status"] as? Int!
                    let message = responseData["message"] as? String!
                    
                    print(status)
                    print(message)
                    
                    if status == 1
                    {
                        
                        DispatchQueue.main.async {
                            Services.sharedInstance.dissMissLoader()
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                            self .present(service, animated: true, completion: nil)
                           
                            //  self.dismiss(animated: true, completion: nil)
                            
                        }
                        
                    }else
                    {
                        DispatchQueue.main.async {
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let service = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
                            self .present(service, animated: true, completion: nil)
                            
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                    
                }
            }
            
        }
    
        
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
            
            
            self.view.endEditing(true)
            
        }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
           
            textField.inputAccessoryView = inputToolbar
            
            return true
        }
        
}
extension JobRequestDetails: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serviceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = priceTV.dequeueReusableCell(withIdentifier: "PriceServiceCell2") as! PriceServiceCell
        
        cell.servLbl.text = serviceArr[indexPath.row]
//        cell.priceTF.text = priceArr[indexPath.row]
//        cell.priceTF.tag = indexPath.row
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 44
        
    }
}
