//
//  ServiceProviderRegister.swift
//  CarFixing
//
//  Created by Suman Volive on 2/6/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ServiceProviderRegister: UIViewController  {
    @IBOutlet weak var brandHieght: NSLayoutConstraint!
    
    @IBOutlet weak var brandTFHieght: NSLayoutConstraint!
    @IBOutlet weak var brandDrop: UIImageView!
    @IBOutlet weak var alreadyRegister: UILabel!
    @IBOutlet var serviceHieght: NSLayoutConstraint!
    @IBOutlet var flagImg: UIImageView!
    @IBOutlet weak var signup: UILabel!
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var shopNameTF: UITextField!
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var ownerNameTF: UITextField!
    @IBOutlet weak var emailId: UILabel!
    @IBOutlet weak var emailIdTF: UITextField!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var countryCodeTF: UITextField!
    @IBOutlet weak var mobileNoTF: UITextField!
    @IBOutlet weak var password: UILabel!
    @IBOutlet weak var confirmPassword: UILabel!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTf: UITextField!
    
    @IBOutlet weak var descriptionTf: UITextView!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    //@IBOutlet weak var descriptionTf: UITextField!
    @IBOutlet weak var serviceOffer: UILabel!
    @IBOutlet weak var serviceTF: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var shopImage: UILabel!
    @IBOutlet weak var shopImageTF: UITextField!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var serviceHeight: NSLayoutConstraint!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var pickBtn: UIButton!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var serviceImg: UIImageView!
    @IBOutlet weak var shopImg: UIImageView!
    
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var brandNameTF: UITextField!
    
    @IBOutlet var termsImg: UIImageView!
    @IBOutlet var termsAndCondition: UILabel!
    
    var servicesStr : String!
    var latStr : String!
    var longStr : String!
    var cityStr : String!
    var stateStr : String!
    var shopImgBool : Bool!
    var serviceBool : Bool!
    var brandBool : Bool!
    var termsBool : Bool!
    
    var countyPickerView: UIPickerView?
    
    var toolbar: UIToolbar?
    var toolbar1: UIToolbar?
    var selectedTextField: UITextField?
    var countryPickerData = [String]()
    var countryFlogArr = [String]()
    var selectedStr : String!
    var checkStr : String!
    var countryCodeId = [String]()
    var countryIDStr : String!
    
    var pickerImage  = UIImage()
    
    var imagePicker = UIImagePickerController()
     
    var appDelegate = AppDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        shopImgBool = false
        serviceBool = false
        brandBool = false
        termsBool = false
        
        UserDefaults.standard.removeObject(forKey: "address")
        
         UserDefaults.standard.set("5", forKey: "notify")
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.serviceStr = "indiv"
//        appDelegate.serviceStr = "repair"
//        appDelegate.serviceStr = "scrap"
        
        //language
        
        signup.text = languageChangeString(a_str: "Sign Up")
        shopName.text = languageChangeString(a_str: "SHOP NAME*")
        ownerName.text = languageChangeString(a_str: "OWNER NAME*")
        mobileNo.text = languageChangeString(a_str: "MOBILE NUMBER*")
        password.text = languageChangeString(a_str: "PASSWORD*")
        confirmPassword.text = languageChangeString(a_str: "CONFIRM PASSWORD*")
        location.text = languageChangeString(a_str: "LOCATION*")
        emailId.text = languageChangeString(a_str: "EMAIL ID*")
        descriptionLbl.text = languageChangeString(a_str: "DESCRIPTION*")
        shopImage.text = languageChangeString(a_str: "SHOP IMAGE*")
        serviceOffer.text = languageChangeString(a_str: "SERVICE OFFERED*")
        brand.text = languageChangeString(a_str: "Specialized in following brands*")
        termsAndCondition.text = languageChangeString(a_str: "I agree to terms and conditions")
        alreadyRegister.text = languageChangeString(a_str: "Already Registered?")
        
        
        pickBtn.setTitle(languageChangeString(a_str: "PICK"), for: UIControlState.normal)
        submitButton.setTitle(languageChangeString(a_str: "SUBMIT"), for: UIControlState.normal)
        login.setTitle(languageChangeString(a_str: "Login"), for: UIControlState.normal)
        uploadBtn.setTitle(languageChangeString(a_str: "UPLOAD"), for: UIControlState.normal)
        
        shopNameTF.placeholder = languageChangeString(a_str: "SHOP NAME*")
         ownerNameTF.placeholder = languageChangeString(a_str: "OWNER NAME*")
         emailIdTF.placeholder = languageChangeString(a_str: "EMAIL ID*")
         //descriptionTf.placeholder = languageChangeString(a_str: "DESCRIPTION*")
        
        if appDelegate.serviceStr == "indiv"
        {
            print("individual Press")
            
        }else if appDelegate.serviceStr == "repair"
        {
            print("repair is Press")
        }else
        {
            serviceBool = true
            print("scrap shop is pressed")
            serviceOffer.isHidden = true
            serviceTF.isHidden = true
            serviceHieght.constant = 0
            serviceImg.isHidden = true
        }
 
        imagePicker.delegate = self
        let line = LineTextField()
        let myColor : UIColor = UIColor.lightGray
        line .textfieldAsLine(myTextfield: shopNameTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: ownerNameTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: mobileNoTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: countryCodeTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: emailIdTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: passwordTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: confirmPasswordTf, lineColor: myColor, myView: self.view)
       //line .textfieldAsLine(myTextfield: descriptionTf, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: serviceTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: shopImageTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: locationTF, lineColor: myColor, myView: self.view)
        line .textfieldAsLine(myTextfield: brandNameTF, lineColor: myColor, myView: self.view)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
       
       
        NotificationCenter.default.addObserver(self, selector: #selector(BrandData), name: NSNotification.Name(rawValue:"brands"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onsiteData), name: NSNotification.Name(rawValue:"service"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(spData), name: NSNotification.Name(rawValue:"serviceSP"), object: nil)
        
       // NotificationCenter.default.addObserver(self, selector: #selector(profileData2), name: NSNotification.Name(rawValue:"service3"), object: nil)
        
        countryCodeService ()
        
        countyPickerView = UIPickerView()
        countyPickerView?.backgroundColor = UIColor.white
        toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar?.barStyle = .blackOpaque
        toolbar?.autoresizingMask = .flexibleWidth
        toolbar?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        toolbar?.frame = CGRect(x: 0,y: (countyPickerView?.frame.origin.y)!-44, width: self.view.frame.size.width,height: 44)
        toolbar?.barStyle = UIBarStyle.default
        toolbar?.isTranslucent = true
        toolbar?.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        toolbar?.backgroundColor = #colorLiteral(red: 0.9550941586, green: 0.674628377, blue: 0.0005892670597, alpha: 1)
        toolbar?.sizeToFit()
        
        countyPickerView?.delegate = self as UIPickerViewDelegate
        countyPickerView?.dataSource = self as UIPickerViewDataSource
        
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.donePicker))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(LoginController.canclePicker))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        toolbar1 = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar1?.barStyle = .blackOpaque
        toolbar1?.autoresizingMask = .flexibleWidth
        toolbar1?.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let doneButton = UIBarButtonItem(title: languageChangeString(a_str: "Done"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
        doneButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: languageChangeString(a_str: "Cancel"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancleBtn))
        cancelButton.tintColor = #colorLiteral(red: 0.9895833333, green: 1, blue: 1, alpha: 1)
        toolbar1?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        toolbar1?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        //self.text.inputAccessoryView = toolbar1
        shopNameTF.inputAccessoryView = toolbar1
        ownerNameTF.inputAccessoryView = toolbar1
        mobileNoTF.inputAccessoryView = toolbar1
        emailIdTF.inputAccessoryView = toolbar1
        passwordTF.inputAccessoryView = toolbar1
        confirmPasswordTf.inputAccessoryView = toolbar1
        descriptionTf.inputAccessoryView = toolbar1
        serviceTF.inputAccessoryView = toolbar1
        shopImageTF.inputAccessoryView = toolbar1
        locationTF.inputAccessoryView = toolbar1
        
    }
    
    func doneButtonAction()
    {
        shopNameTF.resignFirstResponder()
        ownerNameTF.resignFirstResponder()
        mobileNoTF.resignFirstResponder()
        emailIdTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        confirmPasswordTf.resignFirstResponder()
        descriptionTf.resignFirstResponder()
        serviceTF.resignFirstResponder()
        shopImageTF.resignFirstResponder()
        locationTF.resignFirstResponder()
        
    }
    func cancleBtn()
    {
        shopNameTF.resignFirstResponder()
        ownerNameTF.resignFirstResponder()
        mobileNoTF.resignFirstResponder()
        emailIdTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        confirmPasswordTf.resignFirstResponder()
        descriptionTf.resignFirstResponder()
        serviceTF.resignFirstResponder()
        shopImageTF.resignFirstResponder()
        locationTF.resignFirstResponder()
    }
    
    func donePicker ()
    {
        
        //self.countryCodeTF.text = selectedStr
        self.countryCodeTF.resignFirstResponder()
        
    }
    func canclePicker ()
    {
        self.countryCodeTF.resignFirstResponder()
    }
        
    func onsiteData()
    {
        let towStr = UserDefaults.standard.object(forKey: "type") as! String
        
        if towStr == "tow"
        {
            brand.isHidden = true
            brandHieght.constant = 0
            brandNameTF.isHidden = true
            brandTFHieght.constant = 0
            brandDrop.isHidden = true
       
        }else
        {
            brand.isHidden = false
            brandHieght.constant = 38
            brandNameTF.isHidden = false
            brandTFHieght.constant = 39
            brandDrop.isHidden = false
        }
        
        let str = UserDefaults.standard.object(forKey: "idsString")
        print(str as Any)
        self.serviceTF.text = languageChangeString(a_str: "Services Are Detected!")
        //NotificationCenter.default.removeObserver(self)
        
    }
    func spData()
    {
        let str = UserDefaults.standard.object(forKey: "idsString")
        print(str as Any)
        self.serviceTF.text = languageChangeString(a_str: "Services Are Detected!")
       //NotificationCenter.default.removeObserver(self)
        
    }
    func BrandData()
    {
        self.brandNameTF.text = languageChangeString(a_str: "Brands Are Detected!")
       // NotificationCenter.default.removeObserver(self)
    }

    @IBAction func backBtn(_ sender: Any) {
       
        UserDefaults.standard.removeObject(forKey: "idsString")
        UserDefaults.standard.removeObject(forKey: "saveoS")
        UserDefaults.standard.removeObject(forKey: "bandIdsString")
        UserDefaults.standard.removeObject(forKey: "saveS")
        UserDefaults.standard.removeObject(forKey: "savebS")
        UserDefaults.standard.removeObject(forKey: "IdsStringArr")
        UserDefaults.standard.removeObject(forKey: "type")
        
         self.navigationController?.popViewController(animated: true)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        
        if shopNameTF.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Shop Name")!)

        }else if ownerNameTF.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter The Owner Name")!)
        }
       
//        else if emailIdTF.text == ""
//        {
//            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter A Valid Email Id")!)
//
//        }
        
        else if countryCodeTF.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Pick Country Code")!)
        }
        else if mobileNoTF.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Mobile No")!)
            
        }else if passwordTF.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Enter Password")!)
            
        }else if confirmPasswordTf.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Confirm Password")!)
            
        }else if confirmPasswordTf.text != passwordTF.text
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Confirm Password")!)
            
        }else if shopImgBool == false
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Upload The Shop Image")!)
        }
         if locationTF.text == ""
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Select The Location")!)
        }
         
         else if serviceBool == false
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Select The Services")!)
         }
         
//         else if brandBool == false
//         {
//            self.showToastForAlert(message: languageChangeString(a_str: "Please Select The Brands")!)
//         }
         
         else if termsBool == false
         {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Accept Terms And Conditions")!)
         }
        else
        {
            if Reachability.isConnectedToNetwork()
            {
                registerService ()
            }else
            {
                showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
            }
        }
    }
    
    @IBAction func termsBtn(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "TermsAndConditions")
        self.navigationController?.pushViewController(homeVC, animated: true)
        
    }
    @IBOutlet weak var upload: UIButton!
    
    @IBAction func uploadBtn(_ sender: Any) {
        
        self.shopImgBool = true
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "idsString")
        UserDefaults.standard.removeObject(forKey: "saveoS")
        UserDefaults.standard.removeObject(forKey: "bandIdsString")
        UserDefaults.standard.removeObject(forKey: "saveS")
        UserDefaults.standard.removeObject(forKey: "savebS")
        UserDefaults.standard.removeObject(forKey: "IdsStringArr")
         UserDefaults.standard.removeObject(forKey: "type")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let address = UserDefaults.standard.object(forKey: "address") as? String
        
        self.locationTF.text = address ?? ""
        
//        let str = UserDefaults.standard.object(forKey: "checkStr") as? String
//        if str
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
       // NotificationCenter.default.removeObserver(self)
        
    }
    
    @IBAction func locationPicker(_ sender: Any) {
       // self.locationTF.text = "Location Detected"
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "LocationController")
        self .present(service, animated: true, completion: nil)
    }
    
    @IBAction func brandsBtn(_ sender: Any) {
        
        self.brandBool = true
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "BrandSelection")
        
        //service.modalPresentationStyle = .overCurrentContext
        self .present(service, animated: true, completion: nil)
    }
    @IBAction func serviceBtn(_ sender: Any) {
   
        self.serviceBool = true
        
        if appDelegate.serviceStr == "indiv"
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let service = storyBoard.instantiateViewController(withIdentifier: "OnsiteService")
            self .present(service, animated: true, completion: nil)
        
        }else
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let service = storyBoard.instantiateViewController(withIdentifier: "ServiceSelection")
            self .present(service, animated: true, completion: nil)
        
        }
    }
   
    func registerService ()
    {
        
        
       // let vehicles = "\(Base_Url)registration"
        Services.sharedInstance.loader(view: self.view)
        
        let tokenStr = UserDefaults.standard.object(forKey: "deviceToken")
        let lat = UserDefaults.standard.object(forKey: "latitude")
        let long = UserDefaults.standard.object(forKey: "longitude")
        let city = UserDefaults.standard.object(forKey: "city")
        let state = UserDefaults.standard.object(forKey: "state")
        let services = UserDefaults.standard.object(forKey: "idsString")
        let brands = UserDefaults.standard.object(forKey: "bandIdsString")
        let address = UserDefaults.standard.object(forKey: "address")
        let country = UserDefaults.standard.object(forKey: "country")
        let pincode = UserDefaults.standard.object(forKey: "pincode")
        let language = UserDefaults.standard.object(forKey: "currentLanguage")

        if self.countryIDStr == nil
        {
            self.countryIDStr = countryCodeId[0]
        }
        
        if appDelegate.serviceStr == "indiv"
        {

            let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "shop_name":self.shopNameTF.text!,  "owner_name":self.ownerNameTF.text!, "email":self.emailIdTF.text!, "mobile":self.mobileNoTF.text!, "device_type" :"iOS" , "password" :self.passwordTF.text!,"country_code" :self.countryCodeTF.text!, "description" :self.descriptionTf.text! , "device_token":tokenStr ?? "" , "latitude" :lat ?? "" , "longitude" : long ?? "" , "city" : city ?? "", "state" : state ?? "" , "services" : services ?? "" ,"brands" : brands ?? "" , "vendor_type" : "2" ,"address" : address ?? "" ,"country" : country ?? "" ,"pincode" : pincode ?? "" , "lang" : language ?? "","countrycode" : self.countryIDStr! ]
            
            let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            
            print(parameters)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "shop_image",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                  to:"http://volive.in/carfix/services/vendor_registration")
            { (result) in
                
            print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print(response.result.value ?? "")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            self.showToast(message: message)
                            Services.sharedInstance.dissMissLoader()
                            
                            //remove observers
                            UserDefaults.standard.removeObject(forKey: "idsString")
                            UserDefaults.standard.removeObject(forKey: "bandIdsString")
                            UserDefaults.standard.removeObject(forKey: "savebS")
                            UserDefaults.standard.removeObject(forKey: "saveoS")
                            UserDefaults.standard.removeObject(forKey: "IdsStringArr")
                             UserDefaults.standard.removeObject(forKey: "type")
                           // NotificationCenter.default.removeObserver(self)
                            
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
                            self.navigationController?.pushViewController(login!, animated: true)
                            
                        }
                        else
                        {
                            self.showToast(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }else if appDelegate.serviceStr == "repair"{
          
            let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "shop_name":self.shopNameTF.text!,  "owner_name":self.ownerNameTF.text!, "email":self.emailIdTF.text!, "mobile":self.mobileNoTF.text!, "device_type" :"iOS" , "password" :self.passwordTF.text!,"country_code" :self.countryCodeTF.text!, "description" :self.descriptionTf.text! , "device_token":tokenStr ?? "" , "latitude" :lat ?? "" , "longitude" : long ?? "" , "city" : city ?? "", "state" : state ?? "" , "services" : services ?? "","brands" : brands ?? "" , "vendor_type" : "3" ,"address" : address ?? "" ,"country" : country ?? "" ,"pincode" : pincode ?? "" , "lang" : language ?? "","countrycode" : self.countryIDStr!]
            
            
            let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            
            print(parameters)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "shop_image",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"http://volive.in/carfix/services/vendor_registration")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print(response.result.value ?? "")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            self.showToast(message: message)
                            Services.sharedInstance.dissMissLoader()
                            UserDefaults.standard.removeObject(forKey: "idsString")
                            UserDefaults.standard.removeObject(forKey: "bandIdsString")
                            UserDefaults.standard.removeObject(forKey: "saveS")
                             UserDefaults.standard.removeObject(forKey: "savebS")
                             UserDefaults.standard.removeObject(forKey: "IdsStringArr")
                            //NotificationCenter.default.removeObserver(self)
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
                            self.navigationController?.pushViewController(login!, animated: true)
                            
                        }
                        else
                        {
                            self.showToast(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }else {
            
            let parameters: Dictionary<String, Any> = ["API-KEY" :APIKEY, "shop_name":self.shopNameTF.text!,  "owner_name":self.ownerNameTF.text!, "email":self.emailIdTF.text!, "mobile":self.mobileNoTF.text!, "device_type" :"iOS" , "password" :self.passwordTF.text!,"country_code" :self.countryCodeTF.text!, "description" :self.descriptionTf.text! , "device_token":tokenStr ?? "" , "latitude" :lat ?? "" , "longitude" : long ?? "" , "city" : city ?? "", "state" : state ?? "" , "vendor_type" : "4" ,"address" : address ?? "" ,"country" : country ?? "" ,"pincode" : pincode ?? "" , "lang" : language ?? "" ,"brands" : brands ?? "","countrycode" : self.countryIDStr!]
            
            
            let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            
            print(parameters)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "shop_image",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"http://volive.in/carfix/services/vendor_registration")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print(response.result.value ?? "")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            self.showToast(message: message)
                            Services.sharedInstance.dissMissLoader()
                            UserDefaults.standard.removeObject(forKey: "idsString")
                            UserDefaults.standard.removeObject(forKey: "bandIdsString")
                            UserDefaults.standard.removeObject(forKey: "saveS")
                           // NotificationCenter.default.removeObserver(self)
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let login = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
                            self.navigationController?.pushViewController(login!, animated: true)
                            
                        }
                        else
                        {
                            self.showToast(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func countryCodeService ()
    {
        
        //http://voliveafrica.com/carfix/services/countries?API-KEY=98745612
        
        let vehicles = "\(Base_Url)countries?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY]
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                
                if status == 1
                {
                    
                    let servicesCat = responseData["countries"] as? [[String:Any]]
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let codeStr = servicesCat["phonecode"]  as! String
                        let flags = servicesCat["country_flag"]  as! String
                        let countryId = servicesCat["id"]  as! String
 
                        let flogsImg = base_path + flags
                        self.countryFlogArr.append(flogsImg)
                        self.countryCodeId.append(countryId)
                        self.countryPickerData.append(codeStr)
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        self.flagImg.image=UIImage(named: "saudi")
                    }
                    print(self.countryPickerData)
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    @IBAction func termsAndConditionBtn(_ sender: Any) {
        
//        if termsBool == false {
//
//            termsBool = true
//             termsImg.image = UIImage(named:"unCheck")!
//
//        }else{
//
            termsBool = true
        
             termsImg.image = UIImage(named:"check")!
    //}
    
    }
}

extension ServiceProviderRegister: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
       
        picker.dismiss(animated: true, completion: nil)
        let pickedImage2 = info[UIImagePickerControllerOriginalImage]
        shopImg.image = pickedImage2  as? UIImage
        pickerImage = (pickedImage2 as? UIImage)!
      
    }
}

extension ServiceProviderRegister: UITextFieldDelegate{
    public func textFieldDidEndEditing(_ textField: UITextField) {
        shopNameTF.resignFirstResponder()
        ownerNameTF.resignFirstResponder()
        emailIdTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        mobileNoTF.resignFirstResponder()
        confirmPasswordTf.resignFirstResponder()
        //descriptionTf.resignFirstResponder()
        serviceTF.resignFirstResponder()
        shopImageTF.resignFirstResponder()
        scroll.contentOffset.x = 0
        scroll.contentOffset.y = 0
        
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var returnValue: Bool = false
        if textField == shopNameTF {
            
            ownerNameTF.becomeFirstResponder()
            returnValue = true
            
        }
        else if textField == ownerNameTF{
            
            emailIdTF.becomeFirstResponder()
            returnValue = true
        }
        else if textField == emailIdTF{
            mobileNoTF.becomeFirstResponder()
            returnValue = true
        }
        else if textField == mobileNoTF{
            mobileNoTF.becomeFirstResponder()
            returnValue = true
        }
        else if textField == confirmPasswordTf{
            //descriptionTf.becomeFirstResponder()
            returnValue = true
        }
//        else if textField == descriptionTf{
//            //serviceTF.resignFirstResponder()
//            returnValue = true
//        }
        else if textField == serviceTF{
            //serviceTF.resignFirstResponder()
            returnValue = true
        }
        
        return returnValue
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        countryCodeTF.inputView = countyPickerView
        countryCodeTF.inputAccessoryView = toolbar
        
        //   countryCode.text = selectedTextField
        
        return true
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == shopNameTF {
            scroll.contentOffset.y = +20
        }
        else if textField == ownerNameTF{
            scroll.contentOffset.y = +60
        }
        else if textField == emailIdTF{
            scroll.contentOffset.y = +100
        }
        else if textField == mobileNoTF{
            scroll.contentOffset.y = +140
        }
        else if textField == passwordTF{
            scroll.contentOffset.y = +200
        }else if textField == confirmPasswordTf{
            scroll.contentOffset.y = +200
        }
//        else if textField == descriptionTf{
//            scroll.contentOffset.y = +240
//        }
        else if textField == serviceTF{
            
//            API-KEY:98745612
//            mobile:8886084609
//            state:telangana
//            city:warangal
//            email:kalyan6@gmail.com
//            description:fgdfghdhgghfghfghfghfh
//            device_type:iOS
//            latitude:17.43747640
//            longitude:78.45319580
//            password:Suman123
//            device_token:46917C874390E8540AF6DF29BD7008DCFB604BB5E79F45C972B9D2D0B72FF81F
//            shop_name:kalyan shop
//            vendor_type:4
//            owner_name:kalyan guntuka
            
            scroll.contentOffset.y = +280
        }
        
    }
    
}
extension ServiceProviderRegister : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return countryPickerData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == countyPickerView
        {
            countryCodeTF.text = self.countryPickerData[row]
            self.countryIDStr = self.countryCodeId[row]
            
            self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        }
    }
    
   
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        //names
      
        let temp = UILabel()
        let img = UIImageView()
        img.frame = CGRect(x: self.view.frame.origin.x+100 ,y: 0, width: 30,height: 25)
        
        self.countryIDStr = self.countryCodeId[row]
        temp.text = self.countryPickerData[row]
        self.flagImg.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        img.sd_setImage(with: URL(string: self.countryFlogArr[row]), placeholderImage: UIImage(named:"saudi"))
        
        temp.adjustsFontSizeToFitWidth = true
        temp.frame = CGRect(x: self.view.frame.origin.x+155 ,y: 0, width: self.view.frame.size.width-150,height: 30)
        //codes
        let aView = UIView()
        aView.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width,height:30)
        aView.insertSubview(temp, at: 1)
        aView.insertSubview(img, at: 1)
        
        
        return aView
    }
    
}

