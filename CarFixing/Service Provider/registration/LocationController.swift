//
//  LocationController.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/8/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LocationController: UIViewController,CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var gmapView: GMSMapView!
    @IBOutlet weak var markerPin: UIImageView!
    
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var place: UITextField!
    @IBOutlet weak var submit: UIButton!
    
    var typeOfAddress :String!
    var city :String!
    var state :String!
    var country : String!
    var pincode : String!
    var addressOfuser :String!
   
    //dragable marker
    var currentLocation:CLLocationCoordinate2D!
    var finalPositionAfterDragging:CLLocationCoordinate2D?
    var locationMarker:GMSMarker!
    
    let lblNew = UILabel()
    lazy var locationManager: CLLocationManager = {
        
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0
        return _locationManager
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gmapView.delegate = self
        self.markerPin.layer.zPosition = 1
        
        if Reachability.isConnectedToNetwork()
        {
            self.isAuthorizedtoGetUserLocation()
        }else
        {
          showToastForAlert (message: "You must Connect Internet!")
        }
    
    }

    @IBAction func submitBtn(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }
    func isAuthorizedtoGetUserLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("didchange")
        
        //called everytime
        wrapperFunctionToShowPosition(mapView: mapView)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        print("idleAt")
        
        //called when the map is idle
        wrapperFunctionToShowPosition(mapView: mapView)
        
    }
    
    func wrapperFunctionToShowPosition(mapView:GMSMapView){
        let geocoder = GMSGeocoder()
        let latitud = mapView.camera.target.latitude
        let longitud = mapView.camera.target.longitude

       let latitude = String(format: "%.8f", mapView.camera.target.latitude)
       let longitude = String(format: "%.8f", mapView.camera.target.longitude)
        
        let position = CLLocationCoordinate2DMake( latitud, longitud)
        
        let latLang = "\(latitude), \(longitude)"
        
        self.place.text = "\(latLang)"
      
        UserDefaults.standard.set( latitude, forKey: "latitude")
        
        UserDefaults.standard.set( longitude, forKey: "longitude")
        
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                
                print("adress of that location is \(result!)")
                
                let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                self.addressOfuser = address?.strstr(needle: ",")
                
                //print ("toke to \( self.addressOfuser!)")
                
                 self.city = result?.locality
                 self.state = result?.administrativeArea
                self.country = result?.country
                self.pincode = result?.postalCode
                
                UserDefaults.standard.set(self.country, forKey: "country")
                UserDefaults.standard.set(self.pincode, forKey: "pincode")
                UserDefaults.standard.set(self.addressOfuser, forKey: "address")
                
                if self.city == nil {
                    
                    self.cityTF.text = "not available"
                }
                    
                else {
                    
                    self.cityTF.text = "\(self.city!)"
                    UserDefaults.standard.set( self.cityTF.text!, forKey: "city")
                    
                }
                
                if self.state == nil {
                    
                    self.stateTF.text = "not available"
                    
                }
                    
                else {
                    
                    self.stateTF.text = "\(self.state!)"
                    UserDefaults.standard.set( self.stateTF.text!, forKey: "state")
                    
                }
                                
                // self.searchBar.text = address
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdate location")
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
        
        let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
        print(position)
        
      //self.setupLocationMarker(coordinate: position)
    
        DispatchQueue.main.async {
            
            self.gmapView.camera = camera
            
        }
        
        self.gmapView?.animate(to: camera)
        manager.stopUpdatingLocation()
    }
    
    func setupLocationMarker(coordinate: CLLocationCoordinate2D) {
        print("setup location")
        if locationMarker != nil {
            locationMarker.map = nil
        }
        
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = gmapView
        locationMarker.appearAnimation =  .pop
        locationMarker.icon = GMSMarker.markerImage(with: UIColor.blue)
        locationMarker.opacity = 0.75
        locationMarker.isFlat = true
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
