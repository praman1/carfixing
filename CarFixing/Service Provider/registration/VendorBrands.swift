//
//  VendorBrands.swift
//  CarFixing
//
//  Created by Suman Guntuka on 06/09/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class VendorBrands: UIViewController {

    
    @IBOutlet var cancel: UIButton!
    @IBOutlet weak var brands: UILabel!
    @IBOutlet weak var brandTable: UITableView!
    
    @IBOutlet weak var saveBtn: UIButton!
  
    var brandCheckArr = [String]()
   
    var brandArr: Array<String> = []
    var brandIdArr: Array<String> = []
    
    var nameArr = [String]()
    var idArr = [String]()
   
    var checkArr = [String]()
    
    var selectedRows:[IndexPath] = []
    var saveBool : Bool!
    var notifyStr : String!
    var IdStr : String!
    
    var cell : BrandCell!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if Reachability.isConnectedToNetwork()
        {
            serivceListService ()
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet! ")!)
        }
        
    }

    @IBAction func closeBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveBtn(_ sender: Any) {
        
        updatebrandsCall ()
    
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func updatebrandsCall ()
    {
        //http://voliveafrica.com/carfix/services/updatebrands
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        // let vendorIds = UserDefaults.standard.object(forKey: "vendorBandIds")
        
        let vehicles = "\(Base_Url)updatebrands"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? "" , "user_id" : userid ?? "" ,"brands" : IdStr! ]
        print(parameters)
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post , parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        
                        self.serivceListService ()
                    }
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }

    func serivceListService ()
    {
        
        //http://voliveafrica.com/carfix/services/spbrands
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
         let userid = UserDefaults.standard.object(forKey: "user_id")
        let vehicles = "\(Base_Url)spbrands"
        print(vehicles)
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? "" , "user_id" : userid ?? ""]
        print(parameters)
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    let servicesCat = responseData["brands"] as? [[String:Any]]
                    
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let name = servicesCat["brand_name_en"]  as! String
                        
                        let serviceId = servicesCat["id"]  as! String
                        let selct = servicesCat["is_selected"]  as! String
                        
                        self.brandCheckArr.append(selct)
                        print("all services are \(self.brandCheckArr)")
                        
                        self.brandArr.append(name)
                        self.brandIdArr.append(serviceId)
                      self.brandTable.reloadData()
                    }
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
}

extension VendorBrands: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brandArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = self.brandTable.dequeueReusableCell(withIdentifier: "VendorBrandCell", for: indexPath) as! BrandCell
        cell.vendorBrand.text = self.brandArr[indexPath.row]
         let idsting = self.brandIdArr[indexPath.row]
        // print(serviceCheckArr)
        
        if brandCheckArr[indexPath.row] == "0" {
            
            cell.imgBrand.image = UIImage(named: "unCheck")
            cell.vendorBrand.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
        }else if brandCheckArr[indexPath.row] == "1"
        {
            cell.imgBrand.image = UIImage(named: "check color")
            cell.vendorBrand.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
           
            idArr.append(idsting)
            IdStr = idArr.joined(separator: ",")
      
            print("final brands ids \(IdStr)")
            UserDefaults.standard.set( IdStr , forKey: "vendorBandIds")
           
        }else
        {
            IdStr = ""
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        saveBool = true
        
        if brandCheckArr[indexPath.row] == "0" {
            //brandCheckArr[indexPath.row] = "1"
            brandCheckArr.remove(at: indexPath.row)
           brandCheckArr.insert("1", at: indexPath.row)
            //brandCheckArr.append("1")
            print(brandCheckArr)
            
            idArr = [String]()
            IdStr = ""
             tableView.reloadData()
            
        }
        else if brandCheckArr[indexPath.row] == "1"{
            //brandCheckArr[indexPath.row] = "0"
              brandCheckArr.remove(at: indexPath.row)
              brandCheckArr.insert("0", at: indexPath.row)
            //brandCheckArr.append("1")
            idArr = [String]()
            IdStr = ""
             tableView.reloadData()
        }
       
        
        
    }
    
    
}
