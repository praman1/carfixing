//
//  BrandCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 28/04/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class BrandCell: UITableViewCell {

    //@IBOutlet weak var checked: UIButton!
    @IBOutlet weak var checkImgbrand: UIImageView!
    @IBOutlet weak var brandNameLbl: UILabel!
   
    // vendor brands
    
    @IBOutlet var vendorBrand: UILabel!
    @IBOutlet weak var imgBrand: UIImageView!
    //filter brand
    
    @IBOutlet var filterBrandNameLbl: UILabel!
     @IBOutlet weak var filterCheckImgbrand: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
