//
//  ServiceSelection.swift
//  CarFixing
//
//  Created by Suman Volive on 2/7/18.
//  Copyright © 2018 volivesolutions. All rights reserved.

import UIKit
import Alamofire

class ServiceSelection: UIViewController {

    @IBOutlet var cancel: UIButton!
    @IBOutlet weak var serviceType: UILabel!
   
    @IBOutlet weak var save: UIButton!
    var serviceCheckArr = [String]()
    //var servicesArr: [[String:AnyObject]]!
     var servicesArr: Array<String> = []
    var serviceIdArr: Array<String> = []
   
    var nameArr = [String]()
    var idArr = [String]()
    var notifyStr : String!
    var checkArr = [String]()
    
    var saveBool : Bool!
    var typeStr : String!

    @IBOutlet weak var serviceTbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         saveBool = false
        
        SerivceListService ()
        
        notifyStr = UserDefaults.standard.object(forKey: "notify") as! String
        
       // UserDefaults.standard.set("jj", forKey: "saveStr")
        
        print(notifyStr)
        
        serviceType.text = languageChangeString(a_str: "Service type")
        save.setTitle(languageChangeString(a_str: "SAVE"), for: UIControlState.normal)
       // cancel.setTitle(languageChangeString(a_str: "Cancel"), for: UIControlState.normal)
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dissmissService))
//        self.view.addGestureRecognizer(tapGesture)

        //let str = UserDefaults.standard.object(forKey: "saveStr")
    }

    override func viewWillDisappear(_ animated: Bool) {
        
       // UserDefaults.standard.removeObject(forKey: "saveS")
    }

    @IBAction func cancelBtn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelCall"), object: nil)
        self.dismiss(animated: true, completion: nil)
    
    }
    @IBAction func saveBtn(_ sender: Any) {

        if saveBool == true
        {
           // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "service"), object: nil)
            self.dismiss(animated: true, completion: nil)
        }else
        {
             self.showToastForAlert(message: languageChangeString(a_str: "Please Select The Services")!)
        }
        
        if notifyStr == "2"
        {
             UserDefaults.standard.set("filt", forKey: "saveS")
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "service"), object: nil)
        }
        else if notifyStr == "3"
        {
             UserDefaults.standard.set("filt", forKey: "saveS")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serviceSP"), object: nil)
        }
        
        else if notifyStr == "5"
        {
       UserDefaults.standard.set("filt", forKey: "saveS")
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serviceSP"), object: nil)
        
        }else
        {
            UserDefaults.standard.set("filt", forKey: "saveS")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "service"), object: nil)
        }
    }
    func SerivceListService ()
    {
        
        //http://volive.in/carfix/services/available_services?API-KEY=98745612&lang=en&type=3
      
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
       
        typeStr = UserDefaults.standard.object(forKey: "ServiceType") as! String
        
        let vehicles = "\(Base_Url)available_services"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? "" , "type" : typeStr ?? "" ]
        Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    //self.servicesArr = responseData["services"] as! Array<String>
                    
                    let servicesCat = responseData["services"] as? [[String:Any]]
                    
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let name = servicesCat["service_name_en"]  as! String
                        
                        let serviceId = servicesCat["id"]  as! String
    
                        let myInt2 = (serviceId as NSString).integerValue
                       
                        let myString = String(myInt2)
                        
                        self.servicesArr.append(name)
                        
                        self.serviceIdArr.append(myString)
                        
                        // let str = UserDefaults.standard.object(forKey: "saveS") as! String
                        
                        if UserDefaults.standard.object(forKey: "saveS") != nil
                        {
                            self.saveBool = true
                            let idArr = UserDefaults.standard.object(forKey: "idsArr") as! Array<Any>
                            
                            self.checkArr = idArr as! [String]
                            
                            print(self.checkArr)
                            
                            if self.checkArr .contains(myString)
                            {
                                
                                let checkStr = "1"
                                self.serviceCheckArr.append(checkStr)
                            }else
                            {
                                let checkStr = "0"
                                self.serviceCheckArr.append(checkStr)
                            }
                        }
                        else
                        {
                            let checkStr = "0"
                            self.serviceCheckArr.append(checkStr)
                        }
                        
                     /*
                        if str == "filt"
                        {
                            let idArr = UserDefaults.standard.object(forKey: "idsArr") as! Array<Any>
                            
                            self.checkArr = idArr as! [String]
                           
                            print(self.checkArr)
                        
                            if self.checkArr .contains(myString)
                            {
                                let checkStr = "1"
                                self.serviceCheckArr.append(checkStr)
                            }else
                            {
                                let checkStr = "0"
                                self.serviceCheckArr.append(checkStr)
                            }
                            
                        }else
                        {
                            let checkStr = "0"
                            self.serviceCheckArr.append(checkStr)
                        }
                     */
                        
                       
                    }
                    
                    UserDefaults.standard.set(self.servicesArr, forKey: "serviceArr")
                    self.serviceTbl.reloadData()
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension ServiceSelection: UITableViewDataSource , UITableViewDelegate{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = serviceTbl.dequeueReusableCell(withIdentifier: "SelectServiceCell", for: indexPath) as! SelectServiceCell
        cell.serviceTypeLbl.text = self.servicesArr[indexPath.row]
        let idsting = self.serviceIdArr[indexPath.row]
       
        print(serviceCheckArr)
        
        if serviceCheckArr[indexPath.row] == "0" {
            
            cell.checkImg.image = UIImage(named: "unCheck")
            cell.serviceTypeLbl.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)

        }else{
            cell.checkImg.image = UIImage(named: "check color")
            cell.serviceTypeLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            nameArr.append(cell.serviceTypeLbl.text!)
            idArr.append(idsting)
            
            let IdStr = idArr.joined(separator: ",")
       
            UserDefaults.standard.set( IdStr , forKey: "idsString")
            //setObject
           
            UserDefaults.standard.set( idArr , forKey: "idsArr")
        }
      
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        saveBool = true
        
        if serviceCheckArr[indexPath.row] == "0" {
           
            serviceCheckArr[indexPath.row] = "1"
            serviceCheckArr.append("1")
            idArr = [String]()
        }
      
        else if serviceCheckArr[indexPath.row] == "1"{
            
            serviceCheckArr[indexPath.row] = "0"
            serviceCheckArr.append("1")
             idArr = [String]()
        }
        tableView.reloadData()
        
    }
    
}
