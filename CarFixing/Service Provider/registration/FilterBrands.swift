//
//  FilterBrands.swift
//  CarFixing
//
//  Created by Suman Guntuka on 07/09/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class FilterBrands: UIViewController {
   
    @IBOutlet var cancel: UIButton!
    @IBOutlet weak var brands: UILabel!
    @IBOutlet weak var brandTable: UITableView!
    
    @IBAction func cancel(_ sender: Any) {
    }
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet var selectAll: UIButton!
    
    var brandCheckArr = [String]()
    var brandAllCheckArr = [String]()
    //var servicesArr: [[String:AnyObject]]!
    var brandArr: Array<String> = []
    var brandIdArr: Array<String> = []
    
    var nameArr = [String]()
    var idArr = [String]()
    var allCheck : String!
    var checkArr = [String]()
    var IdStr : String!
    
    var selectedRows:[IndexPath] = []
    var saveBool : Bool!
    var notifyStr : String!
    
    var cell : BrandCell!
    override func viewDidLoad() {
        saveBool = false
        SerivceListService ()
        
        brands.text = languageChangeString(a_str: "Brands")
        saveBtn.setTitle(languageChangeString(a_str: "SAVE"), for: UIControlState.normal)
        
    }
    
    @IBAction func saveBtnAction(_ sender: Any) {
     
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "brands"), object: nil)
            self.dismiss(animated: true, completion: nil)
        
            //UserDefaults.standard.set("filt", forKey: "savebS")
        
    }
    func SerivceListService ()
    {
        //http://voliveafrica.com/carfix/services/available_services
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let vehicles = "\(Base_Url)available_brands?"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? ""]
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    //self.servicesArr = responseData["services"] as! Array<String>
                    
                    let servicesCat = responseData["services"] as? [[String:Any]]
                    
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let name = servicesCat["brand_name_en"]  as! String
                        
                        let serviceId = servicesCat["id"]  as! String
                        
                        let myInt2 = (serviceId as NSString).integerValue
                        
                        let checkStr = "0"
                        
                        self.brandCheckArr.append(checkStr)
                        let myString = String(myInt2)
                        self.brandArr.append(name)
                        self.brandIdArr.append(myString)
                       
                    self.brandTable.reloadData()
                    
                }
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}
extension FilterBrands: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brandArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = self.brandTable.dequeueReusableCell(withIdentifier: "FilterBrandCell", for: indexPath) as! BrandCell
        cell.filterBrandNameLbl.text = self.brandArr[indexPath.row]
        let idsting = self.brandIdArr[indexPath.row]
        
        if brandCheckArr[indexPath.row] == "0" {
            
            cell.filterCheckImgbrand.image = UIImage(named: "unCheck")
            cell.filterBrandNameLbl.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
        }else if brandCheckArr[indexPath.row] == "1"
        {
            cell.filterCheckImgbrand.image = UIImage(named: "check color")
            cell.filterBrandNameLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            //nameArr = [String]()
            nameArr.append(cell.brandNameLbl.text!)
            idArr.append(idsting)
            
            IdStr = idArr.joined(separator: ",")
            print(idArr)
            UserDefaults.standard.set( IdStr , forKey: "bandIdsString") //setObject
          
        }
        else
        {
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        saveBool = true
        
        if brandCheckArr[indexPath.row] == "0" {
            
            //            brandCheckArr[indexPath.row] = "1"
            //             brandCheckArr.append("1")
            
            brandCheckArr.remove(at: indexPath.row)
            brandCheckArr.insert("1", at: indexPath.row)
            
            print(brandCheckArr)
            
            IdStr = ""
            idArr = [String]()
            
            tableView.reloadData()
            
        }
        else if brandCheckArr[indexPath.row] == "1"{
            
            //            brandCheckArr[indexPath.row] = "0"
            //            brandCheckArr.append("1")
            
            brandCheckArr.remove(at: indexPath.row)
            brandCheckArr.insert("0", at: indexPath.row)
            
            IdStr = ""
            idArr = [String]()
            
            tableView.reloadData()
            
        }
    }
}
