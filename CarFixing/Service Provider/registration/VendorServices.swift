
//
//  VendorServices.swift
//  CarFixing
//
//  Created by Suman Guntuka on 06/09/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class VendorServices: UIViewController {

    @IBOutlet var cancel: UIButton!
    @IBOutlet weak var serviceType: UILabel!
    
    @IBOutlet weak var save: UIButton!
    var serviceCheckArr = [String]()
    //var servicesArr: [[String:AnyObject]]!
    var servicesArr: Array<String> = []
    var serviceIdArr: Array<String> = []
    
    var nameArr = [String]()
    var idArr = [String]()
    var notifyStr : String!
    var checkArr = [String]()
    var IdStr : String!
    
    var saveBool : Bool!
    var typeStr : String!
    
    @IBOutlet weak var serviceTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        saveBool = false
        
         serivceListService ()
        
      
        serviceType.text = languageChangeString(a_str: "Service type")
        save.setTitle(languageChangeString(a_str: "SAVE"), for: UIControlState.normal)
    }

    @IBAction func saveBtn(_ sender: Any) {
        
        updateServicesCall ()
        
    }
    @IBAction func cancelBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func serivceListService ()
    {
  
        //http://voliveafrica.com/carfix/services/spservices
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
       
        let vehicles = "\(Base_Url)spservices"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? "" , "user_id" : userid ?? "" ]
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post , parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    //self.servicesArr = responseData["services"] as! Array<String>
                    
                    let servicesCat = responseData["services"] as? [[String:Any]]
                    
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let name = servicesCat["service_name_en"]  as! String
                        
                        let serviceId = servicesCat["id"]  as! String
                         let selectType = servicesCat["is_selected"]  as! String
                        
                        self.serviceIdArr.append(serviceId)
                        self.servicesArr.append(name)
                        self.serviceCheckArr.append(selectType)
                        
                    }
                    self.serviceTbl.reloadData()
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    func updateServicesCall ()
    {
        //http://voliveafrica.com/carfix/services/updateservices
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
       // let vendorIds = UserDefaults.standard.object(forKey: "selectVendorIDs")
        
        let vehicles = "\(Base_Url)updateservices"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? "" , "user_id" : userid ?? "" ,"services" : IdStr! ]
        
        Services.sharedInstance.loader(view: self.view)
        
        Alamofire.request(vehicles, method: .post , parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                    DispatchQueue.main.async {
                        
                        Services.sharedInstance.dissMissLoader()
                        
                        self.serivceListService ()
                    }
                    
                }
                else
                {
                    self.showToast(message: message!!)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}



extension VendorServices: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = serviceTbl.dequeueReusableCell(withIdentifier: "VendorServiceCell", for: indexPath) as! SelectServiceCell
        cell.serivceName.text = self.servicesArr[indexPath.row]
        let idsting = self.serviceIdArr[indexPath.row]
        
        print(serviceCheckArr)
        
        if serviceCheckArr[indexPath.row] == "0" {
            
            cell.vendorServiceImg.image = UIImage(named: "unCheck")
            cell.serivceName.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
        }else if serviceCheckArr[indexPath.row] == "1"{
            cell.vendorServiceImg.image = UIImage(named: "check color")
            cell.serivceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            idArr.append(idsting)
             IdStr = idArr.joined(separator: ",")
            print("final ids are .. \(IdStr)")
           // UserDefaults.standard.set( IdStr , forKey: "selectVendorIDs")
        }else
        {
            IdStr = ""
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if serviceCheckArr[indexPath.row] == "0" {
            
            serviceCheckArr.remove(at: indexPath.row)
            serviceCheckArr.insert("1", at: indexPath.row)
            
           
            idArr = [String]()
            IdStr = ""
            tableView.reloadData()
        }
            
        else if serviceCheckArr[indexPath.row] == "1"{
            
            serviceCheckArr.remove(at: indexPath.row)
            serviceCheckArr.insert("0", at: indexPath.row)
            
            idArr = [String]()
            IdStr = ""
            tableView.reloadData()
        }
        
        
    }
    
}
