//
//  OnsiteService.swift
//  CarFixing
//
//  Created by Suman Guntuka on 02/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class OnsiteService: UIViewController {

    @IBOutlet weak var tableHieght: NSLayoutConstraint!
    @IBOutlet weak var others: UILabel!
    @IBOutlet weak var titleTxt: UILabel!
    @IBOutlet weak var alertTxt: UILabel!
    @IBOutlet weak var serviceTable: UITableView!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var serviceType: UILabel!
    @IBOutlet weak var towingLbl: UILabel!
    @IBOutlet weak var towImg: UIImageView!
     @IBOutlet weak var otherImg: UIImageView!
    var cell = SelectServiceCell()
    
    var serviceCheckArr = [String]()
    var servicesArr: Array<String> = []
    var serviceIdArr: Array<String> = []
    
    var nameArr = [String]()
    var idArr = [String]()
    var notifyStr : String!
    var checkArr = [String]()
    
    var saveBool : Bool!
    var typeStr : String!
    var checkBool : Bool!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
     if UserDefaults.standard.object(forKey: "type") != nil
     {
        let typeStr = UserDefaults.standard.object(forKey: "type") as! String
        
        if typeStr == "tow"
        {
            saveBool = true
            tableHieght.constant = 0
            towImg.image = UIImage(named: "checkc")
        }else if typeStr == "other"
        {
             otherImg.image = UIImage(named: "checkc")
            tableHieght.constant = 230
        }else
        {
           tableHieght.constant = 0
        }
        
    }else
     {
         tableHieght.constant = 0
        }
        
        SerivceListService ()
//        if checkBool == true {
//            tableHieght.constant = 230
//        }else
//        {
//           tableHieght.constant = 0
//        }
        
        notifyStr = UserDefaults.standard.object(forKey: "notify") as! String
        
        // UserDefaults.standard.set("jj", forKey: "saveStr")
        
        print(notifyStr)
        serviceType.text = languageChangeString(a_str: "Select Your Services")
        others.text = languageChangeString(a_str: "Others")
        save.setTitle(languageChangeString(a_str: "SAVE"), for: UIControlState.normal)
        towingLbl.text = languageChangeString(a_str: "Towing")
    }

    @IBAction func otherBtn(_ sender: Any) {
       
        UserDefaults.standard.set("other", forKey: "type")
        
        checkBool = true
        otherImg.image = UIImage(named: "checkc")
        towImg.image = UIImage(named: "uncheckc")
        
        tableHieght.constant = 230
        
        
    }
    @IBAction func towingBtn(_ sender: Any) {
       
        saveBool = true
        UserDefaults.standard.set("tow", forKey: "type")
        checkBool = false
       
        towImg.image = UIImage(named: "checkc")
        otherImg.image = UIImage(named: "uncheckc")
        
        tableHieght.constant = 0
     
        saveBool = true
        //towingLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        UserDefaults.standard.removeObject(forKey: "idsString")
        UserDefaults.standard.set( "21" , forKey: "idsString")
        //UserDefaults.standard.removeObject(forKey: "saveoS")
        
        let str1 = UserDefaults.standard.object(forKey: "idsString")
        print(str1 as Any)
        
        for i in 0..<serviceCheckArr.count {

            if i == 1
            {
               serviceCheckArr.insert("0", at: i)

            }else
            {
                serviceCheckArr.insert("0", at: i)
            }
        }
         self.serviceTable.reloadData()
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        
     self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtn(_ sender: Any) {
       
        if saveBool == true
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "service"), object: nil)
            UserDefaults.standard.set("filt", forKey: "saveoS")
            self.dismiss(animated: true, completion: nil)
            
        }else
        {
            self.showToastForAlert(message: languageChangeString(a_str: "Please Select The Services")!)
        }
    }
   
    func SerivceListService ()
    {
        
        //http://volive.in/carfix/services/available_services?API-KEY=98745612&lang=en&type=3
        
        let language = UserDefaults.standard.object(forKey: "currentLanguage")
        
        typeStr = UserDefaults.standard.object(forKey: "ServiceType") as! String
        
        let vehicles = "\(Base_Url)available_services"
        
        let parameters: Dictionary<String, Any> = ["API-KEY": APIKEY , "lang" : language ?? "" , "type" : typeStr ?? "" ]
       // Services.sharedInstance.loader(view: self.view)
        Alamofire.request(vehicles, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as? Int!
                let message = responseData["message"] as? String!
                if status == 1
                {
                  //  Services.sharedInstance.dissMissLoader()
                    
                    let servicesCat = responseData["services"] as? [[String:Any]]
                    
                    print("services categorys are ***** \(servicesCat!)")
                    
                    for servicesCat in servicesCat! {
                        
                        let name = servicesCat["service_name_en"]  as! String
                        
                        let serviceId = servicesCat["id"]  as! String
                        
                        let myInt2 = (serviceId as NSString).integerValue
                        
                        let myString = String(myInt2)
                        
                        self.servicesArr.append(name)
                        
                        self.serviceIdArr.append(myString)
                        
//                            let checkStr = "0"
//                            self.serviceCheckArr.append(checkStr)
                        
                        if UserDefaults.standard.object(forKey: "saveoS") != nil
                        {
                            self.saveBool = true
                            let typeStr = UserDefaults.standard.object(forKey: "type") as! String
                            
                            if typeStr == "tow"
                            {
                                let checkStr = "0"
                                self.serviceCheckArr.append(checkStr)
                            }else if typeStr == "other"
                            {
                                let idArr = UserDefaults.standard.object(forKey: "idsoArr") as! Array<Any>
                                
                                self.checkArr = idArr as! [String]
                                
                                print(self.checkArr)
                                
                                if self.checkArr .contains(myString)
                                {
                                    
                                    let checkStr = "1"
                                    self.serviceCheckArr.append(checkStr)
                                }else
                                {
                                    let checkStr = "0"
                                    self.serviceCheckArr.append(checkStr)
                                }
                            }
                        }
                        else
                        {
                            let checkStr = "0"
                            self.serviceCheckArr.append(checkStr)
                        }
                
                    }
                    UserDefaults.standard.set(self.servicesArr, forKey: "serviceArr")
                    self.serviceTable.reloadData()
                }
                else
                {
                    self.showToast(message: message!!)
                  //  Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension OnsiteService: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = serviceTable.dequeueReusableCell(withIdentifier: "SelectServiceCell1", for: indexPath) as! SelectServiceCell
        cell.serviceLbl.text = self.servicesArr[indexPath.row]
        let idsting = self.serviceIdArr[indexPath.row]
        
        print(serviceCheckArr)
        
        if serviceCheckArr[indexPath.row] == "0" {
            
            cell.serviceImg.image = UIImage(named: "unCheck")
            cell.serviceLbl.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
        }else{
            cell.serviceImg.image = UIImage(named: "check color")
            cell.serviceLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
           
            idArr.append(idsting)
            
            let IdStr = idArr.joined(separator: ",")
            print(IdStr)
            UserDefaults.standard.set( IdStr , forKey: "idsString")
             UserDefaults.standard.set( idArr , forKey: "idsoArr")
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        saveBool = true
       // towingLbl.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
       // towImg.image = UIImage(named: "unCheck")
        
        if serviceCheckArr[indexPath.row] == "0" {
            
            serviceCheckArr[indexPath.row] = "1"
            serviceCheckArr.append("1")
            idArr = [String]()
        }
        else if serviceCheckArr[indexPath.row] == "1"{
            
            serviceCheckArr[indexPath.row] = "0"
            serviceCheckArr.append("1")
            idArr = [String]()
        }
        tableView.reloadData()
    }
}
