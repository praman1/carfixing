//
//  JobCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 01/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class JobCell: UITableViewCell {

    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var view: UIButton!
    @IBOutlet var jobLbl: UILabel!
    
    @IBOutlet var viewDetails: UIButton!
    
    //my bill
    
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var dateBillLbl: UILabel!
    @IBOutlet var billLbl: UILabel!
    
    @IBOutlet var details: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
