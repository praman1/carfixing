//
//  CategoryCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/7/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var catImg: UIImageView!
    @IBOutlet weak var catNameLbl: UILabel!
}
