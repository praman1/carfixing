//
//  NotificationMsgCell.swift
//  carFixing
//
//  Created by Apple on 12/13/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class NotificationMsgCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
