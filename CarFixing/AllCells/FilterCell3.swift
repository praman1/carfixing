//
//  FilterCell3.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/8/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class FilterCell3: UITableViewCell {

    @IBOutlet weak var priceSlider: UISlider!
    @IBOutlet weak var priceLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
