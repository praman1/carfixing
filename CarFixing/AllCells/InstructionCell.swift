//
//  InstructionCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/5/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class InstructionCell: UICollectionViewCell {
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var subLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
}
