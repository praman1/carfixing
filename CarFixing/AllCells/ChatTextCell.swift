//
//  ChatTextCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 23/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class ChatTextCell: UITableViewCell {
    
    @IBOutlet var mytimeLbl: UILabel!
    @IBOutlet weak var myMsgLbl: UILabel!
    @IBOutlet weak var otherMsgLbl: UILabel!
    @IBOutlet weak var otherPimage: UIImageView!
    
    @IBOutlet weak var otherBackView: UIView!
    @IBOutlet weak var myBackView: UIView!
    @IBOutlet var othertimeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
