//
//  ListCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/8/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Cosmos

class ListCell: UITableViewCell {
    @IBOutlet weak var listImg: UIImageView!
    
    @IBOutlet var noOfRateLbl: UILabel!
    @IBOutlet weak var listNameLbl: UILabel!
     @IBOutlet weak var  startingFrom: UILabel!
    
    @IBOutlet var rateView: CosmosView!
    @IBOutlet weak var listAddrLbl: UILabel!
    
    @IBOutlet weak var listCatLbl: UILabel!
    
    
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var listTimeLbl: UILabel!
    
    @IBOutlet weak var listRatingBtn: UIButton!
    
    @IBOutlet weak var listPriceBtn: UILabel!
    
    @IBOutlet weak var sar: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
