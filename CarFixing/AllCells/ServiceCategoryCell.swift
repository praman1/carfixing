//
//  ServiceCategoryCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/11/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class ServiceCategoryCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
