//
//  DetailCollectionViewCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/11/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Cosmos

class DetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var rateView: CosmosView!
    @IBOutlet var noOfRateLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
}
