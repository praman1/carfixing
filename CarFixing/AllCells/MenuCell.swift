//
//  MenuCell.swift
//  carFixing
//
//  Created by Apple on 12/14/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var icon_imageView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
