//
//  ShopImageTableCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 22/02/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class ShopImageTableCell: UITableViewCell {

    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var shopImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
