//
//  DetailBidCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/13/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class DetailBidCell: UITableViewCell {

    @IBOutlet weak var headLbl: UILabel!
    
    @IBOutlet var PRICElABEL: UILabel!
    @IBOutlet var currencyLbl: UILabel!
    
    @IBOutlet weak var acceptLbl: UILabel?
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBOutlet weak var shopNamLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    @IBOutlet weak var detailsTxtView: UITextView!
    
    @IBOutlet weak var bidImgTbl: UIImageView!
    
    @IBOutlet weak var viewDetails: UIButton!
    
    @IBOutlet weak var chat: UILabel!
    @IBOutlet weak var bidAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
