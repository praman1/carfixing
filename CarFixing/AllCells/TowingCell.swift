//
//  TowingCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 04/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class TowingCell: UITableViewCell {

    @IBOutlet var viewDetailsBtn: UIButton!
    @IBOutlet weak var towingImg: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet var chatImg: UIButton!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var statusBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
