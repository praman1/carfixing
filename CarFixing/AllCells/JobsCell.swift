//
//  JobsCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/13/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class JobsCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var edit: UILabel!
    @IBOutlet weak var delete: UILabel!
    @IBOutlet weak var postedOn: UILabel!
    
    @IBOutlet weak var viewDetails: UIButton!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
