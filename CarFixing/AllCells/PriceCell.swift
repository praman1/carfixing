//
//  PriceCell.swift
//  CarFixing
//
//  Created by Suman Volive on 2/15/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {
    @IBOutlet weak var serviceTypeLbl: UILabel!
    
    @IBOutlet weak var priceLbl: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var serviceType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
