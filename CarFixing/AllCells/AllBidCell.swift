//
//  AllBidCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 23/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class AllBidCell: UITableViewCell {

    @IBOutlet weak var bidNameLbl: UILabel!
    @IBOutlet weak var postedDateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var userBidLbl: UILabel!
    
    @IBOutlet weak var viewDetails: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var postedOn: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
