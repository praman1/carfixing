//
//  OptionsCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 26/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class OptionsCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var Img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
