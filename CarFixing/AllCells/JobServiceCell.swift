//
//  JobServiceCell.swift
//  CarFixing
//
//  Created by Suman Guntuka on 09/03/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class JobServiceCell: UITableViewCell {

    
    @IBOutlet weak var serviceLbl: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
