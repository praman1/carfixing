//
//  BannerCell.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/7/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit

class BannerCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImg: UIImageView!
}
