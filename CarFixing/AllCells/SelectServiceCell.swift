//
//  SelectServiceCell.swift
//  CarFixing
//
//  Created by Suman Volive on 2/7/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

import UIKit

class SelectServiceCell: UITableViewCell {

    @IBOutlet weak var serviceTypeLbl: UILabel!
   
    @IBOutlet weak var isChecked: UIButton!
   
    @IBOutlet weak var checkImg: UIImageView!
    
    @IBOutlet weak var serviceImg: UIImageView!
    
    @IBOutlet weak var serviceLbl: UILabel!
    
    //vendor Services
    
    @IBOutlet var serivceName: UILabel!
    
    @IBOutlet var vendorServiceImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
