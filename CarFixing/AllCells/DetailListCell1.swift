//
//  DetailListCell1.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/11/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import Cosmos


class DetailListCell1: UITableViewCell {
//cell2
    
//    @IBOutlet weak var catNameLbl: UILabel!
//
//    //cell3
//
    @IBOutlet weak var subCatNameLbl: UILabel!
//
//    //cell1
//    @IBOutlet weak var headTitleLbl: UILabel!
   // @IBOutlet weak var subTitleLbl: UILabel!
    
    //cell 1
   
    //cell2
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var chat: UILabel!
    
    //cell3
    @IBOutlet weak var workingHrs: UILabel!
    
    //cell4
    @IBOutlet weak var daysLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    //cell5
    @IBOutlet weak var descriptionText: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    
    //cell6
    @IBOutlet weak var servicesOffered: UILabel!
    
    //cell 7
    @IBOutlet weak var serviceTypeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var checkImg: UIImageView!
    
    // cell8
    @IBOutlet weak var calender: UILabel!
    
    @IBOutlet weak var calenderDateLbl: UILabel!
    //var timer:Timer? = nil
    
    var collectionImages = [String]()
    var countStr  : String!
    var rateStr : String!
    var timer:Timer? = nil
    var rateValue : Double!

    @IBOutlet weak var detailCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    override func awakeFromNib() {
       
        super.awakeFromNib()
    }

    func autoScrollImageSlider() {
        
        DispatchQueue.main.async {
            
            let firstIndex = 0
            let lastIndex = (self.collectionImages.count) - 1
            
            let visibleIndices = self.detailCollection.indexPathsForVisibleItems
            let nextIndex = visibleIndices[0].row + 1
            
            self.pageControl.numberOfPages = self.collectionImages.count
            
            let nextIndexPath: IndexPath = IndexPath.init(item: nextIndex, section: 0)
            let firstIndexPath: IndexPath = IndexPath.init(item: firstIndex, section: 0)
            print(self.collectionImages)
            
            if nextIndex > lastIndex {
                
                self.detailCollection.scrollToItem(at: firstIndexPath, at: .centeredHorizontally, animated: true)
                
            } else {
                
                self.detailCollection.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
                
            }
        }
    }
   
    func imageArrCall (myArr: Any)
    {
        collectionImages = myArr as! [String]
        print(collectionImages)
        self.detailCollection.reloadData()
    }
    func countStrCall (countA: String!)
    {
        countStr = countA
        print(countStr)
        self.detailCollection.reloadData()
        
    }
    func rateStrCall (rateA: String!)
    {
        rateStr = rateA
       //rateValue = Double(rateStr)
        print(rateStr)
        
        self.detailCollection.reloadData()
    }

}
extension DetailListCell1: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = detailCollection.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath) as! DetailCollectionViewCell
        
        self.pageControl.numberOfPages = self.collectionImages.count
        cell.noOfRateLbl.text = String(format: "%@%@%@","(",countStr!,")")
        cell.rateView.rating = Double(rateStr)!
         cell.img.sd_setImage(with: URL(string: collectionImages[indexPath.row]), placeholderImage: UIImage(named:""))
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: collectionView.frame.size.width,height:  collectionView.frame.size.height)
        
       
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == self.detailCollection {
            
            self.pageControl.currentPage = indexPath.row
        }
    }
    
}
