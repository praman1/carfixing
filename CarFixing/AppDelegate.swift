//
//  AppDelegate.swift
//  CarFixing
//
//  Created by Mohammad Apsar on 12/4/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var sidemenuStr : String!
    var loginUser : String!
    var logIn : String!
    var serviceStr : String!
    var profileStr : String!
    var chatId : String!
    var jobStr : String!
    var bidStr : String!
    var jobsSepStr : String!
    var sepStr : String!
    var scrapshop : String!
    var typeStr : String!
    var chatTypeStr : String!
    var chatSep : String!
    var onsiteStr :String!
    var towStr : String!
    var onsiteSepStr : String!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //com.volivesolutions.CarFixing
        //previous: AIzaSyDEQ2bYYVfw2f2x9S8rmU19-e4lq56gXD4
        // GMSServices.provideAPIKey("AIzaSyDEQ2bYYVfw2f2x9S8rmU19-e4lq56gXD4")
        GMSServices.provideAPIKey("AIzaSyAoh5OXOp09zoVPLnO2uH6-sSYK-uZc_8M")
        GMSPlacesClient.provideAPIKey("AIzaSyAoh5OXOp09zoVPLnO2uH6-sSYK-uZc_8M")
        
        // Override point for customization after application launch.
          //mainCalling ()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if #available(iOS 10, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    
        return true
    }
    
     func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        let defaults = UserDefaults.standard
        defaults.set(deviceTokenString, forKey: "deviceToken")
        // Persist it in your backend in case it's new
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
        
        let dic = data as NSDictionary
        print("my Data \(dic)")
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        var task = UIBackgroundTaskIdentifier(0)
        task = application.beginBackgroundTask(expirationHandler: {
            print("Expiration handler called \(application.backgroundTimeRemaining)")
            application.endBackgroundTask(task)
            task = UIBackgroundTaskInvalid
        })
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}


@available(iOS 10.0, *)

extension AppDelegate: UNUserNotificationCenterDelegate{
    @available(iOS 10.0, *)
   
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void)
    {
        let userInfo = notification.request.content.userInfo as! Dictionary <String,Any>

        let aps = userInfo["aps"] as? Dictionary<String,Any>

        let infoDict = aps!["info"] as? Dictionary<String,Any>
         let title = infoDict!["type"] as! String
        print(title)
        print(infoDict)
        
        //bid_completed
        
         if title == "message"
        {
            let senderId = infoDict!["user_id"] as! String
            UserDefaults.standard.set(senderId, forKey: "sender")
            
         }else if title == "towing_status"
         {

            UserDefaults.standard.set("towSt", forKey: "towingSet")
            UserDefaults.standard.set(infoDict!, forKey: "postDict")

         }else if title == "towing"
         {
            
            UserDefaults.standard.set("towingSep", forKey: "towingSep")
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let towingRequest = storyboard.instantiateViewController(withIdentifier: "TowingNotificationAlert") as? TowingNotificationAlert
           
            towingRequest?.pushDictionary = infoDict!
            var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: towingRequest!)
            navigation.modalPresentationStyle = .overCurrentContext
            while ((viewcontroller?.presentedViewController) != nil)
            {
                viewcontroller = viewcontroller?.presentedViewController
            }
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
            
        }else if title == "job_completed"
        {
            let adress = infoDict!["address"] as! String
            let shop_name = infoDict!["shop_name"] as! String
            let image = infoDict!["image"] as! String
            let job_id = infoDict!["job_id"] as! String
            let msg = infoDict!["message"] as! String

            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(adress, forKey: "address")
            UserDefaults.standard.set(shop_name, forKey: "shopname")
            UserDefaults.standard.set(image, forKey: "image")
            UserDefaults.standard.set(job_id, forKey: "job_id")
            
            //SuccessReviewController
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyBoard.instantiateViewController(withIdentifier: "SuccessReviewController")
            self.window?.rootViewController = tab;
            
         }
         else if title == "vendor_applied" {
            
            let vendorID = infoDict!["vendor_id"] as! String
            let job_id = infoDict!["bid_id"] as! String
            let msg = infoDict!["message"] as! String
           
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let bidDeatils = storyboard.instantiateViewController(withIdentifier: "DetailBidController") as? DetailBidController
            UserDefaults.standard.set(vendorID, forKey: "vendorid")
            UserDefaults.standard.set(job_id, forKey: "bidid")
            UserDefaults.standard.set(msg, forKey: "status")
            bidDeatils?.bididStr = job_id
            
           
            let top = UIApplication.shared.keyWindow?.rootViewController
            
            let navigationController = UINavigationController.init(rootViewController: bidDeatils!)
            
            top?.present(navigationController, animated: true, completion: nil)
            
            
            //            String body = object.getString("body");
            //            String title = object.getString("title");
            //            String message = object.getString("message");
            //            String bid_id = object.getString("bid_id");
            //            intent.putExtra("bid_id", bid_id);
            
            
         }
         
         
         
         else if title == "bid_completed"{
            let adress = infoDict!["address"] as! String
            let shop_name = infoDict!["shop_name"] as! String
            let image = infoDict!["image"] as! String
            let job_id = infoDict!["bid_id"] as! String
            let msg = infoDict!["message"] as! String
            
            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(adress, forKey: "address")
            UserDefaults.standard.set(shop_name, forKey: "shopname")
            UserDefaults.standard.set(image, forKey: "image")
            UserDefaults.standard.set(job_id, forKey: "bid_id")
            
            //SuccessReviewController
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyBoard.instantiateViewController(withIdentifier: "SuccessReviewController") as! SuccessReviewController
            tab.checkStr = "1"
            self.window?.rootViewController = tab;
         }
        
        else if title == "bid_status"  {
            
            let image = infoDict!["vendor_id"] as! String
            let job_id = infoDict!["bid_id"] as! String
            let msg = infoDict!["message"] as! String
            
            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(image, forKey: "vendor_id")
            UserDefaults.standard.set(job_id, forKey: "bid_id")
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let bidDeatils = storyboard.instantiateViewController(withIdentifier: "DetailBidController") as? DetailBidController
            
            bidDeatils?.bididStr = job_id
            let top = UIApplication.shared.keyWindow?.rootViewController
            
            let navigationController = UINavigationController.init(rootViewController: bidDeatils!)
            
            top?.present(navigationController, animated: true, completion: nil)
            
            
//            let viewcontroller = UIApplication.shared.keyWindow?.rootViewController
//            let navigation = UINavigationController.init(rootViewController: bidDeatils!)
//
//            viewcontroller?.present(navigation, animated: true, completion: nil)
 

        }
         else if title == "new_bid"
         {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "bidData"), object: nil)
         }
         else
         {
            
         }

    completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue)|UInt8(UNNotificationPresentationOptions.sound.rawValue))))
        
        ////pushcalling
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messageSent"), object: nil)
        
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void)
    {
        
        let userInfo = response.notification.request.content.userInfo
         print(userInfo)
        let aps = userInfo["aps"] as? Dictionary<String,Any>
        let infoDict = aps!["info"] as? Dictionary<String,Any>
        
        let msg = infoDict!["message"] as! String
        let title = infoDict!["type"] as! String
        print(title)
        print(infoDict)
        //bid_completed
        if title == "job_completed"
        {
            let adress = infoDict!["address"] as! String
            let shop_name = infoDict!["shop_name"] as! String
            // let vendor_name = infoDict!["vendor_name"] as! String
            let image = infoDict!["image"] as! String
            let job_id = infoDict!["job_id"] as! String
            
            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(adress, forKey: "address")
            UserDefaults.standard.set(shop_name, forKey: "shopname")
            UserDefaults.standard.set(image, forKey: "image")
            UserDefaults.standard.set(job_id, forKey: "job_id")
            
        }else if title == "towing_completed"{
            
          DispatchQueue.main.async {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyBoard.instantiateViewController(withIdentifier: "TabController")
            self.window?.rootViewController = tab;
            
            }
        }else if title == "towing"
        {
            UserDefaults.standard.set("towingSep", forKey: "towingSep")
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let towingRequest = storyboard.instantiateViewController(withIdentifier: "TowingNotificationAlert") as? TowingNotificationAlert
            towingRequest?.pushDictionary = infoDict!
            var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: towingRequest!)
            navigation.modalPresentationStyle = .overCurrentContext
            while ((viewcontroller?.presentedViewController) != nil)
            {
                viewcontroller = viewcontroller?.presentedViewController
            }
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
          
        }else if title == "towing_status"
        {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let towingRequest = storyboard.instantiateViewController(withIdentifier: "TowingMap") as? TowingMap
            towingRequest?.pushDictionary = infoDict!
            towingRequest?.towStr = "towApp"
             UserDefaults.standard.set("towSt", forKey: "towingSet")
            var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: towingRequest!)
            navigation.modalPresentationStyle = .overCurrentContext
            navigation.navigationBar.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            while ((viewcontroller?.presentedViewController) != nil)
            {
                viewcontroller = viewcontroller?.presentedViewController
            }
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
            
        }else if title == "job_request"
        {
            UserDefaults.standard.set("jobReq", forKey: "jobRequest")
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let jobRequest = storyboard.instantiateViewController(withIdentifier: "JobRequestDetails") as? JobRequestDetails
            jobRequest?.pushDictionary = infoDict!
            
            var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: jobRequest!)
            navigation.modalPresentationStyle = .overCurrentContext
            navigation.navigationBar.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
            while ((viewcontroller?.presentedViewController) != nil)
            {
                viewcontroller = viewcontroller?.presentedViewController
            }
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
            
        }
        else if title == "bid_completed"{
            let adress = infoDict!["address"] as! String
            let shop_name = infoDict!["shop_name"] as! String
            // let vendor_name = infoDict!["vendor_name"] as! String
            let image = infoDict!["image"] as! String
            let job_id = infoDict!["bid_id"] as! String
            
            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(adress, forKey: "address")
            UserDefaults.standard.set(shop_name, forKey: "shopname")
            UserDefaults.standard.set(image, forKey: "image")
            UserDefaults.standard.set(job_id, forKey: "bid_id")
            
            
        }
        else if title == "vendor_applied" {
            
            let image = infoDict!["vendor_id"] as! String
            let job_id = infoDict!["bid_id"] as! String
            let msg = infoDict!["message"] as! String
            
            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(image, forKey: "vendor_id")
            UserDefaults.standard.set(job_id, forKey: "bid_id")
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let bidDeatils = storyboard.instantiateViewController(withIdentifier: "DetailBidController") as? DetailBidController
             chatTypeStr = "bid_status"
            bidDeatils?.bididStr = job_id
            
            var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: bidDeatils!)
            navigation.modalPresentationStyle = .overCurrentContext
            navigation.navigationBar.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
            while ((viewcontroller?.presentedViewController) != nil)
            {
                viewcontroller = viewcontroller?.presentedViewController
            }
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
            
            
//            String body = object.getString("body");
//            String title = object.getString("title");
//            String message = object.getString("message");
//            String bid_id = object.getString("bid_id");
//            intent.putExtra("bid_id", bid_id);
            
            
        }
        else if title == "bid_status"  {
            
         
            
            let image = infoDict!["vendor_id"] as! String
            let job_id = infoDict!["bid_id"] as! String
            let msg = infoDict!["message"] as! String
            
            UserDefaults.standard.set(msg, forKey: "message")
            UserDefaults.standard.set(image, forKey: "vendor_id")
            UserDefaults.standard.set(job_id, forKey: "bid_id")
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let bidDeatils = storyboard.instantiateViewController(withIdentifier: "DetailBidController") as? DetailBidController
            
            bidDeatils?.bididStr = job_id
            chatTypeStr = "bid_status"
            //UserDefaults.standard.set("jobReq", forKey: "jobRequest")
            
//            let storyboard = UIStoryboard(name:"Main", bundle: nil)
//            let jobRequest = storyboard.instantiateViewController(withIdentifier: "JobRequestDetails") as? JobRequestDetails
//            jobRequest?.pushDictionary = infoDict!
            
            var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: bidDeatils!)
            navigation.modalPresentationStyle = .overCurrentContext
            navigation.navigationBar.barTintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
            while ((viewcontroller?.presentedViewController) != nil)
            {
                viewcontroller = viewcontroller?.presentedViewController
            }
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
            
        }
        else if title == "new_bid"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "bidData"), object: nil)
        }
        else
        {
        }
        
        if title == "job_completed"
        {
            //SuccessReviewController
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let tab = storyBoard.instantiateViewController(withIdentifier: "SuccessReviewController")
        self.window?.rootViewController = tab;
           
        }else if title == "bid_completed"{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyBoard.instantiateViewController(withIdentifier: "SuccessReviewController") as! SuccessReviewController
            tab.checkStr = "1"
            
            self.window?.rootViewController = tab;
        }
        else if title == "message"
        {

            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let rideComplete = storyboard.instantiateViewController(withIdentifier: "ChatConversationController") as? ChatConversationController
            //rideComplete?.pushDictionary = alert2
                let senderId = infoDict!["user_id"] as! String
            UserDefaults.standard.set(senderId, forKey: "sender")
            chatTypeStr = "appChat"
           
           let viewcontroller = UIApplication.shared.keyWindow?.rootViewController
            let navigation = UINavigationController.init(rootViewController: rideComplete!)
            
            viewcontroller?.present(navigation, animated: true, completion: nil)
            
        }
        else{
            
        }
        completionHandler()
    }
}
