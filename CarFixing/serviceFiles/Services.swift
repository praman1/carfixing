//
//  Services.swift
//  Perfume
//
//  Created by murali krishna on 11/1/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView

class Services : NSObject {
    
    static let sharedInstance = Services()
    //signUp getperamters
    var _name: String!
    var _email: String!
    var _mobileNumber: String!
    var _message: String!
    
    

    var errMessage: String!
    let imageV = UIImageView()
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    func loader(view: UIView) -> () {
        
/*
//      let imageV = UIImageView()
        imageV.frame = CGRect(x: 0,y: 0,width: 100,height: 100)
        imageV.center = view.center
        imageV.image = #imageLiteral(resourceName: "SUV Select")
        indicator.bringSubview(toFront: imageV)
        view.addSubview(imageV)
        */
        indicator.frame = CGRect(x: 0,y: 0,width: 75,height: 75)
        indicator.layer.cornerRadius = 8
        indicator.center = view.center
        indicator.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.addSubview(indicator)
        indicator.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        indicator.bringSubview(toFront: view)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func dissMissLoader()  {
        
         indicator.stopAnimating()
        imageV.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    var name: String {
        
        if _name == nil {
          _name = "Not available"
        }
        return _name
        
    }
    
    var email: String {
        
        if _email == nil {
            _email = "Not available"
        }
        return _email
        
    }
    
    var mobileNumber: String {
        
        if _mobileNumber == nil {
            _mobileNumber = "Not available"
        }
        return _mobileNumber
        
    }
  
func noInternetConnectionlabel (inViewCtrl:UIView) {
    
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: .transitionCurlUp, animations: {
       
        let lblNew = UILabel()
        lblNew.frame = CGRect(x: 0, y: 0, width: inViewCtrl.frame.size.width, height: 50)
        lblNew.backgroundColor = UIColor.gray
        lblNew.textAlignment = .center
        lblNew.text = "No Internet Connection"
        lblNew.textColor = UIColor.white
        inViewCtrl.addSubview(lblNew)
        lblNew.font=UIFont.systemFont(ofSize: 18)
        lblNew.alpha = 0.5
        lblNew.transform = .identity
    }, completion: nil)
}
}
extension UIView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

//// Valodation Toasts

extension UIViewController {
    
    func showToast(message : String) {
        
        
            let appearance = SCLAlertView.SCLAppearance(
                
                 showCircularIcon: true
         
            )
            let alertView = SCLAlertView(appearance: appearance)
            let alertViewIcon = UIImage(named: "splash screen logo") //Replace the IconImage text with the image name
        
        alertView.showInfo(languageChangeString(a_str: "Alert" )!, subTitle: message ,closeButtonTitle: (languageChangeString(a_str: "DONE")!) , circleIconImage: alertViewIcon)
       // alertView.showInfo("", subTitle: "", closeButtonTitle: (languageChangeString(a_str: "DONE")!))
        
        }
    
    func showToastForAlert(message : String) {
        
                let message = message
                let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
                self.present(alert, animated: true)
                // duration in seconds
                let duration: Double = 3
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                    alert.dismiss(animated: true)
        
    }
        
        func showToastForInternet (message : String) {
            
            let message = message
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            
            self.present(alert, animated: true)
            // duration in seconds
            let duration: Double = 3
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
               
                alert.dismiss(animated: true)
            
            }

        
        }
}
    func languageChangeString(a_str: String) -> String?{
        
        var path: Bundle?
        var selectedLanguage:String = String()
        //UserDefaults.standard.set(ARABIC_LANGUAGE, forKey: "currentLanguage")
        
       // selectedLanguage = UserDefaults.standard.integer(forKey: )
        
        selectedLanguage = UserDefaults.standard.string(forKey: "currentLanguage")!
        
        
        if selectedLanguage == ENGLISH_LANGUAGE {
            if let aType = Bundle.main.path(forResource: "en", ofType: "lproj") {
                path = Bundle(path: aType)!
            }
        }
        else if selectedLanguage == ARABIC_LANGUAGE {
            if let aType = Bundle.main.path(forResource: "ar", ofType: "lproj") {
                path = Bundle(path: aType)!
            }
        }
        else {
            if let aType = Bundle.main.path(forResource: "en", ofType: "lproj") {
                path = Bundle(path: aType)!
            }
        }
        // var languageBundle = Bundle(path: path)
        
        let str: String = NSLocalizedString(a_str, tableName: "LocalizableCarFixing", bundle: path!, value: "", comment: "")
        
        
        
        return str
    }
    
}


extension String {
    
    func strstr(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        
        if beforeNeedle {
            return self.substring(to: range.lowerBound)
        }
        
        return self.substring(from: range.upperBound)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



extension UIView {
    
    func bindToKeyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(UIView.keyboardWillChange(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillChange(_ notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let curFrame = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let targetFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let deltaY = targetFrame.origin.y - curFrame.origin.y + 220
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
            
        },completion: {(true) in
            self.layoutIfNeeded()
        })
    }
}

extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}
